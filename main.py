from SRC.test_Definitions.suitRunner import unittestDex
from SRC.test_Definitions.preCbu_test_page_definitions.page_detalii_pkg import test as raportGetter
import os
from SRC.utils_pkg import utilities
from SRC.raportHTML_pkg.raportHTML import generateRaport
from datetime import datetime
import pathlib
import Resources.endpoints as endpoints
from Resources import endpoints

utils = utilities.utilities()
utils.setTempValue('SRCPath', str(pathlib.Path(__file__).parent.absolute()))
utils.setTempValue('RaportName', [])
# Control mediu, ENV vine setat automat din DockerCompose, care vine din Jenkins



def controller(isSanity=False, isFull=False, recursiveLimit=2, rerunDict=None, recursivityInd='', dexEnv=None):
    criticalFailedList = []
    statusDict = {'Result': {}}
    passedTestCases = ""
    passedkeys = passedTestCases.split('\n')
    contor = 1
    countExecution = 0

    execDetails = {'Result': {'Test':''}}
    testExecRaport = {}


    testDict = {
        "t1": {
     'channel': 'RETAIL',
 'searchType': 'MSISDN',
 'searchValue': 733044811,
 'configType': 'ACHIZITIE',
 'abNouType': 'MFP',
 'scenarioNameDC': 'CR_True_Client_Matur',
 'offerChoice': 'PachetPromo',
 'scenarioName_PC': 'BundleOffers_Rate',
 'scenarioName_Utl_Nr': 'Use_existing_SIM_card_No',
 'scenarioName_isTitular': 'Utilizator_titular_No_Minor_No',
 'scenarioName_GDPR': 'Scenariu_GDPR_allYES',
 'scenarioContNou': 'Is_Cont_Nou_Required_Yes_FacturaElectr'
        }
    }

    testDict =    {'Test_CBU_OMS':{
        'channel':'RETAIL',
        'searchType':'CNP',
        'searchValue': 2951104439671,
        'configType':'ACHIZITIE',
        'abNouType':'GA',
        'scenarioNameDC':'CR_True_Client_nonMatur_ValidateToken_Yes',
        'offerChoice':'ConfigOferte',
        'scenarioName_PC':"OferteConfigurabile_S2D_SimOnly_si_Discount",
        'scenarioName_Utl_Nr':'Scenariu_Numar_Random',
        'scenarioName_isTitular':'Utilizator_titular_No_Minor_No',
        'scenarioName_GDPR':'Scenariu_GDPR_allYES',
        'scenarioContNou':'Is_Cont_Nou_Required_Yes_FacturaElectr',
        'scenarioName_PageSearch':'Completare_Informatii_Client_Yes',
        'webAppVersion': 'cbu_oms'
       }}

    testDict = {
        "t1": {
     'channel': 'RETAIL',
 'searchType': 'MSISDN',
 'searchValue': 733044811,
 'configType': 'ACHIZITIE',
 'abNouType': 'MFP',
 'scenarioNameDC': 'CR_True_Client_Matur',
 'offerChoice': 'PachetPromo',
 'scenarioName_PC': 'BundleOffers_Rate',
 'scenarioName_Utl_Nr': 'Use_existing_SIM_card_No',
 'scenarioName_isTitular': 'Utilizator_titular_No_Minor_No',
 'scenarioName_GDPR': 'Scenariu_GDPR_allYES',
 'scenarioContNou': 'Is_Cont_Nou_Required_Yes_FacturaElectr'
        }
    }

    testDict = {
   'Prereq - Sales channels=Retail Login subflow=END Search by=MSISDN Customer found=True DEX - Validate customer details - Subflow=End(14)_DEXAutomation':{
      'channel':'RETAIL',
      'searchType':'MSISDN',
      'searchValue':727277696,
      'configType':'RETENTIE',
      'retention_option':'360_Doar_Servicii',
      'retention_flow':'Service_only',
      'scenarioName_PC':'OferteConfigurabile_ServiceOnly'
   }}

    testDefinitions = utils.dictionaryFromFile(isSanity=isSanity, isFull=isFull, isCustom=True)

    if rerunDict != None:
        testDefinitions = rerunDict
#########################PAY ATENTION HERE IT SHOULD BE COMENTED IN A REAL RUN#######################
    # testDefinitions = testDict
    totalScenarii = len(testDefinitions)

    if os.name == 'posix':
        if os.environ['ENV'] == 'Jenkins':
            utils.setTempValue('URL', endpoints.getEndpoints(dexEnv))
        else:
            utils.setTempValue('URL', endpoints.getEndpoints(os.environ['ENV']))
            dexEnv = os.environ['ENV']

    else:
        utils.setTempValue('URL', endpoints.getEndpoints(dexEnv))

    for key, value in testDefinitions.items():
        # if 'promo' in value['scenarioName_PC'].lower():
        try:
            if "_disc" in value['scenarioName_PC'].lower():
                value['searchValue'] = 733044811
            else:
                value['searchValue'] = 722393969
            # else:
            #     if "_disc" in value['scenarioName_PC'].lower():
            #         value['searchValue'] = 733044811
            #     else:
            #         value['searchValue'] = 722393969
        except Exception:
            value['searchValue'] = 733044811
        if key in key:
            value['webAppVersion'] = 'precbu_retentie'
            value['dexEnv'] = dexEnv
            # print(value['configType'])
            value['configType'] = value['webAppVersion'] + '_' + value['configType']
            # value['webAppVersion'] = webAppVersion
            pega_env = endpoints.getEndpoints(env=dexEnv)['pegaenv']
            value['pega_Env'] = pega_env
            # offersDict = utils.getPegaOfferName(value)
            # print(offersDict)
            offersDict = {'simonly':"Red 14 V52", 'discount':'Red 14 V52'}
            abortedRaport, isAborted = utils.validOfferForScenario(offersDict=offersDict, testName=key, testDefinition=value, env=pega_env)
            isAborted = False
            #Replace search value with Msisdn(whose dependecy is on cbu flow) for conditions and
            # log bad scenarios
            key, value = utils.badScenarioEval(key, value, recursivityInd, value['webAppVersion'])

            if isAborted:
                testExecRaport = abortedRaport
            else:
                unittestDex.main(TC_Definition=value, testName=key,
                                tracker='{} / {}'.format(contor, totalScenarii))
                testExecRaport = raportGetter.getdict(full=False)
            statusDict['Result'].update(generateRaport(resultD=testExecRaport, testName=key, value=value, configType=value['configType']))
            executionDetails = {'testExec': testExecRaport,
                                'params': value}
            # print(executionDetails)
            utils.requestAPI(url="http://devops02.connex.ro:8030/executionSummary/appendExcelData", body=executionDetails, isPost=False)

            try:
                if 'Host' not in execDetails['Result'].keys():
                    execDetails['Result'].update({'Host': testExecRaport[key]["HOST Name"]})
                if 'RLS' not in execDetails['Result'].keys():
                    execDetails['Result'].update({'RLS': testExecRaport[key]["Release"]})
            except (KeyError, ValueError) as e:
                execDetails['Result'].update({'Host': 'Failed to retrive Host from result dictionary'})
                execDetails['Result'].update({'RLS': 'Failed to retrive RLS from result dictionary'})

    print('Executed -->> ', contor, ' <<-- Tests')

    return statusDict, testDefinitions, statusDict['Result'], execDetails['Result']

def main(isSanity=False, isFull = False, recursiveLimit = 2, rerunDict = None, iRecursiveProgressList = None, iStatusDict = None, recursivityInd = '', recursiveTracker = 0, dexEnv=None):
    startExec = datetime.now()
    dict2Rerun = {}
    criticalFailedList = []
    recursiveProgressList = iRecursiveProgressList


    if iStatusDict == None:
        iStatusDictInb = {}
    else:
        iStatusDictInb = iStatusDict

    negativeStatusDict, testDefinitions, statusDict, execDetails = controller(isSanity=isSanity, isFull=isFull, rerunDict=rerunDict, recursivityInd=recursivityInd, dexEnv=dexEnv)

    for key, value in statusDict.items():
        iStatusDictInb.update({key: value})

    dict2Rerun.clear()

    try:
        for key, value in negativeStatusDict['Result'].items():
            if 'CRITICAL' in value.upper():
                dict2Rerun[key] = testDefinitions[key]
                criticalFailedList.append(key)
    except (ValueError, AttributeError):
        pass

    recursiveTextSTOP = ''
    if recursiveLimit == 1 and criticalFailedList == recursiveProgressList:
        recursiveTextSTOP = ('\nIn urma apelului recursiv niciun test nu a fost cu succes, testul s-a oprit dupa {} - incercari, procesul a fost oprit.\n'.format(recursiveLimit))
        recursiveLimit = 0

    elif len(criticalFailedList) > 0 and recursiveLimit <= 2 and recursiveLimit > 0:
        for i in utils.getTempValue("RaportName"):
            os.remove(i)
            print(f'Removed : => {i}')

        main(isSanity=isSanity, isFull=isFull, recursiveLimit=recursiveLimit-1, rerunDict=dict2Rerun, iRecursiveProgressList=criticalFailedList, iStatusDict=iStatusDictInb, recursivityInd='_AfterRecursiv_{}'.format(recursiveLimit-1), recursiveTracker=recursiveTracker+1, dexEnv=dexEnv)
    else:
        recursiveLimit = 0

    statusLst = []
    resultCount = {'Passed': 0,
                   'Failed': 0,
                   'Critical': 0,
                   'Invalid_TestExecution': 0,
                   'INTERNAL_CODE_ERROR': 0,}

    if recursiveLimit == 0:
        try:
            for key, value in iStatusDictInb.items():
                if 'passed' in value.lower():
                    resultCount['Passed'] = resultCount['Passed'] + 1
                elif 'fail' == value.lower():
                    resultCount['Failed'] = resultCount['Failed'] + 1
                elif 'critical' in value.lower():
                    resultCount['Critical'] = resultCount['Critical'] + 1
                elif 'invalid_testexecution' in value.lower():
                    resultCount['Invalid_TestExecution'] = resultCount['Invalid_TestExecution'] + 1
                elif 'INTERNAL_CODE_ERROR' in value.upper():
                    resultCount['INTERNAL_CODE_ERROR'] = resultCount['INTERNAL_CODE_ERROR'] + 1
                statusLst.append(key + ' - ' + str(value))

        except ValueError:
            print('Nothing to populate from iStatusDictInb')

        text2Teams = f"""<pre>TestSuit: "{execDetails['Host']}"\nRelease: "{execDetails['RLS']}"\n\nTest Execution Summary:\n\tTotal: {len(statusLst)}\n\t• PASSED: {resultCount['Passed']}\n\t• FAILED: {resultCount['Failed']}\n\t• CRITICAL: {resultCount['Critical']}\n\t• INTERNAL_CODE_ERROR: {resultCount['INTERNAL_CODE_ERROR']}\n\t• Invalid_TestExecution: {resultCount['Invalid_TestExecution']}\nRecursive attempt: {recursiveTracker}\n\nSee raports at : {'http://devops02.connex.ro:8016/#/'}{recursiveTextSTOP}\n"""
        utils.writeToCSV(statusLst, text2Teams)


if __name__ == '__main__':
    startTime = datetime.now()
    # main(isSanity=False, isFull=True, dexEnv='pega_uat3') #DEXUAT1
    main(isSanity=False, isFull=True, dexEnv='dexuat2') #DEXUAT3/Localhost
    print(datetime.now() - startTime)
    #DockerRun
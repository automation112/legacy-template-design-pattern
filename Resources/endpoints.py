def getEndpoints(env=None, dexEnv=None, choice=None):

   endpoints={'dexuat1': {
   'dex' : 'http://127.0.0.1:10651/retail/login',
   "pegaenv": "pega_uat3", 
   'getSMSToken':'http://devops02.connex.ro:8030/getSMSToken/',
   'getPrepaid':'http://devops02.connex.ro:8020/getPrepaid/',
   'getSID':'http://devops02.connex.ro:8030/getSID/{}',
   'updateRM':'http://devops02.connex.ro:8030/updateRM/{}',
   'pegaOffers':'http://devops02.connex.ro:8020/getData/pegaOffers',
   'getICCID':'http://devops02.connex.ro:8020/getICCID/'
   },
    'dexuat1localhost': {
   'dex' : 'http://127.0.0.1:10651/retail/login',
   "pegaenv": "pega_uat3", 
   'getSMSToken':'http://devops02.connex.ro:8030/getSMSToken/',
   'getPrepaid':'http://devops02.connex.ro:8020/getPrepaid/',
   'getSID':'http://devops02.connex.ro:8030/getSID/{}',
   'updateRM':'http://devops02.connex.ro:8030/updateRM/{}',
   'pegaOffers':'http://devops02.connex.ro:8020/getData/pegaOffers',
   'getICCID':'http://devops02.connex.ro:8020/getICCID/'
   },
  'dexuat2': {
  'dex' : 'https://dexuat2.aws.connex.ro/retail/login',
  "pegaenv": "pega_uat2",
  'getSMSToken':'http://devops02.connex.ro:8030/getSMSToken/',
   'getPrepaid':'http://devops02.connex.ro:8020/getPrepaid/',
   'getSID':'http://devops02.connex.ro:8030/getSID/{}',
   'updateRM':'http://devops02.connex.ro:8030/updateRM/{}',
   'pegaOffers':'http://devops02.connex.ro:8020/getData/pegaOffers',
    'getICCID': 'http://devops02.connex.ro:8020/getICCID/'
  },
  'dexuat2localhost': {
  'dex' : 'http://127.0.0.1:20651/retail/login',
  "pegaenv": "pega_uat2",
  'getSMSToken':'http://devops02.connex.ro:8030/getSMSToken/',
   'getPrepaid':'http://devops02.connex.ro:8020/getPrepaid/',
   'getSID':'http://devops02.connex.ro:8030/getSID/{}',
   'updateRM':'http://devops02.connex.ro:8030/updateRM/{}',
   'pegaOffers':'http://devops02.connex.ro:8020/getData/pegaOffers',
    'getICCID': 'http://devops02.connex.ro:8020/getICCID/'
  },
   'dexuat3localhost':{
       'dex' : 'http://127.0.0.1:30651/retail/login',
       "pegaenv": "pega_uat1",
       'getSMSToken': 'http://devops02.connex.ro:8040/getSMSToken/',
       'getPrepaid': 'http://devops02.connex.ro:8020/getPrepaid/',
       'getSID': 'http://devops02.connex.ro:8040/getSID/{}',
       'updateRM': 'http://devops02.connex.ro:8030/updateRM/{}',
       'pegaOffers': 'http://devops02.connex.ro:8020/getData/pegaOffers',
       'getICCID': 'http://devops02.connex.ro:8020/getICCID/'},

    "dexEnv":{
        "dexuat1": {"url": "https://dexuat1.aws.connex.ro/retail/login",
                        "pegaenv": "pega_uat3",
                        "cm":'20'},
        "dexuat2": {"url": "https://dexuat2.aws.connex.ro/retail/login",
                        "pegaenv": "pega_uat2"},
        "dexuat2localhost": {"url": "http://127.0.0.1:20651/retail/login",
                        "pegaenv": "pega_uat2"},
        "dexuat3localhost": {"url": "http://127.0.0.1:30651/retail/login",
                        "pegaenv": "pega_uat1"}
        }
    }

   if dexEnv is None:
       return endpoints[env]
   else:
       if choice.lower() == 'url':
           return endpoints['dexEnv'][dexEnv.lower()]['url']
       else:
           return endpoints['dexEnv'][dexEnv.lower()]['pegaenv']


if __name__=='__main__':
   print(getEndpoints(dexEnv='dexuat2localhost', choice='url'))
   print(getEndpoints('30'))
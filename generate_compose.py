import yaml
import sys 
import glob
import os


if len(sys.argv) <= 1:
    rlsversion = 'Default'
    testType = 'S'
    env = '20'
else:
    rlsversion = sys.argv[1]
    testType = sys.argv[2]
    env = sys.argv[3]

if testType == 'F':
    FILE_PREFIX = 'retention'
else:
    FILE_PREFIX = 'Sanity'
FilePath = "./TestCaseOp/constants/"
FILE_PATH = './TestCaseOp/constants/{}*'.format(FILE_PREFIX)
FN = [ os.path.basename(x) for x in glob.glob(FILE_PATH)]
VolumeMapping = './Output/'+ rlsversion +':/Code/Output'

dic = {'version': '3.3', 'services': {} }
for f in FN:
    worker = 'worker'+f.split('.')[0]
    source = FilePath+f
    docker = f.split('.')[0]
    v_docker = 'DOCKER='+worker
    v_rls = 'RLS='+ rlsversion
    v_env = 'ENV='+env
    dic['services'][worker]={'image': 'dextest', 'volumes': [{'type': 'bind', 'source': source, 'target': '/Code/constants.txt'}, VolumeMapping , './TestCaseOp:/Code/TestCaseOp'], 'environment': [v_docker, v_rls,v_env], 'network_mode': 'host'}
print(yaml.dump(dic))    

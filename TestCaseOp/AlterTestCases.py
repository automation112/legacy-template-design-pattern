import datetime
import ast
import glob
import pathlib

testsCreatedDict = {
      'CTDATests': 0,
      'MergedTests': 0,
      'SanityTests': 0,
      'FilteredTests': 0,
      'OriginalKeys': []
}

def sortTests(tcDict):

   sortedDict = {'PachetPromo': {},
                 'ConfigOferte': {}}
   try:
      for key, value in tcDict['TestCases'].items():
         if value['offerChoice'] == 'PachetPromo':
            sortedDict['PachetPromo'].update({key: value})
         else:
            sortedDict['ConfigOferte'].update({key: value})

   except AttributeError:
      print(tcDict)
   return sortedDict


def getDictFromFile(fileLocation):

   open_dict = open(r"{}".format(fileLocation), "r")
   read_dict = open_dict.read()
   acc_dict = read_dict
   acc_dict = acc_dict.replace('â€™', '').replace('â€˜', '')

   return {'TestCases': ast.literal_eval(acc_dict)}


def split_dict(input_dict, chunks=2, location=None, key=None, splitType='', sortByKeys = False):
   if 'sanity' not in location.lower():
      with open(location + '\\Full_Updated_tests.txt', 'w') as opened_file:
         opened_file.write(str(input_dict))

   if sortByKeys == False:
      return_list = [dict() for idx in range(chunks)]
      idx = 0
      for k, v in input_dict.items():
         return_list[idx][k] = v
         if idx < chunks - 1:  # indexes start at 0
            idx += 1
         else:
            idx = 0

      contor = 0

      for i in return_list:
         with open('{}/{}Chunk_{}_{}.txt'.format(location, splitType, key, return_list.index(i) + 1), 'w') as opened_file:
            opened_file.write(str(return_list[return_list.index(i)]))
      contor += 1

   else:
      input_dict = sortTests(input_dict)
      print(input_dict)
      for key in input_dict:
         return_list = [dict() for idx in range(chunks)]
         idx = 0
         for k, v in input_dict.items():
            return_list[idx][k] = v
            if idx < chunks - 1:  # indexes start at 0
               idx += 1
            else:
               idx = 0

         contor = 0
         for i in return_list:
            with open('{}/{}Chunk_{}_{}.txt'.format(location, splitType, list(i.keys())[0], return_list.index(i) + 1),
                      'w') as opened_file:
               opened_file.write(str(return_list[return_list.index(i)]))
               # print(return_list[return_list.index(i)])
         contor += 1

   testsCreatedDict['CTDATests'] = len(input_dict)

   print('Files Created')


def sanityTestCases(fileLocation):
   updatedDict = {'rslt': {}}
   splitedDict = {'GA_PP' : {'Matur': {},
                          'NonMatur': {}},
                  'GA_CO': {'Matur': {},
                            'NonMatur': {}},
                  'GA': {},
                  'MNP': {},
                  'MFP': {}}

   testDefinitions = getDictFromFile(fileLocation)

   for key, value in testDefinitions['TestCases'].items():

      if '_nonmatur' in value['scenarioNameDC'].lower():
         value['searchValue'] = 722486663
         updatedDict['rslt'].update({key: value})
      else:
         updatedDict['rslt'].update({key: value})
         value['searchValue'] = 733044811

   offerflows = ['OferteConfigurabile_S2D_VMM_Rate_si_Discount','OferteConfigurabile_ServiceOnly_Discount_VMM', 'Scenariul_Config_Service_D2S' ,'OferteConfigurabile_D2S_Rate' ,'OferteConfigurabile_D2S_SimOnly' ,'OferteConfigurabile_D2S_SimOnly_si_Discount' ,'OferteConfigurabile_D2S_Rate_si_Discount' ,'OferteConfigurabile_S2D_Rate' ,'OferteConfigurabile_S2D_Rate_si_Discount' ,'OferteConfigurabile_S2D_SimOnly_si_Discount' ,'OferteConfigurabile_S2D_SimOnly' ,'OferteConfigurabile_S2D_VMM_Rate' ,'OferteConfigurabile_S2D_VMM_Rate_si_Discount' ,'OferteConfigurabile_ServiceOnly' ,'OferteConfigurabile_ServiceOnly_Discount' ,'OferteConfigurabile_D2S_VMM_Rate_si_Discount' ,'OferteConfigurabile_ServiceOnly_VMM' ,'OferteConfigurabile_S2D_VMM_SimOnly' ,'OferteConfigurabile_S2D_VMM_SimOnly_si_Discount' ,'OferteConfigurabile_D2S_VMM_Rate' ,'OferteConfigurabile_D2S_VMM_SimOnly' ,'OferteConfigurabile_D2S_Rate_si_Discount','BundleOffers_Vezi_Mai_Multe_Rate','BundleOffers_Vezi_Mai_Multe_Discount','BundleOffers_Vezi_Mai_Multe_Rate_Discount','BundleOffers_Vezi_Mai_Multe_SimOnly','BundleOffers_Rate','BundleOffers_SimOnly_si_Discount','BundleOffers_Rate_si_Discount,''BundleOffers_SimOnly']
   sanityDict = {'TestCases': {}}

   for key, value in updatedDict['rslt'].items():

      if value['abNouType'] == 'GA' and '_matur' in value['scenarioNameDC'].lower():
         if value['scenarioName_PC'] in offerflows:
            if '{}'.format(value['scenarioName_PC']) + '_GA_Matur' in sanityDict['TestCases']:
               pass
            else:
               testsCreatedDict['OriginalKeys'].append(key)
               sanityDict['TestCases'].update({'{}'.format(value['scenarioName_PC']) + '_GA_Matur': value})

      elif value['abNouType'] == 'GA' and '_nonmatur' in value['scenarioNameDC'].lower():
         if value['scenarioName_PC'] in offerflows:
            if '{}'.format(value['scenarioName_PC']) + '_GA_NonMatur' in sanityDict['TestCases']:
               pass
            else:
               testsCreatedDict['OriginalKeys'].append(key)
               sanityDict['TestCases'].update({'{}'.format(value['scenarioName_PC']) + '_GA_NonMatur': value})

      elif value['abNouType'] == 'MNP' and '_matur' in value['scenarioNameDC'].lower() and 'POSTPAID' in value['tipServiciu'] :
         if value['scenarioName_PC'] in offerflows:
            if '{}'.format(value['scenarioName_PC']) + '_MNP_MaturPostpaid' in sanityDict['TestCases']:
               pass
            else:
               testsCreatedDict['OriginalKeys'].append(key)
               sanityDict['TestCases'].update({'{}'.format(value['scenarioName_PC']) + '_MNP_MaturPostpaid': value})

      elif value['abNouType'] == 'MNP' and '_matur' in value['scenarioNameDC'].lower() and 'PREPAID' in value['tipServiciu'] :
         if value['scenarioName_PC'] in offerflows:
            if '{}'.format(value['scenarioName_PC']) + '_MNP_MaturPrepaid' in sanityDict['TestCases']:
               pass
            else:
               testsCreatedDict['OriginalKeys'].append(key)
               sanityDict['TestCases'].update({'{}'.format(value['scenarioName_PC']) + '_MNP_MaturPrepaid': value})

      elif value['abNouType'] == 'MNP' and '_nonmatur' in value['scenarioNameDC'].lower() and 'POSTPAID' in value['tipServiciu'] :
         if value['scenarioName_PC'] in offerflows:
            if '{}'.format(value['scenarioName_PC']) + '_MNP_NonMaturPOSTPAID' in sanityDict['TestCases']:
               pass
            else:
               testsCreatedDict['OriginalKeys'].append(key)
               sanityDict['TestCases'].update({'{}'.format(value['scenarioName_PC']) + '_MNP_NonMaturPOSTPAID': value})

      elif value['abNouType'] == 'MNP' and '_nonmatur' in value['scenarioNameDC'].lower() and 'PREPAID' in value['tipServiciu'] :
         if value['scenarioName_PC'] in offerflows:
            if '{}'.format(value['scenarioName_PC']) + '_MNP_NonMaturPrepaid' in sanityDict['TestCases']:
               pass
            else:
               testsCreatedDict['OriginalKeys'].append(key)
               sanityDict['TestCases'].update({'{}'.format(value['scenarioName_PC']) + '_MNP_NonMaturPrepaid': value})

      elif value['abNouType'] == 'MFP' and '_matur' in value['scenarioNameDC'].lower() :
         if value['scenarioName_PC'] in offerflows:
            if '{}'.format(value['scenarioName_PC']) + '_MFP_Matur' in sanityDict['TestCases']:
               pass
            else:
               testsCreatedDict['OriginalKeys'].append(key)
               sanityDict['TestCases'].update({'{}'.format(value['scenarioName_PC']) + '_MFP_Matur': value})

      elif value['abNouType'] == 'MFP' and '_nonmatur' in value['scenarioNameDC'].lower() :
         if value['scenarioName_PC'] in offerflows:
            if '{}'.format(value['scenarioName_PC']) + '_MFP_NonMatur' in sanityDict['TestCases']:
               pass
            else:
               testsCreatedDict['OriginalKeys'].append(key)
               sanityDict['TestCases'].update({'{}'.format(value['scenarioName_PC']) + '_MFP_NonMatur': value})

   fnlSanityTests = sortTests(sanityDict)

   with open('\\\\vffs\RO\\atp2$\Integration\EAI\Automatizare\DEX\TestCases\Sanity\SanityTestCasesFull.txt', 'w') as opened_file:
      opened_file.write(str(fnlSanityTests))

   for key in fnlSanityTests:
      if key == 'PachetPromo':
         chunk = 1
      else:
         chunk = 7

      split_dict(input_dict=fnlSanityTests[key], chunks=chunk, location='\\\\vffs\RO\\atp2$\Integration\EAI\Automatizare\DEX\TestCases\Sanity', key=key)

      testsCreatedDict['SanityTests'] = len(fnlSanityTests['PachetPromo']) + len(fnlSanityTests['ConfigOferte'])

def mergeCTDATests(sourceTestslocation, resultLocation):

   # create/overwrite the output file in the project folder
   # dict = open("\\\\vffs\RO\\atp2$\Integration\EAI\Automatizare\DEX\TestCases\CTDA_TestCases\ResultTestCaseMerge\\test_data_AQ.json", "w+", encoding="utf8")

   dict = open("{}\\{}_test_data_AQ.json".format(resultLocation, int(datetime.datetime.now().strftime('%Y%m%d'))), "w+", encoding="utf8")
   dict.close()
   # open the output file for append
   # dict = open("\\\\vffs\RO\\atp2$\Integration\EAI\Automatizare\DEX\TestCases\CTDA_TestCases\ResultTestCaseMerge\\test_data_AQ.json", "a+", encoding="utf8")
   dict = open("{}\\{}_test_data_AQ.json".format(resultLocation, int(datetime.datetime.now().strftime('%Y%m%d'))), "a+", encoding="utf8")

   # read each file from the tests folder
   # this uses {projects path}/input_data/ as tests folder
   contor = 0
   globFromTxt = glob.glob("{}\\*.txt".format(sourceTestslocation))
   for test in globFromTxt:
      contor += 1

      with open(test, "r", encoding="utf8") as single_test:
         if contor == 1:
            dict.write('{')
         dict.write("\"" + test.replace(sourceTestslocation, '').replace('.txt', '').replace('\\', '') + "\":")
         dict.writelines(single_test.readlines())
         if len(globFromTxt) != contor:
            dict.write(",")
         else:
            dict.write("}")
         dict.write("\n\n")
   dict.close()

   testsCreatedDict['MergedTests'] = contor


def filterResources(input_dict):

   alteredDict = {'Result': {}}
   for key, value in input_dict.items():
      if '_nonmatur' in value['scenarioNameDC'].lower():
         value['searchValue'] = 722486663
         alteredDict['Result'].update({key: value})

      else:
         value['searchValue'] = 733044811
         alteredDict['Result'].update({key: value})


   testsCreatedDict['FilteredTests'] = len(alteredDict['Result'])
   return alteredDict['Result']

def retentionProvSplit(res):

   for val in res.values():
      val['searchValue'] = 727277696

   rslt = {}
   contor = 0
   count = 1
   k = 0
   for key, value in res.items():
      k+=1
      contor += 1
      rslt.update({key: value})
      if contor == 4:
         with open(f'{str(pathlib.Path(__file__).parent.absolute())}\\constants\\retention_demoChunk_{count}.txt', 'w') as opened_file:
            opened_file.write(str(rslt))
         count += 1
         contor = 0
         rslt = {}
         print('Done')

   print(f'Done for {k} tests')

def main(merge=False, isRetention=False):

   if isRetention == False:
      if merge == True:
         mergeCTDATests(sourceTestslocation='\\\\vffs\RO\\atp2$\Integration\EAI\Automatizare\DEX\TestCases\CTDA_TestCases\\Achizitie Mobile', resultLocation='\\\\vffs\RO\\atp2$\Integration\EAI\Automatizare\DEX\TestCases\CTDA_TestCases\ResultTestCaseMerge\\')

      sanityTestCases('\\\\vffs\RO\\atp2$\Integration\EAI\Automatizare\DEX\TestCases\CTDA_TestCases\ResultTestCaseMerge\\{}_test_data_AQ.json'.format(int(datetime.datetime.now().strftime('%Y%m%d'))))

      input_dict = getDictFromFile(
         '\\\\vffs\RO\\atp2$\Integration\EAI\Automatizare\DEX\TestCases\CTDA_TestCases\ResultTestCaseMerge\\{}_test_data_AQ.json'.format(
            int(datetime.datetime.now().strftime('%Y%m%d'))))
      input_dict = sortTests(input_dict)


      for key, val in input_dict.items():
         val = filterResources(val)
         chunk = 0
         if key == 'ConfigOferte':
            chunk = 6
         else:
            chunk = 2
         split_dict(input_dict=val,chunks=chunk, location='\\\\vffs\RO\\atp2$\Integration\EAI\Automatizare\DEX\TestCases\Regresion_ModifiedCTDA_TestCases', splitType='Updated_CTDA_', sortByKeys=False, key=key)
         # split_dict(input_dict=getDictFromFile('\\\\vffs\RO\\atp2$\Integration\EAI\Automatizare\DEX\TestCases\CTDA_TestCases\ResultTestCaseMerge\\{}_test_data_AQ.json'.format(int(datetime.datetime.now().strftime('%Y%m%d')))),chunks=2, location='\\\\vffs\RO\\atp2$\Integration\EAI\Automatizare\DEX\TestCases\Regresion_ModifiedCTDA_TestCases', splitType='Updated_CTDA_', sortByKeys=True)

      testsCreatedDict['CTDATests'] = len(input_dict['PachetPromo']) + len(input_dict['ConfigOferte'])

      if merge == False:
         testsCreatedDict['MergedTests'] = 'Merge Not Required, Overrote Manually'

      with open(r'\\vffs\RO\atp2$\Integration\EAI\Automatizare\DEX\TestCases\AlterTestCases_Log.txt', 'w') as opened_file:
         opened_file.write(str(testsCreatedDict))

      print(testsCreatedDict)

   else:

      # mergeCTDATests(
      #    sourceTestslocation='\\\\vffs\RO\\atp2$\Integration\EAI\Automatizare\DEX\TestCases\CTDA_TestCases\\Retentie\\',
      #    resultLocation='\\\\vffs\RO\\atp2$\Integration\EAI\Automatizare\DEX\TestCases\CTDA_TestCases\ResultTestCaseMerge\\MergeTstRetentie\\')

      input_dict = getDictFromFile(
         '\\\\vffs\RO\\atp2$\Integration\EAI\Automatizare\DEX\TestCases\CTDA_TestCases\ResultTestCaseMerge\\MergeTstRetentie\\{}_test_data_AQ.json'.format(
            int(datetime.datetime.now().strftime('%Y%m%d'))))

      retentionProvSplit(input_dict['TestCases'])

if __name__ == '__main__':
   main(merge=False, isRetention=True)
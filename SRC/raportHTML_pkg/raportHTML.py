from json2html import *
import os
from pathlib import Path, PureWindowsPath
from SRC.utils_pkg import utilities
from SRC.utils_pkg import utilities, loggingMechanism


utils = utilities.utilities()

def generateRaport(resultD, testName, value, configType, retryNo=1):

    flag, reason = raportValidator(resultD, testName, configType)

    proj_path = utils.getTempValue('SRCPath')

    try:
        screenShotsPath = utils.to_posix_full_path('\\Output\\ScreenShots')

        raportPath = utils.to_posix_full_path('\\Output\\')

        dirPath = utils.to_posix_full_path('\\SRC\\raportHTML_pkg\\')

        with open(screenShotsPath + utils.to_posix_path('\\ScreenShots.txt'), 'r') as opened_file:
            screenShotsList = opened_file.readlines()

        screenShotsList = list(map(lambda s:s.strip(),screenShotsList))

        if flag == False:
            status = resultD[testName]['Status']

        elif resultD[testName]['Status'] != 'EXECUTION_ABORTED':
            resultD[testName]['Status'] = 'Invalid_TestExecution'
            status = 'Invalid_TestExecution'
            resultD[testName].update({'FAULT':'Invalid_TestExecution_REASON --> [{}]'.format(reason)})
        else:
            status = resultD[testName]['Status']

        b = json2html.convert(json=resultD)
        # open, write and close html file for each test
        raportName = raportPath + utils.to_posix_path('\\') + status + '-' + testName + ".html"
        open_html = open(raportName, "w")
        open_html.write(b)
        open_html.close()
        # open, read and close html file for each test and replace html file

        read_html = open(raportPath + utils.to_posix_path('\\') + status + '-' + testName + ".html", "r")

        contain = read_html.read()
        contain = contain.replace("<th>Status</th><td>FAIL</td>",
                                  "<th>Status</th><td style='background-color:Orange;'>FAILED</td>") \
            .replace("<th>Status</th><td>PASSED</td>",
                     "<th>Status</th><td style='background-color:MediumSeaGreen;'>PASSED</td>") \
            .replace("<th>Status</th><td>CRITICAL_FAIL</td>",
                     "<th>Status</th><td style='background-color:Tomato;'>CRITICAL_FAIL</td>") \
            .replace("<th>Status</th><td>Invalid_TestExecution</td>",
                     "<th>Status</th><td style='background-color:Tomato;'>Invalid_TestExecution</td>") \
            .replace("<th>Status</th><td>INTERNAL_CODE_ERROR</td>",
                     "<th>Status</th><td style='background-color:Tomato;'>INTERNAL_CODE_ERROR</td>") \
            .replace("test_page_Login", "Login") \
            .replace("test_page_Search", "Search") \
            .replace("test_page_Dashboard", "Dashboard") \
            .replace("test_page_configureaza", "Configureaza Oferte") \
            .replace("test_page_detalii_Utilizator_Si_Numar", "Utilizator si Numar") \
            .replace("test_page_detalii_UtilizatorTitular", "Titual Cont") \
            .replace("test_page_Detail_GDPR", "G.D.P.R.") \
            .replace("test_page_Cont_Nou", "Cont Nou") \
            .replace("test_page_SendToVOS", "Trimite in VOS") \
            .replace(testName.replace('&', '&amp;'),
                     testName + "<br><pre style='text-align:left;'>" + ' ' + str(value).replace("{",
                                                                                           "").replace(
                         "}", "").replace(",", ",\n") + "</pre>")

        with open(dirPath + utils.to_posix_path('\\enrichHTML.txt'), 'r') as opened_file:
            contain = opened_file.read().replace('ToBeReplaced', contain).replace('&amp;', '&')
        for i in screenShotsList:
            i = i.replace('\n', '')
            contain = contain.replace(i, '<br><img src="{}" alt="Snow" style="width:20%" onclick="myFunction(this);">'.format(i.replace('Output'+utils.to_posix_path('\\'),'')))

        read_html.close()
        expectedList = []

        # open, write and close html file for each test
        with open(raportPath + utils.to_posix_path('\\') + status + '-' + testName + ".html", 'w') as opened_file:
            opened_file.write(str(contain))
        # format_html_open = open(raportPath + utils.to_posix_path('\\') + status + '-' + testName + ".html", "w")
        # format_html_write = format_html_open.write(contain.replace('&amp;', '&'))
        # format_html_open.close()

        # trustedRaport = utils.resultValidation(resultD[testName]['Tests Execution Log'], trustedList)
        # oldFileName = projectDir.replace('main.py', "Raport\\" + resultD[testName]['Status'] + ' - ' + testName + ".html")
        # newFileName = projectDir.replace('main.py', "Raport\\" + 'FAULT' + ' - ' 'MISSING: ' + ' - '.join(
        #     trustedRaport) + ' - ' + testName + ".html")
        #
        # print(trustedRaport)
        # if len(trustedRaport) != 0:
        #     os.rename(oldFileName, str(newFileName))

        if 'critical' in raportName.lower():
            utils.setTempValue('RaportName', value=raportName, updateLst=True)

        return {testName: status}

    except FileNotFoundError as e:
        if retryNo == 1:
            with open(Path('Output/ScreenShots/ScreenShots.txt'), 'w') as opened_file:
                opened_file.write('')
        else:
            print(e)
            generateRaport(resultD=resultD, testName=testName, value=value, retryNo=retryNo-1, configType=configType)


def raportValidator(result, testName, configType):

    flag = False
    reason = ''
    trustedList = ['test_page_Login', 'test_page_Search', 'test_page_Dashboard',
                   'test_page_configureaza', 'test_page_detalii_Utilizator_Si_Numar',
                   'test_page_detalii_UtilizatorTitular', 'test_page_Detail_GDPR',
                   'test_page_Cont_Nou', 'test_page_SendToVOS']

    if configType == 'RETENTIE':
        trustedList = ['test_page_Login', 'test_page_Search', 'test_page_Dashboard',
                   'test_page_configureaza']

    keysFromRaport = []

    for key, value in result[testName]['Tests Execution Log'].items():
        if key in trustedList:
            keysFromRaport.append(key)

        if len(value['ExecutionLog']) == 0:
            flag = True
            reason = 'For {} step, no execution found'.format(key)

    if len(trustedList) != len(keysFromRaport):
        flag = True
        reason = 'Execution steps not fulfilled => [{}]'.format(list(set(trustedList) - set(keysFromRaport)))

    if result[testName]['Status'] == 'CRITICAL_FAIL' or result[testName]['Status'] == 'INTERNAL_CODE_ERROR':
        for i in range(len(keysFromRaport)):
            if trustedList[i] == keysFromRaport[i]:
                flag = False
            else:
                flag = True

    return False, reason



if __name__ == '__main__':
    resultD = {'Sales channels=Retail (Franciza & Store) Login=END Search by=MSISDN Customer found=True Flow type=Click on Numar Nou(38)_DEXAutomation': {'Status': 'CRITICAL_FAIL', 'HOST Name': 'Windows OS', 'Release': 'None Given, Hardcoded', 'EXECUTION_TS': '20200917131213', 'Tests Execution Log': {'test_page_Login': {'Status': 'CRITICAL_FAIL', 'ExecutionLog': {'Click_OK': 'PASSED', 'Type_UserName': 'FAIL --> [exceptie Message: \n pentru xPath : //input[@name="username1"]\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)] ScreenShots/Sales channels=Retail (Franciza & Store) Login=END Search by=MSISDN Customer found=True Flow type=Click on Numar Nou(38)_UserName - test_page_Login.png', 'Get Attribute Value_Password': 'PASSED', 'Type_Password': 'PASSED', 'Click_Login': 'PASSED', 'Select by index_DealerSelect': "FAIL --> [exceptie Message: \n pentru xPath : //select[@data-automation-id='select-dealer']\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)] ScreenShots/Sales channels=Retail (Franciza & Store) Login=END Search by=MSISDN Customer found=True Flow type=Click on Numar Nou(38)_DealerSelect - test_page_Login.png", 'Click_Continua': "CRITICAL_FAIL --> [exceptie Message: \n pentru xPath : //button[@data-automation-id='dealer-login-button']\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 10 sec de wait (TimeoutException)] ScreenShots/Sales channels=Retail (Franciza & Store) Login=END Search by=MSISDN Customer found=True Flow type=Click on Numar Nou(38)_Continua - test_page_Login.png"}}}}}
    testName = 'Sales channels=Retail (Franciza & Store) Login=END Search by=MSISDN Customer found=True Flow type=Click on Numar Nou(38)_DEXAutomation'
    value = {'channel': 'RETAIL', 'searchType': 'MSISDN', 'searchValue': 733044811, 'configType': 'ACHIZITIE', 'abNouType': 'GA', 'scenarioNameDC': 'CR_True_Client_Matur', 'offerChoice': 'ConfigOferte', 'scenarioName_PC': 'OferteConfigurabile_S2D_Rate', 'scenarioName_Utl_Nr': 'Scenariu_Numar_Random', 'scenarioName_isTitular': 'Utilizator_titular_Yes', 'scenarioName_GDPR': 'Scenariu_GDPR_allYES', 'scenarioContNou': 'Is_Cont_Nou_Required_Yes_FacturaElectr'}

    # generateRaport(resultD, testName, value)
from datetime import timedelta, datetime
import string
import random
import hashlib
import os
from pathlib import Path
from pathlib import Path, PureWindowsPath
import bcolors


class VDFLogging:

    def __init__(self, methodName=None, xpathDict=None, testCaseName=None, numeScenariu=None, browser=None, configType=None):
        self.methodName = methodName
        self.method_log = {methodName: {}}
        self.xpathDict = xpathDict
        self.TC_Name = testCaseName
        self.testCase_log = {testCaseName: {}}
        self.numeScenariu = numeScenariu
        self.browser = browser
        self.configType = configType
        self.logLevel = 1
        self.elementName = 'NoneGiven'
        self.priority = 'Unknown'
        self.action_type = 'Unknown'
        self.exception = ''
        self.action_Status = ''
        self.xpath = ''


    def priority_handler(self, xpath, priority, action_type, isException, exeption=None, browser=None, element_name=None):
        self.elementName = element_name
        self.priority = priority
        self.action_type = action_type
        self.exception = exeption
        self.xpath = xpath
        try:
            try:
                if element_name == None:
                    element_name = (list(self.xpathDict.keys())[list(self.xpathDict.values()).index(xpath)])
                    self.elementName= element_name
            except (ValueError, KeyError) as e:
                pass
            try:
                # print(self.method_log, '\n elementul este : =>', element_name)
                if 'continua' in element_name.lower() or 'abonament' in element_name.lower() or 'adauga' in element_name.lower() or 'caut' in element_name.lower() or 'login' in element_name.lower() or 'submit' in element_name.lower():
                    priority = 'CRITICAL'

                if  isException == False:
                    self.action_Status = "PASSED"
                    if priority == None:
                        self.method_log[self.methodName].update({action_type + '_' + element_name: "PASSED"})
                    elif priority.upper() == 'IGNORE':
                        pass
                        # print(element_name + action_type + priority + isException)
                    elif priority.upper() == 'CRITICAL':
                        self.method_log[self.methodName].update({action_type + '_' + element_name: "PASSED"})

                elif isException:
                    # print(element_name, priority, isException)
                    screenShot = self.screenShot(element_name + ' - ' + self.methodName)

                    if priority == None:
                        # print('Action log  -->', {action_type + '_' + element_name: 'FAIL --> [{}] {}'.format(exeption, screenShot)})
                        self.action_Status = "FAIL"
                        self.method_log[self.methodName].update({action_type + '_' + element_name: "FAIL --> [{}] {}".format(exeption, screenShot)})
                    elif priority.upper() == 'CRITICAL':
                        self.action_Status = "CRITICAL_FAIL"
                        self.method_log[self.methodName].update({action_type + '_' + element_name: "CRITICAL_FAIL --> [{}] {}".format(exeption, screenShot)})
                    elif priority.upper() == 'IGNORE':
                        pass
                        # print(element_name + action_type + priority + isException)
            except (ValueError, KeyError, AttributeError) as ex:
                pass
        finally: self.terminalLog()

    def screenShot(self, name, retryNo=1):
        screenShotList = []
        try:
            screenShotUUID = random.choice(string.ascii_letters) + str(datetime.now().time()).replace(":", '').replace('.','')
    
            filename_location = PureWindowsPath('Output\\ScreenShots\\')
            universal_filepath = Path(filename_location)
            screenShotName = (str(universal_filepath) + '/' + screenShotUUID + '_' + name + '.png').replace("test_page_Login", "Login") \
                .replace("test_page_Search", "Search") \
                .replace("test_page_Dashboard", "Dashboard") \
                .replace("test_page_configureaza", "Configureaza Oferte") \
                .replace("test_page_detalii_Utilizator_Si_Numar", "Utilizator si Numar") \
                .replace("test_page_detalii_UtilizatorTitular", "Titual Cont") \
                .replace("test_page_Detail_GDPR", "G.D.P.R.") \
                .replace("test_page_Cont_Nou", "Cont Nou") \
                .replace("test_page_SendToVOS", "Trimite in VOS")

            screenShotsPath = str(universal_filepath)

            with open('Output/ScreenShots/ScreenShots.txt', 'r') as opened_file:
                fileContent = opened_file.read()

            screenShotList.append(screenShotName)
            hashList = []
            if len(screenShotList) >= 8:
                for i in list(range(1, 9)):
                    hashList = hashlib.md5(open(screenShotList[int('-'+str(i))], 'rb').read()).hexdigest()
                print('Am generat lista hash --->> ', hashList)

            if len(set(hashList)) <= 1 and len(hashList) == 8:
                for i in screenShotList:
                    os.remove(i)
                    print('Am sters poza --->> ', i)
            else:
                self.browser.execute_script("document.body.style.zoom='70%'")
                self.browser.save_screenshot(screenShotName)
                # print('Am facut poza --->> ', screenShotName)
                self.browser.execute_script("document.body.style.zoom='100%'")

            with open('Output/ScreenShots/ScreenShots.txt', 'r') as opened_file:
                fileContent = opened_file.read()

            if screenShotName not in fileContent:
                with open('Output/ScreenShots/ScreenShots.txt', 'a') as opened_file:
                    opened_file.write(screenShotName + '\n')
            return screenShotName
        except FileNotFoundError as e:
            print(e)
            if retryNo==1:
                with open('Output/ScreenShots/ScreenShots.txt', 'w') as opened_file:
                    opened_file.write('')
                self.screenShot(name=name, retryNo=retryNo-1)


    def log_customStep(self, key, status):

        if 'fail' in status.lower() or 'critical' in status.lower():
            # print(key, self.methodName)
            screenShot = self.screenShot(key + ' - ' + self.methodName)
            self.method_log[self.methodName].update({key: status + ' ' + screenShot})
        else:
            self.method_log[self.methodName].update({key: status})

    #For every log message sent to this class, for current test name, stash into dict
    def constructTestCase_Log(self, methodLoggedRslt):
        self.testCase_log[self.TC_Name].update(methodLoggedRslt)

    def analyseLog(self, log_dict, log_type, host=None, release_name=None, exec_TS=None, connector_log=None):
        isNormalError = False
        isCriticalError = False
        isInternalError = False


        if log_type.upper() == 'METHOD':

            key = list(log_dict.keys())[0]
            value = list(log_dict.values())[0]

            for key, val in log_dict.items():
                for value_ToCheck in value.values():
                    if 'REASON' in value_ToCheck.upper():
                        isInternalError = True
                    elif 'CRITICAL' in value_ToCheck.upper():
                        isCriticalError = True
                    elif 'FAIL' in value_ToCheck.upper():
                        isNormalError = True
                    elif 'GENERATED NO EXECUTION LOG' in value_ToCheck[0].upper():
                        isNormalError = True

            if isInternalError == True:
                return True, {key: {'Status': 'INTERNAL_CODE_ERROR', 'ExecutionLog': value}}

            if isCriticalError == True:
                return True, {key: {'Status': 'CRITICAL_FAIL', 'ExecutionLog': value}}

            elif isNormalError == True:

                return False, {key: {'Status': 'FAIL', 'ExecutionLog': value}}
            else:
                return False, {key: {'Status': 'PASSED', 'ExecutionLog': value}}


        elif log_type.upper() == 'TESTCASE':

            isNormalError = False
            isCriticalError = False
            isInternalError = False

            key = self.TC_Name
            value = log_dict[self.TC_Name]

            for key123, val in log_dict[self.TC_Name].items():

                for value_ToCheck in val['ExecutionLog'].values():
                    if 'REASON' in value_ToCheck.upper():
                        isInternalError = True
                    elif 'CRITICAL' in value_ToCheck.upper():
                        isCriticalError = True
                    elif 'FAIL' in value_ToCheck.upper():
                        isNormalError = True



            # if self.configType == 'RETENTIE':
            #     del exec_TS['test_page_detalii_Utilizator_Si_Numar']
            #     del exec_TS['test_page_detalii_UtilizatorTitular']
            #     del exec_TS['test_page_Detail_GDPR']
            #     del exec_TS['test_page_Cont_Nou']
            #     del  exec_TS['test_page_SendToVOS']


            if isInternalError == True:
                return True, {key: {'Status': 'INTERNAL_CODE_ERROR', 'HOST Name': host, 'Release': release_name,'ConnectorLog': connector_log, 'EXECUTION_TS': exec_TS, 'Tests Execution Log': value}}
            elif isCriticalError == True:
                return True, {key: {'Status': 'CRITICAL_FAIL', 'HOST Name': host, 'Release': release_name,'ConnectorLog': connector_log, 'EXECUTION_TS': exec_TS, 'Tests Execution Log': value}}
            elif isNormalError == True:
                return False, {key: {'Status': 'FAIL', 'HOST Name': host, 'Release': release_name, 'ConnectorLog': connector_log,'EXECUTION_TS': exec_TS, 'Tests Execution Log': value}}
            else:
                return False, {key: {'Status': 'PASSED', 'HOST Name': host, 'Release': release_name,'ConnectorLog': connector_log, 'EXECUTION_TS': exec_TS, 'Tests Execution Log': value}}

    def terminalLog(self):
        if type(self.priority) == type(None):
            self.priority = 'Normal'

        if self.exception == None:
            self.exception = ''
        else:
            self.exception = self.exception.replace('\n', "")
            self.exception = f'\n\t* {bcolors.ERR}{self.exception}{bcolors.ENDC}'

        if type(self.elementName) == type(None):
            self.elementName = self.xpath

        self.elementName = f'{bcolors.BOLD}{self.elementName}{bcolors.ENDC}'

        if 'pass' in self.action_Status.lower():
            self.action_Status = f'{bcolors.OK}{self.action_Status}{bcolors.ENDC}'
            indicator = f'{bcolors.OKMSG}{bcolors.ENDC}'
        else:
            self.action_Status = f'{bcolors.ERR}{self.action_Status}{bcolors.ENDC}'
            indicator = f'{bcolors.ERRMSG}{bcolors.ENDC}'


        if self.logLevel == 0:
            print(f'{indicator}| Elementul este: => "{self.elementName}", executionStatus: => "{self.action_type}: {self.action_Status}" | LoggingPriority: => "{self.priority}" |')
        elif self.logLevel == 1:
            print(f'{indicator}| Elementul este: => "{self.elementName}", executionStatus: => "{self.action_type}: {self.action_Status}" | LoggingPriority: => "{self.priority}" |{self.exception}')
        elif self.logLevel == 2:
            print(f'{indicator}| Elementul este: => "{self.elementName}", executionStatus: => "{self.action_type}: {self.action_Status}" | LoggingPriority: => "{self.priority}" |\n{self.method_log}')
        elif self.logLevel == 3:
            print(f'{indicator}| Elementul este: => "{self.elementName}", executionStatus: => "{self.action_type}: {self.action_Status}" | LoggingPriority: => "{self.priority}" |{self.exception}\n{self.method_log}')
        elif self.logLevel == -1:
            print(f'{self.method_log}')

from selenium import webdriver
import unittest
import os
from SRC.utils_pkg import utilities, loggingMechanism
from SRC.test_Definitions.preCbu_test_page_definitions.page_detalii_pkg import page_SendToVos as sentToVOS, \
    page_Detalil_utilizatorTitular as UtilizatorTitular, page_Detail_ContNou_preCbu as contNou, page_Detail_GDPR as GDPR, \
    page_Detail_UtilizatorSiNumar_preCbu as utilizator_si_Numar, test as setDict
from datetime import datetime as dt
import time, bcolors, sys
from Resources import endpoints


# preCbu
from SRC.test_Definitions.preCbu_test_page_definitions.page_login_precbu import page_login_preCbu
from SRC.test_Definitions.preCbu_test_page_definitions.page_configureaza_pkg.page_configureaza_PreCBU_Achizitie import page_configureaza_PreCbu_Achizitie
from SRC.test_Definitions.preCbu_test_page_definitions.page_configureaza_pkg.page_configureaza_PreCBU_Retentie import page_configureaza_preCbu_Retentie
from SRC.test_Definitions.preCbu_test_page_definitions.page_search_pkg.page_search_preCbu import page_search_preCbu
from SRC.test_Definitions.preCbu_test_page_definitions.page_dashboard_pkg.page_dashboard_PreCbu import page_dashboard_preCbu
from SRC.test_Definitions.preCbu_test_page_definitions.page_detalii_pkg.page_Detail_UtilizatorSiNumar_preCbu import UtilizatorSiNumarLayer_preCbu
from SRC.test_Definitions.preCbu_test_page_definitions.page_detalii_pkg.page_Detail_ContNou_preCbu import contNou_Layer_preCbu
from SRC.test_Definitions.preCbu_test_page_definitions.page_detalii_pkg.page_Detail_Detalii_Client_preCbu import Page_Detail_Detalii_Client_Layer_preCbu


# @Cbu
from SRC.test_Definitions.cbu_test_page_definitions.page_login_cbu_oms import page_login_cbu_oms
from SRC.test_Definitions.cbu_test_page_definitions.page_configureaza_pkg.page_configureaza_CBU_oms_Achizitie import page_configureaza_cbu_oms_Achizitie
from SRC.test_Definitions.cbu_test_page_definitions.page_configureaza_pkg.page_configureaza_CBU_oms_Retentie import page_configureaza_Cbu_oms_Retentie
from SRC.test_Definitions.cbu_test_page_definitions.page_search_pkg.page_search_Cbu_oms import page_search_Cbu_oms
from SRC.test_Definitions.cbu_test_page_definitions.page_dashboard_pkg.page_dashboard_cbu_oms import page_dashboard_cbu_oms
from SRC.test_Definitions.cbu_test_page_definitions.page_detalii_pkg.page_Detail_UtilizatorSiNumar_cbu_oms import UtilizatorSiNumarLayer_cbu_oms
from SRC.test_Definitions.cbu_test_page_definitions.page_detalii_pkg.page_Detail_ContNou_cbu_oms import contNou_Layer_cbu_oms
from SRC.test_Definitions.cbu_test_page_definitions.page_detalii_pkg.page_Detail_Detalii_Client_cbu_oms import Page_Detail_Detalii_Client_Layer_cbu_oms




class CustomResult(unittest.TestResult):
    def addFailure(self, test, err):
        critical = ['test4', 'test7']
        if test._testMethodName in critical:
            print("Critical Failure!")
            self.stop()
        unittest.TestResult.addFailure(self, test, err)


class unittestDex(unittest.TestCase):

    logging = loggingMechanism.VDFLogging()
    utils = utilities.utilities()
    # page_searchObj = page_search_preCbu.page_search()


    page_detalii_UtilizatorTitularObj = UtilizatorTitular.utilizatorTitularLayer()
    page_detalii_GDPRObj = GDPR.GDPR_Layer()
    page_detalii_sentToVosObj = sentToVOS.sendToVOS_Layer()

    page_configureaza_ClassList = [page_configureaza_PreCbu_Achizitie, page_configureaza_preCbu_Retentie, page_configureaza_cbu_oms_Achizitie, page_configureaza_Cbu_oms_Retentie]
    page_Search_ClassList = [page_search_preCbu, page_search_Cbu_oms]
    page_Dasboard_ClassList = [page_dashboard_preCbu, page_dashboard_cbu_oms]
    page_Detalii_UtilizatorSiNumarLayer_ClassList = [UtilizatorSiNumarLayer_cbu_oms, UtilizatorSiNumarLayer_preCbu]
    page_Detalii_contNou_ClassList = [contNou_Layer_cbu_oms, contNou_Layer_preCbu]
    page_Detail_Detalii_Client_Layer_ClassList = [Page_Detail_Detalii_Client_Layer_cbu_oms, Page_Detail_Detalii_Client_Layer_preCbu]
    page_login_ClassList = [page_login_cbu_oms, page_login_preCbu]
    #page_detaliiObj = page_detalii.page_detalii()

    def __init__(self, metname, scenarioName_SendNotif=None, retention_option = None, scenarioName_isTitular=type(None), searchType=type(None), searchValue=type(None), configType=type(None), abNouType=type(None), scenarioNameDC=type(None),
                 channel=type(None), retentieType=type(None), offerChoice=type(None), scenarioName_PC=type(None),scenarioName_Utl_Nr=type(None),
                 scenarioContNou=type(None), scenarioName_GDPR=type(None), tipFactura=type(None), testCaseName=type(None), scenarioName_NrNou=type(None), tip_contract=type(None), tipServiciu=type(None), retention_flow=None, inputDictionary=None):

        super(unittestDex, self).__init__(metname)
        self.inputDictionary = inputDictionary
        self.searchType = searchType
        self.searchValue = searchValue
        self.configType = configType
        self.abNouType = abNouType
        self.scenarioNameDC = scenarioNameDC
        self.retentieType = retentieType
        self.offerChoice = offerChoice
        self.scenarioName_PC = scenarioName_PC
        self.scenarioName_GDPR = scenarioName_GDPR
        self.scenarioName_NrNou = scenarioName_NrNou
        self.tipFactura = tipFactura
        self.testCaseName = testCaseName
        self.tip_contract = tip_contract
        self.scenarioName_Utl_Nr = scenarioName_Utl_Nr
        self.scenarioContNou = scenarioContNou
        self.scenarioName_isTitular = scenarioName_isTitular
        self.channel = channel
        self.tipServiciu = tipServiciu
        self.scenarioName_SendNotif = scenarioName_SendNotif
        self.retention_option = retention_option
        self.retention_flow = retention_flow
        self.logging.__init__(configType=configType, testCaseName=testCaseName)
        self.utils.__init__(numeScenariu=self.testCaseName, inputDictionary=self.inputDictionary)

    @classmethod
    def setUpClass(self):
        if 'nt' in os.name.lower():
            import psutil
            # for proc in psutil.process_iter():
            #     if proc.name() == "chromedriver.exe":
            #         proc.kill()

            time.sleep(1)
            caps = webdriver.DesiredCapabilities.CHROME.copy()
            caps['goog:loggingPrefs'] = {'performance': 'ALL'}
            options = webdriver.ChromeOptions()
            # options.add_argument(("--proxy-server={0}".format(self.proxy.proxy)))
            options.add_argument("--start-maximized")
            options.add_argument('ignore-certificate-errors')
            options.add_argument("--incognito")
            # options.add_argument('headless')
            # driver = ChromeDriver(options)
            caps['acceptInsecureCerts'] = True
            #self.browser = webdriver.Ie(capabilities=caps, executable_path=r'C:\Users\FagetL\Downloads\IEDriverServer_Win32_3.150.1\chromedriver.exe')
            SRCPath = self.utils.getTempValue('SRCPath')
            projectDir = '{}/Resources/Drivers'.format(SRCPath)
            executable_path = os.path.join(projectDir, "chromedriver.exe")
            # self.browser = webdriver.Firefox(executable_path=executable_path,options=options)
            self.browser = webdriver.Chrome(executable_path=executable_path, options=options, desired_capabilities=caps)
            self.browser.get(unittestDex.utils.getTempValue('URL')['dex'])
            self.browser.implicitly_wait(15)
            unittestDex.utils.__init__(browser=self.browser)
            time.sleep(4)

        else:

            import undetected_chromedriver as uc
            uc.install()
            from selenium.webdriver import Chrome, ChromeOptions
            options = ChromeOptions()
            options.headless = True
            # user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36'
            user_agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)'
            options.binary_location = '/usr/bin/google-chrome'
            options.add_argument('no-sandbox')
            options.add_argument('headless')
            options.add_argument('disable-dev-shm-usage')
            options.add_argument(f'user-agent={user_agent}')
            options.add_argument("window-size=1920,1080")
            options.add_argument("--incognito")
            self.browser = Chrome(options=options)
            self.browser.get(unittestDex.utils.getTempValue('URL')['dex'])
            # print(self.browser.page_source)
            time.sleep(10)
            unittestDex.utils.__init__(browser=self.browser)
        #TODO always catch exception if any and handle in custom mechanism or RIP

    def test_Login(self):
        try:

            page_loginObj = unittestDex.utils.generateClassInstance(unittestDex.page_login_ClassList, self.inputDictionary['webAppVersion'])
            page_loginObj.__init__(testCaseName=self.testCaseName, browser=self.browser, channel=self.channel)
            print(f'@Data Object signature ["{page_loginObj}"], parent ClassName = ["{page_loginObj.__class__.__name__}"]')
            rsltDict = page_loginObj.test_Login()

            if type(rsltDict) == type(None):
                rsltDict = {'test_login':'Generated no execution log, please verify for scenario name {}'.format(self.testCaseName)}

            isCritical, rsltDict = unittestDex.logging.analyseLog(log_dict=rsltDict, log_type='METHOD')

            contor = 0
            while unittestDex.utils.element_exists('//button[@data-automation-id="refresh-error-page"]'):
                unittestDex.utils.click_js("""'//button[@data-automation-id="refresh-error-page"]'""")
                rsltDict = page_loginObj.test_Login()
                if type(rsltDict) == type(None):
                    rsltDict = {'test_login': 'Generated no execution log, please verify for scenario name {}'.format(
                        self.testCaseName)}
                isCritical, rsltDict = unittestDex.logging.analyseLog(log_dict=rsltDict, log_type='METHOD')

                if contor == 3:
                    break
                else:
                    contor += 1

            if isCritical == True:
                unittestDex.logging.constructTestCase_Log(rsltDict)
                self.browser.close()
                # unittest.TestResult.addFailure(self, test, err)
                # self.browser.close()
            else:
                unittestDex.logging.constructTestCase_Log(rsltDict)
        except Exception as e:
            e = str(e).replace("\n", "")
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(f'{bcolors.WARN}{bcolors.BLUEIC}=====================================\n', "BrowserID missing, probably from CRITICAL FAIL scenario\n",
                  f'Error name => ["{exc_type}"]\nIn file => ["{fname}"]\nAt line => ["{exc_tb.tb_lineno}"]\nStack trace => ["{self.utils.format_stacktrace()}"]',
                  f'\n-------------------------------------\n{bcolors.ENDC}')

    def test_search(self):

        try:
            page_searchObj = unittestDex.utils.generateClassInstance(unittestDex.page_Search_ClassList, self.inputDictionary['webAppVersion'])
            print(f'@Data Object signature ["{page_searchObj}"], parent ClassName = ["{page_searchObj.__class__.__name__}"]')
            page_searchObj.__init__(testCaseName=self.testCaseName, browser=self.browser, searchType=self.searchType, searchValue=self.searchValue, inputDictionary=self.inputDictionary)
            rsltDict = page_searchObj.page_search()

            if type(rsltDict) == type(None):
                rsltDict = {'test_login': 'Generated no execution log, please verify for scenario name {}'.format(
                    self.testCaseName)}
            isCritical, rsltDict = unittestDex.logging.analyseLog(log_dict=rsltDict, log_type='METHOD')

            contor = 0
            while unittestDex.utils.element_exists('//button[@data-automation-id="refresh-error-page"]'):
                unittestDex.utils.click_js("""'//button[@data-automation-id="refresh-error-page"]'""")

                rsltDict = page_searchObj.page_search()

                if type(rsltDict) == type(None):
                    rsltDict = {'test_login': 'Generated no execution log, please verify for scenario name {}'.format(
                        self.testCaseName)}

                isCritical, rsltDict = unittestDex.logging.analyseLog(log_dict=rsltDict, log_type='METHOD')

                if contor == 3:
                    break
                else:
                    contor += 1

            if isCritical == True:
                unittestDex.logging.constructTestCase_Log(rsltDict)
                self.browser.close()
                # unittest.TestResult.addFailure(self, test, err)
                # self.browser.close()
            else:
                unittestDex.logging.constructTestCase_Log(rsltDict)
        except Exception as e:
            e = str(e).replace("\n", "")
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(f'{bcolors.WARN}{bcolors.BLUEIC}=====================================\n',
                  f'Error name => ["{exc_type}"]\nIn file => ["{fname}"]\nAt line => ["{exc_tb.tb_lineno}"]\nStack trace => ["{self.utils.format_stacktrace()}"]',
                  f'\n-------------------------------------\n{bcolors.ENDC}')

    def test_dashboard_CustDetails(self):
        pass

    def test_dashboard_Config(self):

        try:
            page_dashboardObj = unittestDex.utils.generateClassInstance(unittestDex.page_Dasboard_ClassList, self.inputDictionary['webAppVersion'])
            print(f'@Data Object signature ["{page_dashboardObj}"], parent ClassName = ["{page_dashboardObj.__class__.__name__}"]')
            page_dashboardObj.__init__(testCaseName=self.testCaseName,
                                                   retention_option=self.retention_option, configType=self.configType,
                                                   inputDictionary=self.inputDictionary,
                                                   abNouType=self.abNouType, scenarioNameDC=self.scenarioNameDC,
                                                   browser=self.browser, tipServiciu=self.tipServiciu,
                                                   retention_flow=self.retention_flow)

            rsltDict = page_dashboardObj.test_dashboard_Config()

            if type(rsltDict) == type(None):
                rsltDict = {'test_login': 'Generated no execution log, please verify for scenario name {}'.format(
                    self.testCaseName)}

            isCritical, rsltDict = unittestDex.logging.analyseLog(log_dict=rsltDict, log_type='METHOD')

            contor = 0
            while unittestDex.utils.element_exists('//button[@data-automation-id="refresh-error-page"]'):
                unittestDex.utils.click_js("""//button[@data-automation-id="refresh-error-page"]""")
                unittestDex.utils.wait_loadingCircle()
                rsltDict = page_dashboardObj.test_dashboard_Config()

                if type(rsltDict) == type(None):
                    rsltDict = {'test_login': 'Generated no execution log, please verify for scenario name {}'.format(
                        self.testCaseName)}

                isCritical, rsltDict = unittestDex.logging.analyseLog(log_dict=rsltDict, log_type='METHOD')

                if contor == 3:
                    break
                else:
                    contor += 1

            if isCritical == True:
                unittestDex.logging.constructTestCase_Log(rsltDict)
                self.browser.close()
            else:
                unittestDex.logging.constructTestCase_Log(rsltDict)
        except Exception as e:
            e = str(e).replace("\n", "")
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(f'{bcolors.WARN}{bcolors.BLUEIC}=====================================\n',
                  f'Error name => ["{exc_type}"]\nIn file => ["{fname}"]\nAt line => ["{exc_tb.tb_lineno}"]\nStack trace => ["{self.utils.format_stacktrace()}"]',
                  f'\n-------------------------------------\n{bcolors.ENDC}')

    def test_page_Configureaza(self):

        try:
            page_configureazaObj = unittestDex.utils.generateClassInstance(unittestDex.page_configureaza_ClassList, self.configType)
            print(f'@Data Object signature ["{page_configureazaObj}"], parent ClassName = ["{page_configureazaObj.__class__.__name__}"]')
            page_configureazaObj.__init__(testCaseName=self.testCaseName, inputDictionary=self.inputDictionary, scenarioNameDC=self.scenarioNameDC, offerChoice=self.offerChoice,
                                                      scenarioName_PC=self.scenarioName_PC, browser=self.browser, configType=self.configType,
                                                      channel=self.channel, abNouType=self.abNouType, tipServiciu=self.tipServiciu, retention_flow=self.retention_flow, retention_option=self.retention_option)

            rsltDict = page_configureazaObj.test_page_configureaza()

            if type(rsltDict) == type(None):
                rsltDict = {'test_login': 'Generated no execution log, please verify for scenario name {}'.format(
                    self.testCaseName)}

            isCritical, rsltDict = unittestDex.logging.analyseLog(log_dict=rsltDict, log_type='METHOD')

            contor = 0
            while unittestDex.utils.element_exists('//button[@data-automation-id="refresh-error-page"]'):
                unittestDex.utils.click_js("""'//button[@data-automation-id="refresh-error-page"]'""")
                unittestDex.utils.wait_loadingCircle()
                rsltDict = page_configureazaObj.test_page_configureaza()

                if type(rsltDict) == type(None):
                    rsltDict = {'test_login': 'Generated no execution log, please verify for scenario name {}'.format(
                        self.testCaseName)}

                isCritical, rsltDict = unittestDex.logging.analyseLog(log_dict=rsltDict, log_type='METHOD')

                if contor == 3:
                    break
                else:
                    contor += 1

            if isCritical == True:
                unittestDex.logging.constructTestCase_Log(rsltDict)
                self.browser.close()
            else:
                unittestDex.logging.constructTestCase_Log(rsltDict)
        except Exception as e:
            e = str(e).replace("\n", "")
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(f'{bcolors.WARN}{bcolors.BLUEIC}=====================================\n',
                  f'Error name => ["{exc_type}"]\nIn file => ["{fname}"]\nAt line => ["{exc_tb.tb_lineno}"]\nStack trace => ["{self.utils.format_stacktrace()}"]',
                  f'\n-------------------------------------\n{bcolors.ENDC}')

    def test_page_Detail_Detalii_Client(self):
        try:
            page_detalii_Detalii_Client = unittestDex.utils.generateClassInstance(
                unittestDex.page_Detail_Detalii_Client_Layer_ClassList, self.inputDictionary['webAppVersion'])
            print(
                f'@Data Object signature ["{page_detalii_Detalii_Client}"], parent ClassName = ["{page_detalii_Detalii_Client.__class__.__name__}"]')
            page_detalii_Detalii_Client.__init__(scenarioName_SendNotif=self.scenarioName_SendNotif,
                                                 inputDictionary=self.inputDictionary,
                                                 testCaseName=self.testCaseName, abNouType=self.abNouType,
                                                 scenarioContNou=self.scenarioContNou,
                                                 scenarioName_Utl_Nr=self.scenarioName_Utl_Nr,
                                                 browser=self.browser, tip_contract=self.tip_contract,
                                                 tipServiciu=self.tipServiciu)

            rsltDict = page_detalii_Detalii_Client.test_page_Detail_detalii_Client()

            if type(rsltDict) == type(None):
                rsltDict = {'test_login': 'Generated no execution log, please verify for scenario name {}'.format(
                    self.testCaseName)}

            isCritical, rsltDict = unittestDex.logging.analyseLog(log_dict=rsltDict, log_type='METHOD')

            contor = 0
            while unittestDex.utils.element_exists('//button[@data-automation-id="refresh-error-page"]'):
                unittestDex.utils.click_js("""'//button[@data-automation-id="refresh-error-page"]'""")
                unittestDex.utils.wait_loadingCircle()
                rsltDict = page_detalii_Detalii_Client.test_page_Detail_UtilizatorSiNumar()
                if type(rsltDict) == type(None):
                    rsltDict = {'test_login': 'Generated no execution log, please verify for scenario name {}'.format(
                        self.testCaseName)}

                isCritical, rsltDict = unittestDex.logging.analyseLog(log_dict=rsltDict, log_type='METHOD')

                if contor == 3:
                    break
                else:
                    contor += 1

            if isCritical == True:
                unittestDex.logging.constructTestCase_Log(rsltDict)
                self.browser.close()
            else:
                unittestDex.logging.constructTestCase_Log(rsltDict)
        except Exception as e:
            e = str(e).replace("\n", "")
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(f'{bcolors.WARN}{bcolors.BLUEIC}=====================================\n',
                  f'Error name => ["{exc_type}"]\nIn file => ["{fname}"]\nAt line => ["{exc_tb.tb_lineno}"]\nStack trace => ["{self.utils.format_stacktrace()}"]',
                  f'\n-------------------------------------\n{bcolors.ENDC}')

    def test_page_Detail_UtilizatorSiNumar(self):

        try:
            page_detalii_utilizator_si_NumarObj = unittestDex.utils.generateClassInstance(unittestDex.page_Detalii_UtilizatorSiNumarLayer_ClassList, self.inputDictionary['webAppVersion'])
            print(f'@Data Object signature ["{page_detalii_utilizator_si_NumarObj}"], parent ClassName = ["{page_detalii_utilizator_si_NumarObj.__class__.__name__}"]')
            page_detalii_utilizator_si_NumarObj.__init__(scenarioName_SendNotif=self.scenarioName_SendNotif, inputDictionary=self.inputDictionary, testCaseName=self.testCaseName, abNouType=self.abNouType,scenarioContNou=self.scenarioContNou, scenarioName_Utl_Nr=self.scenarioName_Utl_Nr, browser=self.browser, tip_contract=self.tip_contract, tipServiciu=self.tipServiciu)

            rsltDict = page_detalii_utilizator_si_NumarObj.test_page_Detail_UtilizatorSiNumar()

            if type(rsltDict) == type(None):
                rsltDict = {'test_login': 'Generated no execution log, please verify for scenario name {}'.format(
                    self.testCaseName)}

            isCritical, rsltDict = unittestDex.logging.analyseLog(log_dict=rsltDict, log_type='METHOD')

            contor = 0
            while unittestDex.utils.element_exists('//button[@data-automation-id="refresh-error-page"]'):
                unittestDex.utils.click_js("""'//button[@data-automation-id="refresh-error-page"]'""")
                unittestDex.utils.wait_loadingCircle()
                rsltDict = page_detalii_utilizator_si_NumarObj.test_page_Detail_UtilizatorSiNumar()
                if type(rsltDict) == type(None):
                    rsltDict = {'test_login': 'Generated no execution log, please verify for scenario name {}'.format(
                        self.testCaseName)}

                isCritical, rsltDict = unittestDex.logging.analyseLog(log_dict=rsltDict, log_type='METHOD')

                if contor == 3:
                    break
                else:
                    contor += 1

            if isCritical == True:
                unittestDex.logging.constructTestCase_Log(rsltDict)
                self.browser.close()
            else:
                unittestDex.logging.constructTestCase_Log(rsltDict)
        except Exception as e:
            e = str(e).replace("\n", "")
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(f'{bcolors.WARN}{bcolors.BLUEIC}=====================================\n',
                  f'Error name => ["{exc_type}"]\nIn file => ["{fname}"]\nAt line => ["{exc_tb.tb_lineno}"]\nStack trace => ["{self.utils.format_stacktrace()}"]',
                  f'\n-------------------------------------\n{bcolors.ENDC}')

    def test_page_Detalil_utilizatorTitular(self):

        try:
            unittestDex.page_detalii_UtilizatorTitularObj.__init__(testCaseName=self.testCaseName, browser=self.browser, inputDictionary=self.inputDictionary, scenarioName_isTitular=self.scenarioName_isTitular)
            rsltDict = unittestDex.page_detalii_UtilizatorTitularObj.test_page_Detalil_utilizatorTitular()

            if type(rsltDict) == type(None):
                rsltDict = {'test_login': 'Generated no execution log, please verify for scenario name {}'.format(
                    self.testCaseName)}

            isCritical, rsltDict = unittestDex.logging.analyseLog(log_dict=rsltDict, log_type='METHOD')

            contor = 0
            while unittestDex.utils.element_exists('//button[@data-automation-id="refresh-error-page"]'):
                unittestDex.utils.click_js("""'//button[@data-automation-id="refresh-error-page"]'""")
                unittestDex.utils.wait_loadingCircle()
                rsltDict = unittestDex.page_detalii_UtilizatorTitularObj.test_page_Detalil_utilizatorTitular()

                if type(rsltDict) == type(None):
                    rsltDict = {'test_login': 'Generated no execution log, please verify for scenario name {}'.format(
                        self.testCaseName)}

                isCritical, rsltDict = unittestDex.logging.analyseLog(log_dict=rsltDict, log_type='METHOD')

                if contor == 3:
                    break
                else:
                    contor += 1

            if isCritical == True:
                unittestDex.logging.constructTestCase_Log(rsltDict)
                self.browser.close()
            else:
                unittestDex.logging.constructTestCase_Log(rsltDict)
        except Exception as e:
            e = str(e).replace("\n", "")
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(f'{bcolors.WARN}{bcolors.BLUEIC}=====================================\n',
                  f'Error name => ["{exc_type}"]\n \tIn file => ["{fname}"]\n \t\tAt line => ["{exc_tb.tb_lineno}"]\n \t\t\tStack trace => ["{self.utils.format_stacktrace()}"]',
                  f'\n-------------------------------------\n{bcolors.ENDC}')

    def test_page_Detail_GDPR(self):
        try:
            unittestDex.page_detalii_GDPRObj.__init__(testCaseName=self.testCaseName, inputDictionary=self.inputDictionary, browser=self.browser, scenarioName_GDPR=self.scenarioName_GDPR)
            rsltDict = unittestDex.page_detalii_GDPRObj.test_page_Detail_GDPR()

            if type(rsltDict) == type(None):
                rsltDict = {'test_login': 'Generated no execution log, please verify for scenario name {}'.format(
                    self.testCaseName)}

            isCritical, rsltDict = unittestDex.logging.analyseLog(log_dict=rsltDict, log_type='METHOD')

            contor = 0
            while unittestDex.utils.element_exists('//button[@data-automation-id="refresh-error-page"]'):
                unittestDex.utils.click_js("""'//button[@data-automation-id="refresh-error-page"]'""")
                unittestDex.utils.wait_loadingCircle()
                rsltDict = unittestDex.page_detalii_GDPRObj.test_page_Detail_GDPR()

                if type(rsltDict) == type(None):
                    rsltDict = {'test_login': 'Generated no execution log, please verify for scenario name {}'.format(
                        self.testCaseName)}

                isCritical, rsltDict = unittestDex.logging.analyseLog(log_dict=rsltDict, log_type='METHOD')

                if contor == 3:
                    break
                else:
                    contor += 1

            if isCritical == True:
                unittestDex.logging.constructTestCase_Log(rsltDict)
                self.browser.close()
            else:
                unittestDex.logging.constructTestCase_Log(rsltDict)
        except Exception as e:
            e = str(e).replace("\n", "")
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(f'{bcolors.WARN}{bcolors.BLUEIC}=====================================\n',
                  f'Error name => ["{exc_type}"]\nIn file => ["{fname}"]\nAt line => ["{exc_tb.tb_lineno}"]\nStack trace => ["{self.utils.format_stacktrace()}"]',
                  f'\n-------------------------------------\n{bcolors.ENDC}')

    def test_page_Detail_ContNou(self):
        try:
            page_detalii_contNouObj = unittestDex.utils.generateClassInstance(unittestDex.page_Detalii_contNou_ClassList, self.inputDictionary['webAppVersion'])

            page_detalii_contNouObj.__init__(testCaseName=self.testCaseName, inputDictionary=self.inputDictionary , browser=self.browser, scenarioContNou=self.scenarioContNou)
            rsltDict = page_detalii_contNouObj.test_page_Detail_ContNou()

            if type(rsltDict) == type(None):
                rsltDict = {'test_login': 'Generated no execution log, please verify for scenario name {}'.format(
                    self.testCaseName)}

            isCritical, rsltDict = unittestDex.logging.analyseLog(log_dict=rsltDict, log_type='METHOD')

            contor = 0
            while unittestDex.utils.element_exists('//button[@data-automation-id="refresh-error-page"]'):
                unittestDex.utils.click_js("""'//button[@data-automation-id="refresh-error-page"]'""")
                unittestDex.utils.wait_loadingCircle()
                rsltDict = page_detalii_contNouObj.test_page_Detail_ContNou()

                if type(rsltDict) == type(None):
                    rsltDict = {'test_login': 'Generated no execution log, please verify for scenario name {}'.format(
                        self.testCaseName)}

                isCritical, rsltDict = unittestDex.logging.analyseLog(log_dict=rsltDict, log_type='METHOD')

                if contor == 3:
                    break
                else:
                    contor += 1

            if isCritical == True:
                unittestDex.logging.constructTestCase_Log(rsltDict)
                self.browser.close()
            else:
                unittestDex.logging.constructTestCase_Log(rsltDict)
        except Exception as e:
            e = str(e).replace("\n", "")
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(f'{bcolors.WARN}{bcolors.BLUEIC}=====================================\n',
                  f'Error name => ["{exc_type}"]\nIn file => ["{fname}"]\nAt line => ["{exc_tb.tb_lineno}"]\nStack trace => ["{self.utils.format_stacktrace()}"]',
                  f'\n-------------------------------------\n{bcolors.ENDC}')

    def test_page_detalii_sendToVOS(self):
        try:
            unittestDex.page_detalii_sentToVosObj.__init__(testCaseName=self.testCaseName, browser=self.browser, inputDictionary=self.inputDictionary)
            rsltDict = unittestDex.page_detalii_sentToVosObj.test_sendToVOS_Layer_sendToVOS()

            if type(rsltDict) == type(None):
                rsltDict = {'test_login': 'Generated no execution log, please verify for scenario name {}'.format(
                    self.testCaseName)}

            isCritical, rsltDict = unittestDex.logging.analyseLog(log_dict=rsltDict, log_type='METHOD')

            contor = 0
            while unittestDex.utils.element_exists('//button[@data-automation-id="refresh-error-page"]'):
                unittestDex.utils.click_js("""'//button[@data-automation-id="refresh-error-page"]'""")
                unittestDex.utils.wait_loadingCircle()
                rsltDict = unittestDex.page_detalii_sentToVosObj.test_sendToVOS_Layer_sendToVOS()

                if type(rsltDict) == type(None):
                    rsltDict = {'test_login': 'Generated no execution log, please verify for scenario name {}'.format(
                        self.testCaseName)}

                isCritical, rsltDict = unittestDex.logging.analyseLog(log_dict=rsltDict, log_type='METHOD')

                if contor == 3:
                    break
                else:
                    contor += 1

            if isCritical == True:
                unittestDex.logging.constructTestCase_Log(rsltDict)
                self.browser.close()
            else:
                unittestDex.logging.constructTestCase_Log(rsltDict)
        except Exception as e:
            e = str(e).replace("\n", "")
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(f'{bcolors.WARN}{bcolors.BLUEIC}=====================================\n',
                  f'Error name => ["{exc_type}"]\n \tIn file => ["{fname}"]\n \t\tAt line => ["{exc_tb.tb_lineno}"]\n \t\t\tStack trace => ["{self.utils.format_stacktrace()}"]',
                  f'\n-------------------------------------\n{bcolors.ENDC}')

    def test_getTestCase_Log(self):

        brokenMsisdn = self.utils.getTempValue('brokenMsisdn')
        path = '\\Output\\TraceFiles\\' + str(brokenMsisdn) + '.txt'
        filename = self.utils.to_posix_full_path(path)
        connector_log = filename
        try:
            with open(filename,'w', encoding="utf-8") as fn:
                fn.write(self.utils.retryAPI(API='simpleAPICall',
                                    URL=self.utils.getTempValue('URL')['getSID'].format(str(brokenMsisdn)),
                                    KEY=brokenMsisdn))
        except Exception as e:
            print('File not found error test_getTestCase_Log', e)

        try:
            hostName = os.environ['HOSTNAME'] + ' - ' + os.environ['DOCKER'] + ' - ' + os.environ['ENV']
        except KeyError:
            hostName = 'LocalHost TBD'

        try:
            release_name = os.environ['RLS']
        except KeyError:
            release_name = 'V19 From PyCharm Env'


        isCritical, TCLog = unittestDex.logging.analyseLog(log_dict=unittestDex.logging.testCase_log, log_type='TESTCASE', host=hostName, release_name=release_name, exec_TS= dt.now().strftime('%Y%m%d%H%M%S'),connector_log=connector_log)
        #TODO call POST method for every test exec.
        setDict.setTmpDict(TCLog)
        print(TCLog)
        #json_to_send=json.dumps(TCLog)
        #r = requests.post("http://devops02.connex.ro:8030/getTestResults/test", json=json_to_send)
        #return(TCLog)

    def getBrowser(self):
        return self.browser

    def test_tearDownClass(self):
        try:
            self.browser.close()
            self.browser.quit()
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            e = str(e).replace("\n", "")
            print(f'{bcolors.WARN}{bcolors.BLUEIC}=====================================\n',
                  f'Error name => ["{exc_type}"]\n \tIn file => ["{fname}"]\n \t\tAt line => ["{exc_tb.tb_lineno}"]\n \t\t\tStack trace => ["{self.utils.format_stacktrace()}"]',
                  f'\n-------------------------------------\n{bcolors.ENDC}')


    @staticmethod
    def main(TC_Definition, testName, tracker):
        print(testName + ' ' + tracker)
        suite = unittest.TestSuite()
        if 'ACHIZITIE' in TC_Definition['configType'] and TC_Definition['webAppVersion'].lower() == 'precbu':
            suite.addTest(unittestDex(metname='test_Login', inputDictionary=TC_Definition, testCaseName=testName, configType=unittestDex.utils.evalDictionaryValue(
                inputDictionary=TC_Definition,
                key='configType'),
                                      channel=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition, key='channel'), scenarioNameDC=unittestDex.utils.evalDictionaryValue(
                    inputDictionary=TC_Definition, key='scenarioNameDC')))
            # suite.addTest(unittestDex(metname='test_search', inputDictionary=TC_Definition, testCaseName=testName, configType=unittestDex.utils.evalDictionaryValue(
            #     inputDictionary=TC_Definition,
            #     key='configType'),
            #                           searchType=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
            #                                                                            key='searchType'),
            #                           searchValue=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
            #                                                                             key='searchValue')))
            # suite.addTest(unittestDex(metname='test_dashboard_Config', testCaseName=testName, inputDictionary=TC_Definition,
            #                           configType=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
            #                                                                            key='configType'),
            #                           abNouType=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
            #                                                                           key='abNouType'),
            #                           tipServiciu=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
            #                                                                             key='tipServiciu'),
            #                           retention_option=unittestDex.utils.evalDictionaryValue(
            #                               inputDictionary=TC_Definition,
            #                               key='retention_option'),
            #                           scenarioNameDC=unittestDex.utils.evalDictionaryValue(
            #                               inputDictionary=TC_Definition,
            #                               key='scenarioNameDC'),
            #                           retention_flow=unittestDex.utils.evalDictionaryValue(
            #                               inputDictionary=TC_Definition, key='retention_flow')))
            # suite.addTest(unittestDex(metname='test_page_Configureaza', inputDictionary=TC_Definition, testCaseName=testName,
            #                           scenarioNameDC=unittestDex.utils.evalDictionaryValue(
            #                               inputDictionary=TC_Definition,
            #                               key='scenarioNameDC'),
            #                           configType=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
            #                                                                            key='configType'),
            #                           tipServiciu=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
            #                                                                             key='tipServiciu'),
            #                           offerChoice=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
            #                                                                             key='offerChoice'),
            #                           scenarioName_PC=unittestDex.utils.evalDictionaryValue(
            #                               inputDictionary=TC_Definition,
            #                               key='scenarioName_PC'),
            #                           abNouType=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
            #                                                                           key='abNouType'),
            #                           channel=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition, key='channel'),
            #                           retention_flow=unittestDex.utils.evalDictionaryValue(
            #                               inputDictionary=TC_Definition, key='retention_flow'),
            #                           retention_option=unittestDex.utils.evalDictionaryValue(
            #                               inputDictionary=TC_Definition,
            #                               key='retention_option'), ))
            #
            # suite.addTest(unittestDex(metname='test_page_Detail_UtilizatorSiNumar', testCaseName=testName, inputDictionary=TC_Definition, configType=unittestDex.utils.evalDictionaryValue(
            #     inputDictionary=TC_Definition,
            #     key='configType'),
            #                           scenarioName_SendNotif=unittestDex.utils.evalDictionaryValue(
            #                               inputDictionary=TC_Definition,
            #                               key='scenarioName_SendNotif'),
            #                           scenarioName_Utl_Nr=unittestDex.utils.evalDictionaryValue(
            #                               inputDictionary=TC_Definition,
            #                               key='scenarioName_Utl_Nr'),
            #                           abNouType=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
            #                                                                           key='abNouType'),
            #                           tip_contract=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
            #                                                                              key='tipContract'),
            #                           tipServiciu=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
            #                                                                             key='tipServiciu')))
            # suite.addTest(unittestDex(metname='test_page_Detalil_utilizatorTitular', testCaseName=testName, inputDictionary=TC_Definition, configType=unittestDex.utils.evalDictionaryValue(
            #     inputDictionary=TC_Definition,
            #     key='configType'),
            #                           scenarioName_isTitular=unittestDex.utils.evalDictionaryValue(
            #                               inputDictionary=TC_Definition,
            #                               key='scenarioName_isTitular')))
            # suite.addTest(unittestDex(metname='test_page_Detail_GDPR', testCaseName=testName, inputDictionary=TC_Definition, configType=unittestDex.utils.evalDictionaryValue(
            #     inputDictionary=TC_Definition,
            #     key='configType'),
            #                           scenarioName_GDPR=unittestDex.utils.evalDictionaryValue(
            #                               inputDictionary=TC_Definition,
            #                               key='scenarioName_GDPR')))
            # suite.addTest(unittestDex(metname='test_page_Detail_ContNou', testCaseName=testName, inputDictionary=TC_Definition, configType=unittestDex.utils.evalDictionaryValue(
            #     inputDictionary=TC_Definition,
            #     key='configType'),
            #                           scenarioContNou=unittestDex.utils.evalDictionaryValue(
            #                               inputDictionary=TC_Definition,
            #                               key='scenarioContNou')))
            # suite.addTest(unittestDex(metname='test_page_detalii_sendToVOS', testCaseName=testName, inputDictionary=TC_Definition, configType=unittestDex.utils.evalDictionaryValue(
            #     inputDictionary=TC_Definition,
            #     key='configType'), ))
            # suite.addTest(unittestDex(metname='test_getTestCase_Log',testCaseName =testName))
            # suite.addTest(unittestDex(metname='test_tearDownClass', testCaseName=testName))
            # unittest.TextTestRunner(verbosity=2).run(suite)

        elif 'ACHIZITIE' in TC_Definition['configType'] and TC_Definition['webAppVersion'].lower() == 'cbu_oms':
            suite.addTest(unittestDex(metname='test_Login', inputDictionary=TC_Definition, testCaseName=testName,
                                      configType=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                       key='configType'),
                                      channel=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                    key='channel'),
                                      scenarioNameDC=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                           key='scenarioNameDC')))
            suite.addTest(unittestDex(metname='test_search', inputDictionary=TC_Definition, testCaseName=testName,
                                      configType=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                       key='configType'),
                                      searchType=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                       key='searchType'),
                                      searchValue=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                        key='searchValue')))
            #Do not uncomment#
            #------------------------------------------------------


            # suite.addTest(
            #     unittestDex(metname='test_dashboard_Config', testCaseName=testName, inputDictionary=TC_Definition,
            #                 configType=unittestDex.utils.testCaseDefinition(inputDictionary=TC_Definition,
            #                                                                 key='configType'),
            #                 abNouType=unittestDex.utils.testCaseDefinition(inputDictionary=TC_Definition,
            #                                                                key='abNouType'),
            #                 tipServiciu=unittestDex.utils.testCaseDefinition(inputDictionary=TC_Definition,
            #                                                                  key='tipServiciu'),
            #                 retention_option=unittestDex.utils.testCaseDefinition(inputDictionary=TC_Definition,
            #                                                                       key='retention_option'),
            #                 scenarioNameDC=unittestDex.utils.testCaseDefinition(inputDictionary=TC_Definition,
            #                                                                     key='scenarioNameDC'),
            #                 retention_flow=unittestDex.utils.testCaseDefinition(inputDictionary=TC_Definition,
            #                                                                     key='retention_flow')))

            #------------------------------------------------------


            suite.addTest(
                unittestDex(metname='test_page_Configureaza', inputDictionary=TC_Definition, testCaseName=testName,
                            scenarioNameDC=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                 key='scenarioNameDC'),
                            configType=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                             key='configType'),
                            tipServiciu=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                              key='tipServiciu'),
                            offerChoice=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                              key='offerChoice'),
                            scenarioName_PC=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                  key='scenarioName_PC'),
                            abNouType=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                            key='abNouType'),
                            channel=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition, key='channel'),
                            retention_flow=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                 key='retention_flow'),
                            retention_option=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                   key='retention_option'), ))

            suite.addTest(unittestDex(metname='test_page_Detail_Detalii_Client', testCaseName=testName,
                                      inputDictionary=TC_Definition,
                                      configType=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                       key='configType'),
                                      scenarioName_SendNotif=unittestDex.utils.evalDictionaryValue(
                                          inputDictionary=TC_Definition,
                                          key='scenarioName_SendNotif'),
                                      scenarioName_Utl_Nr=unittestDex.utils.evalDictionaryValue(
                                          inputDictionary=TC_Definition,
                                          key='scenarioName_Utl_Nr'),
                                      abNouType=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                      key='abNouType'),
                                      tip_contract=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                         key='tipContract'),
                                      tipServiciu=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                        key='tipServiciu')))

            suite.addTest(unittestDex(metname='test_page_Detail_UtilizatorSiNumar', testCaseName=testName,
                                      inputDictionary=TC_Definition,
                                      configType=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                       key='configType'),
                                      scenarioName_SendNotif=unittestDex.utils.evalDictionaryValue(
                                          inputDictionary=TC_Definition,
                                          key='scenarioName_SendNotif'),
                                      scenarioName_Utl_Nr=unittestDex.utils.evalDictionaryValue(
                                          inputDictionary=TC_Definition,
                                          key='scenarioName_Utl_Nr'),
                                      abNouType=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                      key='abNouType'),
                                      tip_contract=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                         key='tipContract'),
                                      tipServiciu=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                        key='tipServiciu')))
            suite.addTest(unittestDex(metname='test_page_Detalil_utilizatorTitular', testCaseName=testName,
                                      inputDictionary=TC_Definition,
                                      configType=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                       key='configType'),
                                      scenarioName_isTitular=unittestDex.utils.evalDictionaryValue(
                                          inputDictionary=TC_Definition,
                                          key='scenarioName_isTitular')))
            # suite.addTest(
            #     unittestDex(metname='test_page_Detail_GDPR', testCaseName=testName, inputDictionary=TC_Definition,
            #                 configType=unittestDex.utils.testCaseDefinition(inputDictionary=TC_Definition,
            #                                                                 key='configType'),
            #                 scenarioName_GDPR=unittestDex.utils.testCaseDefinition(inputDictionary=TC_Definition,
            #                                                                        key='scenarioName_GDPR')))
            suite.addTest(
                unittestDex(metname='test_page_Detail_ContNou', testCaseName=testName, inputDictionary=TC_Definition,
                            configType=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                             key='configType'),
                            scenarioContNou=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                  key='scenarioContNou')))
            # suite.addTest(
            #     unittestDex(metname='test_page_detalii_sendToVOS', testCaseName=testName, inputDictionary=TC_Definition,
            #                 configType=unittestDex.utils.testCaseDefinition(inputDictionary=TC_Definition,
            #                                                                 key='configType'), ))
            suite.addTest(unittestDex(metname='test_getTestCase_Log'))
            suite.addTest(unittestDex(metname='test_tearDownClass'))
            unittest.TextTestRunner(verbosity=2).run(suite)

        else:

            suite.addTest(unittestDex(metname='test_Login', testCaseName=testName, inputDictionary=TC_Definition,
                                      channel=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                    key='channel'),
                                      scenarioNameDC=unittestDex.utils.evalDictionaryValue(
                                          inputDictionary=TC_Definition,
                                          key='scenarioNameDC')))
            suite.addTest(unittestDex(metname='test_search', testCaseName=testName, inputDictionary=TC_Definition,
                                      searchType=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                       key='searchType'),
                                      searchValue=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                        key='searchValue')))
            suite.addTest(unittestDex(metname='test_dashboard_Config', testCaseName=testName, inputDictionary=TC_Definition,
                                      configType=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                       key='configType'),
                                      abNouType=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                      key='abNouType'),
                                      tipServiciu=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                        key='tipServiciu'),
                                      retention_option=unittestDex.utils.evalDictionaryValue(
                                          inputDictionary=TC_Definition,
                                          key='retention_option'),
                                      scenarioNameDC=unittestDex.utils.evalDictionaryValue(
                                          inputDictionary=TC_Definition,
                                          key='scenarioNameDC'),
                                      retention_flow=unittestDex.utils.evalDictionaryValue(
                                          inputDictionary=TC_Definition,
                                          key='retention_flow')))
            suite.addTest(unittestDex(metname='test_page_Configureaza', inputDictionary=TC_Definition, testCaseName=testName,
                                      scenarioNameDC=unittestDex.utils.evalDictionaryValue(
                                          inputDictionary=TC_Definition,
                                          key='scenarioNameDC'),
                                      configType=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                       key='configType'),
                                      tipServiciu=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                        key='tipServiciu'),
                                      offerChoice=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                        key='offerChoice'),
                                      scenarioName_PC=unittestDex.utils.evalDictionaryValue(
                                          inputDictionary=TC_Definition,
                                          key='scenarioName_PC'),
                                      abNouType=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                      key='abNouType'),
                                      channel=unittestDex.utils.evalDictionaryValue(inputDictionary=TC_Definition,
                                                                                    key='channel'),
                                      retention_flow=unittestDex.utils.evalDictionaryValue(
                                          inputDictionary=TC_Definition,
                                          key='retention_flow'),
                                      retention_option=unittestDex.utils.evalDictionaryValue(
                                          inputDictionary=TC_Definition,
                                          key='retention_option'), ))
            suite.addTest(unittestDex(metname='test_getTestCase_Log', testCaseName=testName))
            suite.addTest(unittestDex(metname='test_tearDownClass', testCaseName=testName))
            unittest.TextTestRunner(verbosity=2).run(suite)






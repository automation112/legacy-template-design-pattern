from SRC.utils_pkg import utilities
from collections import defaultdict
import time as t


class page_dashboard:


    utils = utilities.utilities()

    def __init__(self, retention_option=None, testCaseName=None, configType = None, abNouType = None, scenarioNameDC = None, browser = None, tipServiciu=None, retention_flow=None):
        self.configType = configType
        self.abNouType = abNouType
        self.scenarioNameDC = scenarioNameDC
        self.browser = browser
        self.tipServiciu = tipServiciu
        self.retention_option = retention_option
        self.retention_flow = retention_flow
        self.dasboardDict = {'Abonament_nou':"//div[@data-automation-id='new-subscriber-link']",
                             'Numar_Nou(GA)': '//li[@data-automation-id="tab-tab-key-0"]',
                             'Credit_Rate_Da': '//label[1]//*[@name="isCustomerConsent"]',
                             'Credit_Rate_Nu': '//label[2]//*[@name="isCustomerConsent"]',
                             'Numar_Contact': '//input[@name="contactNumber"]',
                             'continua': "//button[contains(@class,'primary bold right-align')]",
                             'Portare': '//li[@data-automation-id="tab-tab-key-1"]',
                             'Numar_Portat': '//input[@name="portedNumber"]',
                             'Donor': '//select[@name="provider"]',
                             'Numar_Contact_Portare': "//div[2]/div[@class='fields-wrap' and 1]/div[1]/input[1]",
                             'POSTPAID': '//input[@name="portedNumberType" and @value="postpaid"]',
                             'Prepaid': '//input[@name="portedNumberType" and @value="prepaid"]',
                             'Migrare_de_la_Prepaid': "//li[@data-automation-id='tab-tab-key-2']",
                             'Number_To_Migrate': '//input[@name="numberToMigrate"]',
                             'Trimtie Token': "//span[contains(text(),'TRIMITE TOKEN')]",
                             'Introdu Token': "//input[@placeholder='Introdu TOKEN']",
                             'Valideaza Token': """//a[@data-automation-id="token-consent-validate-token"]""",
                             'Factura_1': "//input[@name='invoice1']",
                             'Factura_2': "//input[@name='invoice2']",
                             'Factura_3': "//input[@name='invoice3']",
                              'adauga': "//button[contains(@class,'bold nbo-button')]",
                             'BundleBannerOffer': "//span[contains(text(),'Împreuna cu')]",
                             '360_OferteMobile': """//span[contains(text(),'OFERTE MOBILE')]""",
                             '360_Doar_Servicii': "//span[contains(text(),'DOAR SERVICII')]",
                             }
        page_dashboard.utils.__init__(browser=self.browser, numeScenariu=testCaseName, xpathDict=self.dasboardDict, methodName='test_page_Dashboard')


    def test_dashboard_Config(self):
        # self.browser.refresh()
        try:
            if '_ACHIZITIE' in self.configType:
                page_dashboard.utils.wait_loadingCircle()
                #self.browser.find_element_by_xpath("//div[@data-automation-id='new-subscriber-link']").send_keys(Keys.PAGE_DOWN)

                page_dashboard.utils.click_js(self.dasboardDict['Abonament_nou'])
                page_dashboard.utils.wait_loadingCircle()
                if self.abNouType == 'GA':
                    t.sleep(0.5)
                    page_dashboard.utils.click_js(self.dasboardDict['Numar_Nou(GA)'])

                    if self.scenarioNameDC == 'CR_True_Client_Matur':
                        page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')
                        #page_dashboard.utils.click_js( """//input[@name="isCustomerConsent"][1]""").click()

                    elif self.scenarioNameDC == 'CR_False_Client_Matur':
                        page_dashboard.utils.logScenariu('click Credit Rate = "Nu"')
                        page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Nu'])
                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')

                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_ValidateToken_Yes':
                        page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')
                        page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        page_dashboard.utils.wait_loadingCircle()
                        count = 3

                        while page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                            count -= 1
                            page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                            page_dashboard.utils.wait_loadingCircle()

                        if page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                            page_dashboard.utils.log_customStep('Trimite token fail after Retry', 'FAIL')
                        else:
                            tokenId = page_dashboard.utils.retryAPI(API='Token', URL='http://devops02.connex.ro:8030/getSMSToken/', KEY=page_dashboard.utils.getTempValue('brokenMsisdn'))
                            page_dashboard.utils.wait_loadingCircle()
                            page_dashboard.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=tokenId)
                            page_dashboard.utils.click_js(xpath="""//a[@data-automation-id="token-consent-validate-token"]""")
                            page_dashboard.utils.wait_loadingCircle()

                            if page_dashboard.utils.element_exists(xPath="//div[@class='error-text']") == True:
                                page_dashboard.utils.log_customStep('VALID TOKEN', 'FAIL')
                            else:
                                page_dashboard.utils.log_customStep('VALID TOKEN', 'PASSED')

                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_ValidateToken_No':
                        page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')
                        page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        page_dashboard.utils.wait_loadingCircle()

                        count = 3
                        while page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                            count -= 1
                            page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                            page_dashboard.utils.wait_loadingCircle()

                        if page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                            page_dashboard.utils.log_customStep('Trimite token fail after Retry', 'FAIL')
                        else:
                            tokenId = page_dashboard.utils.retryAPI(API='Token',
                                                                    URL='http://devops02.connex.ro:8030/getSMSToken/',
                                                                    KEY=page_dashboard.utils.getTempValue('brokenMsisdn'))
                            page_dashboard.utils.wait_loadingCircle()

                            page_dashboard.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=tokenId)

                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_Token_Fall_through':
                        page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')

                        page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        page_dashboard.utils.wait_loadingCircle()

                        count = 3
                        while page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                            count -= 1
                            page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                            page_dashboard.utils.wait_loadingCircle()

                        if page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                            page_dashboard.utils.log_customStep('Trimite token fail after Retry', 'FAIL')

                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_max4TokensSent':
                        page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')

                        for i in range(1, 6):
                            page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                            page_dashboard.utils.wait_loadingCircle()

                            count = 3
                            while page_dashboard.utils.element_exists(
                                    "//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                                count -= 1
                                page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                                page_dashboard.utils.wait_loadingCircle()

                            if page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                                page_dashboard.utils.log_customStep('Max4Tkn_Count_{}_Trimite token fail after Retry'.format(i), 'FAIL')

                            if i >= 2 and i < 6:
                                if page_dashboard.utils.element_exists(xPath="//div[@class='attempts-remaining']") == True:
                                    page_dashboard.utils.log_customStep('Incercari ramase Displayed', 'PASSED')
                                else:
                                    page_dashboard.utils.log_customStep('Incercari ramase Displayed', 'FAIL')

                        if page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                            page_dashboard.utils.log_customStep('Max4Tkn_Count_{}_Trimite token fail after Retry'.format(i),
                                                                'FAIL')
                        else:
                            if page_dashboard.utils.element_exists("//div[@class='token-validate-block']//button") == True:
                                page_dashboard.utils.log_customStep('Trimite Token isDisabled', "PASSED")
                            else:
                                page_dashboard.utils.log_customStep('Trimite Token isDisabled', "FAIL")

                            tokenId = page_dashboard.utils.retryAPI(API='Token',
                                                                    URL='http://devops02.connex.ro:8030/getSMSToken/',
                                                                    KEY=page_dashboard.utils.getTempValue('brokenMsisdn'))

                            page_dashboard.utils.wait_loadingCircle()
                            page_dashboard.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=tokenId)
                            page_dashboard.utils.click_js(xpath="//label[contains(text(),'VALIDEAZ')]")

                            if page_dashboard.utils.element_exists(xPath="//div[@class='error-text']") == True:
                                page_dashboard.utils.log_customStep('VALID TOKEN', 'FAIL')
                            else:
                                page_dashboard.utils.log_customStep('VALID TOKEN', 'PASSED')

                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_TokenIgnored':
                        page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')

                    elif self.scenarioNameDC == 'CR_False_Client_nonMatur':
                        page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Nu'])
                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')

                    page_dashboard.utils.click_js(self.dasboardDict['continua'])

                elif self.abNouType == 'MNP':
                    t.sleep(0.5)
                    contact = '734953572'
                    ported_no = '730044448'
                    page_dashboard.utils.setTempValue('ported_no', ported_no)

                    page_dashboard.utils.logScenariu('Click pe Portare')
                    page_dashboard.utils.click_js(self.dasboardDict['Portare'])
                    page_dashboard.utils.type_js(xpath=self.dasboardDict['Numar_Contact_Portare'], value=contact)
                    page_dashboard.utils.type_js(xpath=self.dasboardDict['Numar_Portat'], value=ported_no, enter='yes')
                    page_dashboard.utils.dropDownSelector_Index(self.dasboardDict['Donor'], 1)

                    if self.tipServiciu == "POSTPAID":
                        page_dashboard.utils.click_js(self.dasboardDict['POSTPAID'])
                    elif self.tipServiciu == "PREPAID":
                        page_dashboard.utils.click_js(self.dasboardDict['Prepaid'])

                    if self.scenarioNameDC == 'CR_True_Client_Matur':
                        page_dashboard.utils.logScenariu('click Credit Rate = "Da"')
                        page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Da'])

                    elif self.scenarioNameDC == 'CR_False_Client_Matur':
                        page_dashboard.utils.logScenariu('click Credit Rate = "Nu"')
                        page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Nu'])

                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_ValidateToken_Yes':
                        page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                        if self.tipServiciu == 'POSTPAID':
                            page_dashboard.utils.type_js(self.dasboardDict['Factura_1'], '300')
                            page_dashboard.utils.type_js(self.dasboardDict['Factura_2'], '300')
                            page_dashboard.utils.type_js(self.dasboardDict['Factura_3'], '300')

                        page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        page_dashboard.utils.wait_loadingCircle()

                        page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        page_dashboard.utils.wait_loadingCircle()

                        count = 3
                        while page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:

                            count -= 1
                            page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                            page_dashboard.utils.wait_loadingCircle()

                        if page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                            page_dashboard.utils.log_customStep('Trimite token fail after Retry', 'FAIL')
                        else:
                            tokenId = page_dashboard.utils.retryAPI(API='Token',
                                                                    URL='http://devops02.connex.ro:8030/getSMSToken/',
                                                                    KEY=page_dashboard.utils.getTempValue('brokenMsisdn'))
                            page_dashboard.utils.wait_loadingCircle()
                            page_dashboard.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=tokenId)
                            page_dashboard.utils.click_js(xpath="//label[contains(text(),'VALIDEAZ')]")

                        if page_dashboard.utils.element_exists(xPath="//div[@class='error-text']") == True:
                            page_dashboard.utils.log_customStep('VALID TOKEN', 'FAIL')
                        else:
                            page_dashboard.utils.log_customStep('VALID TOKEN', 'PASSED')

                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_ValidateToken_No':
                        page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Da'])

                        if self.tipServiciu == 'POSTPAID':
                            page_dashboard.utils.type_js(self.dasboardDict['Factura_1'], '300')
                            page_dashboard.utils.type_js(self.dasboardDict['Factura_2'], '300')
                            page_dashboard.utils.type_js(self.dasboardDict['Factura_3'], '300')

                        page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        page_dashboard.utils.wait_loadingCircle()

                        count = 3
                        while page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                            count -= 1
                            page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                            page_dashboard.utils.wait_loadingCircle()

                        if page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                            page_dashboard.utils.log_customStep('Trimite token fail after Retry', 'FAIL')
                        else:
                            tokenId = page_dashboard.utils.retryAPI(API='Token',
                                                                    URL='http://devops02.connex.ro:8030/getSMSToken/',
                                                                    KEY=page_dashboard.utils.getTempValue('brokenMsisdn'))
                            page_dashboard.utils.wait_loadingCircle()

                            page_dashboard.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=tokenId)

                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_Token_Fall_through':
                        if self.tipServiciu == 'POSTPAID':

                            page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                            page_dashboard.utils.type_js(self.dasboardDict['Factura_1'], '300')
                            page_dashboard.utils.type_js(self.dasboardDict['Factura_2'], '300')
                            page_dashboard.utils.type_js(self.dasboardDict['Factura_3'], '300')

                        page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        page_dashboard.utils.wait_loadingCircle()

                        count = 3
                        while page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                            count -= 1
                            page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                            page_dashboard.utils.wait_loadingCircle()

                        if page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                            page_dashboard.utils.log_customStep('Trimite token fail after Retry', 'FAIL')


                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_max4TokensSent':

                        if self.tipServiciu == 'POSTPAID':
                            page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                            page_dashboard.utils.type_js(self.dasboardDict['Factura_1'], '300')
                            page_dashboard.utils.type_js(self.dasboardDict['Factura_2'], '300')
                            page_dashboard.utils.type_js(self.dasboardDict['Factura_3'], '300')
                        for i in range(1, 6):

                            page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                            page_dashboard.utils.wait_loadingCircle()

                            count = 3
                            while page_dashboard.utils.element_exists(
                                    "//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                                count -= 1
                                page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                                page_dashboard.utils.wait_loadingCircle()

                            if page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                                page_dashboard.utils.log_customStep('Max4Tkn_Count_{}_Trimite token fail after Retry'.format(i), 'FAIL')

                            if i >= 2 and i < 6:
                                if page_dashboard.utils.element_exists(xPath="//div[@class='attempts-remaining']") == True:
                                    page_dashboard.utils.log_customStep('Incercari ramase Displayed', 'PASSED')
                                else:
                                    page_dashboard.utils.log_customStep('Incercari ramase Displayed', 'FAIL')

                        if page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                            page_dashboard.utils.log_customStep('Trimite token fail after Retry',
                                                                'FAIL')
                        else:
                            if page_dashboard.utils.element_exists("//div[@class='token-validate-block']//button") == True:
                                page_dashboard.utils.log_customStep('Trimite Token isDisabled', "PASSED")
                            else:
                                page_dashboard.utils.log_customStep('Trimite Token isDisabled', "FAIL")

                            tokenId = page_dashboard.utils.retryAPI(API='Token',
                                                                    URL='http://devops02.connex.ro:8030/getSMSToken/',
                                                                    KEY=page_dashboard.utils.getTempValue('brokenMsisdn'))

                            page_dashboard.utils.wait_loadingCircle()
                            page_dashboard.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=tokenId)

                            page_dashboard.utils.click_js(xpath="//label[contains(text(),'VALIDEAZ')]")
                            if page_dashboard.utils.element_exists(xPath="//div[@class='error-text']") == True:
                                page_dashboard.utils.log_customStep('VALID TOKEN', 'FAIL')
                            else:
                                page_dashboard.utils.log_customStep('VALID TOKEN', 'PASSED')

                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_TokenIgnored':
                        page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')

                    elif self.scenarioNameDC == 'CR_False_Client_nonMatur':
                        page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Nu'])
                        #
                        # if self.tipServiciu == 'POSTPAID':
                        #     page_dashboard.utils.type_js(self.dasboardDict['Factura_1'], '300')
                        #     page_dashboard.utils.type_js(self.dasboardDict['Factura_2'], '300')
                        #     page_dashboard.utils.type_js(self.dasboardDict['Factura_3'], '300')


                    page_dashboard.utils.click_js(self.dasboardDict['continua'])

                elif self.abNouType == 'MFP':
                    t.sleep(0.5)
                    #pre_msisdn = page_dashboard.utils.retryAPI(API='Prepaid', URL='http://devops02.connex.ro:8020/getPrepaid/')
                    pre_msisdn = 726983525
                    page_dashboard.utils.logScenariu('MIGRARE DE LA PREPAID')
                    page_dashboard.utils.click_js(self.dasboardDict['Migrare_de_la_Prepaid'])

                    if self.scenarioNameDC == 'CR_True_Client_Matur':
                        page_dashboard.utils.logScenariu('click Credit Rate = "Da"')
                        page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')
                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Number_To_Migrate'], value=pre_msisdn)

                    elif self.scenarioNameDC == 'CR_False_Client_Matur':

                        page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Nu'])
                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')
                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Number_To_Migrate'], value=pre_msisdn)

                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_ValidateToken_Yes':
                        # print('im here')
                        page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Da'])

                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')

                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Number_To_Migrate'], value=pre_msisdn)

                        page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        page_dashboard.utils.wait_loadingCircle()

                        count = 3
                        while page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                            count -= 1
                            page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                            page_dashboard.utils.wait_loadingCircle()

                        if page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                            page_dashboard.utils.log_customStep('Trimite token fail after Retry', 'FAIL')
                        else:

                            tokenId = page_dashboard.utils.retryAPI(API='Token',
                                                                    URL='http://devops02.connex.ro:8030/getSMSToken/',
                                                                    KEY=page_dashboard.utils.getTempValue('brokenMsisdn'))

                            page_dashboard.utils.wait_loadingCircle()

                            page_dashboard.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=tokenId)

                            page_dashboard.utils.click_js(xpath="//label[contains(text(),'VALIDEAZ')]")

                            if page_dashboard.utils.element_exists(xPath="//div[@class='error-text']") == True:

                                page_dashboard.utils.log_customStep('VALID TOKEN', 'FAIL')

                            else:

                                page_dashboard.utils.log_customStep('VALID TOKEN', 'PASSED')

                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_ValidateToken_No':

                        page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Da'])

                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')

                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Number_To_Migrate'], value=pre_msisdn)

                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_Token_Fall_through':

                        page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Da'])

                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')

                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Number_To_Migrate'], value=pre_msisdn)

                        page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        page_dashboard.utils.wait_loadingCircle()

                        count = 3
                        while page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                            count -= 1
                            page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                            page_dashboard.utils.wait_loadingCircle()

                        if page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                            page_dashboard.utils.log_customStep('Trimite token fail after Retry', 'FAIL')


                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_max4TokensSent':

                        page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Da'])

                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')

                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Number_To_Migrate'], value=pre_msisdn)

                        for i in range(1, 6):

                            page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                            page_dashboard.utils.wait_loadingCircle()

                            count = 3
                            while page_dashboard.utils.element_exists(
                                    "//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                                count -= 1
                                page_dashboard.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                                page_dashboard.utils.wait_loadingCircle()

                            if page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                                page_dashboard.utils.log_customStep('Max4Tkn_{}_Trimite token fail after Retry'.format(i), 'FAIL')

                            if i >= 2 and i < 6:

                                if page_dashboard.utils.element_exists(xPath="//div[@class='attempts-remaining']") == True:

                                    page_dashboard.utils.log_customStep('Incercari ramase Displayed', 'PASSED')

                                else:

                                    page_dashboard.utils.log_customStep('Incercari ramase Displayed', 'FAIL')

                        if page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                            page_dashboard.utils.log_customStep('Trimite token fail after Retry', 'FAIL')
                        else:

                            if page_dashboard.utils.element_exists("//div[@class='token-validate-block']//button") == True:

                                page_dashboard.utils.log_customStep('Trimite Token isDisabled', "PASSED")

                            else:

                                page_dashboard.utils.log_customStep('Trimite Token isDisabled', "FAIL")

                            if page_dashboard.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                                page_dashboard.utils.log_customStep('Trimite token fail after Retry', 'FAIL')
                            else:
                                tokenId = page_dashboard.utils.retryAPI(API='Token',
                                                                        URL='http://devops02.connex.ro:8030/getSMSToken/',
                                                                        KEY=page_dashboard.utils.getTempValue(
                                                                            'brokenMsisdn'))

                                # print('This is the token', tokenId,  'this the shiet', page_dashboard.utils.getTempValue('brokenMsisdn'))

                                page_dashboard.utils.wait_loadingCircle()

                                page_dashboard.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=tokenId)

                                page_dashboard.utils.click_js(xpath="//label[contains(text(),'VALIDEAZ')]")

                                if page_dashboard.utils.element_exists(xPath="//div[@class='error-text']") == True:
                                    page_dashboard.utils.log_customStep('VALID TOKEN', 'FAIL')
                                else:
                                    page_dashboard.utils.log_customStep('VALID TOKEN', 'PASSED')

                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_TokenIgnored':
                        page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')
                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Number_To_Migrate'], value=pre_msisdn)

                    elif self.scenarioNameDC == 'CR_False_Client_nonMatur':

                        page_dashboard.utils.click_js(self.dasboardDict['Credit_Rate_Nu'])

                        page_dashboard.utils.logScenariu('Type Numar Contact')

                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')

                        page_dashboard.utils.type_js(xpath=self.dasboardDict['Number_To_Migrate'], value=pre_msisdn)

                    #page_dashboard.utils.type_js(xpath=self.dasboardDict['Number_To_Migrate'], value=pre_msisdn)
                    page_dashboard.utils.wait_loadingCircle()
                    t.sleep(1.5)
                    page_dashboard.utils.click_js(self.dasboardDict['continua'])

            elif '_RETENTIE' in self.configType:

                if self.retention_option == '360_BannerOffer':
                    
                    if self.retention_flow == 'BannerOffer_Service_and_Device':
                        print('bundle banner offer')

                        page_dashboard.utils.setTempValue('BannerType', 'Service_and_Device')

                        # devAndServiceXpaths = {'Device_Info': ["//div[@class='banner-line'][2]//span[@class='hero-device-banner-2'][{}]"],
                        #                        'Rate': ["//div[@class='banner-line'][3]//span[@class='hero-device-banner-4'][{}]"],
                        #                        'Service_Info': ["//div[@class='banner-line'][5]//span[@class='hero-device-banner-3'][{}]", "//div[@class='banner-line'][5]//span[@class='hero-device-banner-2'][{}]"]
                        #                        }

                        devAndServiceXpaths = {
                            'Device_Info': '//div[@class="banner-line"][2]',
                            'Rate_Info': '//div[@class="banner-line"][3]',
                            'Service_Info': '//div[@class="banner-line"][5]'
                            }

                        collectInfoDict = defaultdict(dict)

                        for key, value in devAndServiceXpaths.items():
                            txtVal = page_dashboard.utils.get_text(value)
                            collectInfoDict[key] = txtVal
                            page_dashboard.utils.log_customStep('Banner_{}'.format(key), 'PASSED --> [{}]'.format(txtVal.replace('ă','')))

                        page_dashboard.utils.setTempValue('BannerInfo', dict(collectInfoDict))
                        print('collection dict = ', collectInfoDict)
                        page_dashboard.utils.click_js(self.dasboardDict['adauga'], priority='CRITICAL')

                    elif self.retention_flow == 'BannerOffer_ServiceOnly':
                        page_dashboard.utils.setTempValue('BannerType', 'ServiceOnly')

                        devAndServiceXpaths = {
                            'Service_Info': [
                                "//div[@class='banner-line'][5]//span[@class='hero-device-banner-3'][{}]",
                                "//div[@class='banner-line'][5]//span[@class='hero-device-banner-2'][{}]"]
                        }

                        collectInfoList = defaultdict(dict)

                        for i in list(range(1, 7)):
                            for key, value in devAndServiceXpaths.items():
                                collectInfoList[key] = []
                                for h in value:
                                    textValue = page_dashboard.utils.get_text(h.format(i), timeout=False)
                                    if textValue != False:
                                        collectInfoList[key].append(textValue.strip())

                        page_dashboard.utils.setTempValue('BannerInfo', dict(collectInfoList))
                        page_dashboard.utils.click_js(self.dasboardDict['adauga'], priority='CRITICAL')

                    elif self.retention_flow == 'BannerOffer_DeviceOnly':
                        page_dashboard.utils.setTempValue('BannerType', 'DeviceOnly')

                        devAndServiceXpaths = {
                            'Device_Info': ["//div[@class='banner-line'][2]//span[@class='hero-device-banner-2'][{}]"],
                            'Rate': ["//div[@class='banner-line'][3]//span[@class='hero-device-banner-4'][{}]"],
                        }

                        collectInfoList = defaultdict(dict)

                        for i in list(range(1, 7)):
                            for key, value in devAndServiceXpaths.items():
                                collectInfoList[key] = []
                                for h in value:
                                    textValue = page_dashboard.utils.get_text(h.format(i), customTimeout=0.1)
                                    if textValue != False:
                                        collectInfoList[key].append(textValue.strip())

                        page_dashboard.utils.setTempValue('BannerInfo', dict(collectInfoList))
                        page_dashboard.utils.click_js(self.dasboardDict['adauga'], priority='CRITICAL')

                elif self.retention_option == '360_Doar_Servicii':
                    if self.retention_flow == 'Service_only':
                        self.utils.click_js("//span[contains(text(),'DOAR SERVICII')]")

                elif self.retention_option == '360_OFERTE_MOBILE':
                    if self.retention_flow == 'ConfigureazaOferte':
                        self.utils.click_js("//span[contains(text(),'OFERTE MOBILE')]")
                    elif self.retention_flow == 'PachetPromo':
                        print('sunt pe pachete promo')
                        self.utils.click_js("//span[contains(text(),'OFERTE MOBILE')]")

        except Exception as e:
            page_dashboard.utils.log_customStep('INTERNAL_CODE_ERROR', 'REASON = [{}]'.format(e))
        finally:
            return (page_dashboard.utils.VDFloggingObj.method_log)

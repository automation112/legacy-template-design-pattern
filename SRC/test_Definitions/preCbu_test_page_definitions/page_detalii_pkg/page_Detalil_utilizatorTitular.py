from SRC.utils_pkg import utilities
import datetime
import random
import calendar


class utilizatorTitularLayer:

    utils = utilities.utilities()

    def __init__(self, scenarioName_isTitular = None, testCaseName=None, browser=None, inputDictionary=None):

        self.browser = browser
        self.inputDictionary = inputDictionary
        self.scenarioName_isTitular = scenarioName_isTitular
        self.utl_Titular = {'utilizator_titular': '//input[@data-automation-id="apply-personal-details-to-contact"]//ancestor::label/span',
                           'minor_nu': '//input[@name="minor" and @value="false"]',
                           'minor_da': '//input[@name="minor" and @value="true"]',
                           'nume': "//input[@name='personFirstName']",
                           'prenume': "//input[@name='personLastName']",
                           'contact_email': "//input[@name='contactEmail']",
                           'contact_phone': "//input[@name='contactPhone']",
                           'titlu': "//select[@name='personTitle']",
                           'ziua_nasterii': "//select[@name='birthDay']",
                           'luna_nasterii': "//select[@name='birthDateMonth']",
                           'anul_nasterii': "//select[@name='birthDateYear']",
                           }
        utilizatorTitularLayer.utils.__init__(browser=self.browser, numeScenariu=testCaseName, xpathDict=self.utl_Titular, methodName='test_page_detalii_UtilizatorTitular')



    def test_page_Detalil_utilizatorTitular(self):
        try:
            if self.scenarioName_isTitular == 'Utilizator_titular_Yes':
                utilizatorTitularLayer.utils.check_button(xpath='//input[@data-automation-id="apply-personal-details-to-contact"]',
                                                xpathCLick=self.utl_Titular['utilizator_titular'],
                                                value='value')

            elif self.scenarioName_isTitular == 'Utilizator_titular_No_Minor_No':

                utilizatorTitularLayer.utils.logScenariu('Utilizator titular = false')
                utilizatorTitularLayer.utils.uncheck_button(xpath='//input[@data-automation-id="apply-personal-details-to-contact"]',
                                                            xpathCLick=self.utl_Titular['utilizator_titular'],
                                                            value='value')
                utilizatorTitularLayer.utils.logScenariu('Click on minor Nu')
                utilizatorTitularLayer.utils.click_js(self.utl_Titular['minor_nu'])

                # Completeaza campurile
                utilizatorTitularLayer.utils.dropDownSelector_Index(xpath=self.utl_Titular['titlu'], index=2)
                utilizatorTitularLayer.utils.type_js(self.utl_Titular['nume'], value='Vasile')
                utilizatorTitularLayer.utils.type_js(self.utl_Titular['prenume'], value='Namazescu')
                utilizatorTitularLayer.utils.type_js(self.utl_Titular['contact_email'], value='vasile.namazescu@email.com')
                utilizatorTitularLayer.utils.type_js(self.utl_Titular['contact_phone'], value='721321321')

            elif self.scenarioName_isTitular == 'Utilizator_titular_No_Minor_Yes':

                utilizatorTitularLayer.utils.uncheck_button(xpath='//input[@data-automation-id="apply-personal-details-to-contact"]',
                                                            xpathCLick=self.utl_Titular['utilizator_titular'],
                                                            value='value')

                utilizatorTitularLayer.utils.logScenariu('Click on minor Da')
                utilizatorTitularLayer.utils.click_js(self.utl_Titular['minor_da'])

                # Completeaza campurile
                utilizatorTitularLayer.utils.wait_loadingCircle()
                utilizatorTitularLayer.utils.dropDownSelector_Index(xpath=self.utl_Titular['titlu'], index=2)
                utilizatorTitularLayer.utils.type_js(self.utl_Titular['nume'], value='Vasile')
                utilizatorTitularLayer.utils.type_js(self.utl_Titular['prenume'], value='Namazescu')
                utilizatorTitularLayer.utils.type_js(self.utl_Titular['contact_email'], value='vasile.namazescu@email.com')
                utilizatorTitularLayer.utils.type_js(self.utl_Titular['contact_phone'], value='721321321')

                #data nastere if minor=da
                now = datetime.datetime.now()

                utilizatorTitularLayer.utils.dropDownSelector_Index(xpath=self.utl_Titular['ziua_nasterii'], index=random.randint(1, calendar.monthrange(now.year, now.month)[1]))
                utilizatorTitularLayer.utils.dropDownSelector_Index(xpath=self.utl_Titular['luna_nasterii'], index=random.randint(1, 12))
                utilizatorTitularLayer.utils.dropDownSelector_Index(xpath=self.utl_Titular['anul_nasterii'], index=random.randint(1, 25))
        except Exception as e:
            page_login.utils.log_customStep('INTERNAL_CODE_ERROR', 'REASON = [{}]'.format(e))
        finally:
            return (utilizatorTitularLayer.utils.VDFloggingObj.method_log)

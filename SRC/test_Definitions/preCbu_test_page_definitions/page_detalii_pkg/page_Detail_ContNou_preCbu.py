from SRC.utils_pkg import utilities
from SRC.test_Definitions.test_baaseline_Classes.page_Detail_ContNou_baseline import contNou_Layer_baseline


class contNou_Layer_preCbu(contNou_Layer_baseline):
    def __init__(self, inputDictionary=None, scenarioName_isTitular=None, testCaseName=None, abNouType=None,
                 scenarioName_Sim=None, browser=None, tip_contract=None, scenarioName_GDPR=None, scenarioContNou=None):
        contNou_Layer_baseline.__init__(self, inputDictionary, scenarioName_isTitular, testCaseName, abNouType, scenarioName_Sim, browser,
                         tip_contract, scenarioName_GDPR, scenarioContNou)


    def test_page_Detail_ContNou(self):
        try:
            if self.scenarioContNou == 'Is_Cont_Nou_Required_No':

                #self.utils.click_js(self.contNou_dict['Factura_Ok'])
                is_displayed = self.utils.is_element_Selected(self.contNou_dict['Factura_Ok'])
                print('test_page_Detail_ContNou --->>  ', is_displayed)
                if is_displayed:
                    self.utils.log_customStep('Date_Facturare_Billing','PASSED')
                else:
                    self.utils.log_customStep('Date_Facturare_Billing', 'FAIL')
                    self.utils.click_js(self.contNou_dict['Factura_Ok'])

            elif self.scenarioContNou == 'Is_Cont_Nou_Required_Yes_FacturaElectr':

                self.utils.logScenariu("scenarioContNou == 'Is_Cont_Nou_Required_Yes_FacturaElectr'")
                self.utils.click_js(self.contNou_dict['Cont_Nou'])
                self.utils.type_js(self.contNou_dict['Tara'], 'ROMANIA', enter=True)
                self.utils.type_js(self.contNou_dict['Judet'], 'Bras', enter=True)
                self.utils.type_js(self.contNou_dict['Localitate'], 'Brasov', enter=True)
                self.utils.type_js(self.contNou_dict['Type'], 'Str.', enter=True)
                self.utils.type_js(self.contNou_dict['Strada'], 'Stejerisului', enter=True)
                self.utils.type_js(self.contNou_dict['Numar'], '27')
                cod_postal = self.utils.get_text(self.contNou_dict['cod_postal'])

                if type(cod_postal) is str:
                    self.utils.log_customStep('Cod postal Generat', 'PASSED')
                else:
                    self.utils.log_customStep('Cod postal Generat', 'FAIL')

                self.utils.type_js(self.contNou_dict['Email'], 'laurentiu.faget1@vodafone.com')
                self.utils.click_js(self.contNou_dict['Continua'])

                is_displayed = self.utils.is_element_Displayed(self.contNou_dict['DATA FACTURARE_Generat'])

            elif self.scenarioContNou == 'Is_Cont_Nou_Required_Yes_FacturaTiparita':
                self.utils.click_js(self.contNou_dict['Cont_Nou'])
                self.utils.type_js(self.contNou_dict['Tara'], 'ROMANIA', enter=True)
                self.utils.type_js(self.contNou_dict['Judet'], 'Bras', enter=True)
                self.utils.type_js(self.contNou_dict['Localitate'], 'Brasov', enter=True)
                self.utils.type_js(self.contNou_dict['Type'], 'Str.', enter=True)
                self.utils.type_js(self.contNou_dict['Strada'], 'Stejerisului', enter=True)
                self.utils.type_js(self.contNou_dict['Numar'], '27')
                cod_postal = self.utils.get_text(self.contNou_dict['cod_postal'])

                if type(cod_postal) is str:
                    self.utils.log_customStep('Cod postal Generat', 'PASSED')
                else:
                    self.utils.log_customStep('Cod postal Generat', 'FAIL')

                self.utils.click_js(xpath=self.contNou_dict['Factura_Tiparita'])
                self.utils.click_js(self.contNou_dict['Continua'])

                # check if adresa de mai sus a fost adaugat in pagina spre VOS.
                is_displayed = self.utils.is_element_Displayed(self.contNou_dict['DATA FACTURARE_Generat'])
        except Exception as e:
            self.utils.log_customStep('INTERNAL_CODE_ERROR', 'REASON = [{}]'.format(e))
        finally:
            return (self.utils.VDFloggingObj.method_log)

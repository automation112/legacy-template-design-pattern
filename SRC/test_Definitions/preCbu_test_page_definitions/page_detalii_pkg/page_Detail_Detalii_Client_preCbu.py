from SRC.utils_pkg import utilities
import datetime
from selenium.common.exceptions import NoSuchElementException
import time
from SRC.test_Definitions.test_baaseline_Classes.page_Detail_Detalii_Client_baseline import Detalii_Client_Layer_baseline
import bcolors, sys, os

class Page_Detail_Detalii_Client_Layer_preCbu(Detalii_Client_Layer_baseline):

    def __init__(self, inputDictionary=None, scenarioName_SendNotif=None, scenarioName_isTitular=None,
                 testCaseName=None, abNouType=None, scenarioName_Utl_Nr=None, browser=None, tip_contract=None,
                 scenarioName_GDPR=None, scenarioContNou=None, tipServiciu=None):

        Detalii_Client_Layer_baseline.__init__(self, inputDictionary, scenarioName_SendNotif, scenarioName_isTitular, testCaseName, abNouType,
                         scenarioName_Utl_Nr, browser, tip_contract, scenarioName_GDPR, scenarioContNou, tipServiciu)


    def test_page_Detail_Detalii_Client(self):
        try:
            if self.abNouType == 'GA':
                pass
            elif self.abNouType == 'MNP':
                pass
            elif self.abNouType == 'MFP':
                pass
        except Exception as e:
            e = str(e).replace("\n", "")
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(f'{bcolors.WARN}{bcolors.BLUEIC}=====================================\n',
                  f'Error name => ["{exc_type}"]\nIn file => ["{fname}"]\nAt line => ["{exc_tb.tb_lineno}"]\nStack trace => ["{self.utils.format_stacktrace()}"]',
                  f'\n-------------------------------------\n{bcolors.ENDC}')
            self.utils.log_customStep('INTERNAL_CODE_ERROR', 'REASON = [{}]'.format(e))
        finally:
            return (self.utils.VDFloggingObj.method_log)

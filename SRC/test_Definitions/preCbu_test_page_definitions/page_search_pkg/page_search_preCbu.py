import time
from random import randint
from SRC.test_Definitions.test_baaseline_Classes.page_search_baseline import page_search_baseline

class page_search_preCbu(page_search_baseline):

    def __init__(self, testCaseName=None, searchType=None, searchValue=None, browser=None, configType=None, inputDictionary=None):
        page_search_baseline.__init__(self, testCaseName=testCaseName, searchType=searchType, searchValue=searchValue, browser=browser, configType=configType, inputDictionary=inputDictionary)

    def page_search(self):
          try:
              if self.searchType == 'MSISDN':

                  msisdn = '722848655'
                  time.sleep(3)
                  page_search_preCbu.utils.click_js(self.searchDict['MSISDN'])
                  brokenMsisdn = randint(100, 999999999)
                  page_search_preCbu.utils.setTempValue('brokenMsisdn', brokenMsisdn)
                  page_search_preCbu.utils.type_js(self.searchDict['searchKey_MSISDN'], brokenMsisdn)
                  page_search_preCbu.utils.click_js(self.searchDict['cauta'])
                  # for i in self.searchValue.split():
                  #     input_msisdn = page_search.utils.type_js(self.searchDict['searchKey_MSISDN'], i)
                  #     page_search.utils.click_js(self.searchDict['cauta'])
                  #     print(i)
                  #     if page_search.utils.element_exists(self.searchDict['cauta']) == False:
                  #         print(i)
                  page_search_preCbu.utils.type_js(self.searchDict['searchKey_MSISDN'], self.searchValue)
                  time.sleep(10)
                  page_search_preCbu.utils.click_js(self.searchDict['cauta'])
                  print(brokenMsisdn)
                  page_search_preCbu.utils.wait_loadingCircle()

              elif self.searchType == 'CNP':
                  page_search_preCbu.utils.click_js(self.searchDict['CNP'])
                  page_search_preCbu.utils.type_js(self.searchDict['searchKey_CNP'], self.searchValue)

              elif self.searchType == 'CONTCLIENT':
                  page_search_preCbu.utils.click_js(self.searchDict['CONT_CLIENT'])
                  page_search_preCbu.utils.type_js(self.searchDict['searchKey_CONT_CLIENT'], self.searchValue)

              elif self.searchType == 'ADRESA':
                  page_search_preCbu.utils.click_js(self.searchDict['ADDRESS'])
                  page_search_preCbu.utils.type_js(self.searchDict['searchKey_ADDRESS'], self.searchValue)

              else:
                  print('No input')
          except Exception as e:
              page_search_preCbu.utils.log_customStep('INTERNAL_CODE_ERROR', 'REASON = [{}]'.format(e))
          finally:
              return (page_search_preCbu.utils.VDFloggingObj.method_log)

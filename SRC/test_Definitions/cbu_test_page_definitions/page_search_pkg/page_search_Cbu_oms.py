import time
from random import randint
from SRC.test_Definitions.test_baaseline_Classes.page_search_baseline import page_search_baseline
import bcolors, sys, os


class page_search_Cbu_oms(page_search_baseline):

    def __init__(self, testCaseName=None, searchType=None, searchValue=None, browser=None, configType=None,
                 inputDictionary=None):
        page_search_baseline.__init__(self, testCaseName=testCaseName, searchType=searchType, searchValue=searchValue,
                                      browser=browser, configType=configType, inputDictionary=inputDictionary)
        self.abonament_nou_Scenario = self.utils.evalDictionaryValue(inputDictionary, 'abonament_nou_Scenario')
        self.scenarioName_PageSearch = self.utils.evalDictionaryValue(inputDictionary, 'scenarioName_PageSearch')
        self.informatii_client_dictionary = {
            'prenume':'Vasile',
            'nume':'Namazescu',
            'email':'vasile.namazescu@vodafone.com',
            'judet':'Sector 3',
            'localitate':'Bucuresti',
            'tip_strada':'Str.',
            'nume_strada':'Schitului',
            'numar_strada':'3',
            'bloc':'A3',
            'scara':'B3',
            'apartament':'20'
        
        }

        self.utils.setTempValue('info_clienti', self.informatii_client_dictionary)



    def page_search(self):
        try:

            if self.searchType == 'MSISDN':

                msisdn = '722848655'
                time.sleep(3)
                page_search_Cbu_oms.utils.click_js(self.searchDict['MSISDN'])
                brokenMsisdn = randint(100, 999999999)
                page_search_Cbu_oms.utils.setTempValue('brokenMsisdn', brokenMsisdn)
                page_search_Cbu_oms.utils.type_js(self.searchDict['searchKey_MSISDN'], brokenMsisdn)
                page_search_Cbu_oms.utils.click_js(self.searchDict['cauta'])
                # for i in self.searchValue.split():
                #     input_msisdn = page_search.utils.type_js(self.searchDict['searchKey_MSISDN'], i)
                #     page_search.utils.click_js(self.searchDict['cauta'])
                #     print(i)
                #     if page_search.utils.element_exists(self.searchDict['cauta']) == False:
                #         print(i)
                page_search_Cbu_oms.utils.type_js(self.searchDict['searchKey_MSISDN'], self.searchValue)
                page_search_Cbu_oms.utils.click_js(self.searchDict['cauta'])

                page_search_Cbu_oms.utils.wait_loadingCircle()
                print(brokenMsisdn)
            elif self.searchType == 'CNP':
                #Random search to retrive session Id from connetors.log
                page_search_Cbu_oms.utils.click_js(self.searchDict['MSISDN'])
                brokenMsisdn = randint(100, 999999999)
                page_search_Cbu_oms.utils.setTempValue('brokenMsisdn', brokenMsisdn)
                page_search_Cbu_oms.utils.type_js(self.searchDict['searchKey_MSISDN'], brokenMsisdn)
                page_search_Cbu_oms.utils.click_js(self.searchDict['cauta'])

                #Actual Search
                page_search_Cbu_oms.utils.click_js(self.searchDict['CNP'])
                page_search_Cbu_oms.utils.type_js(self.searchDict['searchKey_CNP'], self.searchValue)
                page_search_Cbu_oms.utils.click_js(self.searchDict['cauta'])
                self.abonament_nou_Pannel()

            elif self.searchType == 'CONTCLIENT':
                page_search_Cbu_oms.utils.click_js(self.searchDict['CONT_CLIENT'])
                page_search_Cbu_oms.utils.type_js(self.searchDict['searchKey_CONT_CLIENT'], self.searchValue)

            elif self.searchType == 'ADRESA':
                page_search_Cbu_oms.utils.click_js(self.searchDict['ADDRESS'])
                page_search_Cbu_oms.utils.type_js(self.searchDict['searchKey_ADDRESS'], self.searchValue)

            else:
                print('No input')

        except Exception as e:

            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            e = str(e).replace("\n", "")
            print(f'{bcolors.WARN}{bcolors.BLUEIC}=====================================\n',
                  f'Error name => ["{exc_type}"]\n \tIn file => ["{fname}"]\n \t\tAt line => ["{exc_tb.tb_lineno}"]\n \t\t\tStack trace => ["{self.utils.format_stacktrace()}"]',
                  f'\n-------------------------------------\n{bcolors.ENDC}')
            page_search_Cbu_oms.utils.log_customStep('INTERNAL_CODE_ERROR', 'REASON = [{}]'.format(e))
        finally:
            return (page_search_Cbu_oms.utils.VDFloggingObj.method_log)

    def abonament_nou_Pannel(self):
        try:
            if '_ACHIZITIE' in self.configType:
                self.utils.wait_loadingCircle()
                # self.browser.find_element_by_xpath("//div[@data-automation-id='new-subscriber-link']").send_keys(Keys.PAGE_DOWN)
                time.sleep(5)
                self.utils.click_js(xpath="//button[contains(text(),'CLIENT NOU - MOBIL')]",
                                    element_name='Client Nou')

                self.utils.wait_loadingCircle()

                if self.abNouType == 'GA':
                    time.sleep(0.5)
                    self.utils.click_js(self.searchDict['Numar_Nou(GA)'])

                    if self.scenarioNameDC == 'CR_True_Client_nonMatur_ValidateToken_Yes':
                        self.utils.click_js(self.searchDict['Credit_Rate_Da'])
                        self.utils.type_js(xpath=self.searchDict['Numar_Contact'], value='730963536')
                        time.sleep(10)
                        self.do_token_actions()

                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_ValidateToken_No':
                        self.utils.click_js(self.searchDict['Credit_Rate_Da'])
                        self.utils.type_js(xpath=self.searchDict['Numar_Contact'], value='730963536')
                        self.do_token_actions()


                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_Token_Fall_through':
                        self.utils.click_js(self.searchDict['Credit_Rate_Da'])
                        self.utils.type_js(xpath=self.searchDict['Numar_Contact'], value='730963536')
                        self.do_token_actions()


                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_max4TokensSent':
                        self.utils.click_js(self.searchDict['Credit_Rate_Da'])
                        self.utils.type_js(xpath=self.searchDict['Numar_Contact'], value='730963536')

                        for i in range(1, 6):
                            self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                            self.utils.wait_loadingCircle()

                            count = 3
                            while self.utils.element_exists(
                                    "//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                                count -= 1
                                self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                                self.utils.wait_loadingCircle()

                            if self.utils.element_exists(
                                    "//span[contains(text(),'Trimiterea Token-ului a e')]"):
                                self.utils.log_customStep(
                                    'Max4Tkn_Count_{}_Trimite token fail after Retry'.format(i), 'FAIL')

                            if i >= 2 and i < 6:
                                if self.utils.element_exists(
                                        xPath="//div[@class='attempts-remaining']") == True:
                                    self.utils.log_customStep('Incercari ramase Displayed', 'PASSED')
                                else:
                                    self.utils.log_customStep('Incercari ramase Displayed', 'FAIL')

                        if self.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                            self.utils.log_customStep(
                                'Max4Tkn_Count_{}_Trimite token fail after Retry'.format(i),
                                'FAIL')
                        else:
                            if self.utils.element_exists(
                                    "//div[@class='token-validate-block']//button") == True:
                                self.utils.log_customStep('Trimite Token isDisabled', "PASSED")
                            else:
                                self.utils.log_customStep('Trimite Token isDisabled', "FAIL")

                            tokenId = self.utils.retryAPI(API='Token',
                                                          URL=self.utils.getTempValue('URL')['getSMSToken'],
                                                          KEY=self.utils.getTempValue(
                                                              'brokenMsisdn'))

                            self.utils.wait_loadingCircle()
                            self.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=tokenId)
                            self.utils.click_js(xpath="//label[contains(text(),'VALIDEAZ')]")

                            if self.utils.element_exists(xPath="//div[@class='error-text']") == True:
                                self.utils.log_customStep('VALID TOKEN', 'FAIL')
                            else:
                                self.utils.log_customStep('VALID TOKEN', 'PASSED')

                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_TokenIgnored':
                        self.utils.click_js(self.searchDict['Credit_Rate_Da'])
                        self.utils.type_js(xpath=self.searchDict['Numar_Contact'], value='730963536')

                    elif self.scenarioNameDC == 'CR_False_Client_nonMatur':
                        self.utils.click_js(self.searchDict['Credit_Rate_Nu'])
                        self.utils.type_js(xpath=self.searchDict['Numar_Contact'], value='730963536')

                    self.utils.click_js(self.searchDict['continua'])
                    self.abonament_nou()


                elif self.abNouType == 'MNP':
                    time.sleep(0.5)
                    contact = '734953572'
                    ported_no = '730044448'
                    self.utils.setTempValue('ported_no', ported_no)

                    self.utils.logScenariu('Click pe Portare')
                    self.utils.click_js(self.searchDict['Portare'])
                    self.utils.type_js(xpath=self.searchDict['Numar_Contact_Portare'], value=contact)
                    self.utils.type_js(xpath=self.searchDict['Numar_Portat'], value=ported_no, enter='yes')
                    self.utils.dropDownSelector_Index(self.searchDict['Donor'], 1)

                    if self.tipServiciu == "POSTPAID":
                        self.utils.click_js(self.searchDict['POSTPAID'])
                    elif self.tipServiciu == "PREPAID":
                        self.utils.click_js(self.searchDict['Prepaid'])

                    if self.scenarioNameDC == 'CR_True_Client_Matur':
                        self.utils.logScenariu('click Credit Rate = "Da"')
                        self.utils.click_js(self.searchDict['Credit_Rate_Da'])

                    elif self.scenarioNameDC == 'CR_False_Client_Matur':
                        self.utils.logScenariu('click Credit Rate = "Nu"')
                        self.utils.click_js(self.searchDict['Credit_Rate_Nu'])

                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_ValidateToken_Yes':
                        self.utils.click_js(self.searchDict['Credit_Rate_Da'])
                        if self.tipServiciu == 'POSTPAID':
                            self.utils.type_js(self.searchDict['Factura_1'], '300')
                            self.utils.type_js(self.searchDict['Factura_2'], '300')
                            self.utils.type_js(self.searchDict['Factura_3'], '300')

                        self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        self.utils.wait_loadingCircle()

                        self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        self.utils.wait_loadingCircle()

                        count = 3
                        while self.utils.element_exists(
                                "//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                            count -= 1
                            self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                            self.utils.wait_loadingCircle()

                        if self.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                            self.utils.log_customStep('Trimite token fail after Retry', 'FAIL')
                        else:
                            tokenId = self.utils.retryAPI(API='Token',
                                                          URL=self.utils.getTempValue('URL')['getSMSToken'],
                                                          KEY=self.utils.getTempValue(
                                                              'brokenMsisdn'))
                            self.utils.wait_loadingCircle()
                            self.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=tokenId)
                            self.utils.click_js(xpath="//label[contains(text(),'VALIDEAZ')]")

                        if self.utils.element_exists(xPath="//div[@class='error-text']") == True:
                            self.utils.log_customStep('VALID TOKEN', 'FAIL')
                        else:
                            self.utils.log_customStep('VALID TOKEN', 'PASSED')

                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_ValidateToken_No':
                        self.utils.click_js(self.searchDict['Credit_Rate_Da'])

                        if self.tipServiciu == 'POSTPAID':
                            self.utils.type_js(self.searchDict['Factura_1'], '300')
                            self.utils.type_js(self.searchDict['Factura_2'], '300')
                            self.utils.type_js(self.searchDict['Factura_3'], '300')

                        self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        self.utils.wait_loadingCircle()

                        count = 3
                        while self.utils.element_exists(
                                "//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                            count -= 1
                            self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                            self.utils.wait_loadingCircle()

                        if self.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                            self.utils.log_customStep('Trimite token fail after Retry', 'FAIL')
                        else:
                            tokenId = self.utils.retryAPI(API='Token',
                                                          URL=self.utils.getTempValue('URL')['getSMSToken'],
                                                          KEY=self.utils.getTempValue(
                                                              'brokenMsisdn'))
                            self.utils.wait_loadingCircle()

                            self.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=tokenId)

                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_Token_Fall_through':
                        if self.tipServiciu == 'POSTPAID':
                            self.utils.click_js(self.searchDict['Credit_Rate_Da'])
                            self.utils.type_js(self.searchDict['Factura_1'], '300')
                            self.utils.type_js(self.searchDict['Factura_2'], '300')
                            self.utils.type_js(self.searchDict['Factura_3'], '300')

                        self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        self.utils.wait_loadingCircle()

                        count = 3
                        while self.utils.element_exists(
                                "//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                            count -= 1
                            self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                            self.utils.wait_loadingCircle()

                        if self.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                            self.utils.log_customStep('Trimite token fail after Retry', 'FAIL')


                    elif self.scenarioNameDC == 'CR_True_Client_nonMatur_max4TokensSent':

                        if self.tipServiciu == 'POSTPAID':
                            self.utils.click_js(self.searchDict['Credit_Rate_Da'])
                            self.utils.type_js(self.searchDict['Factura_1'], '300')
                            self.utils.type_js(self.searchDict['Factura_2'], '300')
                            self.utils.type_js(self.searchDict['Factura_3'], '300')
                        for i in range(1, 6):

                            self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                            self.utils.wait_loadingCircle()

                            count = 3
                            while self.utils.element_exists(
                                    "//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                                count -= 1
                                self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                                self.utils.wait_loadingCircle()
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            e = str(e).replace("\n", "")
            print(f'{bcolors.WARN}{bcolors.BLUEIC}=====================================\n',
                  f'Error name => ["{exc_type}"]\n \tIn file => ["{fname}"]\n \t\tAt line => ["{exc_tb.tb_lineno}"]\n \t\t\tStack trace => ["{self.utils.format_stacktrace()}"]',
                  f'\n-------------------------------------\n{bcolors.ENDC}')

    def do_token_actions(self):
        self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]", js=False)
        self.utils.wait_loadingCircle()
        time.sleep(5)
        count = 3
        while self.utils.element_exists(xPath="//label[contains(text(),'VALIDEAZĂ')]") == False and count >= 0:
            print('-------------->>', count)
            self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
            # self.utils.wait_loadingCircle()
            if self.utils.element_exists(xPath="//label[contains(text(),'VALIDEAZĂ')]") == True:
                # self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                # self.utils.wait_loadingCircle()
                break
            else:
                count -= 1
        if count < 0:
            self.utils.log_customStep('Trimite token fail after Retry', 'FAIL')
        #
        # if self.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
        #     self.utils.log_customStep('Trimite token fail after Retry', 'FAIL')
        # else: ->
        tokenId = self.utils.retryAPI(API='Token',
                                      URL='http://devops02.connex.ro:8030/getSMSToken/',
                                      KEY=self.utils.getTempValue(
                                          'brokenMsisdn'))
        self.utils.wait_loadingCircle()
        self.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=tokenId)
        self.utils.click_js(
            xpath="""//a[@data-automation-id="token-consent-validate-token"]""")
        self.utils.wait_loadingCircle()
        if self.utils.element_exists(xPath="//div[@class='error-text']") == True:
            self.utils.log_customStep('VALID TOKEN', 'FAIL')
        else:
            self.utils.log_customStep('VALID TOKEN', 'PASSED')

    def abonament_nou(self):
        if '_true_' in self.scenarioNameDC.lower():
            if self.scenarioName_PageSearch == 'Completare_Informatii_Client_Yes':

                self.check_continua('Initial_Check')

                self.utils.type_js(xpath='//input[@name="personFirstName"]',value=self.informatii_client_dictionary['prenume'],element_name='Prenume')
                self.check_continua('prenume')

                self.utils.type_js(xpath='//input[@name="personLastName"]',value=self.informatii_client_dictionary['nume'],element_name='Nume')
                self.check_continua('nume')

                self.utils.type_js(xpath='//input[@aria-activedescendant="react-select-3--value"]', value=self.informatii_client_dictionary['judet'],element_name='Judet',enter=True)
                self.check_continua('judet')

                self.utils.type_js(xpath='//input[@aria-activedescendant="react-select-4--value"]', value=self.informatii_client_dictionary['localitate'],element_name='Localitate',enter=True)
                self.check_continua('localitate')

                self.utils.type_js(xpath='//input[@aria-activedescendant="react-select-5--value"]', value=self.informatii_client_dictionary['tip_strada'],element_name='Tip_Strada',enter=True)
                self.check_continua('tip_strada')

                self.utils.type_js(xpath='//input[@aria-activedescendant="react-select-6--value"]', value=self.informatii_client_dictionary['nume_strada'],element_name='Nume_Strada',enterWait=0.5)
                self.check_continua('nume_strada')

                self.utils.type_js(xpath='//input[@name="streetNumber"]', value=self.informatii_client_dictionary['numar_strada'],element_name='Numar_strada')
                self.check_continua('numar_strada')

                self.utils.type_js(xpath='//input[@name="blockNumber"]', value=self.informatii_client_dictionary['bloc'],element_name='Numar_Bloc')
                self.check_continua('bloc')

                self.utils.type_js(xpath='//input[@name="blockEntrance"]', value=self.informatii_client_dictionary['scara'],element_name='Scara_Bloc')
                self.check_continua('scara')

                self.utils.type_js(xpath='//input[@name="apartment"]', value=self.informatii_client_dictionary['apartament'],element_name='Apartament')

                if len(self.utils.get_text(xPath="//body/div[@id='root']/div[contains(@class,'mask-popup')]/div[@class='wrap-popup new-subscriber-popup']/div[contains(@class,'modalContent subscribers-popup-body')]/div[@class='demographics-information-section']/div[@class='address-details-wrapper']/div[@class='content-data address-content new-customer']/div/form/div[@class='popup-line align-content-address justify-content-flex-start']/div[4]/div[1]/div[1]/div[1]/div[1]")) > 2:
                    self.utils.log_customStep('Cod_postal','Pass')
                else:
                    self.utils.log_customStep('Cod_postal', 'Fail')
                self.check_continua('Cod_postal')

                self.utils.click_js(xpath='//button[@data-automation-id="subscribers-popup-continue-button"]')



        elif '_false_' in self.scenarioNameDC.lower():
            if self.scenarioName_PageSearch == 'Completare_Informatii_Client_No':

                self.check_continua('Initial_Check')
                self.utils.click_js(xpath='//button[@data-automation-id="subscribers-popup-continue-button"]')

            elif self.scenarioName_PageSearch == 'Completare_Informatii_Client_Yes_CR_False':
                pass


                self.utils.click_js(xpath="//div[@class='common-customer-wrapper padding-bottom-0']//label[contains(@class,'switch')]//span[@class='slider round']",
                                    element_name='Date_demografice')
                self.check_continua('date_demografice')

                self.utils.type_js(xpath='//input[@aria-activedescendant="react-select-3--value"]',
                                   value=self.informatii_client_dictionary['judet'], element_name='Judet', enter=True)
                self.check_continua('judet')

                self.utils.type_js(xpath='//input[@aria-activedescendant="react-select-4--value"]',
                                   value=self.informatii_client_dictionary['localitate'], element_name='Localitate',
                                   enter=True)
                self.check_continua('localitate')

                self.utils.type_js(xpath='//input[@aria-activedescendant="react-select-5--value"]',
                                   value=self.informatii_client_dictionary['tip_strada'], element_name='Tip_Strada',
                                   enter=True)
                self.check_continua('tip_strada')

                self.utils.type_js(xpath='//input[@aria-activedescendant="react-select-6--value"]',
                                   value=self.informatii_client_dictionary['nume_strada'], element_name='Nume_Strada',
                                   enterWait=0.5)
                self.check_continua('nume_strada')

                self.utils.type_js(xpath='//input[@name="streetNumber"]',
                                   value=self.informatii_client_dictionary['numar_strada'], element_name='Numar_strada')
                self.check_continua('numar_strada')

                self.utils.type_js(xpath='//input[@name="blockNumber"]',
                                   value=self.informatii_client_dictionary['bloc'], element_name='Numar_Bloc')
                self.check_continua('bloc')

                self.utils.type_js(xpath='//input[@name="blockEntrance"]',
                                   value=self.informatii_client_dictionary['scara'], element_name='Scara_Bloc',)
                self.check_continua('scara')

                self.utils.type_js(xpath='//input[@name="apartment"]',
                                   value=self.informatii_client_dictionary['apartament'], element_name='Apartament')
                self.check_continua('apartament')

                if len(self.utils.get_text(
                        xPath="//body/div[@id='root']/div[contains(@class,'mask-popup')]/div[@class='wrap-popup new-subscriber-popup']/div[contains(@class,'modalContent subscribers-popup-body')]/div[@class='demographics-information-section']/div[@class='address-details-wrapper']/div[@class='content-data address-content new-customer']/div/form/div[@class='popup-line align-content-address justify-content-flex-start']/div[4]/div[1]/div[1]/div[1]/div[1]")) > 2:
                    self.utils.log_customStep('Cod_postal', 'Pass')
                else:
                    self.utils.log_customStep('Cod_postal', 'Fail')
                self.check_continua('Cod_postal')

                self.utils.click_js(xpath='//button[@data-automation-id="subscribers-popup-continue-button"]')


    def check_continua(self,inputElement):
        if self.scenarioName_PageSearch == 'Completare_Informatii_Client_Yes':
            if (self.utils.is_element_Enabled(xPath='//div[@class="buttons-right"]/button[@data-automation-id="subscribers-popup-continue-button"]')) == False:
                if inputElement in ['nume','prenume','judet','localitate','tip_strada','Initial_Check']:
                    self.utils.log_customStep('Am gasit buton Continua Disable pe '+inputElement, 'PASSED')
                else:
                    self.utils.log_customStep('Am gasit buton Continua Enable pe '+ inputElement, 'CRITICAL')

            elif (self.utils.is_element_Enabled(xPath='//div[@class="buttons-right"]/button[@data-automation-id="subscribers-popup-continue-button"]')) == True:
                if inputElement in ['nume_strada','numar_strada','bloc','scara','apartament','Cod_postal']:
                    self.utils.log_customStep('Am gasit buton Continua Enable pe ' + inputElement, 'PASSED')
                else:
                    self.utils.log_customStep('Am gasit buton Continua Disable pe ' + inputElement, 'CRITICAL')

        elif self.scenarioName_PageSearch == 'Completare_Informatii_Client_No' or self.scenarioName_PageSearch == 'Completare_Informatii_Client_Yes_CR_False':
            if (self.utils.is_element_Enabled(xPath='//div[@class="buttons-right"]/button[@data-automation-id="subscribers-popup-continue-button"]')) == False:
                if inputElement in ['date_demografice','judet','localitate','tip_strada',]:
                    self.utils.log_customStep('Continua Disable dupa completarea campului: ' + inputElement, 'PASSED')
                else:
                    self.utils.log_customStep('Continua Enable  dupa completarea campului: ' + inputElement, 'CRITICAL')

            elif (self.utils.is_element_Enabled(xPath='//div[@class="buttons-right"]/button[@data-automation-id="subscribers-popup-continue-button"]')) == True:
                if inputElement in ['Initial_Check','nume','prenume','nume_strada','numar_strada','bloc','scara','apartament','Cod_postal']:
                    self.utils.log_customStep('Continua Disable dupa completarea campului: ' + inputElement, 'PASSED')
                else:
                    self.utils.log_customStep('Continua Enable  dupa completarea campului: ' + inputElement, 'CRITICAL')


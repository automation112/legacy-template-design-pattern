import time, os, sys, bcolors
from SRC.test_Definitions.test_baaseline_Classes.page_login_baseline import page_login_baseline

class page_login_cbu_oms(page_login_baseline):

    def __init__(self, browser=None, testCaseName=None, channel=None):
        page_login_baseline.__init__(self, browser, testCaseName, channel)

    def test_Login(self):

        try:
            if self.channel == 'RETAIL':
                time.sleep(2)
                # Click ok
                self.utils.click_js(xpath=self.login_dict['OK'])
                # Find username and insert user
                self.utils.type_js(self.login_dict['UserName'],'florin.dedu')
                # Find password and insert pass
                self.utils.type_js(self.login_dict['Password'], 'abc123')
                self.utils.click_js(self.login_dict['Login'], priority='CRITICAL')
                #self.utils.wait_loadingCircle()
                # Search dealer(1) on page and click continue
                self.utils.dropDownSelector_Index(self.login_dict['DealerSelect'], 1)
                # self.utils.log_customStep('Test', 'FAIL')
                self.utils.click_js(self.login_dict['Continua'], priority='CRITICAL')

            elif self.channel == 'TELESALES':
                # Click ok
                self.utils.click_js(self.login_dict['OK'])
                self.utils.type_js(self.login_dict['UserName'], 'cm.test1')
                # Find password and insert pass
                self.utils.type_js(self.login_dict['Password'], 'abc123')
                self.utils.click_js(self.login_dict['Login'])
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            e = str(e).replace("\n", "")
            print(f'{bcolors.WARN}{bcolors.BLUEIC}=====================================\n',
                  f'Error name => ["{exc_type}"]\n \tIn file => ["{fname}"]\n \t\tAt line => ["{exc_tb.tb_lineno}"]\n \t\t\tStack trace => ["{self.utils.format_stacktrace()}"]',
                  f'\n-------------------------------------\n{bcolors.ENDC}')
            self.utils.log_customStep('INTERNAL_CODE_ERROR', 'REASON = [{}]'.format(e))
        finally:
            return (self.utils.VDFloggingObj.method_log)

from SRC.test_Definitions.test_baaseline_Classes.page_configureaza_baseline import page_configureaza_baseLine
import time
from collections import defaultdict
import bcolors, os, sys

class page_configureaza_cbu_oms_Achizitie(page_configureaza_baseLine):

    
    def __init__(self, scenarioNameDC=None, testCaseName=None, abNouType=None, tipServiciu=None, offerChoice=None,
                 scenarioName_PC=None, browser=None, channel=None, configType=None, retention_flow=None,
                 retention_option=None, inputDictionary=None):

        page_configureaza_baseLine.__init__(self, inputDictionary=inputDictionary, scenarioNameDC=scenarioNameDC, testCaseName=testCaseName, abNouType=abNouType, tipServiciu=tipServiciu, offerChoice=offerChoice, scenarioName_PC=scenarioName_PC, browser=browser, channel=channel, configType=configType, retention_flow=retention_flow, retention_option=retention_option)
    def test_page_configureaza(self):
        try:
            offerName = self.getPegaOfferName()
            if '_ACHIZITIE' in self.configType:
                if self.offerChoice == 'PachetPromo':
                    common = None
                    scenario_simplu = 0

                    if self.scenarioName_PC == 'BundleOffers_Vezi_Mai_Multe_Rate':
                        common = self.pageConfigDict['PachetePromo_Rate']
                    elif self.scenarioName_PC == 'BundleOffers_Vezi_Mai_Multe_Discount':
                        common = self.pageConfigDict['PachetePromo_Discount']
                    elif self.scenarioName_PC == 'BundleOffers_Vezi_Mai_Multe_Rate_Discount':
                        common = self.pageConfigDict['PachetePromo_Rate_and_Discount']
                    elif self.scenarioName_PC == 'BundleOffers_Vezi_Mai_Multe_SimOnly':
                        common = self.pageConfigDict['PachetePromo_Default']
                    elif self.scenarioName_PC == 'BundleOffers_Rate':

                        common = self.pageConfigDict['PachetePromo_Rate'].replace('3', '0')
                        scenario_simplu = 1
                        self.utils.click_js('//div[@class="accordion_head_button"]')
                        self.utils.type_js(
                            '//div[@class="fields-wrap-plan search"][1]//input[@data-automation-id="search-plan-input-filter-popup"]',
                            offerName['rate'])
                        self.utils.click_js("//button[@class='primary bold']")
                        self.utils.wait_loadingCircle()

                    elif self.scenarioName_PC == 'BundleOffers_SimOnly_si_Discount':
                        common = self.pageConfigDict['PachetePromo_Discount'].replace('3', '0')
                        scenario_simplu = 1
                        self.utils.click_js('//div[@class="accordion_head_button"]')
                        self.utils.type_js(
                            '//div[@class="fields-wrap-plan search"][1]//input[@data-automation-id="search-plan-input-filter-popup"]',
                            offerName['discount'])
                        self.utils.click_js("//button[@class='primary bold']")
                        self.utils.wait_loadingCircle()

                    elif self.scenarioName_PC == 'BundleOffers_Rate_Discount':
                        print('am intrat pe rate_discount')
                        common = self.pageConfigDict['PachetePromo_Rate_and_Discount'].replace('3', '0')
                        scenario_simplu = 1
                        self.utils.click_js('//div[@class="accordion_head_button"]')
                        self.utils.type_js(
                            '//div[@class="fields-wrap-plan search"][1]//input[@data-automation-id="search-plan-input-filter-popup"]',
                            offerName['discount'])
                        self.utils.click_js("//button[@class='primary bold']")
                        self.utils.wait_loadingCircle()

                    elif self.scenarioName_PC == 'BundleOffers_SimOnly':
                        common = self.pageConfigDict['PachetePromo_Default'].replace('3', '0')
                        scenario_simplu = 1
                        self.utils.click_js('//div[@class="accordion_head_button"]')
                        self.utils.type_js(
                            '//div[@class="fields-wrap-plan search"][1]//input[@data-automation-id="search-plan-input-filter-popup"]',
                            offerName['simonly'])
                        self.utils.click_js("//button[@class='primary bold']")
                        self.utils.wait_loadingCircle()

                    selectat = common.replace('> 3', '= 1')
                    get_stock_val = selectat + '//span[contains(text(),"STOC")]'
                    selecteaza = common + '//*[contains(text(),"Select")]'
                    if scenario_simplu == 0:
                        self.utils.click_js(self.pageConfigDict['vezi mai multe'])
                        # scroll to element , 4 scroll by default, maximul 10.
                        self.utils.scroll_to_element(selecteaza)
                    else:
                        time.sleep(10)
                        print('vom da click pe ' + selecteaza)
                        self.utils.click_js(selecteaza, priority='CRITICAL')
                        # self.utils.click_js('//div[@class="fixed-bundle-offers"]/div/div/div')
                        self.utils.click_js(
                            '//div[@class="accordion    "]//div[@class="accordion_head_title "]/span')
                    time.sleep(10)
                    stock = self.utils.get_text(get_stock_val)
                    try:
                        stock_strip_l = stock.replace(':', ' ').split()[1]
                        stock_strip = int(stock_strip_l)
                        if int(stock_strip) == 0:
                            stock_val = stock_strip
                        elif int(stock_strip) > 0:
                            stock_val = stock_strip
                    except ValueError:
                        stock_strip = stock.split()[1]
                        if stock_strip == 'INDISPONIBIL':
                            stock_val = -1
                        else:
                            stock_val = -999
                    except IndexError:
                        stock_val = -888
                        self.utils.log_customStep('Informatie Stock indisponibila', 'FAIL')
                    except AttributeError:
                        stock_val = -8888
                        self.utils.log_customStep('Informatie Stock indisponibila', 'FAIL')

                    print(stock_val)
                    carton_d = {
                        'nume_abon': '//*[@class="color_title"]',
                        'perioada_contract': '//div[@class="det_months uppercase"]//span',
                        'nume_device': '//span[@class="model_phone ellipsis-tooltip"]',
                        'card_total': '//div[@class="price"]/h4',
                        'card_total_fara_reducere': '//div[@class="price"]/div[@class="strikethroughDiscount"]',
                        'card_upfront_device': '//div[@class="price_details"]/span[@class="price"]',
                        # 'card_upfront_total': '//div[@class="price"]/h4',
                        'card_upfront_total': '//div[@class="price_details"]//span[@class="price"]',
                        'card_lunar_device': '//div[@class="installment-section"]/span[@class="p_instalments"]',
                        'numar_rate': '//div[@class="num_instalments"]/span'}
                    card_d_r = {}
                    for key, value in carton_d.items():
                        full_xpath = selectat + value
                        if key == 'nume_abon':
                            if self.scenarioName_PC == 'BundleOffers_Vezi_Mai_Multe_SimOnly':
                                if '...' in self.utils.get_text(full_xpath):
                                    card_d_r[key] = self.utils.get_attrValue(full_xpath, 'title')
                                else:
                                    card_d_r[key] = self.utils.get_text(full_xpath)
                            elif self.scenarioName_PC == 'BundleOffers_SimOnly':
                                if '...' in self.utils.get_text(full_xpath):
                                    card_d_r[key] = self.utils.get_attrValue(full_xpath, 'title')
                                else:
                                    card_d_r[key] = self.utils.get_text(full_xpath)
                        elif key == 'perioada_contract':
                            if self.utils.get_text(full_xpath) is not False:
                                try:
                                    card_d_r[key] = self.utils.get_text(full_xpath).split()[0]
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            else:
                                card_d_r[key] = False
                        elif key == 'numar_rate':
                            if self.utils.get_text(full_xpath) is not False:
                                card_d_r[key] = self.utils.get_text(full_xpath).split()[0]
                            else:
                                card_d_r[key] = False
                        else:
                            # print(key)
                            card_d_r[key] = self.utils.get_text(full_xpath)
                    print(card_d_r)
                    cart_d = {
                        'nume_abon': '//*[@class="cart-accordion-panel show-cart-accordion-panel"][1]/section[@class="cart-items-section"]//div[@class="cart-item-title-nonCapitalize"]',
                        'perioada_contract': '//*[@class="cart-accordion-panel show-cart-accordion-panel"][1]/section[@class="cart-items-section"]//span[@class="duration-span"]',
                        'card_total': '//section[@class="cart-total-items-section"]//div[@class="item-right-wrap"]//div[2]/div',
                        # 'card_total': '(//div[@class="cart-price"])[7]',
                        'card_total_fara_reducere': '//section[@class="cart-total-items-section"]//div[@class="item-wrap-prices"]//div[@class="cart-strikethrough"]',
                        # 'card_upfront_total': '//section[@class="cart-total-items-section"]//div[@class="item-wrap-prices normalize-price"]//div[@class="cart-price"]',
                        # 'card_upfront_total': '(//div[@class="cart-price"])[6]',
                        'card_upfront_total': '//section[@class="cart-total-items-section"]//div[@class="item-right-wrap"]//div[1]/div',
                        'card_upfront_device': '//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div/section[@class="cart-items-section"]//div[@class="item-right-wrap align-top"]/div[1]',
                        'card_lunar_device': '//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div/section[@class="cart-items-section"]//div[@class="item-right-wrap align-top"]/div[2]',
                        # 'lunar': '//*[@class="cart-accordion-panel show-cart-accordion-panel"][1]/section[@class="cart-items-section"]//div[@class="cart-price"]',
                        'nume_device': '//*[@class="cart-accordion-panel show-cart-accordion-panel"][1]/div//section[2]//span[@class="txt_left_side"]',
                        'numar_rate': '//*[@class="cart-accordion-panel show-cart-accordion-panel"][1]/div//section[2]//*[@class="duration-span"]'}
                    cart_r = {}
                    self.utils.click_js("//button[contains(@class,'img-wrap')]//span")
                    for key, value in cart_d.items():
                        if key == 'perioada_contract':
                            # cart_r[key] = self.utils.get_text(value).split()[0]
                            # cart_r[key] = '24'
                            if self.utils.get_text(value) is not False:
                                try:
                                    cart_r[key] = self.utils.get_text(value).split()[0]
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            else:
                                cart_r[key] = False
                        else:
                            if self.utils.get_text(value) == '- - -':
                                # print('aaaaaa '+key)
                                cart_r[key] = False
                            elif self.utils.get_text(value) is None:
                                # print('bbbbbb '+key)
                                card_d_r[key] = False
                            else:
                                # print('ccccc '+ key)
                                cart_r[key] = self.utils.get_text(value)
                    print(cart_r)
                    added, removed, modified, same, shared_keys = self.dict_compare(cart_r, card_d_r)
                    if len(same) != len(shared_keys):
                        for key, value in modified.items():
                            self.utils.log_customStep('Card VS Shopping Chart ' + key,
                                                                   'FAILED ' + '--> [' + str(value) + ']')
                        cart_r.pop(key)
                        for key, value in cart_r.items():
                            self.utils.log_customStep(
                                key, 'PASSED ' + '--> [' + str(value) + ']')
                    else:
                        for key, value in cart_r.items():
                            self.utils.log_customStep(
                                key, 'PASSED ' + '--> [' + str(value) + ']')
                    if stock_val == 0 and self.utils.is_element_Enabled(
                            self.pageConfigDict['PachetePromo_Continua']) is True:
                        if self.channel == 'RETAIL':
                            self.utils.log_customStep(
                                'Continua enabled when channel=Retail and device with stock 0', 'CRITICAL_FAIL')
                        else:
                            self.utils.click_js(self.pageConfigDict['PachetePromo_Continua'])
                    elif stock_val == -1:
                        self.utils.click_js(self.pageConfigDict['PachetePromo_Continua'])
                    elif stock_val > 0:
                        self.utils.click_js(self.pageConfigDict['PachetePromo_Continua'])
                    else:
                        print('Stock neinterpretabil')

                elif self.offerChoice == 'ConfigOferte':

                    self.utils.logScenariu('Click pe Configureaza Oferte')
                    self.utils.click_js(self.pageConfigDict['configureaza_oferte'])
                    self.utils.wait_loadingCircle()

                    if self.scenarioName_PC == 'OferteConfigurabile_D2S_Rate':
                        self.filterDevice()
                        self.testStepsExec()

                    elif self.scenarioName_PC == 'OferteConfigurabile_D2S_3YRS_Rate':
                        self.filterDevice()
                        self.testStepsExec()

                    elif self.scenarioName_PC == 'OferteConfigurabile_D2S_SimOnly_si_Discount':

                        self.testStepsExec()

                    elif self.scenarioName_PC == 'OferteConfigurabile_D2S_Rate_si_Discount':
                        self.filterDevice()
                        self.testStepsExec()

                    elif self.scenarioName_PC == 'OferteConfigurabile_D2S_3YRS_Rate_si_Discount':
                        self.filterDevice()
                        self.testStepsExec()

                    elif self.scenarioName_PC == 'OferteConfigurabile_D2S_SimOnly':
                        self.testStepsExec()

                    elif self.scenarioName_PC == 'OferteConfigurabile_S2D_Rate':
                        self.utils.click_js(
                            self.pageConfigDict['incepe cu servicii'])
                        self.filterDevice()
                        self.testStepsExec()

                    elif self.scenarioName_PC == 'OferteConfigurabile_S2D_3YRS_Rate':
                        self.utils.click_js(
                            self.pageConfigDict['incepe cu servicii'])
                        self.filterDevice()
                        self.testStepsExec()

                    elif self.scenarioName_PC == 'OferteConfigurabile_S2D_Rate_si_Discount':
                        self.utils.click_js(
                            self.pageConfigDict['incepe cu servicii'])
                        self.filterDevice()
                        self.testStepsExec()

                    elif self.scenarioName_PC == 'OferteConfigurabile_S2D_3YRS_Rate_si_Discount':
                        self.utils.click_js(
                            self.pageConfigDict['incepe cu servicii'])
                        self.filterDevice()
                        self.testStepsExec()

                    elif self.scenarioName_PC == 'OferteConfigurabile_S2D_SimOnly_si_Discount':
                        self.utils.logScenariu(
                            'Merge si verifica logica pe stock in functie de channel pe Scenariul_Config_Service_S2D')
                        self.utils.click_js(
                            self.pageConfigDict['incepe cu servicii'])
                        self.filterDevice()
                        self.testStepsExec()

                    elif self.scenarioName_PC == 'OferteConfigurabile_S2D_SimOnly':
                        self.utils.click_js(
                            self.pageConfigDict['incepe cu servicii'])
                        self.testStepsExec()

                    elif self.scenarioName_PC == 'OferteConfigurabile_S2D_VMM_Rate':
                        time.sleep(2)
                        self.utils.click_js(
                            self.pageConfigDict['incepe cu servicii'], js=False)
                        self.filterDevice()
                        self.testStepsExec()

                    elif self.scenarioName_PC == 'OferteConfigurabile_S2D_VMM_SimOnly_si_Discount':
                        self.utils.logScenariu(
                            'Merge si verifica logica pe stock in functie de channel pe Scenariul_Config_Service_S2D')
                        self.utils.click_js(
                            self.pageConfigDict['incepe cu servicii'])
                        self.testStepsExec()

                    elif self.scenarioName_PC == "OferteConfigurabile_S2D_VMM_SimOnly":
                        self.utils.logScenariu(
                            'Merge si verifica logica pe stock in functie de channel pe Scenariul_Config_Service_S2D')
                        self.utils.click_js(
                            self.pageConfigDict['incepe cu servicii'])
                        self.testStepsExec()

                    elif self.scenarioName_PC == 'OferteConfigurabile_ServiceOnly':
                        print('SADYQWE')
                        self.utils.click_js(
                            self.pageConfigDict['incepe cu servicii'])

                        self.utils.check_button(self.pageConfigDict['doar_servicii_chk'],
                                                             self.pageConfigDict['doar_servicii'],
                                                             'value')
                        self.testStepsExec()

                    elif self.scenarioName_PC == 'OferteConfigurabile_ServiceOnly_Discount':
                        self.utils.click_js(
                            self.pageConfigDict['incepe cu servicii'])

                        self.utils.check_button(self.pageConfigDict['doar_servicii_chk'],
                                                             self.pageConfigDict['doar_servicii'],
                                                             'value')
                        self.testStepsExec()

                    elif self.scenarioName_PC == 'OferteConfigurabile_D2S_VMM_Rate':
                        self.filterDevice()
                        self.testStepsExec()

                    elif self.scenarioName_PC == 'OferteConfigurabile_D2S_VMM_Rate_si_Discount':
                        self.filterDevice()
                        self.testStepsExec()

                    elif self.scenarioName_PC == 'OferteConfigurabile_D2S_VMM_SimOnly_si_Discount':
                        self.filterDevice()
                        self.testStepsExec()

                    elif self.scenarioName_PC == 'OferteConfigurabile_D2S_VMM_SimOnly':
                        self.testStepsExec()

                    elif self.scenarioName_PC == 'OferteConfigurabile_ServiceOnly_VMM':
                        self.utils.click_js(
                            self.pageConfigDict['incepe cu servicii'])

                        self.utils.check_button(self.pageConfigDict['doar_servicii_chk'],
                                                             self.pageConfigDict['doar_servicii'],
                                                             'value')
                        self.testStepsExec()

                    elif self.scenarioName_PC == 'OferteConfigurabile_ServiceOnly_Discount_VMM':
                        self.utils.click_js(
                            self.pageConfigDict['incepe cu servicii'])

                        self.utils.check_button(self.pageConfigDict['doar_servicii_chk'],
                                                             self.pageConfigDict['doar_servicii'],
                                                             'value')
                        self.testStepsExec()

                    elif self.scenarioName_PC == 'OferteConfigurabile_S2D_VMM_Rate_si_Discount':
                        self.utils.click_js(
                            self.pageConfigDict['incepe cu servicii'])
                        self.filterDevice()
                        self.testStepsExec()
        except Exception as e:
            e = str(e).replace("\n", "")
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(f'{bcolors.WARN}{bcolors.BLUEIC}=====================================\n',
                  f'Error name => ["{exc_type}"]\nIn file => ["{fname}"]\nAt line => ["{exc_tb.tb_lineno}"]\nStack trace => ["{self.utils.format_stacktrace()}"]',
                  f'\n-------------------------------------\n{bcolors.ENDC}')
            page_configureaza_cbu_oms_Achizitie.utils.log_customStep('INTERNAL_CODE_ERROR', 'REASON = [{}]'.format(e))
        finally:
            return (page_configureaza_cbu_oms_Achizitie.utils.VDFloggingObj.method_log)

    def testStepsExec(self):
        offerName = self.getPegaOfferName()

        print('offerName is --> ', offerName)
        if 'ACHIZITIE' in self.configType:

            if self.offerChoice == 'ConfigOferte':
                print('FLow is on track')
                if self.scenarioName_PC == 'Scenariul_Config_Service_D2S':

                    cardXPathsDict = {'device': {
                        'Offer_S2D_Rate_CardToBeSelected': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]""",
                        'Offer_S2D_Rate_SelectedOfferCard': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//button[contains(@class,'general_btn button-selected')]//ancestor::div[@class="card mobile-card"][1]""",
                        'Oferta_S2D_Rate_NumeOferta': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                        'Oferta_S2D_Rate_PerioadaContractuala': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                        'Oferta_S2D_Rate_oPrice': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
                    },
                        'offers': {
                            'Offer_Rate_CardToBeSelected': '//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]',
                            'Offer_Rate_SelectedCard': """//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//div[@class='card-footer card-footer-button']//span[contains(text(),'SELECTAT')]//ancestor::div[@class="card mobile-card"][1]""",
                            'Offer_Rate_RateMultiple': '//span[@class="link-title blue bold"]',
                            'Offer_Rate_Name': '//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//div[@class="model_phone"]',
                            'Offer_Rate_Upfront': '//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//span[@class="price"]',
                            'Offer_Rate_InstallmentValue': '//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//div[@class="align-justify two_sides undefined"]//span[@class="txt_right_side after_discount"]',
                            'Offer_Rate_Stock': '//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//div[@class="align-justify final_p"]/span[2]',
                        },
                        'total': {
                            'Total_S2D_Rate_TotalLunar': """//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="price total-device-price"]/h4""",
                            'Device_Rate_Avans': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                        }}

                elif self.scenarioName_PC == 'OferteConfigurabile_D2S_Rate':
                    print('sceriu sd')

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {
                        'offers': {
                            'Offer_D2S_Rate_CardToBeSelected': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]""",
                            'Offer_D2S_Rate_SelectedOfferCard': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//button[contains(@class,'general_btn button-selected')]//ancestor::div[@class="card mobile-card"][1]""",
                            'Oferta_D2S_Rate_NumeOferta': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                            'Oferta_D2S_Rate_PerioadaContractuala': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                            # 'Oferta_D2S_Rate_oPrice': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
                            # 'Oferta_D2S_Rate_LastInstallment': '//div[@class="white_tooltip box_tooltip  visible-tooltip"]/div[last()]',
                            'Oferta_D2S_Rate_InstallmentValue': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//span[@class="txt_right_side after_discount"]""",
                        },

                        'device': {
                            'Device_D2S_Rate_CardToBeSelected': '//div[@class="card_wrap card-wrap-device device-offer "]',
                            'Device_D2S_Rate_SelectedCard': """//div[@class="card_wrap card-wrap-device device-offer "]//span[contains(text(),'SELECTAT')]//ancestor::div[@class="card_wrap card-wrap-device device-offer "][1]""",
                            'Device_D2S_Rate_Name': '//div[@class="card_wrap card-wrap-device device-offer "]//div[@class="model_phone"]',
                            'Device_D2S_Rate_Upfront': '//div[@class="card_wrap card-wrap-device device-offer "]//span[@class="price"]',

                        },
                        'total': {
                            'Total_D2S_Rate_TotalLunar': """//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
                            'Device_D2S_Rate_Avans': """//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//span[@class="price"]""",
                        }}

                    time.sleep(5)
                    for key, val in cardXPathsDict['device'].items():
                        if key != 'Device_D2S_Rate_SelectedCard':
                            s2dCardInfo['device'][key] = self.utils.get_text(val)

                    # Selecteaza DEVICE
                    self.utils.click_js(self.pageConfigDict['Device_D2S_Rate_Selecteaza'])
                    self.utils.wait_loadingCircle()
                    time.sleep(2)
                    self.checkMinimBanners(expandElement='Device_D2S')

                    for key, value in s2dCardInfo['device'].items():
                        print(key)
                        if key != 'Device_D2S_Rate_CardToBeSelected':
                            if key == 'Device_D2S_Rate_RateMultiple':
                                try:
                                    value = value.split('\n')[0].replace('x', '')
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):

                                    print(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value))
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[3],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[3],
                                        'FAIL --> [{}]'.format(value))

                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                    s2dCardInfo['device']['Device_D2S_Rate_SelectedCard'] = self.utils.get_text(
                        cardXPathsDict['device']['Device_D2S_Rate_SelectedCard'])

                    # time.sleep(10)
                    self.utils.click_js(
                        """//div[@class="fixed-bundle-offers"][2]//div[@data-automation-id="open-filter"]""")

                    self.utils.type_js('//input[@name="searchInput"]', offerName['rate'])

                    self.utils.click_js("//button[@class='primary bold']")
                    self.utils.wait_loadingCircle()
                    time.sleep(20)
                    self.utils.click_js(self.pageConfigDict['Oferta_D2S_Rate_RateMultiple'], customWait=20)
                    self.utils.wait_loadingCircle()
                    self.utils.click_js(self.pageConfigDict['Oferta_D2S_Rate_LastInstallment'])
                    self.utils.wait_loadingCircle()

                    ##GET OFFER DETAILS
                    for key, val in cardXPathsDict['offers'].items():
                        if key != 'Offer_D2S_Rate_SelectedOfferCard':
                            print('the key is ', key)
                            s2dCardInfo['offer'][key] = self.utils.get_text(val)
                            print('s2ddict -- . ', s2dCardInfo)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_D2S_Rate_Select'])
                    self.utils.wait_loadingCircle()
                    cardNotSelected = str(s2dCardInfo['device']['Device_D2S_Rate_CardToBeSelected']).replace(
                        'SELECTEAZĂ', '')
                    cardSelected = str(s2dCardInfo['device']['Device_D2S_Rate_SelectedCard']).replace('SELECTAT', '')

                    if cardNotSelected == cardSelected:
                        self.utils.log_customStep('Selected Device_Card values VS Device_Card before select values',
                                                  'PASSED [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))
                    else:
                        self.utils.log_customStep('Selected Device_Card values VS Device_Card before select values',
                                                  'FAIL [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected)
                                                  )

                    del s2dCardInfo['device']['Device_D2S_Rate_CardToBeSelected']
                    del s2dCardInfo['device']['Device_D2S_Rate_SelectedCard']

                    self.checkMinimBanners(expandElement='Servicii_D2S')
                    s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard'] = self.utils.get_text(
                        cardXPathsDict['offers']['Offer_D2S_Rate_SelectedOfferCard'])

                    cardNotSelected = str(s2dCardInfo['offer']['Offer_D2S_Rate_CardToBeSelected']).replace('SELECTEAZA',
                                                                                                           '')
                    cardSelected = str(s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard']).replace('SELECTAT', '')

                    if cardNotSelected == cardSelected:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'PASSED [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))
                    else:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'FAIL [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))

                    del s2dCardInfo['offer']['Offer_D2S_Rate_CardToBeSelected']
                    del s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard']

                    for key, value in s2dCardInfo['offer'].items():

                        if key == 'Oferta_D2S_Rate_PerioadaContractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        # elif key =='Oferta_S2D_Rate_Perioada_Contractuala':
                        #     value = value.split('\n')[0].replace('x','')
                        print('offervals ', key)

                        if key == 'Oferta_D2S_Rate_InstallmentValue':
                            cartXpath = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div//section[@class="cart-items-section"]//*[contains(text(),'{}')]"""
                        else:
                            cartXpath = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]"""

                        print(cartXpath.format(value))

                        if self.utils.element_exists(cartXpath.format(value)):
                            try:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        else:
                            try:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'FAIL --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                    for key, value in s2dCardInfo['total'].items():
                        try:
                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                        value)):
                                print(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                        value))
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[3],
                                                                       'FAIL --> [{}]'.format(value))
                        except AttributeError as e:
                            print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                  '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                  '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 444 ', )

                    # self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    time.sleep(10)
                    isStrikeThrough = self.utils.element_exists(
                        """//div[@class="cart-menu-wrap"]//*[@class="cart-strikethrough"]""")
                    print('isStrikeThrough', isStrikeThrough)
                    if isStrikeThrough == False:
                        self.utils.log_customStep('Niciun element cu StrikeThrough Identificat in Cart', 'PASSED')
                    else:
                        self.utils.log_customStep('Niciun element cu StrikeThrough Identificat in Cart', 'FAIL')

                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                elif self.scenarioName_PC == 'OferteConfigurabile_D2S_3YRS_Rate':
                    print('sceriu sd')

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {
                        'offers': {
                            'Offer_D2S_Rate_CardToBeSelected': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]""",
                            'Offer_D2S_Rate_SelectedOfferCard': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//button[contains(@class,'general_btn button-selected')]//ancestor::div[@class="card mobile-card"][1]""",
                            'Oferta_D2S_Rate_NumeOferta': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                            'Oferta_D2S_Rate_PerioadaContractuala': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                            # 'Oferta_D2S_Rate_oPrice': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
                            # 'Oferta_D2S_Rate_LastInstallment': '//div[@class="white_tooltip box_tooltip  visible-tooltip"]/div[last()]',
                            'Oferta_D2S_Rate_InstallmentValue': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//span[@class="txt_right_side after_discount"]""",
                        },

                        'device': {
                            'Device_D2S_Rate_CardToBeSelected': '//div[@class="card_wrap card-wrap-device device-offer "]',
                            'Device_D2S_Rate_SelectedCard': """//div[@class="card_wrap card-wrap-device device-offer "]//span[contains(text(),'SELECTAT')]//ancestor::div[@class="card_wrap card-wrap-device device-offer "][1]""",
                            'Device_D2S_Rate_Name': '//div[@class="card_wrap card-wrap-device device-offer "]//div[@class="model_phone"]',
                            'Device_D2S_Rate_Upfront': '//div[@class="card_wrap card-wrap-device device-offer "]//span[@class="price"]',

                        },
                        'total': {
                            'Total_D2S_Rate_TotalLunar': """//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
                            'Device_D2S_Rate_Avans': """//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//span[@class="price"]""",
                        }}

                    time.sleep(5)
                    for key, val in cardXPathsDict['device'].items():
                        if key != 'Device_D2S_Rate_SelectedCard':
                            s2dCardInfo['device'][key] = self.utils.get_text(val)

                    # Selecteaza DEVICE
                    self.utils.click_js(self.pageConfigDict['Device_D2S_Rate_Selecteaza'])
                    self.utils.wait_loadingCircle()
                    time.sleep(2)
                    self.checkMinimBanners(expandElement='Device_D2S')

                    for key, value in s2dCardInfo['device'].items():
                        print(key)
                        if key != 'Device_D2S_Rate_CardToBeSelected':
                            if key == 'Device_D2S_Rate_RateMultiple':
                                try:
                                    value = value.split('\n')[0].replace('x', '')
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):

                                    print(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value))
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[3],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[3],
                                        'FAIL --> [{}]'.format(value))

                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                    s2dCardInfo['device']['Device_D2S_Rate_SelectedCard'] = self.utils.get_text(
                        cardXPathsDict['device']['Device_D2S_Rate_SelectedCard'])

                    # time.sleep(10)
                    self.utils.click_js(
                        """//div[@class="fixed-bundle-offers"][2]//div[@data-automation-id="open-filter"]""")

                    self.utils.type_js('//input[@name="searchInput"]', offerName['rate'])

                    self.utils.click_js("//button[@class='primary bold']")
                    self.utils.wait_loadingCircle()
                    time.sleep(20)
                    self.utils.click_js(self.pageConfigDict['Oferta_D2S_Rate_RateMultiple'], customWait=20)
                    self.utils.wait_loadingCircle()
                    self.utils.click_js(self.pageConfigDict['Oferta_D2S3YRS_Rate_LastInstallment'])
                    self.utils.wait_loadingCircle()

                    ##GET OFFER DETAILS
                    for key, val in cardXPathsDict['offers'].items():
                        if key != 'Offer_D2S_Rate_SelectedOfferCard':
                            print('the key is ', key)
                            s2dCardInfo['offer'][key] = self.utils.get_text(val)
                            print('s2ddict -- . ', s2dCardInfo)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_D2S_Rate_Select'])
                    self.utils.wait_loadingCircle()
                    cardNotSelected = str(s2dCardInfo['device']['Device_D2S_Rate_CardToBeSelected']).replace(
                        'SELECTEAZĂ', '')
                    cardSelected = str(s2dCardInfo['device']['Device_D2S_Rate_SelectedCard']).replace('SELECTAT', '')

                    if cardNotSelected == cardSelected:
                        self.utils.log_customStep('Selected Device_Card values VS Device_Card before select values',
                                                  'PASSED [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))
                    else:
                        self.utils.log_customStep('Selected Device_Card values VS Device_Card before select values',
                                                  'FAIL [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected)
                                                  )

                    del s2dCardInfo['device']['Device_D2S_Rate_CardToBeSelected']
                    del s2dCardInfo['device']['Device_D2S_Rate_SelectedCard']

                    self.checkMinimBanners(expandElement='Servicii_D2S')
                    s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard'] = self.utils.get_text(
                        cardXPathsDict['offers']['Offer_D2S_Rate_SelectedOfferCard'])

                    cardNotSelected = str(s2dCardInfo['offer']['Offer_D2S_Rate_CardToBeSelected']).replace('SELECTEAZA',
                                                                                                           '')
                    cardSelected = str(s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard']).replace('SELECTAT', '')

                    if cardNotSelected == cardSelected:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'PASSED [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))
                    else:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'FAIL [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))

                    del s2dCardInfo['offer']['Offer_D2S_Rate_CardToBeSelected']
                    del s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard']

                    for key, value in s2dCardInfo['offer'].items():

                        if key == 'Oferta_D2S_Rate_PerioadaContractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        # elif key =='Oferta_S2D_Rate_Perioada_Contractuala':
                        #     value = value.split('\n')[0].replace('x','')
                        print('offervals ', key)

                        if key == 'Oferta_D2S_Rate_InstallmentValue':
                            cartXpath = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div//section[@class="cart-items-section"]//*[contains(text(),'{}')]"""
                        else:
                            cartXpath = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]"""

                        print(cartXpath.format(value))

                        if self.utils.element_exists(cartXpath.format(value)):
                            try:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        else:
                            try:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'FAIL --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                    for key, value in s2dCardInfo['total'].items():
                        try:
                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                        value)):
                                print(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                        value))
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[3],
                                                                       'FAIL --> [{}]'.format(value))
                        except AttributeError as e:
                            print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                  '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                  '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 444 ', )

                    # self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    time.sleep(10)
                    isStrikeThrough = self.utils.element_exists(
                        """//div[@class="cart-menu-wrap"]//*[@class="cart-strikethrough"]""")
                    print('isStrikeThrough', isStrikeThrough)
                    if isStrikeThrough == False:
                        self.utils.log_customStep('Niciun element cu StrikeThrough Identificat in Cart', 'PASSED')
                    else:
                        self.utils.log_customStep('Niciun element cu StrikeThrough Identificat in Cart', 'FAIL')

                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                elif self.scenarioName_PC == 'OferteConfigurabile_D2S_SimOnly':
                    print('sceriu sd')

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {
                        'offers': {
                            'Offer_D2S_Rate_CardToBeSelected': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]""",
                            'Offer_D2S_Rate_SelectedOfferCard': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//button[contains(@class,'general_btn button-selected')]//ancestor::div[@class="card mobile-card"][1]""",
                            'Oferta_D2S_Rate_NumeOferta': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                            'Oferta_D2S_Rate_PerioadaContractuala': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                            'Oferta_D2S_Rate_oPrice': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
                            # 'Oferta_D2S_Rate_LastInstallment': '//div[@class="white_tooltip box_tooltip  visible-tooltip"]/div[last()]',
                            # 'Oferta_D2S_Rate_InstallmentValue': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//span[@class="txt_right_side after_discount"]""",
                        },

                        'device': {
                            'Device_D2S_Rate_CardToBeSelected': '//div[@class="card_wrap card-wrap-device device-offer "]',
                            'Device_D2S_Rate_SelectedCard': """//div[@class="card_wrap card-wrap-device device-offer "]//span[contains(text(),'SELECTAT')]//ancestor::div[@class="card_wrap card-wrap-device device-offer "][1]""",
                            'Device_D2S_Rate_Name': '//div[@class="card_wrap card-wrap-device device-offer "]//div[@class="model_phone"]',
                            'Device_D2S_Rate_Upfront': '//div[@class="card_wrap card-wrap-device device-offer "]//span[@class="price"]',
                        },

                        'total': {
                            'Total_D2S_Rate_TotalLunar': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
                            'Device_D2S_Rate_Avans': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//span[@class="price"]""",
                        }}

                    time.sleep(5)
                    for key, val in cardXPathsDict['device'].items():
                        if key != 'Device_D2S_Rate_SelectedCard':
                            s2dCardInfo['device'][key] = self.utils.get_text(val)

                    # Selecteaza DEVICE
                    self.utils.click_js(self.pageConfigDict['Device_D2S_Rate_Selecteaza'])
                    self.utils.wait_loadingCircle()

                    self.checkMinimBanners(expandElement='Device_D2S')

                    for key, value in s2dCardInfo['device'].items():
                        if key != 'Device_D2S_Rate_CardToBeSelected':
                            if key == 'Device_D2S_Rate_RateMultiple':
                                try:
                                    value = value.split('\n')[0].replace('x', '')
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):

                                    print(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value))
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[3],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[3],
                                        'FAIL --> [{}]'.format(value))

                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                    # time.sleep(25)
                    # self.checkMinimBanners('Device_D2S')
                    s2dCardInfo['device']['Device_D2S_Rate_SelectedCard'] = self.utils.get_text(
                        cardXPathsDict['device']['Device_D2S_Rate_SelectedCard'])

                    # time.sleep(10)
                    self.utils.click_js(
                        """//div[@class="fixed-bundle-offers"][2]//div[@data-automation-id="open-filter"]""")

                    self.utils.type_js('//input[@name="searchInput"]', offerName['simonly'])

                    self.utils.click_js("//button[@class='primary bold']")
                    self.utils.wait_loadingCircle()
                    time.sleep(20)
                    # self.utils.click_js(self.pageConfigDict['Oferta_D2S_SimOnly_RateMultiple'], customWait=20)
                    # self.utils.wait_loadingCircle()
                    # self.utils.click_js(self.pageConfigDict['Oferta_D2S_SimOnly_LastInstallment'])
                    # self.utils.wait_loadingCircle()

                    ##GET OFFER DETAILS
                    for key, val in cardXPathsDict['offers'].items():
                        if key != 'Offer_D2S_Rate_SelectedOfferCard':
                            s2dCardInfo['offer'][key] = self.utils.get_text(val)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_D2S_SimOnly_Select'])
                    self.utils.wait_loadingCircle()
                    cardNotSelected = str(s2dCardInfo['device']['Device_D2S_Rate_CardToBeSelected']).replace(
                            'SELECTEAZĂ', '')
                    cardSelected = str(s2dCardInfo['device']['Device_D2S_Rate_SelectedCard']).replace('SELECTAT', '')

                    if cardNotSelected == cardSelected:
                        self.utils.log_customStep('Selected Device_Card values VS Device_Card before select values',
                                                  'PASSED [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))
                    else:
                        self.utils.log_customStep('Selected Device_Card values VS Device_Card before select values',
                                                  'FAIL [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected)
                                                  )

                    del s2dCardInfo['device']['Device_D2S_Rate_CardToBeSelected']
                    del s2dCardInfo['device']['Device_D2S_Rate_SelectedCard']

                    self.checkMinimBanners(expandElement='Servicii_D2S')
                    s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard'] = self.utils.get_text(
                        cardXPathsDict['offers']['Offer_D2S_Rate_SelectedOfferCard'])

                    cardNotSelected = str(s2dCardInfo['offer']['Offer_D2S_Rate_CardToBeSelected']).replace('SELECTEAZA',
                                                                                                           '')
                    cardSelected = str(s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard']).replace('SELECTAT', '')

                    if cardNotSelected == cardSelected:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'PASSED [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))
                    else:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'FAIL [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))

                    del s2dCardInfo['offer']['Offer_D2S_Rate_CardToBeSelected']
                    del s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard']

                    for key, value in s2dCardInfo['offer'].items():

                        if key == 'Oferta_D2S_Rate_PerioadaContractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        # elif key =='Oferta_S2D_Rate_Perioada_Contractuala':
                        #     value = value.split('\n')[0].replace('x','')

                        if key == 'Oferta_D2S_Rate_InstallmentValue':
                            cartXpath = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div//section[@class="cart-items-section"]//*[contains(text(),'{}')]"""
                        else:
                            cartXpath = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]"""

                        if self.utils.element_exists(cartXpath.format(value)):
                            try:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                        else:
                            try:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'FAIL --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                    for key, value in s2dCardInfo['total'].items():
                        try:
                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                            value)):
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[3],
                                                                       'FAIL --> [{}]'.format(value))
                        except AttributeError as e:
                            print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                  '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                  '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 444 ', )

                    # self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    # time.sleep(10)
                    isStrikeThrough = self.utils.element_exists(
                        """//div[@class="cart-menu-wrap"]//*[@class="cart-strikethrough"]""")
                    if isStrikeThrough == False:
                        self.utils.log_customStep('Niciun element cu StrikeThrough Identificat in Cart', 'PASSED')
                    else:
                        self.utils.log_customStep('Niciun element cu StrikeThrough Identificat in Cart', 'FAIL')

                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                elif self.scenarioName_PC == 'OferteConfigurabile_D2S_SimOnly_si_Discount':
                    print('sceriu sd')

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {
                        'offers': {
                            'Offer_D2S_Rate_CardToBeSelected': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]""",
                            'Offer_D2S_Rate_SelectedOfferCard': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//button[contains(@class,'general_btn button-selected')]//ancestor::div[@class="card mobile-card"][1]""",
                            'Oferta_D2S_Rate_NumeOferta': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                            'Oferta_D2S_Rate_PerioadaContractuala': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                            'Oferta_D2S_Rate_oPrice': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
                            # 'Oferta_D2S_Rate_LastInstallment': '//div[@class="white_tooltip box_tooltip  visible-tooltip"]/div[last()]',
                            # 'Oferta_D2S_Rate_InstallmentValue': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//span[@class="txt_right_side after_discount"]""",
                        },

                        'device': {
                            'Device_D2S_Rate_CardToBeSelected': '//div[@class="card_wrap card-wrap-device device-offer "]',
                            'Device_D2S_Rate_SelectedCard': """//div[@class="card_wrap card-wrap-device device-offer "]//span[contains(text(),'SELECTAT')]//ancestor::div[@class="card_wrap card-wrap-device device-offer "][1]""",
                            'Device_D2S_Rate_Name': '//div[@class="card_wrap card-wrap-device device-offer "]//div[@class="model_phone"]',
                            'Device_D2S_Rate_Upfront': '//div[@class="card_wrap card-wrap-device device-offer "]//span[@class="price"]',
                        },

                        'total': {
                            'Total_D2S_Rate_TotalLunar': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
                            'Device_D2S_Rate_Avans': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//span[@class="price"]""",
                        }}

                    time.sleep(5)
                    for key, val in cardXPathsDict['device'].items():
                        if key != 'Device_D2S_Rate_SelectedCard':
                            s2dCardInfo['device'][key] = self.utils.get_text(val)

                    # Selecteaza DEVICE
                    self.utils.click_js(self.pageConfigDict['Device_D2S_Rate_Selecteaza'])
                    self.utils.wait_loadingCircle()

                    self.checkMinimBanners(expandElement='Device_D2S')

                    for key, value in s2dCardInfo['device'].items():
                        if key != 'Device_D2S_Rate_CardToBeSelected':
                            if key == 'Device_D2S_Rate_RateMultiple':
                                try:
                                    value = value.split('\n')[0].replace('x', '')
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):

                                    print(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value))
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[3],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[3],
                                        'FAIL --> [{}]'.format(value))

                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                    # time.sleep(25)
                    # self.checkMinimBanners('Device_D2S')
                    s2dCardInfo['device']['Device_D2S_Rate_SelectedCard'] = self.utils.get_text(
                        cardXPathsDict['device']['Device_D2S_Rate_SelectedCard'])

                    # time.sleep(10)
                    self.utils.click_js(
                        """//div[@class="fixed-bundle-offers"][2]//div[@data-automation-id="open-filter"]""")

                    self.utils.type_js('//input[@name="searchInput"]', offerName['discount'])

                    self.utils.click_js("//button[@class='primary bold']")
                    self.utils.wait_loadingCircle()
                    time.sleep(20)

                    ##GET OFFER DETAILS
                    for key, val in cardXPathsDict['offers'].items():
                        if key != 'Offer_D2S_Rate_SelectedOfferCard':
                            s2dCardInfo['offer'][key] = self.utils.get_text(val)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_D2S_SimOnlyDiscount_Select'])
                    self.utils.wait_loadingCircle()
                    cardNotSelected = str(s2dCardInfo['device']['Device_D2S_Rate_CardToBeSelected']).replace(
                        'SELECTEAZĂ', '')
                    cardSelected = str(s2dCardInfo['device']['Device_D2S_Rate_SelectedCard']).replace('SELECTAT', '')

                    if cardNotSelected == cardSelected:
                        self.utils.log_customStep('Selected Device_Card values VS Device_Card before select values',
                                                  'PASSED [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))
                    else:
                        self.utils.log_customStep('Selected Device_Card values VS Device_Card before select values',
                                                  'FAIL [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected)
                                                  )

                    del s2dCardInfo['device']['Device_D2S_Rate_CardToBeSelected']
                    del s2dCardInfo['device']['Device_D2S_Rate_SelectedCard']

                    self.checkMinimBanners(expandElement='Servicii_D2S')
                    s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard'] = self.utils.get_text(
                        cardXPathsDict['offers']['Offer_D2S_Rate_SelectedOfferCard'])

                    cardNotSelected = str(s2dCardInfo['offer']['Offer_D2S_Rate_CardToBeSelected']).replace('SELECTEAZA',
                                                                                                           '')
                    cardSelected = str(s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard']).replace('SELECTAT', '')

                    if cardNotSelected == cardSelected:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'PASSED [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))
                    else:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'FAIL [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))

                    del s2dCardInfo['offer']['Offer_D2S_Rate_CardToBeSelected']
                    del s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard']

                    for key, value in s2dCardInfo['offer'].items():

                        if key == 'Oferta_D2S_Rate_PerioadaContractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        # elif key =='Oferta_S2D_Rate_Perioada_Contractuala':
                        #     value = value.split('\n')[0].replace('x','')

                        if key == 'Oferta_D2S_Rate_InstallmentValue':
                            cartXpath = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div//section[@class="cart-items-section"]//*[contains(text(),'{}')]"""
                        else:
                            cartXpath = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]"""

                        if self.utils.element_exists(cartXpath.format(value)):
                            try:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        else:
                            try:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'FAIL --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                    for key, value in s2dCardInfo['total'].items():
                        try:
                            if key == 'Total_D2S_Rate_TotalLunar':
                                print('THIS IS SHITTETETETETET')
                                try:
                                    print('asdasd123', value)

                                    value = value.split()[:-2]
                                    print('asdasd12223', value)

                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                                for i in value:
                                    if value.index(i) == 0:
                                        logTxt = 'ShoppingCart_TotalLunar_PriceAfterDiscount'
                                        var = """//section[@class="cart-total-items-section"]//*[@class="cart-price" and contains(text(),'{}')]""".format(
                                            i)
                                    else:
                                        logTxt = 'ShoppingCart_TotalLunar_StrikeThroughPrice'
                                        var = """//section[@class="cart-total-items-section"]//*[@class="cart-strikethrough" and contains(text(),'{}')]""".format(
                                            i)

                                    try:
                                        if self.utils.element_exists(var):
                                            self.utils.log_customStep(logTxt,
                                                                                   'PASSED --> [{}]'.format(i))
                                        else:
                                            self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                                    except AttributeError as e:
                                        print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                              '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                              '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                                if len(value) < 2:
                                    print('ShoppingCart_TotalLunar_StrikeThroughPrice',
                                          'FAIL --> ["STRIKETHROUGH VALUE NOT FOUND ON PAGE"]')

                            elif self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[3],
                                                                       'FAIL --> [{}]'.format(value))
                        except AttributeError as e:
                            print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                  '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                  '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 444 ', )

                    # self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    # time.sleep(10)
                    isStrikeThrough = self.utils.element_exists(
                        """//div[@class="cart-menu-wrap"]//*[@class="cart-strikethrough"]""")
                    if isStrikeThrough == False:
                        self.utils.log_customStep('Element cu StrikeThrough Identificat in Cart', 'FAIL')
                    else:
                        self.utils.log_customStep('Element cu StrikeThrough Identificat in Cart', 'PASSED')

                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                elif self.scenarioName_PC == 'OferteConfigurabile_D2S_Rate_si_Discount':
                    print('sceriu sd')

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {
                        'offers': {
                            # 'Oferta_D2S_Rate_RateMultiple': """//div[@class="card mobile-card"][position() > 3]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//span[@class="link-title blue bold"]""",
                            'Offer_D2S_Rate_CardToBeSelected': """//div[@class="card mobile-card"][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]""",
                            'Offer_D2S_Rate_SelectedOfferCard': """//div[@class="card mobile-card"][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//button[contains(@class,'general_btn button-selected')]//ancestor::div[@class="card mobile-card"][1]""",
                            'Oferta_D2S_Rate_NumeOferta': """//div[@class="card mobile-card"][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                            'Oferta_D2S_Rate_PerioadaContractuala': """//div[@class="card mobile-card"][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                            # 'Oferta_D2S_Rate_oPrice': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
                            # 'Oferta_D2S_Rate_LastInstallment': '//div[@class="white_tooltip box_tooltip  visible-tooltip"]/div[last()]',
                            'Oferta_D2S_Rate_InstallmentValue': """//div[@class="card mobile-card"][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//span[@class="txt_right_side after_discount"]""",
                        },
                        'device': {
                            'Device_D2S_Rate_CardToBeSelected': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 0]',
                            'Device_D2S_Rate_SelectedCard': """//div[@class="card_wrap card-wrap-device device-offer "]//span[contains(text(),'SELECTAT')]//ancestor::div[@class="card_wrap card-wrap-device device-offer "][1]""",
                            'Device_D2S_Rate_Name': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 0]//div[@class="model_phone"]',
                            'Device_D2S_Rate_Upfront': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 0]//span[@class="price"]',
                        },

                        'total': {
                            'Total_D2S_Rate_TotalLunar': """//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
                            'Device_D2S_Rate_Avans': """//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//span[@class="price"]""",
                        }}

                    time.sleep(5)
                    for key, val in cardXPathsDict['device'].items():

                        if key != 'Device_D2S_Rate_SelectedCard':
                            s2dCardInfo['device'][key] = self.utils.get_text(val)

                    # Selecteaza DEVICE
                    self.utils.click_js(self.pageConfigDict['Device_D2S_RateDiscount_Selecteaza'])
                    self.utils.wait_loadingCircle()

                    self.checkMinimBanners(expandElement='Device_D2S')

                    for key, value in s2dCardInfo['device'].items():
                        print(key)
                        if key != 'Device_D2S_Rate_CardToBeSelected':
                            if key == 'Device_D2S_34Rate_RateMultiple':
                                try:
                                    value = value.split('\n')[0].replace('x', '')
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):

                                    print(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value))
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[3],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[3],
                                        'FAIL --> [{}]'.format(value))

                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                    # time.sleep(25)
                    # self.checkMinimBanners('Device_D2S')
                    s2dCardInfo['device']['Device_D2S_Rate_SelectedCard'] = self.utils.get_text(
                        cardXPathsDict['device']['Device_D2S_Rate_SelectedCard'])

                    # time.sleep(10)
                    self.utils.click_js(
                        """//div[@class="fixed-bundle-offers"][2]//div[@data-automation-id="open-filter"]""")

                    self.utils.type_js('//input[@name="searchInput"]', offerName['discount'])

                    self.utils.click_js("//button[@class='primary bold']")
                    self.utils.wait_loadingCircle()
                    time.sleep(20)
                    self.utils.click_js(self.pageConfigDict['Oferta_D2S_RateDiscount_RateMultiple'],
                                                     customWait=20)
                    self.utils.wait_loadingCircle()
                    self.utils.click_js(self.pageConfigDict['Oferta_D2S_RateDiscount_LastInstallment'])
                    self.utils.wait_loadingCircle()

                    ##GET OFFER DETAILS
                    for key, val in cardXPathsDict['offers'].items():
                        if key != 'Offer_D2S_Rate_SelectedOfferCard':
                            print('the key is ', key)
                            s2dCardInfo['offer'][key] = self.utils.get_text(val)
                            print('s2ddict -- . ', s2dCardInfo)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_D2S_RateDiscount_Select'])
                    self.utils.wait_loadingCircle()
                    cardNotSelected = str(s2dCardInfo['device']['Device_D2S_Rate_CardToBeSelected']).replace(
                        'SELECTEAZĂ', '')
                    cardSelected = str(s2dCardInfo['device']['Device_D2S_Rate_SelectedCard']).replace('SELECTAT', '')

                    if cardNotSelected == cardSelected:
                        self.utils.log_customStep('Selected Device_Card values VS Device_Card before select values',
                                                  'PASSED [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))
                    else:
                        self.utils.log_customStep('Selected Device_Card values VS Device_Card before select values',
                                                  'FAIL [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected)
                                                  )

                    del s2dCardInfo['device']['Device_D2S_Rate_CardToBeSelected']
                    del s2dCardInfo['device']['Device_D2S_Rate_SelectedCard']

                    self.checkMinimBanners(expandElement='Servicii_D2S')
                    s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard'] = self.utils.get_text(
                        cardXPathsDict['offers']['Offer_D2S_Rate_SelectedOfferCard'])

                    cardNotSelected = str(s2dCardInfo['offer']['Offer_D2S_Rate_CardToBeSelected']).replace('SELECTEAZA',
                                                                                                           '')
                    cardSelected = str(s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard']).replace('SELECTAT', '')

                    if cardNotSelected == cardSelected:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'PASSED [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))
                    else:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'FAIL [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))

                    del s2dCardInfo['offer']['Offer_D2S_Rate_CardToBeSelected']
                    del s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard']
                    # del s2dCardInfo['offer']['Oferta_D2S_Rate_RateMultiple']

                    for key, value in s2dCardInfo['offer'].items():

                        if key == 'Oferta_D2S_Rate_PerioadaContractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        # elif key =='Oferta_S2D_Rate_Perioada_Contractuala':
                        #     value = value.split('\n')[0].replace('x','')
                        print('offervals ', key)

                        if key == 'Oferta_D2S_Rate_InstallmentValue':
                            cartXpath = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div//section[@class="cart-items-section"]//*[contains(text(),'{}')]"""
                        else:
                            cartXpath = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]"""

                        print(cartXpath.format(value))

                        if self.utils.element_exists(cartXpath.format(value)):
                            try:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                        else:
                            self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                   'FAIL --> [{}]'.format(value))

                    for key, value in s2dCardInfo['total'].items():
                        try:
                            if key == 'Total_D2S_Rate_TotalLunar':
                                try:

                                    value = value.split()[:-2]

                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                                for i in value:
                                    if value.index(i) == 0:
                                        logTxt = 'ShoppingCart_TotalLunar_PriceAfterDiscount'
                                        var = """//section[@class="cart-total-items-section"]//*[@class="cart-price" and contains(text(),'{}')]""".format(
                                            i)
                                    else:
                                        logTxt = 'ShoppingCart_TotalLunar_StrikeThroughPrice'
                                        var = """//section[@class="cart-total-items-section"]//*[@class="cart-strikethrough" and contains(text(),'{}')]""".format(
                                            i)

                                    try:
                                        if self.utils.element_exists(var):
                                            self.utils.log_customStep(logTxt,
                                                                                   'PASSED --> [{}]'.format(i))
                                        else:
                                            self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                                    except AttributeError as e:
                                        print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                              '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                              '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                                if len(value) < 2:
                                    print('ShoppingCart_TotalLunar_StrikeThroughPrice',
                                          'FAIL --> ["STRIKETHROUGH VALUE NOT FOUND ON PAGE"]')

                            elif self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                        value)):
                                print(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                        value))
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[3],
                                                                       'FAIL --> [{}]'.format(value))
                        except AttributeError as e:
                            print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                  '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                  '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 444 ', )

                    # self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    time.sleep(10)
                    isStrikeThrough = self.utils.element_exists(
                        """//div[@class="cart-menu-wrap"]//*[@class="cart-strikethrough"]""")
                    print('isStrikeThrough', isStrikeThrough)
                    if isStrikeThrough == False:
                        self.utils.log_customStep('Niciun element cu StrikeThrough Identificat in Cart', 'PASSED')
                    else:
                        self.utils.log_customStep('Niciun element cu StrikeThrough Identificat in Cart', 'FAIL')

                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                elif self.scenarioName_PC == 'OferteConfigurabile_D2S_3YRS_Rate_si_Discount':
                    print('sceriu sd')

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {
                        'offers': {
                            # 'Oferta_D2S_Rate_RateMultiple': """//div[@class="card mobile-card"][position() > 3]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//span[@class="link-title blue bold"]""",
                            'Offer_D2S_Rate_CardToBeSelected': """//div[@class="card mobile-card"][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]""",
                            'Offer_D2S_Rate_SelectedOfferCard': """//div[@class="card mobile-card"][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//button[contains(@class,'general_btn button-selected')]//ancestor::div[@class="card mobile-card"][1]""",
                            'Oferta_D2S_Rate_NumeOferta': """//div[@class="card mobile-card"][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                            'Oferta_D2S_Rate_PerioadaContractuala': """//div[@class="card mobile-card"][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                            # 'Oferta_D2S_Rate_oPrice': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
                            # 'Oferta_D2S_Rate_LastInstallment': '//div[@class="white_tooltip box_tooltip  visible-tooltip"]/div[last()]',
                            'Oferta_D2S_Rate_InstallmentValue': """//div[@class="card mobile-card"][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//span[@class="txt_right_side after_discount"]""",
                        },
                        'device': {
                            'Device_D2S_Rate_CardToBeSelected': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 0]',
                            'Device_D2S_Rate_SelectedCard': """//div[@class="card_wrap card-wrap-device device-offer "]//span[contains(text(),'SELECTAT')]//ancestor::div[@class="card_wrap card-wrap-device device-offer "][1]""",
                            'Device_D2S_Rate_Name': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 0]//div[@class="model_phone"]',
                            'Device_D2S_Rate_Upfront': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 0]//span[@class="price"]',
                        },

                        'total': {
                            'Total_D2S_Rate_TotalLunar': """//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
                            'Device_D2S_Rate_Avans': """//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//span[@class="price"]""",
                        }}

                    time.sleep(5)
                    for key, val in cardXPathsDict['device'].items():

                        if key != 'Device_D2S_Rate_SelectedCard':
                            s2dCardInfo['device'][key] = self.utils.get_text(val)

                    # Selecteaza DEVICE
                    self.utils.click_js(self.pageConfigDict['Device_D2S_RateDiscount_Selecteaza'])
                    self.utils.wait_loadingCircle()

                    self.checkMinimBanners(expandElement='Device_D2S')

                    for key, value in s2dCardInfo['device'].items():
                        print(key)
                        if key != 'Device_D2S_Rate_CardToBeSelected':
                            if key == 'Device_D2S_34Rate_RateMultiple':
                                try:
                                    value = value.split('\n')[0].replace('x', '')
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):

                                    print(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value))
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[3],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[3],
                                        'FAIL --> [{}]'.format(value))

                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                    # time.sleep(25)
                    # self.checkMinimBanners('Device_D2S')
                    s2dCardInfo['device']['Device_D2S_Rate_SelectedCard'] = self.utils.get_text(
                        cardXPathsDict['device']['Device_D2S_Rate_SelectedCard'])

                    # time.sleep(10)
                    self.utils.click_js(
                        """//div[@class="fixed-bundle-offers"][2]//div[@data-automation-id="open-filter"]""")

                    self.utils.type_js('//input[@name="searchInput"]', offerName['discount'])

                    self.utils.click_js("//button[@class='primary bold']")
                    self.utils.wait_loadingCircle()
                    time.sleep(20)
                    self.utils.click_js(self.pageConfigDict['Oferta_D2S_RateDiscount_RateMultiple'],
                                                     customWait=20)
                    self.utils.wait_loadingCircle()
                    self.utils.click_js(self.pageConfigDict['Oferta_D2S3YRS_RateDiscount_LastInstallment'])
                    self.utils.wait_loadingCircle()

                    ##GET OFFER DETAILSOferta_D2S_RateDiscount_LastInstallment
                    for key, val in cardXPathsDict['offers'].items():
                        if key != 'Offer_D2S_Rate_SelectedOfferCard':
                            print('the key is ', key)
                            s2dCardInfo['offer'][key] = self.utils.get_text(val)
                            print('s2ddict -- . ', s2dCardInfo)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_D2S_RateDiscount_Select'])
                    self.utils.wait_loadingCircle()
                    cardNotSelected = str(s2dCardInfo['device']['Device_D2S_Rate_CardToBeSelected']).replace(
                        'SELECTEAZĂ', '')
                    cardSelected = str(s2dCardInfo['device']['Device_D2S_Rate_SelectedCard']).replace('SELECTAT', '')

                    if cardNotSelected == cardSelected:
                        self.utils.log_customStep('Selected Device_Card values VS Device_Card before select values',
                                                  'PASSED [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))
                    else:
                        self.utils.log_customStep('Selected Device_Card values VS Device_Card before select values',
                                                  'FAIL [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected)
                                                  )

                    del s2dCardInfo['device']['Device_D2S_Rate_CardToBeSelected']
                    del s2dCardInfo['device']['Device_D2S_Rate_SelectedCard']

                    self.checkMinimBanners(expandElement='Servicii_D2S')
                    s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard'] = self.utils.get_text(
                        cardXPathsDict['offers']['Offer_D2S_Rate_SelectedOfferCard'])

                    cardNotSelected = str(s2dCardInfo['offer']['Offer_D2S_Rate_CardToBeSelected']).replace('SELECTEAZA',
                                                                                                           '')
                    cardSelected = str(s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard']).replace('SELECTAT', '')

                    if cardNotSelected == cardSelected:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'PASSED [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))
                    else:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'FAIL [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))

                    del s2dCardInfo['offer']['Offer_D2S_Rate_CardToBeSelected']
                    del s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard']
                    # del s2dCardInfo['offer']['Oferta_D2S_Rate_RateMultiple']

                    for key, value in s2dCardInfo['offer'].items():

                        if key == 'Oferta_D2S_Rate_PerioadaContractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        # elif key =='Oferta_S2D_Rate_Perioada_Contractuala':
                        #     value = value.split('\n')[0].replace('x','')
                        print('offervals ', key)

                        if key == 'Oferta_D2S_Rate_InstallmentValue':
                            cartXpath = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div//section[@class="cart-items-section"]//*[contains(text(),'{}')]"""
                        else:
                            cartXpath = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]"""

                        print(cartXpath.format(value))

                        if self.utils.element_exists(cartXpath.format(value)):
                            try:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                        else:
                            self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                   'FAIL --> [{}]'.format(value))

                    for key, value in s2dCardInfo['total'].items():
                        try:
                            if key == 'Total_D2S_Rate_TotalLunar':
                                try:

                                    value = value.split()[:-2]

                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                                for i in value:
                                    if value.index(i) == 0:
                                        logTxt = 'ShoppingCart_TotalLunar_PriceAfterDiscount'
                                        var = """//section[@class="cart-total-items-section"]//*[@class="cart-price" and contains(text(),'{}')]""".format(
                                            i)
                                    else:
                                        logTxt = 'ShoppingCart_TotalLunar_StrikeThroughPrice'
                                        var = """//section[@class="cart-total-items-section"]//*[@class="cart-strikethrough" and contains(text(),'{}')]""".format(
                                            i)

                                    try:
                                        if self.utils.element_exists(var):
                                            self.utils.log_customStep(logTxt,
                                                                                   'PASSED --> [{}]'.format(i))
                                        else:
                                            self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                                    except AttributeError as e:
                                        print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                              '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                              '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                                if len(value) < 2:
                                    print('ShoppingCart_TotalLunar_StrikeThroughPrice',
                                          'FAIL --> ["STRIKETHROUGH VALUE NOT FOUND ON PAGE"]')

                            elif self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                        value)):
                                print(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                        value))
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[3],
                                                                       'FAIL --> [{}]'.format(value))
                        except AttributeError as e:
                            print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                  '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                  '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 444 ', )

                    # self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    time.sleep(10)
                    isStrikeThrough = self.utils.element_exists(
                        """//div[@class="cart-menu-wrap"]//*[@class="cart-strikethrough"]""")
                    print('isStrikeThrough', isStrikeThrough)
                    if isStrikeThrough == False:
                        self.utils.log_customStep('Niciun element cu StrikeThrough Identificat in Cart', 'PASSED')
                    else:
                        self.utils.log_customStep('Niciun element cu StrikeThrough Identificat in Cart', 'FAIL')

                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                elif self.scenarioName_PC == 'OferteConfigurabile_S2D_Rate':

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {'offers': {
                        'Oferta_S2D_Rate_NumeOferta': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                        'Oferta_S2D_Rate_PerioadaContractuala': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                        'Oferta_S2D_Rate_oPrice': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
                    },
                        'device': {'Device_Rate_RateMultiple': '//span[@class="link-title blue bold"]',
                                   'Device_Rate_Name': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="model_phone"]',
                                   'Device_Rate_Upfront': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                                   'Device_Rate_InstallmentValue': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify two_sides undefined"]//span[@class="txt_right_side after_discount"]',
                                   'Device_Rate_Stock': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify final_p"]/span[2]',
                                   },
                        'total': {
                            'Total_S2D_Rate_TotalLunar': """//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="price total-device-price"]/h4""",
                            'Device_Rate_Avans': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                        }}

                    self.utils.click_js(
                        """//div[@class="fixed-bundle-offers"][1]//div[@class='accordion_head']//div[@class="accordion_head_button"]""")

                    self.utils.type_js('//input[@name="searchInput"]', offerName['rate'])

                    self.utils.click_js("//button[@class='primary bold']")
                    self.utils.wait_loadingCircle()

                    for key, val in cardXPathsDict['offers'].items():
                        s2dCardInfo['offer'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_S2D_Rate_Select'])
                    self.utils.wait_loadingCircle()

                    self.utils.click_js(self.pageConfigDict['Device_Rate_RateMultiple'], customWait=20)
                    self.utils.wait_loadingCircle()
                    self.utils.click_js(self.pageConfigDict['Device_Rate_LastInstallment'])
                    self.utils.wait_loadingCircle()

                    for key, val in cardXPathsDict['device'].items():
                        s2dCardInfo['device'][key] = self.utils.get_text(val)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Device_Rate_Selecteaza'])
                    self.utils.wait_loadingCircle()

                    self.checkMinimBanners(expandElement='Servicii')

                    self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    for key, value in s2dCardInfo['offer'].items():
                        if key == 'Oferta_S2D_Rate_PerioadaContractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        # elif key =='Oferta_S2D_Rate_Perioada_Contractuala':
                        #     value = value.split('\n')[0].replace('x','')
                        print('offervals ', value)
                        print(
                            """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                value))

                        if self.utils.element_exists(
                                """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                            try:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                        else:
                            try:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'FAIL --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                    self.checkMinimBanners(expandElement='Device')

                    for key, value in s2dCardInfo['device'].items():
                        if key != 'Device_Rate_Stock':
                            if key == 'Device_Rate_RateMultiple':
                                try:
                                    value = value.split('\n')[0].replace('x', '')
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                                value)):

                                    print(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value))
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'FAIL --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                        else:
                            pass

                    for key, value in s2dCardInfo['total'].items():
                        try:
                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                            value)):
                                print(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                        value))
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[2],
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[2],
                                                                       'FAIL --> [{}]'.format(value))
                        except AttributeError as e:
                            print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                  '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                  '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 444 ', )

                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                elif self.scenarioName_PC == 'OferteConfigurabile_S2D_3YRS_Rate':

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {'offers': {
                        'Oferta_S2D_Rate_NumeOferta': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                        'Oferta_S2D_Rate_PerioadaContractuala': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                        'Oferta_S2D_Rate_oPrice': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
                    },
                        'device': {'Device_Rate_RateMultiple': '//span[@class="link-title blue bold"]',
                                   'Device_Rate_Name': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="model_phone"]',
                                   'Device_Rate_Upfront': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                                   'Device_Rate_InstallmentValue': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify two_sides undefined"]//span[@class="txt_right_side after_discount"]',
                                   'Device_Rate_Stock': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify final_p"]/span[2]',
                                   },
                        'total': {
                            'Total_S2D_Rate_TotalLunar': """//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="price total-device-price"]/h4""",
                            'Device_Rate_Avans': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                        }}

                    self.utils.click_js(
                        """//div[@class="fixed-bundle-offers"][1]//div[@class='accordion_head']//div[@class="accordion_head_button"]""")

                    self.utils.type_js('//input[@name="searchInput"]', offerName['rate'])

                    self.utils.click_js("//button[@class='primary bold']")
                    self.utils.wait_loadingCircle()

                    for key, val in cardXPathsDict['offers'].items():
                        s2dCardInfo['offer'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_S2D_Rate_Select'])
                    self.utils.wait_loadingCircle()

                    self.utils.click_js(self.pageConfigDict['Device_Rate_RateMultiple'], customWait=20)
                    self.utils.wait_loadingCircle()
                    self.utils.click_js(self.pageConfigDict['Device_Rate3YRS_LastInstallment'])
                    self.utils.wait_loadingCircle()

                    for key, val in cardXPathsDict['device'].items():
                        s2dCardInfo['device'][key] = self.utils.get_text(val)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Device_Rate_Selecteaza'])
                    self.utils.wait_loadingCircle()

                    self.checkMinimBanners(expandElement='Servicii')

                    self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    for key, value in s2dCardInfo['offer'].items():
                        if key == 'Oferta_S2D_Rate_PerioadaContractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        # elif key =='Oferta_S2D_Rate_Perioada_Contractuala':
                        #     value = value.split('\n')[0].replace('x','')
                        print('offervals ', value)
                        print(
                            """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                value))

                        if self.utils.element_exists(
                                """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                            try:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                        else:
                            try:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'FAIL --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                    self.checkMinimBanners(expandElement='Device')

                    for key, value in s2dCardInfo['device'].items():
                        if key != 'Device_Rate_Stock':
                            if key == 'Device_Rate_RateMultiple':
                                try:
                                    value = value.split('\n')[0].replace('x', '')
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                                value)):

                                    print(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value))
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'FAIL --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                        else:
                            pass

                    for key, value in s2dCardInfo['total'].items():
                        try:
                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                            value)):
                                print(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                        value))
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[2],
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[2],
                                                                       'FAIL --> [{}]'.format(value))
                        except AttributeError as e:
                            print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                  '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                  '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 444 ', )

                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                elif self.scenarioName_PC == 'OferteConfigurabile_S2D_Rate_si_Discount':

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {
                        'offers': {
                            'Offer_S2D_Rate_CardToBeSelected': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]""",
                            'Offer_S2D_Rate_SelectedOfferCard': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//button[contains(@class,'general_btn button-selected')]//ancestor::div[@class="card mobile-card"][1]""",
                            'Oferta_S2D_RateDiscount_Perioada_Contractuala': """//div[@class="card mobile-card"]//*[contains(text(),'rate')]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                            'Oferta_S2D_RateDiscount_NumeOferta': """//div[@class="card mobile-card"]//*[contains(text(),'rate')]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                            'Oferta_S2D_RateDiscount_oPrice': """//div[@class="card mobile-card"]//*[contains(text(),'rate')]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
                        },

                        'device': {
                            'Device_Rate_RateMultiple': '//span[@class="link-title blue bold"]',
                            # 'Device_Rate_LastInstallment': '//div[@class="white_tooltip box_tooltip  visible-tooltip"]/div[last()]',
                            'Device_Rate_Name': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="model_phone"]',
                            'Device_Rate_Upfront': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                            'Device_Rate_InstallmentValue': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify two_sides undefined"]//span[@class="txt_right_side after_discount"]',
                        },

                        'total': {
                            '_OfferPriceDiscount': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="Overview with_tooltip"]//span[@class="txt_right_side after_discount"]',
                            'Total_SimOnly_TotalLunar': """//div[@class="card-footer card-footer-button"]//div[@class="price total-device-price"]""",
                            'Device_RateDisc_Avans': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                        }}

                    self.utils.click_js(
                        """//div[@class="fixed-bundle-offers"][1]//div[@class='accordion_head']//div[@class="accordion_head_button"]""")
                    self.utils.type_js('//input[@name="searchInput"]', offerName['discount'])
                    self.utils.click_js("//button[@class='primary bold']")
                    self.utils.wait_loadingCircle()

                    for key, val in cardXPathsDict['offers'].items():
                        if key != "Offer_S2D_Rate_SelectedOfferCard":
                            s2dCardInfo['offer'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_S2D_RateDiscount_Selecteaza'])
                    self.utils.wait_loadingCircle()
                    # time.sleep(10)
                    self.utils.click_js(self.pageConfigDict['Device_Rate_RateMultiple'], customWait=20)
                    self.utils.wait_loadingCircle()
                    self.utils.click_js(self.pageConfigDict['Device_Rate_LastInstallment'])
                    self.utils.wait_loadingCircle()

                    for key, val in cardXPathsDict['device'].items():
                        print('this is val ', val)
                        s2dCardInfo['device'][key] = self.utils.get_text(val)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Device_Rate_Selecteaza'])
                    self.utils.wait_loadingCircle()

                    self.checkMinimBanners(expandElement='Servicii')

                    s2dCardInfo['offer']['Offer_S2D_Rate_SelectedOfferCard'] = self.utils.get_text(
                        cardXPathsDict['offers']['Offer_S2D_Rate_SelectedOfferCard'])

                    self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    if str(s2dCardInfo['offer']['Offer_S2D_Rate_CardToBeSelected']).replace('SELECTEAZA', '') == str(
                            s2dCardInfo['offer']['Offer_S2D_Rate_SelectedOfferCard']).replace('SELECTAT', ''):
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'PASSED')
                    else:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'FAIL')

                    del s2dCardInfo['offer']['Offer_S2D_Rate_CardToBeSelected']
                    del s2dCardInfo['offer']['Offer_S2D_Rate_SelectedOfferCard']

                    for key, value in s2dCardInfo['offer'].items():
                        if key == 'Oferta_S2D_RateDiscount_Perioada_Contractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        elif key == 'Oferta_S2D_RateDiscount_oPrice':
                            try:
                                value = value.split()[:-2]
                                for i in value:
                                    if value.index(i) == 0:
                                        logTxt = 'ShoppingCart_OfferInfo_PriceAfterDiscount'
                                    else:
                                        logTxt = 'ShoppingCart_OfferInfo_StrikeThroughPrice'

                                    if self.utils.element_exists(
                                            """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                                i)):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                            except (AttributeError, ValueError) as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ')
                            except TypeError:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_Price',
                                                                       'FAIL --> [Offer Price nu a fost identificat pe Cartonas]')

                        elif self.utils.element_exists(
                                """//section[@class="cart-items-section"][1]//*[contains(text(),"{}")]""".format(
                                    value)):
                            print("""//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                value))
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'PASSED --> [{}]'.format(value))
                        else:
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'FAIL --> [{}]'.format(value))

                        if self.utils.element_exists(
                                """//div[@class="item-wrap-prices "][1]//div[@class="cart-price cart-no-value" and contains(text(), '- - -')]""",
                                enable_Logging=False):
                            self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans', 'PASSED --> [---]')
                        else:
                            self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans',
                                                                   'FAIL --> [{}]'.format(
                                                                       self.utils.get_text(
                                                                           """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]/div[@class="items-wrap flex"]//div[@class="item-wrap-prices "][1]""")))

                    self.checkMinimBanners(expandElement='Device')

                    for key, value in s2dCardInfo['device'].items():
                        if key == 'Device_Rate_RateMultiple':
                            try:
                                value = value.split('\n')[0].replace('x', '')
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):

                                    print(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value))
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'FAIL --> [{}]'.format(value))

                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        elif key == 'Device_SimOnly_Discount':
                            try:
                                print('we here')

                                value = value.split()
                            except AttributeError as e:
                                print('SimOnly si discount Exceptie:\nAttribute error exception for : ', value,
                                      "EXCEPTION --> ", e)

                            print('val pe scenariu este', value)
                            for i in value:
                                if value.index(i) == 0:
                                    logTxt = 'ShoppingCart_DeviceInfoOffer_PriceAfterDiscount'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-price" and contains(text(),'{}')]""".format(
                                        i)
                                else:
                                    logTxt = 'ShoppingCart_DeviceInfoOffer_StrikeThroughPrice'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-strikethrough" and contains(text(),'{}')]""".format(
                                        i)

                                try:
                                    if self.utils.element_exists(var):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                          '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                          '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )
                        else:

                            try:
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                                value)):

                                    print(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value))
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'FAIL --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                    for key, value in s2dCardInfo['total'].items():

                        if key == 'Device_RateDisc_Avans':
                            value = value.replace('€0', '€')
                            time.sleep(0.5)
                            print('total check is',
                                  """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                      value))
                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'FAIL --> [{}]'.format(value))

                        elif key == 'Total_SimOnly_TotalLunar':
                            try:
                                value = value.split()[:-2]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                            for i in value:
                                if value.index(i) == 0:
                                    logTxt = 'ShoppingCart_TotalLunar_PriceAfterDiscount'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-price" and contains(text(),'{}')]""".format(
                                        i)
                                else:
                                    logTxt = 'ShoppingCart_TotalLunar_StrikeThroughPrice'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-strikethrough" and contains(text(),'{}')]""".format(
                                        i)

                                try:
                                    if self.utils.element_exists(var):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                          '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                          '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                        elif key == '_OfferPriceDiscount':
                            try:
                                value = value.split()
                                for i in value:
                                    if value.index(i) == 0:
                                        var = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[@class="cart-price" and contains(text(),'{}')]""".format(
                                            i)
                                        logTxt = 'ShoppingCart_DeviceInfoOffer_PriceAfterDiscount'

                                    else:
                                        var = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[@class="cart-strikethrough" and contains(text(),'{}')]""".format(
                                            i)
                                        logTxt = 'ShoppingCart_DeviceInfoOffer_StrikeThroughPrice'

                                    if self.utils.element_exists(var):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))

                            except (AttributeError, ValueError) as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY')
                            except TypeError:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_Price',
                                                                       'FAIL --> [Offer Price nu a fost identificat pe Cartonas]')

                        else:

                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'FAIL --> [{}]'.format(value))

                        # print('s2dcardinfo', key, ' ', value)
                        # print(
                        #     """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                        #         value))

                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                elif self.scenarioName_PC == 'OferteConfigurabile_S2D_Rate_si_Discount':

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {
                        'offers': {
                            'Offer_S2D_Rate_CardToBeSelected': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]""",
                            'Offer_S2D_Rate_SelectedOfferCard': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//button[contains(@class,'general_btn button-selected')]//ancestor::div[@class="card mobile-card"][1]""",
                            'Oferta_S2D_RateDiscount_Perioada_Contractuala': """//div[@class="card mobile-card"]//*[contains(text(),'rate')]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                            'Oferta_S2D_RateDiscount_NumeOferta': """//div[@class="card mobile-card"]//*[contains(text(),'rate')]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                            'Oferta_S2D_RateDiscount_oPrice': """//div[@class="card mobile-card"]//*[contains(text(),'rate')]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
                        },

                        'device': {
                            'Device_Rate_RateMultiple': '//span[@class="link-title blue bold"]',
                            # 'Device_Rate_LastInstallment': '//div[@class="white_tooltip box_tooltip  visible-tooltip"]/div[last()]',
                            'Device_Rate_Name': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="model_phone"]',
                            'Device_Rate_Upfront': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                            'Device_Rate_InstallmentValue': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify two_sides undefined"]//span[@class="txt_right_side after_discount"]',
                        },

                        'total': {
                            '_OfferPriceDiscount': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="Overview with_tooltip"]//span[@class="txt_right_side after_discount"]',
                            'Total_SimOnly_TotalLunar': """//div[@class="card-footer card-footer-button"]//div[@class="price total-device-price"]""",
                            'Device_RateDisc_Avans': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                        }}

                    self.utils.click_js(
                        """//div[@class="fixed-bundle-offers"][1]//div[@class='accordion_head']//div[@class="accordion_head_button"]""")
                    self.utils.type_js('//input[@name="searchInput"]', offerName['discount'])
                    self.utils.click_js("//button[@class='primary bold']")
                    self.utils.wait_loadingCircle()

                    for key, val in cardXPathsDict['offers'].items():
                        if key != "Offer_S2D_Rate_SelectedOfferCard":
                            s2dCardInfo['offer'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_S2D_RateDiscount_Selecteaza'])
                    self.utils.wait_loadingCircle()
                    # time.sleep(10)
                    self.utils.click_js(self.pageConfigDict['Device_Rate_RateMultiple'], customWait=20)
                    self.utils.wait_loadingCircle()
                    self.utils.click_js(self.pageConfigDict['Device_Rate3YRS_LastInstallment'])
                    self.utils.wait_loadingCircle()

                    for key, val in cardXPathsDict['device'].items():
                        print('this is val ', val)
                        s2dCardInfo['device'][key] = self.utils.get_text(val)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Device_Rate_Selecteaza'])
                    self.utils.wait_loadingCircle()

                    self.checkMinimBanners(expandElement='Servicii')

                    s2dCardInfo['offer']['Offer_S2D_Rate_SelectedOfferCard'] = self.utils.get_text(
                        cardXPathsDict['offers']['Offer_S2D_Rate_SelectedOfferCard'])

                    self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    if str(s2dCardInfo['offer']['Offer_S2D_Rate_CardToBeSelected']).replace('SELECTEAZA', '') == str(
                            s2dCardInfo['offer']['Offer_S2D_Rate_SelectedOfferCard']).replace('SELECTAT', ''):
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'PASSED')
                    else:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'FAIL')

                    del s2dCardInfo['offer']['Offer_S2D_Rate_CardToBeSelected']
                    del s2dCardInfo['offer']['Offer_S2D_Rate_SelectedOfferCard']

                    for key, value in s2dCardInfo['offer'].items():
                        if key == 'Oferta_S2D_RateDiscount_Perioada_Contractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        elif key == 'Oferta_S2D_RateDiscount_oPrice':
                            try:
                                value = value.split()[:-2]
                                for i in value:
                                    if value.index(i) == 0:
                                        logTxt = 'ShoppingCart_OfferInfo_PriceAfterDiscount'
                                    else:
                                        logTxt = 'ShoppingCart_OfferInfo_StrikeThroughPrice'

                                    if self.utils.element_exists(
                                            """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                                i)):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                            except (AttributeError, ValueError) as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ')
                            except TypeError:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_Price',
                                                                       'FAIL --> [Offer Price nu a fost identificat pe Cartonas]')

                        elif self.utils.element_exists(
                                """//section[@class="cart-items-section"][1]//*[contains(text(),"{}")]""".format(
                                    value)):
                            print("""//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                value))
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'PASSED --> [{}]'.format(value))
                        else:
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'FAIL --> [{}]'.format(value))

                        if self.utils.element_exists(
                                """//div[@class="item-wrap-prices "][1]//div[@class="cart-price cart-no-value" and contains(text(), '- - -')]""",
                                enable_Logging=False):
                            self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans', 'PASSED --> [---]')
                        else:
                            self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans',
                                                                   'FAIL --> [{}]'.format(
                                                                       self.utils.get_text(
                                                                           """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]/div[@class="items-wrap flex"]//div[@class="item-wrap-prices "][1]""")))

                    self.checkMinimBanners(expandElement='Device')

                    for key, value in s2dCardInfo['device'].items():
                        if key == 'Device_Rate_RateMultiple':
                            try:
                                value = value.split('\n')[0].replace('x', '')
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):

                                    print(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value))
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'FAIL --> [{}]'.format(value))

                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        elif key == 'Device_SimOnly_Discount':
                            try:
                                print('we here')

                                value = value.split()
                            except AttributeError as e:
                                print('SimOnly si discount Exceptie:\nAttribute error exception for : ', value,
                                      "EXCEPTION --> ", e)

                            print('val pe scenariu este', value)
                            for i in value:
                                if value.index(i) == 0:
                                    logTxt = 'ShoppingCart_DeviceInfoOffer_PriceAfterDiscount'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-price" and contains(text(),'{}')]""".format(
                                        i)
                                else:
                                    logTxt = 'ShoppingCart_DeviceInfoOffer_StrikeThroughPrice'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-strikethrough" and contains(text(),'{}')]""".format(
                                        i)

                                try:
                                    if self.utils.element_exists(var):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                          '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                          '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )
                        else:

                            try:
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                                value)):

                                    print(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value))
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'FAIL --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                    for key, value in s2dCardInfo['total'].items():

                        if key == 'Device_RateDisc_Avans':
                            value = value.replace('€0', '€')
                            time.sleep(0.5)
                            print('total check is',
                                  """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                      value))
                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'FAIL --> [{}]'.format(value))

                        elif key == 'Total_SimOnly_TotalLunar':
                            try:
                                value = value.split()[:-2]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                            for i in value:
                                if value.index(i) == 0:
                                    logTxt = 'ShoppingCart_TotalLunar_PriceAfterDiscount'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-price" and contains(text(),'{}')]""".format(
                                        i)
                                else:
                                    logTxt = 'ShoppingCart_TotalLunar_StrikeThroughPrice'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-strikethrough" and contains(text(),'{}')]""".format(
                                        i)

                                try:
                                    if self.utils.element_exists(var):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                          '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                          '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                        elif key == '_OfferPriceDiscount':
                            try:
                                value = value.split()
                                for i in value:
                                    if value.index(i) == 0:
                                        var = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[@class="cart-price" and contains(text(),'{}')]""".format(
                                            i)
                                        logTxt = 'ShoppingCart_DeviceInfoOffer_PriceAfterDiscount'

                                    else:
                                        var = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[@class="cart-strikethrough" and contains(text(),'{}')]""".format(
                                            i)
                                        logTxt = 'ShoppingCart_DeviceInfoOffer_StrikeThroughPrice'

                                    if self.utils.element_exists(var):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))

                            except (AttributeError, ValueError) as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY')
                            except TypeError:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_Price',
                                                                       'FAIL --> [Offer Price nu a fost identificat pe Cartonas]')

                        else:

                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'FAIL --> [{}]'.format(value))

                        # print('s2dcardinfo', key, ' ', value)
                        # print(
                        #     """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                        #         value))

                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                elif self.scenarioName_PC == 'OferteConfigurabile_S2D_SimOnly_si_Discount':
                    print('sceriu sdonl123y')

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {
                        'offers': {
                            'Oferta_SimOnly_Discount_Perioada_Contractuala': """//div[@class="card mobile-card"]//*[(contains(text(),'SIM Only'))]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                            'Oferta_SimOnly_Discount_NumeOferta': """//div[@class="card mobile-card"]//*[(contains(text(),'SIM Only'))]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                            'Oferta_SimOnly_Discount_oPrice': """//div[@class="card mobile-card"]//*[(contains(text(),'SIM Only'))]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
                        },
                        'device': {
                            'Device_SimOnly_Name': '//div[@class="card_wrap card-wrap-device  "]//div[@class="model_phone"]',
                            'Device_SimOnly_Price': '//div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                            'Device_SimOnly_Discount': '//div[@class="card_wrap card-wrap-device  "]//span[@class="txt_right_side after_discount"]',
                            # 'Device_SimOnly_Stock': '//div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify final_p"]/span[2]',
                            # 'Device_Rate_OfferPriceDiscount': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="Overview with_tooltip"]//span[@class="txt_right_side after_discount"]',

                        },
                        'total': {
                            'Total_SimOnly_TotalLunar': """//div[@class="card-footer card-footer-button"]//div[@class="price total-device-price"]""",
                            'Device_SimOnly_Avans': '//div[@class="align-justify two_sides undefined"]//div[@class="tooltip_price_wrap"]',
                        }}

                    self.utils.click_js(
                        """//div[@class="fixed-bundle-offers"][1]//div[@class='accordion_head']//div[@class="accordion_head_button"]""")
                    self.utils.type_js('//input[@name="searchInput"]', offerName['discount'])
                    self.utils.click_js("//button[@class='primary bold']")
                    self.utils.wait_loadingCircle()

                    for key, val in cardXPathsDict['offers'].items():
                        s2dCardInfo['offers'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_SimOnly_Discount_Selecteaza'])
                    self.utils.wait_loadingCircle()

                    for key, value in cardXPathsDict['device'].items():
                        s2dCardInfo['device'][key] = self.utils.get_text(value)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    time.sleep(4)
                    self.utils.click_js(self.pageConfigDict['Device_SimOnly_Selecteaza'])
                    self.utils.wait_loadingCircle()

                    self.checkMinimBanners(expandElement='Servicii')

                    self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    for key, value in s2dCardInfo['offers'].items():
                        if key == 'Oferta_SimOnly_Discount_Perioada_Contractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        elif key == 'Oferta_SimOnly_Discount_oPrice':
                            try:
                                value = value.split()[:-2]
                                for i in value:
                                    if value.index(i) == 0:
                                        logTxt = 'ShoppingCart_OfferInfo_PriceAfterDiscount'
                                    else:
                                        logTxt = 'ShoppingCart_OfferInfo_StrikeThroughPrice'

                                    if self.utils.element_exists(
                                            """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                                i)):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))

                            except (AttributeError, ValueError) as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ')
                            except TypeError:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_Price',
                                                                       'FAIL --> [Offer Price nu a fost identificat pe Cartonas]')

                        elif self.utils.element_exists(
                                """//section[@class="cart-items-section"][1]//*[contains(text(),"{}")]""".format(
                                    value)):
                            print("""//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                value))
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'PASSED --> [{}]'.format(value))
                        else:
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'FAIL --> [{}]'.format(value))

                    if self.utils.element_exists(
                            """//div[@class="item-wrap-prices "][1]//div[@class="cart-price cart-no-value" and contains(text(), '- - -')]""",
                            enable_Logging=False):
                        self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans', 'PASSED --> [---]')
                    else:
                        self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans', 'FAIL --> [{}]'.format(
                            self.utils.get_text(
                                """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]/div[@class="items-wrap flex"]//div[@class="item-wrap-prices "][1]""")))

                    self.checkMinimBanners(expandElement='Device')

                    for key, value in s2dCardInfo['device'].items():
                        print('key is ', key, 'the val is ', value)
                        if key == 'Device_Rate_multiple':
                            try:
                                value = value.split('\n')[0].replace('x', '')
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'FAIL --> [{}]'.format(value))

                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        elif key == 'Device_SimOnly_Discount':
                            try:
                                print('we here')

                                value = value.split()
                            except AttributeError as e:
                                print('SimOnly si discount Exceptie:\nAttribute error exception for : ', value,
                                      "EXCEPTION --> ", e)

                            print('val pe scenariu este', value)
                            for i in value:
                                if value.index(i) == 0:
                                    logTxt = 'ShoppingCart_DeviceInfoOffer_PriceAfterDiscount'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-price" and contains(text(),'{}')]""".format(
                                        i)
                                else:
                                    logTxt = 'ShoppingCart_DeviceInfoOffer_StrikeThroughPrice'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-strikethrough" and contains(text(),'{}')]""".format(
                                        i)

                                try:
                                    if self.utils.element_exists(var):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                          '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                          '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                        else:
                            try:
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'FAIL --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                    for key, value in s2dCardInfo['total'].items():
                        if key == 'Device_SimOnly_Avans':
                            value = value.replace('€0', '€')

                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'FAIL --> [{}]'.format(value))

                        elif key == 'Total_SimOnly_TotalLunar':
                            try:

                                value = value.split()[:-2]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                            for i in value:
                                if value.index(i) == 0:
                                    logTxt = 'ShoppingCart_TotalLunar_PriceAfterDiscount'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-price" and contains(text(),'{}')]""".format(
                                        i)
                                else:
                                    logTxt = 'ShoppingCart_TotalLunar_StrikeThroughPrice'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-strikethrough" and contains(text(),'{}')]""".format(
                                        i)

                                try:
                                    if self.utils.element_exists(var):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                          '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                          '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                        else:

                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'FAIL --> [{}]'.format(value))

                        # print('s2dcardinfo', key, ' ', value)
                        # print(
                        #     """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                        #         value))

                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                elif self.scenarioName_PC == 'OferteConfigurabile_S2D_SimOnly':
                    print('sceriu sdonly')

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {
                        'offers': {
                            'Oferta_SimOnly_Perioada_Contractuala': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                            'Oferta_SimOnly_NumeOferta': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                            'Oferta_SimOnly_oPrice': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
                        },
                        'device': {
                            'Device_SimOnly_Name': '//div[@class="card_wrap card-wrap-device  "]//div[@class="model_phone"]',
                            'Device_SimOnly_Price': '//div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                            # 'Device_SimOnly_Discount': '//div[@class="card_wrap card-wrap-device  "]//span[@class="txt_right_side after_discount"]',
                            'Device_SimOnly_Stock': '//div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify final_p"]/span[2]',
                            # 'Device_SimOnly_Selecteaza': 'div[@class="card_wrap card-wrap-device  "]//button[@class="general_btn "]',
                        },
                        'total': {
                            'Total_SimOnly_TotalLunar': """//div[@class="card-footer card-footer-button"]//div[@class="price total-device-price"]""",
                            'Device_SimOnly_Avans': '//div[@class="align-justify two_sides undefined"]//div[@class="tooltip_price_wrap"]',
                        }}

                    self.utils.click_js(
                        """//div[@class="fixed-bundle-offers"][1]//div[@class='accordion_head']//div[@class="accordion_head_button"]""")
                    self.utils.type_js('//input[@name="searchInput"]', offerName['simonly'])
                    self.utils.click_js("//button[@class='primary bold']")
                    self.utils.wait_loadingCircle()

                    for key, val in cardXPathsDict['offers'].items():
                        s2dCardInfo['offers'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_SimOnly_Selecteaza'])
                    self.utils.wait_loadingCircle()

                    for key, val in cardXPathsDict['device'].items():
                        # print(val, ' VALIS ' ,a)
                        s2dCardInfo['device'][key] = self.utils.get_text(val)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    # 'Device_SimOnly_Name': 'div[@class="card_wrap card-wrap-device  "]//div[@class="model_phone"]',
                    # 'Device_SimOnly_Price': 'div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                    # 'Device_SimOnly_Discount': 'div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify two_sides undefined"]//span[@class="txt_right_side after_discount"]',
                    # 'Device_SimOnly_Stock': 'div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify final_p"]/span[2]',
                    # 'Device_SimOnly_Selecteaza': 'div[@class="card_wrap card-wrap-device  "]//button[@class="general_btn "]',

                    time.sleep(4)
                    self.utils.click_js(self.pageConfigDict['Device_SimOnly_Selecteaza'])
                    self.utils.wait_loadingCircle()

                    self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    # Total_SimOnly_TotalLunar
                    # ': """//div[@class="card-footer card-footer-button"]//div[@class="price total-device-price"]""",
                    # 'Device_SimOnly_Avans':

                    for key, value in s2dCardInfo['offers'].items():
                        if key == 'Oferta_SimOnly_Perioada_Contractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        elif key == 'Oferta_SimOnly_oPrice':
                            try:
                                value = value.split()[:-2]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                for i in value:
                                    logTxt = 'ShoppingCart_OfferInfo_Price'
                                    if self.utils.element_exists(
                                            """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                                i)):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ')
                            except TypeError:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_Price',
                                                                       'FAIL --> [Offer Price nu a fost identificat pe Cartonas]')

                        elif self.utils.element_exists(
                                """//section[@class="cart-items-section"][1]//*[contains(text(),"{}")]""".format(
                                    value)):
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'PASSED --> [{}]'.format(value))
                        else:
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'FAIL --> [{}]'.format(value))

                    if self.utils.element_exists(
                            """//div[@class="item-wrap-prices "][1]//div[@class="cart-price cart-no-value" and contains(text(), '- - -')]""",
                            enable_Logging=False):
                        self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans', 'PASSED --> [---]')
                    else:
                        self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans', 'FAIL --> [{}]'.format(
                            self.utils.get_text(
                                """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]/div[@class="items-wrap flex"]//div[@class="item-wrap-prices "][1]""")))

                    self.checkMinimBanners(expandElement='Device')

                    for key, value in s2dCardInfo['device'].items():
                        if key != 'Device_SimOnly_Stock':
                            if key == 'Device_Rate_multiple':
                                try:
                                    value = value.split('\n')[0].replace('x', '')
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'FAIL --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 874')

                        else:
                            pass

                    for key, value in s2dCardInfo['total'].items():
                        if key == 'Device_SimOnly_Avans':
                            value = value.replace('€0', '€')

                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'FAIL --> [{}]'.format(value))

                        elif key == 'Total_SimOnly_TotalLunar':
                            try:
                                value = value.split()[:-2]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                for i in value:
                                    if value.index(i) == 0:
                                        logTxt = 'ShoppingCart_TotalLunar_Price'
                                    else:
                                        logTxt = 'ShoppingCart_TotalLunar_StrikeThroughPrice'

                                    if self.utils.element_exists(
                                            """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                                i)):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 1164 09032020 ')
                            except TypeError:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_Price',
                                                                       'FAIL --> [Offer Price nu a fost identificat pe Cartonas]')

                        else:

                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'FAIL --> [{}]'.format(value))

                        # print('s2dcardinfo', key, ' ', value)
                        # print(
                        #     """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                        #         value))

                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                    # s2dCardInfo['Oferta_S2D_Rate_Perioada_Contractuala'] = self.utils.get_text(
                    #     self.pageConfigDict['Oferta_S2D_Rate_Perioada_Contractuala'])
                    # s2dCardInfo['Oferta_S2D_Rate_Nume_Oferta'] = self.utils.get_text(
                    #     self.pageConfigDict['Oferta_S2D_Rate_Nume_Oferta'])
                    # s2dCardInfo['Oferta_S2D_Rate_oPrice'] = self.utils.get_text(
                    #     self.pageConfigDict['Oferta_S2D_Rate_oPrice'])
                    # self.utils.click_js(
                    #     self.pageConfigDict['Oferta_S2D_Rate_Select'])

                elif self.scenarioName_PC == 'OferteConfigurabile_ServiceOnly':
                    print('sceriu sdonly')

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {
                        'offers': {
                            'Oferta_SimOnly_Perioada_Contractuala': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                            'Oferta_SimOnly_NumeOferta': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                            'Oferta_SimOnly_oPrice': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
                        },
                        'device': {
                            'Device_SimOnly_Name': '//div[@class="card_wrap card-wrap-device  "]//div[@class="model_phone"]',
                            'Device_SimOnly_Price': '//div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                            # 'Device_SimOnly_Discount': '//div[@class="card_wrap card-wrap-device  "]//span[@class="txt_right_side after_discount"]',
                            'Device_SimOnly_Stock': '//div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify final_p"]/span[2]',
                            # 'Device_SimOnly_Selecteaza': 'div[@class="card_wrap card-wrap-device  "]//button[@class="general_btn "]',
                        },
                        'total': {
                            'Total_SimOnly_TotalLunar': """//div[@class="card mobile-card"]//*[(contains(text(),'SIM Only'))]//ancestor::div[@class="card mobile-card"]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
                            # 'Device_SimOnly_Avans': '//div[@class="align-justify two_sides undefined"]//div[@class="tooltip_price_wrap"]',
                        }}

                    self.utils.click_js(
                        """//div[@class="fixed-bundle-offers"][1]//div[@class='accordion_head']//div[@class="accordion_head_button"]""")
                    self.utils.type_js('//input[@name="searchInput"]', offerName['simonly'])
                    self.utils.click_js("//button[@class='primary bold']")
                    self.utils.wait_loadingCircle()

                    for key, val in cardXPathsDict['offers'].items():
                        s2dCardInfo['offers'][key] = self.utils.get_text(val)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_SimOnly_Selecteaza'])
                    self.utils.wait_loadingCircle()

                    self.checkMinimBanners("ServiceOnly")

                    for key, value in s2dCardInfo['offers'].items():
                        if key == 'Oferta_SimOnly_Perioada_Contractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        elif key == 'Oferta_SimOnly_oPrice':
                            try:
                                value = value.split()[:-2]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                for i in value:
                                    logTxt = 'ShoppingCart_OfferInfo_Price'
                                    if self.utils.element_exists(
                                            """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                                i)):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ')
                            except TypeError:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_Price',
                                                                       'FAIL --> [Offer Price nu a fost identificat pe Cartonas]')

                        elif self.utils.element_exists(
                                """//section[@class="cart-items-section"][1]//*[contains(text(),"{}")]""".format(
                                    value)):
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'PASSED --> [{}]'.format(value))
                        else:
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'FAIL --> [{}]'.format(value))

                    if self.utils.element_exists(
                            """//div[@class="item-wrap-prices "][1]//div[@class="cart-price cart-no-value" and contains(text(), '- - -')]""",
                            enable_Logging=False):
                        self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans', 'PASSED --> [---]')
                    else:
                        self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans', 'FAIL --> [{}]'.format(
                            self.utils.get_text(
                                """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]/div[@class="items-wrap flex"]//div[@class="item-wrap-prices "][1]""")))

                    for key, value in s2dCardInfo['total'].items():
                        if key == 'Device_SimOnly_Avans':
                            value = value.replace('€0', '€')

                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'FAIL --> [{}]'.format(value))

                        elif key == 'Total_SimOnly_TotalLunar':
                            try:
                                value = value.split()[:-2]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                for i in value:
                                    if value.index(i) == 0:
                                        logTxt = 'ShoppingCart_TotalLunar_Price'
                                    else:
                                        logTxt = 'ShoppingCart_TotalLunar_StrikeThroughPrice'

                                    if self.utils.element_exists(
                                            """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                                i)):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 1164 09032020 ')
                            except TypeError:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_Price',
                                                                       'FAIL --> [Offer Price nu a fost identificat pe Cartonas]')

                        else:

                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'FAIL --> [{}]'.format(value))

                        # print('s2dcardinfo', key, ' ', value)
                        # print(
                        #     """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                        #         value))
                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                elif self.scenarioName_PC == 'OferteConfigurabile_ServiceOnly_Discount':

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {
                        'offers': {
                            'Oferta_SimOnly_Discount_Perioada_Contractuala': """//div[@class="card mobile-card"]//*[(contains(text(),'SIM Only'))]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                            'Oferta_SimOnly_Discount_NumeOferta': """//div[@class="card mobile-card"]//*[(contains(text(),'SIM Only'))]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                            # 'Oferta_SimOnly_Discount_oPrice': """//div[@class="card mobile-card"]//*[(contains(text(),'SIM Only'))]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
                        },
                        'device': {
                            'Device_SimOnly_Name': '//div[@class="card_wrap card-wrap-device  "]//div[@class="model_phone"]',
                            'Device_SimOnly_Price': '//div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                            'Device_SimOnly_Discount': '//div[@class="card_wrap card-wrap-device  "]//span[@class="txt_right_side after_discount"]',
                            # 'Device_SimOnly_Stock': '//div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify final_p"]/span[2]',
                            # 'Device_Rate_OfferPriceDiscount': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="Overview with_tooltip"]//span[@class="txt_right_side after_discount"]',

                        },
                        'total': {
                            'Total_SimOnly_TotalLunar': """//div[@class="card mobile-card"]//*[(contains(text(),'SIM Only'))]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
                            # 'Device_SimOnly_Avans': '//div[@class="align-justify two_sides undefined"]//div[@class="tooltip_price_wrap"]',
                        }}

                    self.utils.click_js(
                        """//div[@class="fixed-bundle-offers"][1]//div[@class='accordion_head']//div[@class="accordion_head_button"]""")
                    self.utils.type_js('//input[@name="searchInput"]', offerName['discount'])
                    self.utils.click_js("//button[@class='primary bold']")
                    self.utils.wait_loadingCircle()

                    for key, val in cardXPathsDict['offers'].items():
                        s2dCardInfo['offers'][key] = self.utils.get_text(val)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_SimOnly_Discount_Selecteaza'])
                    self.utils.wait_loadingCircle()

                    self.checkMinimBanners(expandElement='ServiceOnly')

                    # self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    for key, value in s2dCardInfo['offers'].items():
                        if key == 'Oferta_SimOnly_Discount_Perioada_Contractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        elif key == 'Oferta_SimOnly_Discount_oPrice':
                            try:
                                value = value.split()[:-2]
                                for i in value:
                                    if value.index(i) == 0:
                                        logTxt = 'ShoppingCart_OfferInfo_PriceAfterDiscount'
                                    else:
                                        logTxt = 'ShoppingCart_OfferInfo_StrikeThroughPrice'

                                    if self.utils.element_exists(
                                            """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                                i)):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))

                            except (AttributeError, ValueError) as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ')
                            except TypeError:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_Price',
                                                                       'FAIL --> [Offer Price nu a fost identificat pe Cartonas]')

                        elif self.utils.element_exists(
                                """//section[@class="cart-items-section"][1]//*[contains(text(),"{}")]""".format(
                                    value)):
                            print("""//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                value))
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'PASSED --> [{}]'.format(value))
                        else:
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'FAIL --> [{}]'.format(value))

                    if self.utils.element_exists(
                            """//div[@class="item-wrap-prices "][1]//div[@class="cart-price cart-no-value" and contains(text(), '- - -')]""",
                            enable_Logging=False):
                        self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans', 'PASSED --> [---]')
                    else:
                        self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans', 'FAIL --> [{}]'.format(
                            self.utils.get_text(
                                """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]/div[@class="items-wrap flex"]//div[@class="item-wrap-prices "][1]""")))

                    for key, value in s2dCardInfo['total'].items():
                        if key == 'Device_SimOnly_Avans':
                            value = value.replace('€0', '€')

                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'FAIL --> [{}]'.format(value))

                        elif key == 'Total_SimOnly_TotalLunar':
                            try:

                                value = value.split()[:-2]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                for i in value:
                                    if value.index(i) == 0:
                                        logTxt = 'ShoppingCart_TotalLunar_PriceAfterDiscount'
                                        var = """//section[@class="cart-total-items-section"]//*[@class="cart-price" and contains(text(),'{}')]""".format(
                                            i)
                                    else:
                                        logTxt = 'ShoppingCart_TotalLunar_StrikeThroughPrice'
                                        var = """//section[@class="cart-total-items-section"]//*[@class="cart-strikethrough" and contains(text(),'{}')]""".format(
                                            i)

                                    try:
                                        if self.utils.element_exists(var):
                                            self.utils.log_customStep(logTxt,
                                                                                   'PASSED --> [{}]'.format(i))
                                        else:
                                            self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                                    except AttributeError as e:
                                        print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                              '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                              '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )
                            except TypeError as e:
                                print('OferteConfigurabile_ServiceOnly_Discount, BOOL ERROR', e)

                        else:

                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'FAIL --> [{}]'.format(value))

                        # print('s2dcardinfo', key, ' ', value)
                        # print(
                        #     """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                        #         value))

                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                elif self.scenarioName_PC == 'OferteConfigurabile_D2S_VMM_Rate_si_Discount':
                    print('sceriu sd')

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {
                        'offers': {
                            'Offer_D2S_Rate_CardToBeSelected': """//div[@class="card mobile-card"][position() > 3]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]""",
                            'Offer_D2S_Rate_SelectedOfferCard': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//button[contains(@class,'general_btn button-selected')]//ancestor::div[@class="card mobile-card"][1]""",
                            'Oferta_D2S_Rate_RateMultiple': """//div[@class="card mobile-card"][position() > 3]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//span[@class="link-title blue bold"]""",
                            'Oferta_D2S_Rate_NumeOferta': """//div[@class="card mobile-card"][position() > 3]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                            'Oferta_D2S_Rate_PerioadaContractuala': """//div[@class="card mobile-card"][position() > 3]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                            # 'Oferta_D2S_Rate_oPrice': """//div[@class="card mobile-card"][position() > 3]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
                            # 'Oferta_D2S_Rate_LastInstallment': '//div[@class="white_tooltip box_tooltip  visible-tooltip"]/div[last()]',
                            'Oferta_D2S_Rate_InstallmentValue': """//div[@class="card mobile-card"][position() > 3]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//span[@class="txt_right_side after_discount"]""",
                        },

                        'device': {
                            'Device_D2S_Rate_CardToBeSelected': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 3]',
                            'Device_D2S_Rate_SelectedCard': """//div[@class="card_wrap card-wrap-device device-offer "]//span[contains(text(),'SELECTAT')]//ancestor::div[@class="card_wrap card-wrap-device device-offer "][1]""",
                            'Device_D2S_Rate_Name': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 3]//div[@class="model_phone"]',
                            'Device_D2S_Rate_Upfront': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 3]//span[@class="price"]',
                        },

                        'total': {
                            'Total_D2S_Rate_TotalLunar': """//div[@class="card mobile-card"][position() > 3]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
                            'Device_D2S_Rate_Avans': """//div[@class="card mobile-card"][position() > 3]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//span[@class="price"]""",
                        }}

                    time.sleep(self.global_Wait)
                    self.utils.click_js(self.pageConfigDict['Vezi Mai Multe_1stPannel'])
                    self.utils.scroll_to_element(self.pageConfigDict['Device_D2S_VMM_Rate_Selecteaza'],
                                                              initialScroll=2, defaultClick=False)

                    time.sleep(5)
                    for key, val in cardXPathsDict['device'].items():
                        if key != 'Device_D2S_Rate_SelectedCard':
                            s2dCardInfo['device'][key] = self.utils.get_text(val)

                    # Selecteaza DEVICE
                    self.utils.click_js(self.pageConfigDict['Device_D2S_VMM_RateDiscount_Selecteaza'])
                    self.utils.wait_loadingCircle()

                    self.checkMinimBanners(expandElement='Device_D2S')

                    for key, value in s2dCardInfo['device'].items():
                        print(key)
                        if key != 'Device_D2S_Rate_CardToBeSelected':
                            if key == 'Device_D2S_34Rate_RateMultiple':
                                try:
                                    value = value.split('\n')[0].replace('x', '')
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):

                                    print(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value))
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[3],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[3],
                                        'FAIL --> [{}]'.format(value))

                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                    # time.sleep(25)
                    # self.checkMinimBanners('Device_D2S')
                    s2dCardInfo['device']['Device_D2S_Rate_SelectedCard'] = self.utils.get_text(
                        cardXPathsDict['device']['Device_D2S_Rate_SelectedCard'])

                    # time.sleep(10)
                    time.sleep(self.global_Wait)
                    self.utils.click_js(self.pageConfigDict['Vezi Mai Multe_2ndPannel'])
                    time.sleep(3)
                    self.utils.scroll_to_element(self.pageConfigDict['Oferta_D2S_VMM_RateDiscount_Select'],
                                                              initialScroll=4, defaultClick=False)
                    time.sleep(5)

                    self.utils.click_js(self.pageConfigDict['Oferta_D2S_VMM_RateDiscount_RateMultiple'],
                                                     customWait=20)
                    self.utils.wait_loadingCircle()
                    self.utils.click_js(self.pageConfigDict['Oferta_D2S_VMM_RateDiscount_LastInstallment'])
                    self.utils.wait_loadingCircle()

                    ##GET OFFER DETAILS
                    for key, val in cardXPathsDict['offers'].items():
                        if key != 'Offer_D2S_Rate_SelectedOfferCard':
                            print('the key is ', key)
                            s2dCardInfo['offer'][key] = self.utils.get_text(val)
                            print('s2ddict -- . ', s2dCardInfo)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_D2S_VMM_RateDiscount_Select'])
                    self.utils.wait_loadingCircle()
                    cardNotSelected = str(s2dCardInfo['device']['Device_D2S_Rate_CardToBeSelected']).replace(
                        'SELECTEAZĂ', '')

                    cardSelected = str(s2dCardInfo['device']['Device_D2S_Rate_SelectedCard']).replace('SELECTAT', '')

                    if cardNotSelected == cardSelected:
                        self.utils.log_customStep('Selected Device_Card values VS Device_Card before select values',
                                                  'PASSED [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))
                    else:
                        self.utils.log_customStep('Selected Device_Card values VS Device_Card before select values',
                                                  'FAIL [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected)
                                                  )

                    del s2dCardInfo['device']['Device_D2S_Rate_CardToBeSelected']
                    del s2dCardInfo['device']['Device_D2S_Rate_SelectedCard']

                    self.checkMinimBanners(expandElement='Servicii_D2S')
                    s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard'] = self.utils.get_text(
                        cardXPathsDict['offers']['Offer_D2S_Rate_SelectedOfferCard'])

                    cardNotSelected = str(s2dCardInfo['offer']['Offer_D2S_Rate_CardToBeSelected']).replace('SELECTEAZA',
                                                                                                           '')
                    cardSelected = str(s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard']).replace('SELECTAT', '')

                    if cardNotSelected == cardSelected:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'PASSED [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))
                    else:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'FAIL [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))

                    del s2dCardInfo['offer']['Offer_D2S_Rate_CardToBeSelected']
                    del s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard']

                    for key, value in s2dCardInfo['offer'].items():
                        if key == 'Oferta_D2S_Rate_RateMu32ltiple':
                            try:
                                print(value, 'rarsarasrsarsa')
                                value = value.split('\n')[0].replace('x', '')
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):

                                    print(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value))
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[3],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[3],
                                        'FAIL --> [{}]'.format(value))

                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        if key == 'Oferta_D2S_Rate_PerioadaContractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        # elif key =='Oferta_S2D_Rate_Perioada_Contractuala':
                        #     value = value.split('\n')[0].replace('x','')
                        print('offervals ', key)

                        if key == 'Oferta_D2S_Rate_InstallmentValue':
                            cartXpath = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div//section[@class="cart-items-section"]//*[contains(text(),'{}')]"""
                        else:
                            cartXpath = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]"""

                        print(cartXpath.format(value))

                        if self.utils.element_exists(cartXpath.format(value)):
                            try:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                        else:
                            try:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'FAIL --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                    for key, value in s2dCardInfo['total'].items():
                        try:
                            if key == 'Total_D2S_Rate_TotalLunar':
                                try:

                                    value = value.split()[:-2]

                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                                for i in value:
                                    if value.index(i) == 0:
                                        logTxt = 'ShoppingCart_TotalLunar_PriceAfterDiscount'
                                        var = """//section[@class="cart-total-items-section"]//*[@class="cart-price" and contains(text(),'{}')]""".format(
                                            i)
                                    else:
                                        logTxt = 'ShoppingCart_TotalLunar_StrikeThroughPrice'
                                        var = """//section[@class="cart-total-items-section"]//*[@class="cart-strikethrough" and contains(text(),'{}')]""".format(
                                            i)

                                    try:
                                        if self.utils.element_exists(var):
                                            self.utils.log_customStep(logTxt,
                                                                                   'PASSED --> [{}]'.format(i))
                                        else:
                                            self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                                    except AttributeError as e:
                                        print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                              '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                              '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                                if len(value) < 2:
                                    print('ShoppingCart_TotalLunar_StrikeThroughPrice',
                                          'FAIL --> ["STRIKETHROUGH VALUE NOT FOUND ON PAGE"]')

                            elif self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                        value)):
                                print(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                        value))
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[3],
                                                                       'FAIL --> [{}]'.format(value))
                        except AttributeError as e:
                            print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                  '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                  '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 444 ', )

                    # self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    time.sleep(10)
                    isStrikeThrough = self.utils.element_exists(
                        """//div[@class="cart-menu-wrap"]//*[@class="cart-strikethrough"]""")
                    print('isStrikeThrough', isStrikeThrough)
                    if isStrikeThrough == False:
                        self.utils.log_customStep('Niciun element cu StrikeThrough Identificat in Cart', 'PASSED')
                    else:
                        self.utils.log_customStep('Niciun element cu StrikeThrough Identificat in Cart', 'FAIL')

                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                elif self.scenarioName_PC == 'OferteConfigurabile_ServiceOnly_VMM':
                    print('sceriu sdonly')

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {
                        'offers': {
                            'Oferta_SimOnly_Perioada_Contractuala': """//div[@class="card mobile-card"][position() > 3]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                            'Oferta_SimOnly_NumeOferta': """//div[@class="card mobile-card"][position() > 3]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                            'Oferta_SimOnly_oPrice': """//div[@class="card mobile-card"][position() > 3]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
                        },
                        'device': {
                            'Device_SimOnly_Name': '//div[@class="card_wrap card-wrap-device  "]//div[@class="model_phone"]',
                            'Device_SimOnly_Price': '//div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                            # 'Device_SimOnly_Discount': '//div[@class="card_wrap card-wrap-device  "]//span[@class="txt_right_side after_discount"]',
                            'Device_SimOnly_Stock': '//div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify final_p"]/span[2]',
                            # 'Device_SimOnly_Selecteaza': 'div[@class="card_wrap card-wrap-device  "]//button[@class="general_btn "]',
                        },
                        'total': {
                            'Total_SimOnly_TotalLunar': """//div[@class="card mobile-card"][position() > 3]//*[(contains(text(),'SIM Only'))]//ancestor::div[@class="card mobile-card"]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
                            # 'Device_SimOnly_Avans': '//div[@class="align-justify two_sides undefined"]//div[@class="tooltip_price_wrap"]',
                        }}

                    time.sleep(self.global_Wait)
                    self.utils.click_js(self.pageConfigDict['Vezi Mai Multe_1stPannel'])
                    self.utils.scroll_to_element(self.pageConfigDict['Oferta_VMMServiceOnly_Selecteaza'],
                                                              initialScroll=2, defaultClick=False)

                    for key, val in cardXPathsDict['offers'].items():
                        s2dCardInfo['offers'][key] = self.utils.get_text(val)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_VMMServiceOnly_Selecteaza'])
                    self.utils.wait_loadingCircle()

                    time.sleep(5)
                    self.checkMinimBanners("ServiceOnly")

                    for key, value in s2dCardInfo['offers'].items():
                        if key == 'Oferta_SimOnly_Perioada_Contractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        elif key == 'Oferta_SimOnly_oPrice':
                            try:
                                value = value.split()[:-2]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                for i in value:
                                    logTxt = 'ShoppingCart_OfferInfo_Price'
                                    if self.utils.element_exists(
                                            """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                                i)):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ')
                            except TypeError:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_Price',
                                                                       'FAIL --> [Offer Price nu a fost identificat pe Cartonas]')

                        elif self.utils.element_exists(
                                """//section[@class="cart-items-section"][1]//*[contains(text(),"{}")]""".format(
                                    value)):
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'PASSED --> [{}]'.format(value))
                        else:
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'FAIL --> [{}]'.format(value))

                    if self.utils.element_exists(
                            """//div[@class="item-wrap-prices "][1]//div[@class="cart-price cart-no-value" and contains(text(), '- - -')]""",
                            enable_Logging=False):
                        self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans', 'PASSED --> [---]')
                    else:
                        self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans', 'FAIL --> [{}]'.format(
                            self.utils.get_text(
                                """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]/div[@class="items-wrap flex"]//div[@class="item-wrap-prices "][1]""")))

                    for key, value in s2dCardInfo['total'].items():
                        if key == 'Device_SimOnly_Avans':
                            value = value.replace('€0', '€')

                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'FAIL --> [{}]'.format(value))

                        elif key == 'Total_SimOnly_TotalLunar':
                            try:
                                value = value.split()[:-2]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                for i in value:
                                    if value.index(i) == 0:
                                        logTxt = 'ShoppingCart_TotalLunar_Price'
                                    else:
                                        logTxt = 'ShoppingCart_TotalLunar_StrikeThroughPrice'

                                    if self.utils.element_exists(
                                            """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                                i)):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 1164 09032020 ')
                            except TypeError:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_Price',
                                                                       'FAIL --> [Offer Price nu a fost identificat pe Cartonas]')

                        else:

                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'FAIL --> [{}]'.format(value))

                        # print('s2dcardinfo', key, ' ', value)
                        # print(
                        #     """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                        #         value))
                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                elif self.scenarioName_PC == 'OferteConfigurabile_ServiceOnly_Discount_VMM':

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {
                        'offers': {
                            'Oferta_SimOnly_Discount_Perioada_Contractuala': """//div[@class="card mobile-card"][position() > 3]//*[(contains(text(),'SIM Only'))]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                            'Oferta_SimOnly_Discount_NumeOferta': """//div[@class="card mobile-card"][position() > 3]//*[(contains(text(),'SIM Only'))]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                            # 'Oferta_SimOnly_Discount_oPrice': """//div[@class="card mobile-card"][position() > 3]//*[(contains(text(),'SIM Only'))]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
                        },
                        'device': {
                            'Device_SimOnly_Name': '//div[@class="card_wrap card-wrap-device  "]//div[@class="model_phone"]',
                            'Device_SimOnly_Price': '//div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                            'Device_SimOnly_Discount': '//div[@class="card_wrap card-wrap-device  "]//span[@class="txt_right_side after_discount"]',
                            # 'Device_SimOnly_Stock': '//div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify final_p"]/span[2]',
                            # 'Device_Rate_OfferPriceDiscount': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="Overview with_tooltip"]//span[@class="txt_right_side after_discount"]',

                        },
                        'total': {
                            'Total_SimOnly_TotalLunar': """//div[@class="card mobile-card"][position() > 3]//*[(contains(text(),'SIM Only'))]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
                            # 'Device_SimOnly_Avans': '//div[@class="align-justify two_sides undefined"][position() > 3]//div[@class="tooltip_price_wrap"]',
                        }}

                    time.sleep(self.global_Wait)
                    self.utils.click_js(self.pageConfigDict['Vezi Mai Multe_1stPannel'])
                    self.utils.scroll_to_element(self.pageConfigDict['Oferta_VMMServiceOnlyDiscount_Selecteaza'],
                                                              initialScroll=2, defaultClick=False)

                    for key, val in cardXPathsDict['offers'].items():
                        s2dCardInfo['offers'][key] = self.utils.get_text(val)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_VMMServiceOnlyDiscount_Selecteaza'])
                    self.utils.wait_loadingCircle()

                    self.checkMinimBanners(expandElement='ServiceOnly')

                    # self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    for key, value in s2dCardInfo['offers'].items():
                        if key == 'Oferta_SimOnly_Discount_Perioada_Contractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        elif key == 'Oferta_SimOnly_Discount_oPrice':
                            try:
                                value = value.split()[:-2]
                                for i in value:
                                    if value.index(i) == 0:
                                        logTxt = 'ShoppingCart_OfferInfo_PriceAfterDiscount'
                                    else:
                                        logTxt = 'ShoppingCart_OfferInfo_StrikeThroughPrice'

                                    if self.utils.element_exists(
                                            """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                                i)):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))

                            except (AttributeError, ValueError) as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ')
                            except TypeError:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_Price',
                                                                       'FAIL --> [Offer Price nu a fost identificat pe Cartonas]')

                        elif self.utils.element_exists(
                                """//section[@class="cart-items-section"][1]//*[contains(text(),"{}")]""".format(
                                    value)):
                            print("""//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                value))
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'PASSED --> [{}]'.format(value))
                        else:
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'FAIL --> [{}]'.format(value))

                    if self.utils.element_exists(
                            """//div[@class="item-wrap-prices "][1]//div[@class="cart-price cart-no-value" and contains(text(), '- - -')]""",
                            enable_Logging=False):
                        self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans', 'PASSED --> [---]')
                    else:
                        self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans', 'FAIL --> [{}]'.format(
                            self.utils.get_text(
                                """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]/div[@class="items-wrap flex"]//div[@class="item-wrap-prices "][1]""")))

                    for key, value in s2dCardInfo['total'].items():
                        if key == 'Device_SimOnly_Avans':
                            value = value.replace('€0', '€')

                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'FAIL --> [{}]'.format(value))

                        elif key == 'Total_SimOnly_TotalLunar':
                            try:

                                value = value.split()[:-2]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                            for i in value:
                                if value.index(i) == 0:
                                    logTxt = 'ShoppingCart_TotalLunar_PriceAfterDiscount'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-price" and contains(text(),'{}')]""".format(
                                        i)
                                else:
                                    logTxt = 'ShoppingCart_TotalLunar_StrikeThroughPrice'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-strikethrough" and contains(text(),'{}')]""".format(
                                        i)

                                try:
                                    if self.utils.element_exists(var):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                          '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                          '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                        else:

                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'FAIL --> [{}]'.format(value))

                        # print('s2dcardinfo', key, ' ', value)
                        # print(
                        #     """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                        #         value))

                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                elif self.scenarioName_PC == 'OferteConfigurabile_S2D_VMM_SimOnly':
                    print('sceriu sdonly')

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {
                        'offers': {
                            'Oferta_SimOnly_Perioada_Contractuala': """//div[@class="card mobile-card"][position() > 3]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                            'Oferta_SimOnly_NumeOferta': """//div[@class="card mobile-card"][position() > 3]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                            'Oferta_SimOnly_oPrice': """//div[@class="card mobile-card"][position() > 3]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
                        },
                        'device': {
                            'Device_SimOnly_Name': '//div[@class="card_wrap card-wrap-device  "][position() > 3]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="model_phone"]',
                            'Device_SimOnly_Price': '//div[@class="card_wrap card-wrap-device  "][position() > 3]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                            # 'Device_SimOnly_Discount': '//div[@class="card_wrap card-wrap-device  "]//span[@class="txt_right_side after_discount"]',
                            'Device_SimOnly_Stock': '//div[@class="card_wrap card-wrap-device  "][position() > 3]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify final_p"]/span[2]',
                            # 'Device_SimOnly_Selecteaza': 'div[@class="card_wrap card-wrap-device  "]//button[@class="general_btn "]',
                        },
                        'total': {
                            'Total_SimOnly_TotalLunar': """//div[@class="card_wrap card-wrap-device  "][position() > 3]//div[@class="price total-device-price"]""",
                            'Device_SimOnly_Avans': '//div[@class="card_wrap card-wrap-device  "][position() > 3]//span[@class="price"]'
                        }}

                    time.sleep(self.global_Wait)
                    self.utils.click_js(self.pageConfigDict['Vezi Mai Multe_1stPannel'])
                    self.utils.scroll_to_element(self.pageConfigDict['Oferta_VMM_SimOnly_Selecteaza'],
                                                              initialScroll=2, defaultClick=False)
                    for key, val in cardXPathsDict['offers'].items():
                        print(self.utils.get_text(val))
                        s2dCardInfo['offer'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_VMM_SimOnly_Selecteaza'])
                    self.utils.wait_loadingCircle()
                    time.sleep(self.global_Wait)

                    self.utils.click_js(self.pageConfigDict['Vezi Mai Multe_2ndPannel'])

                    self.utils.scroll_to_element(self.pageConfigDict['Device_VMM_SimOnly_Selecteaza'],
                                                              initialScroll=2, defaultClick=False)

                    for key, value in cardXPathsDict['device'].items():
                        s2dCardInfo['device'][key] = self.utils.get_text(value)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    time.sleep(4)
                    self.utils.click_js(self.pageConfigDict['Device_VMM_SimOnly_Selecteaza'],
                                                     customWait=20)
                    self.utils.wait_loadingCircle()

                    self.checkMinimBanners(expandElement='Servicii')

                    self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    for key, value in s2dCardInfo['offers'].items():
                        if key == 'Oferta_SimOnly_Perioada_Contractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        elif key == 'Oferta_SimOnly_oPrice':
                            try:
                                value = value.split()[:-2]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                for i in value:
                                    logTxt = 'ShoppingCart_OfferInfo_Price'
                                    if self.utils.element_exists(
                                            """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                                i)):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ')
                            except TypeError:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_Price',
                                                                       'FAIL --> [Offer Price nu a fost identificat pe Cartonas]')

                        elif self.utils.element_exists(
                                """//section[@class="cart-items-section"][1]//*[contains(text(),"{}")]""".format(
                                    value)):
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'PASSED --> [{}]'.format(value))
                        else:
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'FAIL --> [{}]'.format(value))

                    if self.utils.element_exists(
                            """//div[@class="item-wrap-prices "][1]//div[@class="cart-price cart-no-value" and contains(text(), '- - -')]""",
                            enable_Logging=False):
                        self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans', 'PASSED --> [---]')
                    else:
                        self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans', 'FAIL --> [{}]'.format(
                            self.utils.get_text(
                                """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]/div[@class="items-wrap flex"]//div[@class="item-wrap-prices "][1]""")))

                    self.checkMinimBanners(expandElement='Device')

                    for key, value in s2dCardInfo['device'].items():
                        if key != 'Device_SimOnly_Stock':
                            if key == 'Device_Rate_multiple':
                                try:
                                    value = value.split('\n')[0].replace('x', '')
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'FAIL --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 874')

                        else:
                            pass

                    for key, value in s2dCardInfo['total'].items():
                        if key == 'Device_SimOnly_Avans':
                            value = value.replace('€0', '€')

                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'FAIL --> [{}]'.format(value))

                        elif key == 'Total_SimOnly_TotalLunar':
                            try:
                                value = value.split()[:-2]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                for i in value:
                                    if value.index(i) == 0:
                                        logTxt = 'ShoppingCart_TotalLunar_PriceAfterDiscount'
                                    else:
                                        logTxt = 'ShoppingCart_TotalLunar_StrikeThroughPrice'

                                    if self.utils.element_exists(
                                            """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                                i)):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 1164 09032020 ')
                            except TypeError:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_Price',
                                                                       'FAIL --> [Offer Price nu a fost identificat pe Cartonas]')

                        else:

                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'FAIL --> [{}]'.format(value))

                        # print('s2dcardinfo', key, ' ', value)
                        # print(
                        #     """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                        #         value))

                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                    # s2dCardInfo['Oferta_S2D_Rate_Perioada_Contractuala'] = self.utils.get_text(
                    #     self.pageConfigDict['Oferta_S2D_Rate_Perioada_Contractuala'])
                    # s2dCardInfo['Oferta_S2D_Rate_Nume_Oferta'] = self.utils.get_text(
                    #     self.pageConfigDict['Oferta_S2D_Rate_Nume_Oferta'])
                    # s2dCardInfo['Oferta_S2D_Rate_oPrice'] = self.utils.get_text(
                    #     self.pageConfigDict['Oferta_S2D_Rate_oPrice'])
                    # self.utils.click_js(
                    #     self.pageConfigDict['Oferta_S2D_Rate_Select'])

                elif self.scenarioName_PC == 'OferteConfigurabile_S2D_VMM_SimOnly_si_Discount':
                    print('sceriu sdonl123y')

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {
                        'offers': {
                            'Oferta_SimOnly_Discount_Perioada_Contractuala': """//div[@class="card mobile-card"][position() > 3]//*[(contains(text(),'SIM Only'))]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                            'Oferta_SimOnly_Discount_NumeOferta': """//div[@class="card mobile-card"][position() > 3]//*[(contains(text(),'SIM Only'))]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                            'Oferta_SimOnly_Discount_oPrice': """//div[@class="card mobile-card"][position() > 3]//*[(contains(text(),'SIM Only'))]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
                        },
                        'device': {
                            'Device_SimOnly_Name': '//div[@class="card_wrap card-wrap-device  "][position() > 3]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="model_phone"]',
                            'Device_SimOnly_Price': '//div[@class="card_wrap card-wrap-device  "][position() > 3]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                            'Device_SimOnly_Discount': '//div[@class="card_wrap card-wrap-device  "][position() > 3]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="txt_right_side after_discount"]',
                            # 'Device_SimOnly_Stock': '//div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify final_p"]/span[2]',
                            # 'Device_Rate_OfferPriceDiscount': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="Overview with_tooltip"]//span[@class="txt_right_side after_discount"]',

                        },
                        'total': {
                            'Total_SimOnly_TotalLunar': """//div[@class="card_wrap card-wrap-device  "][position() > 3]//div[@class="price total-device-price"]""",
                            'Device_SimOnly_Avans': '//div[@class="card_wrap card-wrap-device  "][position() > 3]//span[@class="price"]'
                        }}
                    time.sleep(self.global_Wait)

                    self.utils.click_js(self.pageConfigDict['Vezi Mai Multe_1stPannel'])
                    self.utils.scroll_to_element(
                        self.pageConfigDict['Oferta_VMM_SimOnly_Discount_Selecteaza'], initialScroll=7,
                        defaultClick=False)

                    for key, val in cardXPathsDict['offers'].items():
                        print(self.utils.get_text(val))
                        s2dCardInfo['offer'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_VMM_SimOnly_Discount_Selecteaza'])
                    self.utils.wait_loadingCircle()
                    time.sleep(self.global_Wait)

                    self.utils.click_js(self.pageConfigDict['Vezi Mai Multe_2ndPannel'])
                    self.utils.scroll_to_element(self.pageConfigDict['Device_VMM_SimOnly_Selecteaza'],
                                                              initialScroll=7, defaultClick=False)

                    for key, value in cardXPathsDict['device'].items():
                        s2dCardInfo['device'][key] = self.utils.get_text(value)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    time.sleep(4)
                    self.utils.click_js(self.pageConfigDict['Device_VMM_SimOnly_Selecteaza'],
                                                     customWait=20)
                    self.utils.wait_loadingCircle()

                    self.checkMinimBanners(expandElement='Servicii')

                    self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    for key, value in s2dCardInfo['offers'].items():
                        if key == 'Oferta_SimOnly_Discount_Perioada_Contractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        elif key == 'Oferta_SimOnly_Discount_oPrice':
                            try:
                                value = value.split()[:-2]
                                for i in value:
                                    if value.index(i) == 0:
                                        logTxt = 'ShoppingCart_OfferInfo_PriceAfterDiscount'
                                    else:
                                        logTxt = 'ShoppingCart_OfferInfo_StrikeThroughPrice'

                                    if self.utils.element_exists(
                                            """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                                i)):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))

                            except (AttributeError, ValueError) as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ')
                            except TypeError:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_Price',
                                                                       'FAIL --> [Offer Price nu a fost identificat pe Cartonas]')

                        elif self.utils.element_exists(
                                """//section[@class="cart-items-section"][1]//*[contains(text(),"{}")]""".format(
                                    value)):
                            print("""//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                value))
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'PASSED --> [{}]'.format(value))
                        else:
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'FAIL --> [{}]'.format(value))

                    if self.utils.element_exists(
                            """//div[@class="item-wrap-prices "][1]//div[@class="cart-price cart-no-value" and contains(text(), '- - -')]""",
                            enable_Logging=False):
                        self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans', 'PASSED --> [---]')
                    else:
                        self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans', 'FAIL --> [{}]'.format(
                            self.utils.get_text(
                                """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]/div[@class="items-wrap flex"]//div[@class="item-wrap-prices "][1]""")))

                    self.checkMinimBanners(expandElement='Device')

                    for key, value in s2dCardInfo['device'].items():
                        print('key is ', key, 'the val is ', value)
                        if key == 'Device_Rate_multiple':
                            try:
                                value = value.split('\n')[0].replace('x', '')
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'FAIL --> [{}]'.format(value))

                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        elif key == 'Device_SimOnly_Discount':
                            try:
                                print('we here')

                                value = value.split()
                            except AttributeError as e:
                                print('SimOnly si discount Exceptie:\nAttribute error exception for : ', value,
                                      "EXCEPTION --> ", e)

                            print('val pe scenariu este', value)
                            for i in value:
                                if value.index(i) == 0:
                                    logTxt = 'ShoppingCart_DeviceInfoOffer_PriceAfterDiscount'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-price" and contains(text(),'{}')]""".format(
                                        i)
                                else:
                                    logTxt = 'ShoppingCart_DeviceInfoOffer_StrikeThroughPrice'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-strikethrough" and contains(text(),'{}')]""".format(
                                        i)

                                try:
                                    if self.utils.element_exists(var):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                          '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                          '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                        else:
                            try:
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'FAIL --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                    for key, value in s2dCardInfo['total'].items():
                        if key == 'Device_SimOnly_Avans':
                            value = value.replace('€0', '€')

                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'FAIL --> [{}]'.format(value))

                        elif key == 'Total_SimOnly_TotalLunar':
                            try:
                                print("raaaaaaaaaaaaaaaaaaaaaaaaz", value)
                                value = value.split()[:-2]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                            for i in value:
                                if value.index(i) == 0:
                                    logTxt = 'ShoppingCart_TotalLunar_PriceAfterDiscount'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-price" and contains(text(),'{}')]""".format(
                                        i)
                                else:
                                    logTxt = 'ShoppingCart_TotalLunar_StrikeThroughPrice'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-strikethrough" and contains(text(),'{}')]""".format(
                                        i)

                                try:
                                    if self.utils.element_exists(var):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                          '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                          '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                        else:

                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'FAIL --> [{}]'.format(value))

                        # print('s2dcardinfo', key, ' ', value)
                        # print(
                        #     """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                        #         value))
                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                elif self.scenarioName_PC == 'OferteConfigurabile_D2S_VMM_Rate':
                    print('OferteConfigurabile_D2S_Rate')

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {
                        'offers': {
                            'Offer_D2S_Rate_CardToBeSelected': """//div[@class="card mobile-card"][position() > 3]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]""",
                            'Offer_D2S_Rate_SelectedOfferCard': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//button[contains(@class,'general_btn button-selected')]//ancestor::div[@class="card mobile-card"][1]""",
                            'Oferta_D2S_Rate_NumeOferta': """//div[@class="card mobile-card"][position() > 3]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                            'Oferta_D2S_Rate_PerioadaContractuala': """//div[@class="card mobile-card"][position() > 3]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                            'Oferta_D2S_Rate_RateMultiple': """//div[@class="card mobile-card"][position() > 3]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//span[@class="link-title blue bold"]""",
                            # 'Oferta_D2S_Rate_oPrice': """//div[@class="card mobile-card"][position() > 3]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
                            # 'Oferta_D2S_Rate_LastInstallment': '//div[@class="white_tooltip box_tooltip  visible-tooltip"]/div[last()]',
                            'Oferta_D2S_Rate_InstallmentValue': """//div[@class="card mobile-card"][position() > 3]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//span[@class="txt_right_side after_discount"]""",
                        },

                        'device': {
                            'Device_D2S_Rate_CardToBeSelected': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 3]//ancestor::div[@class="card_wrap card-wrap-device device-offer "]',
                            'Device_D2S_Rate_SelectedCard': """//div[@class="card_wrap card-wrap-device device-offer "]//ancestor::div[@class="card_wrap card-wrap-device device-offer "]//span[contains(text(),'SELECTAT')]//ancestor::div[@class="card_wrap card-wrap-device device-offer "][1]""",
                            'Device_D2S_Rate_Name': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 3]//ancestor::div[@class="card_wrap card-wrap-device device-offer "]//div[@class="model_phone"]',
                            'Device_D2S_Rate_Upfront': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 3]//ancestor::div[@class="card_wrap card-wrap-device device-offer "]//span[@class="price"]',
                        },
                        'total': {
                            'Total_D2S_Rate_TotalLunar': """//div[@class="card mobile-card"][position() > 3]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
                            'Device_D2S_Rate_Avans': """//div[@class="card mobile-card"][position() > 3]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//span[@class="price"]""",
                        }}

                    time.sleep(self.global_Wait)
                    self.utils.click_js(self.pageConfigDict['Vezi Mai Multe_1stPannel'])
                    self.utils.scroll_to_element(self.pageConfigDict['Device_D2S_VMM_Rate_Selecteaza'],
                                                              initialScroll=2, defaultClick=False)

                    for key, val in cardXPathsDict['device'].items():
                        if key != 'Device_D2S_Rate_SelectedCard':
                            s2dCardInfo['device'][key] = self.utils.get_text(val)

                    # Selecteaza DEVICE
                    self.utils.click_js(self.pageConfigDict['Device_D2S_VMM_Rate_Selecteaza'])
                    self.utils.wait_loadingCircle()
                    time.sleep(10)
                    self.checkMinimBanners(expandElement='Device_D2S')

                    for key, value in s2dCardInfo['device'].items():
                        print(key)
                        if key != 'Device_D2S_Rate_CardToBeSelected':
                            if key == 'Device_D2S_Rate_Rate32Multiple':
                                try:
                                    value = value.split('\n')[0].replace('x', '')
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):

                                    print(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value))
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[3],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[3],
                                        'FAIL --> [{}]'.format(value))

                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                    # time.sleep(25)
                    # self.checkMinimBanners('Device_D2S')
                    s2dCardInfo['device']['Device_D2S_Rate_SelectedCard'] = self.utils.get_text(
                        cardXPathsDict['device']['Device_D2S_Rate_SelectedCard'])

                    # time.sleep(10)

                    self.utils.wait_loadingCircle()
                    time.sleep(self.global_Wait)
                    self.utils.click_js(self.pageConfigDict['Vezi Mai Multe_2ndPannel'])
                    self.utils.scroll_to_element(self.pageConfigDict['Oferta_D2S_VMM_Rate_Select'],
                                                              initialScroll=3, defaultClick=False)

                    self.utils.click_js(self.pageConfigDict['Oferta_D2S_VMM_Rate_RateMultiple'],
                                                     customWait=20)
                    self.utils.wait_loadingCircle()

                    self.utils.click_js(self.pageConfigDict['Oferta_D2S_VMM_Rate_LastInstallment'])
                    self.utils.wait_loadingCircle()

                    ##GET OFFER DETAILS
                    for key, val in cardXPathsDict['offers'].items():
                        if key != 'Offer_D2S_Rate_SelectedOfferCard':
                            s2dCardInfo['offer'][key] = self.utils.get_text(val)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_D2S_VMM_Rate_Select'])
                    self.utils.wait_loadingCircle()

                    cardNotSelected = str(s2dCardInfo['device']['Device_D2S_Rate_CardToBeSelected']).replace(
                        'SELECTEAZĂ', '')
                    cardSelected = str(s2dCardInfo['device']['Device_D2S_Rate_SelectedCard']).replace('SELECTAT', '')

                    if cardNotSelected == cardSelected:
                        self.utils.log_customStep('Selected Device_Card values VS Device_Card before select values',
                                                  'PASSED [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))
                    else:
                        self.utils.log_customStep('Selected Device_Card values VS Device_Card before select values',
                                                  'FAIL [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected)
                                                  )

                    del s2dCardInfo['device']['Device_D2S_Rate_CardToBeSelected']
                    del s2dCardInfo['device']['Device_D2S_Rate_SelectedCard']

                    self.checkMinimBanners(expandElement='Servicii_D2S')
                    s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard'] = self.utils.get_text(
                        cardXPathsDict['offers']['Offer_D2S_Rate_SelectedOfferCard'])

                    cardNotSelected = str(s2dCardInfo['offer']['Offer_D2S_Rate_CardToBeSelected']).replace('SELECTEAZA',
                                                                                                           '')
                    cardSelected = str(s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard']).replace('SELECTAT', '')

                    if cardNotSelected == cardSelected:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'PASSED [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))
                    else:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'FAIL [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))

                    del s2dCardInfo['offer']['Offer_D2S_Rate_CardToBeSelected']
                    del s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard']
                    # del s2dCardInfo['offer']['Oferta_D2S_Rate_RateMultiple']

                    for key, value in s2dCardInfo['offer'].items():

                        if key == 'Oferta_D2S_Rate_PerioadaContractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        # elif key =='Oferta_S2D_Rate_Perioada_Contractuala':
                        #     value = value.split('\n')[0].replace('x','')
                        print('offervals ', key)

                        if key == 'Oferta_D2S_Rate_InstallmentValue':
                            cartXpath = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div//section[@class="cart-items-section"]//*[contains(text(),'{}')]"""
                        else:
                            cartXpath = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]"""

                        print(cartXpath.format(value))

                        if self.utils.element_exists(cartXpath.format(value)):
                            try:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                        else:
                            try:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'FAIL --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                    for key, value in s2dCardInfo['total'].items():
                        try:
                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                        value)):
                                print(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                        value))
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[3],
                                                                       'FAIL --> [{}]'.format(value))
                        except AttributeError as e:
                            print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                  '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                  '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 444 ', )

                    # self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    time.sleep(10)
                    isStrikeThrough = self.utils.element_exists(
                        """//div[@class="cart-menu-wrap"]//*[@class="cart-strikethrough"]""")
                    print('isStrikeThrough', isStrikeThrough)
                    if isStrikeThrough == False:
                        self.utils.log_customStep('Niciun element cu StrikeThrough Identificat in Cart', 'PASSED')
                    else:
                        self.utils.log_customStep('Niciun element cu StrikeThrough Identificat in Cart', 'FAIL')

                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                elif self.scenarioName_PC == 'OferteConfigurabile_D2S_VMM_SimOnly_si_Discount':
                    print('sceriu sd')

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {
                        'offers': {
                            'Offer_D2S_Rate_CardToBeSelected': """//div[@class="card mobile-card"][position()  >3]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]""",
                            'Offer_D2S_Rate_SelectedOfferCard': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//button[contains(@class,'general_btn button-selected')]//ancestor::div[@class="card mobile-card"][1]""",
                            'Oferta_D2S_Rate_NumeOferta': """//div[@class="card mobile-card"][position()  >3]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                            'Oferta_D2S_Rate_PerioadaContractuala': """//div[@class="card mobile-card"][position()  >3]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                            'Oferta_D2S_Rate_oPrice': """//div[@class="card mobile-card"][position()  >3]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
                            # 'Oferta_D2S_Rate_LastInstallment': '//div[@class="white_tooltip box_tooltip  visible-tooltip"]/div[last()]',
                            # 'Oferta_D2S_Rate_InstallmentValue': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//span[@class="txt_right_side after_discount"]""",
                        },

                        'device': {
                            'Device_D2S_Rate_CardToBeSelected': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 3]',
                            'Device_D2S_Rate_SelectedCard': """//div[@class="card_wrap card-wrap-device device-offer "]//span[contains(text(),'SELECTAT')]//ancestor::div[@class="card_wrap card-wrap-device device-offer "][1]""",
                            'Device_D2S_Rate_Name': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 3]//div[@class="model_phone"]',
                            'Device_D2S_Rate_Upfront': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 3]//span[@class="price"]',
                        },

                        'total': {
                            'Total_D2S_Rate_TotalLunar': """//div[@class="card mobile-card"][position()  >3]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
                            'Device_D2S_Rate_Avans': """//div[@class="card mobile-card"][position()  >3]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//span[@class="price"]""",
                        }}

                    time.sleep(self.global_Wait)
                    self.utils.click_js(self.pageConfigDict['Vezi Mai Multe_1stPannel'])
                    self.utils.scroll_to_element(self.pageConfigDict['Device_D2S_VMM_Rate_Selecteaza'],
                                                              initialScroll=3, defaultClick=False)
                    for key, val in cardXPathsDict['device'].items():
                        if key != 'Device_D2S_Rate_SelectedCard':
                            s2dCardInfo['device'][key] = self.utils.get_text(val)

                    # Selecteaza DEVICE
                    self.utils.click_js(self.pageConfigDict['Device_D2S_VMM_Rate_Selecteaza'])
                    self.utils.wait_loadingCircle()

                    self.checkMinimBanners(expandElement='Device_D2S')

                    for key, value in s2dCardInfo['device'].items():
                        if key != 'Device_D2S_Rate_CardToBeSelected':

                            print('am intrat,', key)
                            if key == 'Device_D2S_Rate_RateMultiple':
                                try:
                                    value = value.split('\n')[0].replace('x', '')
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):

                                    print(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value))
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[3],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[3],
                                        'FAIL --> [{}]'.format(value))

                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                    # time.sleep(25)
                    # self.checkMinimBanners('Device_D2S')
                    s2dCardInfo['device']['Device_D2S_Rate_SelectedCard'] = self.utils.get_text(
                        cardXPathsDict['device']['Device_D2S_Rate_SelectedCard'])

                    # time.sleep(10)
                    # self.utils.click_js(
                    #     """//div[@class="fixed-bundle-offers"][2]//div[@data-automation-id="open-filter"]""")
                    #
                    # self.utils.type_js('//input[@name="searchInput"]', offerName['simonly'])
                    #
                    # self.utils.click_js("//button[@class='primary bold']")
                    time.sleep(self.global_Wait)
                    self.utils.click_js(self.pageConfigDict['Vezi Mai Multe_2ndPannel'])
                    self.utils.scroll_to_element(
                        self.pageConfigDict['Oferta_D2S_VMM_SimOnlyDiscount_Select'],
                        initialScroll=3, defaultClick=False)
                    self.utils.wait_loadingCircle()
                    time.sleep(20)
                    # self.utils.click_js(self.pageConfigDict['Oferta_D2S_SimOnly_RateMultiple'], customWait=20)
                    # self.utils.wait_loadingCircle()
                    # self.utils.click_js(self.pageConfigDict['Oferta_D2S_SimOnly_LastInstallment'])
                    # self.utils.wait_loadingCircle()

                    ##GET OFFER DETAILS
                    for key, val in cardXPathsDict['offers'].items():
                        if key != 'Offer_D2S_Rate_SelectedOfferCard':
                            s2dCardInfo['offer'][key] = self.utils.get_text(val)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_D2S_VMM_SimOnlyDiscount_Select'])
                    self.utils.wait_loadingCircle()
                    cardNotSelected = str(s2dCardInfo['device']['Device_D2S_Rate_CardToBeSelected']).replace(
                        'SELECTEAZĂ', '')
                    cardSelected = str(s2dCardInfo['device']['Device_D2S_Rate_SelectedCard']).replace('SELECTAT', '')

                    if cardNotSelected == cardSelected:
                        self.utils.log_customStep('Selected Device_Card values VS Device_Card before select values',
                                                  'PASSED [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))
                    else:
                        self.utils.log_customStep('Selected Device_Card values VS Device_Card before select values',
                                                  'FAIL [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected)
                                                  )

                    del s2dCardInfo['device']['Device_D2S_Rate_CardToBeSelected']
                    del s2dCardInfo['device']['Device_D2S_Rate_SelectedCard']

                    self.checkMinimBanners(expandElement='Servicii_D2S')
                    s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard'] = self.utils.get_text(
                        cardXPathsDict['offers']['Offer_D2S_Rate_SelectedOfferCard'])

                    cardNotSelected = str(s2dCardInfo['offer']['Offer_D2S_Rate_CardToBeSelected']).replace('SELECTEAZA',
                                                                                                           '')
                    cardSelected = str(s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard']).replace('SELECTAT', '')

                    if cardNotSelected == cardSelected:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'PASSED [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))
                    else:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'FAIL [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))

                    del s2dCardInfo['offer']['Offer_D2S_Rate_CardToBeSelected']
                    del s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard']

                    for key, value in s2dCardInfo['offer'].items():

                        if key == 'Oferta_D2S_Rate_PerioadaContractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        # elif key =='Oferta_S2D_Rate_Perioada_Contractuala':
                        #     value = value.split('\n')[0].replace('x','')

                        if key == 'Oferta_D2S_Rate_InstallmentValue':
                            cartXpath = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div//section[@class="cart-items-section"]//*[contains(text(),'{}')]"""
                        else:
                            cartXpath = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]"""
                        try:
                            if self.utils.element_exists(cartXpath.format(value)):

                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'FAIL --> [{}]'.format(value))
                        except AttributeError as e:
                            print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                    for key, value in s2dCardInfo['total'].items():
                        try:
                            if key == 'Total_D2S_Rate_TotalLunar':
                                print('THIS IS SHITTETETETETET')
                                try:
                                    print('asdasd123', value)

                                    value = value.split()[:-2]
                                    print('asdasd12223', value)

                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                                for i in value:
                                    if value.index(i) == 0:
                                        logTxt = 'ShoppingCart_TotalLunar_PriceAfterDiscount'
                                        var = """//section[@class="cart-total-items-section"]//*[@class="cart-price" and contains(text(),'{}')]""".format(
                                            i)
                                    else:
                                        logTxt = 'ShoppingCart_TotalLunar_StrikeThroughPrice'
                                        var = """//section[@class="cart-total-items-section"]//*[@class="cart-strikethrough" and contains(text(),'{}')]""".format(
                                            i)

                                    try:
                                        if self.utils.element_exists(var):
                                            self.utils.log_customStep(logTxt,
                                                                                   'PASSED --> [{}]'.format(i))
                                        else:
                                            self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                                    except AttributeError as e:
                                        print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                              '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                              '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                                if len(value) < 2:
                                    print('ShoppingCart_TotalLunar_StrikeThroughPrice',
                                          'FAIL --> ["STRIKETHROUGH VALUE NOT FOUND ON PAGE"]')

                            elif self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[3],
                                                                       'FAIL --> [{}]'.format(value))
                        except AttributeError as e:
                            print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                  '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                  '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 444 ', )

                    # self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    # time.sleep(10)
                    isStrikeThrough = self.utils.element_exists(
                        """//div[@class="cart-menu-wrap"]//*[@class="cart-strikethrough"]""")
                    if isStrikeThrough == False:
                        self.utils.log_customStep('Element cu StrikeThrough Identificat in Cart', 'FAIL')
                    else:
                        self.utils.log_customStep('Element cu StrikeThrough Identificat in Cart', 'PASSED')

                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                elif self.scenarioName_PC == 'OferteConfigurabile_D2S_VMM_SimOnly':
                    print('sceriu sd')

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {
                        'offers': {
                            'Offer_D2S_Rate_CardToBeSelected': """//div[@class="card mobile-card"][position()  >3]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]""",
                            'Offer_D2S_Rate_SelectedOfferCard': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//button[contains(@class,'general_btn button-selected')]//ancestor::div[@class="card mobile-card"][1]""",
                            'Oferta_D2S_Rate_NumeOferta': """//div[@class="card mobile-card"][position()  >3]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                            'Oferta_D2S_Rate_PerioadaContractuala': """//div[@class="card mobile-card"][position()  >3]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                            'Oferta_D2S_Rate_oPrice': """//div[@class="card mobile-card"][position()  >3]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
                            # 'Oferta_D2S_Rate_LastInstallment': '//div[@class="white_tooltip box_tooltip  visible-tooltip"]/div[last()]',
                            # 'Oferta_D2S_Rate_InstallmentValue': """//div[@class="card mobile-card"]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//span[@class="txt_right_side after_discount"]""",
                        },

                        'device': {
                            'Device_D2S_Rate_CardToBeSelected': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 3]',
                            'Device_D2S_Rate_SelectedCard': """//div[@class="card_wrap card-wrap-device device-offer "]//span[contains(text(),'SELECTAT')]//ancestor::div[@class="card_wrap card-wrap-device device-offer "][1]""",
                            'Device_D2S_Rate_Name': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 3]//div[@class="model_phone"]',
                            'Device_D2S_Rate_Upfront': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 3]//span[@class="price"]',
                        },

                        'total': {
                            'Total_D2S_Rate_TotalLunar': """//div[@class="card mobile-card"][position()  >3]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
                            'Device_D2S_Rate_Avans': """//div[@class="card mobile-card"][position()  >3]//*[(contains(@title, 'SIM Only')) or (contains(text(), 'SIM Only'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//span[@class="price"]""",
                        }}

                    time.sleep(self.global_Wait)
                    self.utils.click_js(self.pageConfigDict['Vezi Mai Multe_1stPannel'])
                    self.utils.scroll_to_element(self.pageConfigDict['Device_D2S_VMM_Rate_Selecteaza'],
                                                              initialScroll=3, defaultClick=False)

                    for key, val in cardXPathsDict['device'].items():
                        if key != 'Device_D2S_Rate_SelectedCard':
                            s2dCardInfo['device'][key] = self.utils.get_text(val)

                    # Selecteaza DEVICE

                    self.utils.click_js(self.pageConfigDict['Device_D2S_VMM_Rate_Selecteaza'])
                    self.utils.wait_loadingCircle()

                    self.checkMinimBanners(expandElement='Device_D2S')

                    for key, value in s2dCardInfo['device'].items():
                        if key != 'Device_D2S_Rate_CardToBeSelected':
                            if key == 'Device_D2S_Rate_RateMultiple':
                                try:
                                    value = value.split('\n')[0].replace('x', '')
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):

                                    print(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value))
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[3],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[3],
                                        'FAIL --> [{}]'.format(value))

                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                    # time.sleep(25)
                    # self.checkMinimBanners('Device_D2S')
                    s2dCardInfo['device']['Device_D2S_Rate_SelectedCard'] = self.utils.get_text(
                        cardXPathsDict['device']['Device_D2S_Rate_SelectedCard'])
                    time.sleep(self.global_Wait)

                    # time.sleep(10)
                    # self.utils.click_js(
                    #     """//div[@class="fixed-bundle-offers"][2]//div[@data-automation-id="open-filter"]""")
                    #
                    # self.utils.type_js('//input[@name="searchInput"]', offerName['simonly'])
                    #
                    # self.utils.click_js("//button[@class='primary bold']")
                    self.utils.click_js(self.pageConfigDict['Vezi Mai Multe_2ndPannel'])
                    self.utils.scroll_to_element(self.pageConfigDict['Oferta_D2S_VMM_SimOnly_Select'],
                                                              initialScroll=3, defaultClick=False)

                    self.utils.wait_loadingCircle()
                    time.sleep(20)
                    # self.utils.click_js(self.pageConfigDict['Oferta_D2S_SimOnly_RateMultiple'], customWait=20)
                    # self.utils.wait_loadingCircle()
                    # self.utils.click_js(self.pageConfigDict['Oferta_D2S_SimOnly_LastInstallment'])
                    # self.utils.wait_loadingCircle()

                    ##GET OFFER DETAILS
                    for key, val in cardXPathsDict['offers'].items():
                        if key != 'Offer_D2S_Rate_SelectedOfferCard':
                            s2dCardInfo['offer'][key] = self.utils.get_text(val)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_D2S_VMM_SimOnly_Select'])
                    self.utils.wait_loadingCircle()
                    cardNotSelected = str(s2dCardInfo['device']['Device_D2S_Rate_CardToBeSelected']).replace(
                        'SELECTEAZĂ', '')
                    cardSelected = str(s2dCardInfo['device']['Device_D2S_Rate_SelectedCard']).replace('SELECTAT', '')

                    if cardNotSelected == cardSelected:
                        self.utils.log_customStep('Selected Device_Card values VS Device_Card before select values',
                                                  'PASSED [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))
                    else:
                        self.utils.log_customStep('Selected Device_Card values VS Device_Card before select values',
                                                  'FAIL [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected)
                                                  )

                    del s2dCardInfo['device']['Device_D2S_Rate_CardToBeSelected']
                    del s2dCardInfo['device']['Device_D2S_Rate_SelectedCard']

                    self.checkMinimBanners(expandElement='Servicii_D2S')
                    s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard'] = self.utils.get_text(
                        cardXPathsDict['offers']['Offer_D2S_Rate_SelectedOfferCard'])

                    cardNotSelected = str(s2dCardInfo['offer']['Offer_D2S_Rate_CardToBeSelected']).replace('SELECTEAZA',
                                                                                                           '')
                    cardSelected = str(s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard']).replace('SELECTAT', '')

                    if cardNotSelected == cardSelected:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'PASSED [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))
                    else:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'FAIL [CardNotSelected = [{}] vs [{}] = CardSelected'.format(
                                                      cardNotSelected, cardSelected))

                    del s2dCardInfo['offer']['Offer_D2S_Rate_CardToBeSelected']
                    del s2dCardInfo['offer']['Offer_D2S_Rate_SelectedOfferCard']

                    for key, value in s2dCardInfo['offer'].items():

                        if key == 'Oferta_D2S_Rate_PerioadaContractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        # elif key =='Oferta_S2D_Rate_Perioada_Contractuala':
                        #     value = value.split('\n')[0].replace('x','')

                        if key == 'Oferta_D2S_Rate_InstallmentValue':
                            cartXpath = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div//section[@class="cart-items-section"]//*[contains(text(),'{}')]"""
                        else:
                            cartXpath = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]"""
                        try:
                            if self.utils.element_exists(cartXpath.format(value)):
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'FAIL --> [{}]'.format(value))
                        except AttributeError as e:
                            print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                    for key, value in s2dCardInfo['total'].items():
                        try:
                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[3],
                                                                       'FAIL --> [{}]'.format(value))
                        except AttributeError as e:
                            print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                  '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                  '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 444 ', )

                    # self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    # time.sleep(10)
                    isStrikeThrough = self.utils.element_exists(
                        """//div[@class="cart-menu-wrap"]//*[@class="cart-strikethrough"]""")
                    if isStrikeThrough == False:
                        self.utils.log_customStep('Niciun element cu StrikeThrough Identificat in Cart', 'PASSED')
                    else:
                        self.utils.log_customStep('Niciun element cu StrikeThrough Identificat in Cart', 'FAIL')

                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                elif self.scenarioName_PC == 'OferteConfigurabile_S2D_VMM_Rate_si_Discount':

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {
                        'offers': {
                            'Offer_S2D_Rate_CardToBeSelected': """//div[@class="card mobile-card"][position() > 3]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]""",
                            'Offer_S2D_Rate_SelectedOfferCard': """//div[@class="card mobile-card"]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//button[contains(@class,'general_btn button-selected')]//ancestor::div[@class="card mobile-card"][1]""",
                            'Oferta_S2D_RateDiscount_Perioada_Contractuala': """//div[@class="card mobile-card"][position() > 3]//*[contains(text(),'rate')]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                            'Oferta_S2D_RateDiscount_NumeOferta': """//div[@class="card mobile-card"][position() > 3]//*[contains(text(),'rate')]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                            'Oferta_S2D_RateDiscount_oPrice': """//div[@class="card mobile-card"][position() > 3]//*[contains(text(),'rate')]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
                        },

                        'device': {
                            'Device_Rate_RateMultiple': '//div[@class="card_wrap card-wrap-device  "][position() > 3]//span[@class="link-title blue bold"]',
                            # 'Device_Rate_LastInstallment': '//div[@class="white_tooltip box_tooltip  visible-tooltip"]/div[last()]',
                            'Device_Rate_Name': '//div[@class="card_wrap card-wrap-device  "][position() > 3]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="model_phone"]',
                            'Device_Rate_Upfront': '//div[@class="card_wrap card-wrap-device  "][position() > 3]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                            'Device_Rate_InstallmentValue': '//div[@class="card_wrap card-wrap-device  "][position() > 3]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify two_sides undefined"]//span[@class="txt_right_side after_discount"]',
                        },

                        'total': {
                            '_OfferPriceDiscount': '//div[@class="card_wrap card-wrap-device  "][position() > 3]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="Overview with_tooltip"]//span[@class="txt_right_side after_discount"]',
                            'Total_SimOnly_TotalLunar': """//div[@class="card_wrap card-wrap-device  "][position() > 3]//div[@class="card-footer card-footer-button"]//div[@class="price total-device-price"]""",
                            'Device_RateDisc_Avans': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                        }}

                    time.sleep(self.global_Wait)
                    self.utils.click_js(self.pageConfigDict['Vezi Mai Multe_1stPannel'])
                    self.utils.scroll_to_element(
                        self.pageConfigDict['Oferta_S2D_RateDiscVMM_Selecteaza'], initialScroll=7,
                        defaultClick=False)

                    for key, val in cardXPathsDict['offers'].items():
                        if key != "Offer_S2D_Rate_SelectedOfferCard":
                            s2dCardInfo['offer'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_S2D_RateDiscVMM_Selecteaza'])
                    self.utils.wait_loadingCircle()

                    time.sleep(self.global_Wait)
                    self.utils.click_js(self.pageConfigDict['Vezi Mai Multe_2ndPannel'])
                    self.utils.scroll_to_element(self.pageConfigDict['Device_RateDiscVMM_Selecteaza'],
                                                              initialScroll=7, defaultClick=False)

                    self.utils.click_js(self.pageConfigDict['Device_RateDiscVMM_RateMultiple'],
                                                     customWait=20)
                    self.utils.wait_loadingCircle()
                    self.utils.click_js(self.pageConfigDict['Device_RateDiscVMM_LastInstallment'])
                    self.utils.wait_loadingCircle()

                    for key, val in cardXPathsDict['device'].items():
                        print('this is val ', val)
                        s2dCardInfo['device'][key] = self.utils.get_text(val)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Device_RateDiscVMM_Selecteaza'])
                    self.utils.wait_loadingCircle()

                    self.checkMinimBanners(expandElement='Servicii')
                    time.sleep(3)
                    s2dCardInfo['offer']['Offer_S2D_Rate_SelectedOfferCard'] = self.utils.get_text(
                        cardXPathsDict['offers']['Offer_S2D_Rate_SelectedOfferCard'])

                    self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    if str(s2dCardInfo['offer']['Offer_S2D_Rate_CardToBeSelected']).replace('SELECTEAZA', '') == str(
                            s2dCardInfo['offer']['Offer_S2D_Rate_SelectedOfferCard']).replace('SELECTAT', ''):
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'PASSED')
                    else:
                        self.utils.log_customStep('Selected Offer_Card values VS Offer_Card before select values',
                                                  'FAIL')

                    del s2dCardInfo['offer']['Offer_S2D_Rate_CardToBeSelected']
                    del s2dCardInfo['offer']['Offer_S2D_Rate_SelectedOfferCard']

                    for key, value in s2dCardInfo['offer'].items():
                        if key == 'Oferta_S2D_RateDiscount_Perioada_Contractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        elif key == 'Oferta_S2D_RateDiscount_oPrice':
                            try:
                                value = value.split()[:-2]
                                for i in value:
                                    if value.index(i) == 0:
                                        logTxt = 'ShoppingCart_OfferInfo_PriceAfterDiscount'
                                    else:
                                        logTxt = 'ShoppingCart_OfferInfo_StrikeThroughPrice'

                                    if self.utils.element_exists(
                                            """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                                i)):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                            except (AttributeError, ValueError) as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ')
                            except TypeError:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_Price',
                                                                       'FAIL --> [Offer Price nu a fost identificat pe Cartonas]')

                        elif self.utils.element_exists(
                                """//section[@class="cart-items-section"][1]//*[contains(text(),"{}")]""".format(
                                    value)):
                            print("""//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                value))
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'PASSED --> [{}]'.format(value))
                        else:
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'FAIL --> [{}]'.format(value))

                        if self.utils.element_exists(
                                """//div[@class="item-wrap-prices "][1]//div[@class="cart-price cart-no-value" and contains(text(), '- - -')]""",
                                enable_Logging=False):
                            self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans', 'PASSED --> [---]')
                        else:
                            self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans',
                                                                   'FAIL --> [{}]'.format(
                                                                       self.utils.get_text(
                                                                           """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]/div[@class="items-wrap flex"]//div[@class="item-wrap-prices "][1]""")))

                    self.checkMinimBanners(expandElement='Device')

                    for key, value in s2dCardInfo['device'].items():
                        if key == 'Device_Rate_RateMultiple':
                            try:
                                print(value, 'rarsarasrsarsa')
                                value = value.split('\n')[0].replace('x', '')
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):

                                    print(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value))
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'FAIL --> [{}]'.format(value))

                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        elif key == 'Device_SimOnly_Discount':
                            try:
                                print('we here')

                                value = value.split()
                            except AttributeError as e:
                                print('SimOnly si discount Exceptie:\nAttribute error exception for : ', value,
                                      "EXCEPTION --> ", e)

                            print('val pe scenariu este', value)
                            for i in value:
                                if value.index(i) == 0:
                                    logTxt = 'ShoppingCart_DeviceInfoOffer_PriceAfterDiscount'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-price" and contains(text(),'{}')]""".format(
                                        i)
                                else:
                                    logTxt = 'ShoppingCart_DeviceInfoOffer_StrikeThroughPrice'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-strikethrough" and contains(text(),'{}')]""".format(
                                        i)

                                try:
                                    if self.utils.element_exists(var):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                          '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                          '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )
                        else:

                            try:
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                                value)):

                                    print(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value))
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'FAIL --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                    for key, value in s2dCardInfo['total'].items():

                        if key == 'Device_RateDisc_Avans':
                            value = value.replace('€0', '€')
                            time.sleep(0.5)
                            print('total check is',
                                  """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                      value))
                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'FAIL --> [{}]'.format(value))

                        elif key == 'Total_SimOnly_TotalLunar':
                            try:
                                value = value.split()[:-2]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                            for i in value:
                                if value.index(i) == 0:
                                    logTxt = 'ShoppingCart_TotalLunar_PriceAfterDiscount'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-price" and contains(text(),'{}')]""".format(
                                        i)
                                else:
                                    logTxt = 'ShoppingCart_TotalLunar_StrikeThroughPrice'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-strikethrough" and contains(text(),'{}')]""".format(
                                        i)

                                try:
                                    if self.utils.element_exists(var):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                          '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                          '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                        elif key == '_OfferPriceDiscount':
                            try:
                                value = value.split()
                                for i in value:
                                    if value.index(i) == 0:
                                        var = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[@class="cart-price" and contains(text(),'{}')]""".format(
                                            i)
                                        logTxt = 'ShoppingCart_DeviceInfoOffer_PriceAfterDiscount'

                                    else:
                                        var = """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[@class="cart-strikethrough" and contains(text(),'{}')]""".format(
                                            i)
                                        logTxt = 'ShoppingCart_DeviceInfoOffer_StrikeThroughPrice'

                                    if self.utils.element_exists(var):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))

                            except (AttributeError, ValueError) as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY')
                            except TypeError:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_Price',
                                                                       'FAIL --> [Offer Price nu a fost identificat pe Cartonas]')

                        else:

                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'FAIL --> [{}]'.format(value))

                        # print('s2dcardinfo', key, ' ', value)
                        # print(
                        #     """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                        #         value))

                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                elif self.scenarioName_PC == 'OferteConfigurabile_S2D_VMM_Rate':
                    print('sceriu sd')

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {'offers': {
                        'Oferta_S2D_Rate_NumeOferta': """//div[@class="card mobile-card"][position() > 3]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                        'Oferta_S2D_Rate_PerioadaContractuala': """//div[@class="card mobile-card"][position() > 3]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                        'Oferta_S2D_Rate_oPrice': """//div[@class="card mobile-card"][position() > 3]//*[(contains(@title, 'rate')) or (contains(text(), 'rate'))]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
                    },
                        'device': {
                            'Device_Rate_RateMultiple': '//div[@class="card_wrap card-wrap-device  "][position() > 3]//span[@class="link-title blue bold"]',
                            'Device_Rate_Name': '//div[@class="card_wrap card-wrap-device  "][position() >3]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="model_phone"]',
                            'Device_Rate_Upfront': '//div[@class="card_wrap card-wrap-device  "][position() >3]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                            'Device_Rate_InstallmentValue': '//div[@class="card_wrap card-wrap-device  "][position() >3]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="txt_right_side after_discount"]',
                            'Device_Rate_Stock': '//div[@class="card_wrap card-wrap-device  "][position() >3]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify final_p"]/span[2]',
                        },
                        'total': {
                            'Total_S2D_Rate_TotalLunar': """//div[@class="card_wrap card-wrap-device  "][position() >3]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="price total-device-price"]/h4""",
                            'Device_Rate_Avans': '//div[@class="card_wrap card-wrap-device  "][position() >3]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                        }}
                    time.sleep(self.global_Wait)
                    self.utils.click_js(self.pageConfigDict['Vezi Mai Multe_1stPannel'])
                    self.utils.scroll_to_element(
                        self.pageConfigDict['Oferta_S2D_VMM_Rate_Select'], initialScroll=2,
                        defaultClick=False)
                    self.utils.wait_loadingCircle()

                    for key, val in cardXPathsDict['offers'].items():
                        s2dCardInfo['offer'][key] = self.utils.get_text(val)
                    self.utils.click_js(self.pageConfigDict['Oferta_S2D_VMM_Rate_Select'])
                    time.sleep(self.global_Wait)
                    self.utils.click_js(self.pageConfigDict['Vezi Mai Multe_2ndPannel'])
                    self.utils.scroll_to_element(
                        self.pageConfigDict['Device_VMM_Rate_Selecteaza'], initialScroll=2,
                        defaultClick=False)
                    self.utils.click_js(self.pageConfigDict['Device_VMM_Rate_RateMultiple'], customWait=20)
                    self.utils.wait_loadingCircle()
                    time.sleep(2)
                    self.utils.click_js(self.pageConfigDict['Device_VMM_Rate_LastInstallment'])
                    self.utils.wait_loadingCircle()

                    for key, val in cardXPathsDict['device'].items():
                        s2dCardInfo['device'][key] = self.utils.get_text(val)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Device_VMM_Rate_Selecteaza'])
                    self.utils.wait_loadingCircle()

                    self.checkMinimBanners(expandElement='Servicii')

                    self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    for key, value in s2dCardInfo['offer'].items():
                        if key == 'Oferta_S2D_Rate_PerioadaContractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        # elif key =='Oferta_S2D_Rate_Perioada_Contractuala':
                        #     value = value.split('\n')[0].replace('x','')
                        print('offervals ', value)
                        print(
                            """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                value))
                        try:
                            if self.utils.element_exists(
                                    """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_' + key.split('_')[3],
                                                                       'FAIL --> [{}]'.format(value))
                        except AttributeError as e:
                            print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                    self.checkMinimBanners(expandElement='Device')

                    for key, value in s2dCardInfo['device'].items():
                        if key != 'Device_Rate_Stock':
                            if key == 'Device_Rate_RateMultiple':
                                try:
                                    value = value.split('\n')[0].replace('x', '')
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e)
                            try:
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):

                                    print(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value))
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'FAIL --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                        else:
                            pass

                    for key, value in s2dCardInfo['total'].items():
                        try:
                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                        value)):
                                print(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                        value))
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[2],
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_TotalInfo_' + key.split('_')[2],
                                                                       'FAIL --> [{}]'.format(value))
                        except AttributeError as e:
                            print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                  '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                  '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 444 ', )

                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])

                elif self.scenarioName_PC == 'OferteConfigurabile_S2D_VMM_SimOnly_si_Discount':
                    print('sceriu sdonl123y')

                    s2dCardInfo = defaultdict(dict)

                    cardXPathsDict = {
                        'offers': {
                            'Oferta_SimOnly_Discount_Perioada_Contractuala': """//div[@class="card mobile-card"][position() > 3]//*[(contains(text(),'SIM Only'))]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
                            'Oferta_SimOnly_Discount_NumeOferta': """//div[@class="card mobile-card"][position() > 3]//*[(contains(text(),'SIM Only'))]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
                            'Oferta_SimOnly_Discount_oPrice': """//div[@class="card mobile-card"][position() > 3]//*[(contains(text(),'SIM Only'))]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
                        },
                        'device': {
                            'Device_SimOnly_Name': '//div[@class="card_wrap card-wrap-device  "][position() > 3]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="model_phone"]',
                            'Device_SimOnly_Price': '//div[@class="card_wrap card-wrap-device  "][position() > 3]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
                            'Device_SimOnly_Discount': '//div[@class="card_wrap card-wrap-device  "][position() > 3]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="txt_right_side after_discount"]',
                            # 'Device_SimOnly_Stock': '//div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify final_p"]/span[2]',
                            # 'Device_Rate_OfferPriceDiscount': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="Overview with_tooltip"]//span[@class="txt_right_side after_discount"]',

                        },
                        'total': {
                            'Total_SimOnly_TotalLunar': """//div[@class="card_wrap card-wrap-device  "][position() > 3]//div[@class="price total-device-price"]""",
                            'Device_SimOnly_Avans': '//div[@class="card_wrap card-wrap-device  "][position() > 3]//span[@class="price"]'
                        }}
                    time.sleep(self.global_Wait)
                    print('am intrat corect')
                    self.utils.click_js(self.pageConfigDict['Vezi Mai Multe_1stPannel'])
                    self.utils.scroll_to_element(
                        self.pageConfigDict['Oferta_VMM_SimOnly_Discount_Selecteaza'], initialScroll=7,
                        defaultClick=False)
                    for key, val in cardXPathsDict['offers'].items():
                        print(self.utils.get_text(val))
                        s2dCardInfo['offer'][key] = self.utils.get_text(val)

                    self.utils.click_js(self.pageConfigDict['Oferta_VMM_SimOnly_Selecteaza'])
                    self.utils.wait_loadingCircle()
                    time.sleep(self.global_Wait)
                    self.utils.click_js(self.pageConfigDict['Vezi Mai Multe_2ndPannel'])

                    self.utils.scroll_to_element(self.pageConfigDict['Device_VMM_SimOnly_Selecteaza'],
                                                              initialScroll=7, defaultClick=False)

                    for key, value in cardXPathsDict['device'].items():
                        s2dCardInfo['device'][key] = self.utils.get_text(value)

                    for key, val in cardXPathsDict['total'].items():
                        s2dCardInfo['total'][key] = self.utils.get_text(val)

                    time.sleep(4)
                    self.utils.click_js(self.pageConfigDict['Device_VMM_SimOnly_Selecteaza'],
                                                     customWait=20)
                    self.utils.wait_loadingCircle()

                    self.checkMinimBanners(expandElement='Servicii')

                    self.utils.click_js(self.pageConfigDict['ShoppingCart'])

                    for key, value in s2dCardInfo['offers'].items():
                        if key == 'Oferta_SimOnly_Discount_Perioada_Contractuala':
                            try:
                                value = value.split(' LUNI')[0]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        elif key == 'Oferta_SimOnly_Discount_oPrice':
                            try:
                                value = value.split()[:-2]
                                for i in value:
                                    if value.index(i) == 0:
                                        logTxt = 'ShoppingCart_OfferInfo_PriceAfterDiscount'
                                    else:
                                        logTxt = 'ShoppingCart_OfferInfo_StrikeThroughPrice'

                                    if self.utils.element_exists(
                                            """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                                i)):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))

                            except (AttributeError, ValueError) as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ')
                            except TypeError:
                                self.utils.log_customStep('ShoppingCart_OfferInfo_Price',
                                                                       'FAIL --> [Offer Price nu a fost identificat pe Cartonas]')

                        elif self.utils.element_exists(
                                """//section[@class="cart-items-section"][1]//*[contains(text(),"{}")]""".format(
                                    value)):
                            print("""//section[@class="cart-total-items-section"]//*[contains(text(),"{}")]""".format(
                                value))
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'PASSED --> [{}]'.format(value))
                        else:
                            self.utils.log_customStep('ShoppingCart_OfferInfo',
                                                                   'FAIL --> [{}]'.format(value))

                    if self.utils.element_exists(
                            """//div[@class="item-wrap-prices "][1]//div[@class="cart-price cart-no-value" and contains(text(), '- - -')]""",
                            enable_Logging=False):
                        self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans', 'PASSED --> [---]')
                    else:
                        self.utils.log_customStep('ShoppingCart_OfferInfo_inAvans', 'FAIL --> [{}]'.format(
                            self.utils.get_text(
                                """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]/div[@class="items-wrap flex"]//div[@class="item-wrap-prices "][1]""")))

                    self.checkMinimBanners(expandElement='Device')

                    for key, value in s2dCardInfo['device'].items():
                        print('key is ', key, 'the val is ', value)
                        if key == 'Device_Rate_multiple':
                            try:
                                value = value.split('\n')[0].replace('x', '')
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'FAIL --> [{}]'.format(value))

                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                        elif key == 'Device_SimOnly_Discount':
                            try:
                                print('we here')

                                value = value.split()
                            except AttributeError as e:
                                print('SimOnly si discount Exceptie:\nAttribute error exception for : ', value,
                                      "EXCEPTION --> ", e)

                            print('val pe scenariu este', value)
                            for i in value:
                                if value.index(i) == 0:
                                    logTxt = 'ShoppingCart_DeviceInfoOffer_PriceAfterDiscount'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-price" and contains(text(),'{}')]""".format(
                                        i)
                                else:
                                    logTxt = 'ShoppingCart_DeviceInfoOffer_StrikeThroughPrice'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-strikethrough" and contains(text(),'{}')]""".format(
                                        i)

                                try:
                                    if self.utils.element_exists(var):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                          '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                          '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                        else:
                            try:
                                if self.utils.element_exists(
                                        """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/div[1]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                                            value)):
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'PASSED --> [{}]'.format(value))
                                else:
                                    self.utils.log_customStep(
                                        'ShoppingCart_DeviceInfo_' + key.split('_')[2],
                                        'FAIL --> [{}]'.format(value))
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                      '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                      '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                    for key, value in s2dCardInfo['total'].items():
                        if key == 'Device_SimOnly_Avans':
                            value = value.replace('€0', '€')

                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total_Avans',
                                                                       'FAIL --> [{}]'.format(value))

                        elif key == 'Total_SimOnly_TotalLunar':
                            try:
                                print("raaaaaaaaaaaaaaaaaaaaaaaaz", value)
                                value = value.split()[:-2]
                            except AttributeError as e:
                                print('Attribute error exception for : ', value, "EXCEPTION --> ", e)

                            for i in value:
                                if value.index(i) == 0:
                                    logTxt = 'ShoppingCart_TotalLunar_PriceAfterDiscount'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-price" and contains(text(),'{}')]""".format(
                                        i)
                                else:
                                    logTxt = 'ShoppingCart_TotalLunar_StrikeThroughPrice'
                                    var = """//section[@class="cart-total-items-section"]//*[@class="cart-strikethrough" and contains(text(),'{}')]""".format(
                                        i)

                                try:
                                    if self.utils.element_exists(var):
                                        self.utils.log_customStep(logTxt,
                                                                               'PASSED --> [{}]'.format(i))
                                    else:
                                        self.utils.log_customStep(logTxt, 'FAIL --> [{}]'.format(i))
                                except AttributeError as e:
                                    print('Attribute error exception for : ', value, "EXCEPTION --> ", e,
                                          '\nThis will affect LOGGING for scenarioPC : ', self.scenarioName_PC,
                                          '\n AT SHOPPINGCART VALIDATION KEY creation referenced at line 427 ', )

                        else:

                            if self.utils.element_exists(
                                    """//section[@class="cart-total-items-section"]//*[contains(text(),'{}')]""".format(
                                        value)):
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'PASSED --> [{}]'.format(value))
                            else:
                                self.utils.log_customStep('ShoppingCart_Total',
                                                                       'FAIL --> [{}]'.format(value))

                        # print('s2dcardinfo', key, ' ', value)
                        # print(
                        #     """//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section[@class="cart-items-section"]//*[contains(text(),'{}')]""".format(
                        #         value))
                    self.utils.click_js(self.pageConfigDict['ShoppingCart_Continua'])


if __name__ == '__main__':
   testObj = page_configureaza_cbu_oms_Achizitie()

from SRC.utils_pkg import utilities


class sendToVOS_Layer:

    utils = utilities.utilities()

    def __init__(self, testCaseName=None, browser=None, inputDictionary=None):
        self.inputDictionary = inputDictionary
        self.browser = browser
        self.stvDict = {'submit': "//button[@data-automation-id='summary-configuration-submit-btn']",
                        'email' : "//input[@name='email']",
                        'contact': "//input[@name='phone']",
                        'send_to_vos': "//button[@data-automation-id='validate-contact-submit-button']"
                        }
        sendToVOS_Layer.utils.__init__(browser=self.browser, methodName='test_page_SendToVOS', numeScenariu=testCaseName, xpathDict=self.stvDict)


    def test_sendToVOS_Layer_sendToVOS(self):
        try:
            sendToVOS_Layer.utils.click_js(xpath=self.stvDict['submit'])
            sendToVOS_Layer.utils.type_js(xpath=self.stvDict['email'], value='laurentiu.faget1@vodafone.com')
            sendToVOS_Layer.utils.type_js(xpath=self.stvDict['contact'], value='730044448')
            sendToVOS_Layer.utils.is_element_Enabled(self.stvDict['send_to_vos'])

        except Exception as e:
            page_login.utils.log_customStep('INTERNAL_CODE_ERROR', 'REASON = [{}]'.format(e))
        finally:
            return (sendToVOS_Layer.utils.VDFloggingObj.method_log)


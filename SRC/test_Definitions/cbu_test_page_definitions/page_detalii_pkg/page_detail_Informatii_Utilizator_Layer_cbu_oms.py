from SRC.utils_pkg import utilities
import datetime
import random
import calendar, bcolors, os, sys
from SRC.test_Definitions.test_baaseline_Classes.page_Detail_Informatii_Utilizator_Layer_baseline import Informatii_Utilizator_Layer_baseline

class Informatii_Utilizator_Layer_cbu_oms(Informatii_Utilizator_Layer_baseline):


    def __init__(self, scenarioName_isTitular=None, testCaseName=None, browser=None, inputDictionary=None):

        Informatii_Utilizator_Layer_baseline.__init__(self, scenarioName_isTitular, testCaseName, browser, inputDictionary)

    def test_page_Detalil_Informatii_Utilizator_Layer(self):
        try:
            if self.scenarioName_isTitular == 'Utilizator_titular_Yes':
                self.utils.check_button(xpath='//input[@data-automation-id="apply-personal-details-to-contact"]',
                                                                       xpathCLick=self.utl_Titular['utilizator_titular'],
                                                                       value='value')

            elif self.scenarioName_isTitular == 'Utilizator_titular_No_Minor_No':

                self.utils.logScenariu('Utilizator titular = false')
                self.utils.uncheck_button(xpath='//input[@data-automation-id="apply-personal-details-to-contact"]',
                                                                         xpathCLick=self.utl_Titular['utilizator_titular'],
                                                                         value='value')
                self.utils.logScenariu('Click on minor Nu')
                self.utils.click_js(self.utl_Titular['minor_nu'])

                # Completeaza campurile
                self.utils.dropDownSelector_Index(xpath=self.utl_Titular['titlu'], index=2)
                self.utils.type_js(self.utl_Titular['nume'], value='Vasile')
                self.utils.type_js(self.utl_Titular['prenume'], value='Namazescu')
                self.utils.type_js(self.utl_Titular['contact_email'], value='vasile.namazescu@email.com')
                self.utils.type_js(self.utl_Titular['contact_phone'], value='721321321')

            elif self.scenarioName_isTitular == 'Utilizator_titular_No_Minor_Yes':

                self.utils.uncheck_button(xpath='//input[@data-automation-id="apply-personal-details-to-contact"]',
                                                                         xpathCLick=self.utl_Titular['utilizator_titular'],
                                                                         value='value')

                self.utils.logScenariu('Click on minor Da')
                self.utils.click_js(self.utl_Titular['minor_da'])

                # Completeaza campurile
                self.utils.wait_loadingCircle()
                self.utils.dropDownSelector_Index(xpath=self.utl_Titular['titlu'], index=2)
                self.utils.type_js(self.utl_Titular['nume'], value='Vasile')
                self.utils.type_js(self.utl_Titular['prenume'], value='Namazescu')
                self.utils.type_js(self.utl_Titular['contact_email'], value='vasile.namazescu@email.com')
                self.utils.type_js(self.utl_Titular['contact_phone'], value='721321321')

                #data nastere if minor=da
                now = datetime.datetime.now()

                self.utils.dropDownSelector_Index(xpath=self.utl_Titular['ziua_nasterii'], index=random.randint(1, calendar.monthrange(now.year, now.month)[1]))
                self.utils.dropDownSelector_Index(xpath=self.utl_Titular['luna_nasterii'], index=random.randint(1, 12))
                self.utils.dropDownSelector_Index(xpath=self.utl_Titular['anul_nasterii'], index=random.randint(1, 25))

        except Exception as e:
            e = str(e).replace("\n", "")
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(f'{bcolors.WARN}{bcolors.BLUEIC}=====================================\n',
                  f'Error name => ["{exc_type}"]\n \tIn file => ["{fname}"]\n \t\tAt line => ["{exc_tb.tb_lineno}"]\n \t\t\tStack trace => ["{self.utils.format_stacktrace()}"]',
                  f'\n-------------------------------------\n{bcolors.ENDC}')
            Informatii_Utilizator_Layer_cbu_oms.utils.log_customStep('INTERNAL_CODE_ERROR', 'REASON = [{}]'.format(e))
        finally:
            return (self.utils.VDFloggingObj.method_log)

from SRC.utils_pkg import utilities
import datetime
from selenium.common.exceptions import NoSuchElementException
import time
from SRC.test_Definitions.test_baaseline_Classes.page_Detail_UtilizatorSiNumar_baseline import UtilizatorSiNumarLayer_baseline
import bcolors, sys, os

class UtilizatorSiNumarLayer_cbu_oms(UtilizatorSiNumarLayer_baseline):

    def __init__(self, inputDictionary=None, scenarioName_SendNotif=None, scenarioName_isTitular=None,
                 testCaseName=None, abNouType=None, scenarioName_Utl_Nr=None, browser=None, tip_contract=None,
                 scenarioName_GDPR=None, scenarioContNou=None, tipServiciu=None):

        UtilizatorSiNumarLayer_baseline.__init__(self, inputDictionary, scenarioName_SendNotif, scenarioName_isTitular, testCaseName, abNouType,
                         scenarioName_Utl_Nr, browser, tip_contract, scenarioName_GDPR, scenarioContNou, tipServiciu)


    def test_page_Detail_UtilizatorSiNumar(self):
        try:
            if self.abNouType == 'GA':

                self.utils.logScenariu('Flow GA start abNouType == "GA"')

                if self.scenarioName_Utl_Nr == 'Scenariu_Numar_Special':

                    self.utils.logScenariu(self.scenarioName_Utl_Nr)

                    # Alege numar actions
                    self.utils.logScenariu('click alege numar')
                    self.utils.click_js(self.util_nr['alege_numar'])

                    self.utils.wait_loadingCircle()

                    self.utils.logScenariu('click Numar Special')
                    self.utils.click_js(self.util_nr['numar_special'])

                    self.utils.logScenariu('Introdu search cifre 22')
                    alege_no_spl = self.utils.click_js(self.util_nr['first_digits'])
                    dig = 22
                    # type 22 in first digit
                    self.utils.type_js(self.util_nr['first_digits'], dig)
                    cauta_no_spl = self.utils.click_js(self.util_nr['cauta'])

                    try:
                        while self.browser.find_element_by_xpath("//*[contains(text(),'Nu au fost')]"):
                            input_no_spl_c = self.utils.click_js(self.util_nr['sterge'])
                            dig += 1
                            self.utils.type_js(self.util_nr['first_digits'], dig)
                            cauta_no_spl = self.utils.click_js(self.util_nr['cauta'])
                            time.sleep(2)
                    except NoSuchElementException:
                        print('Numar special a fost gasit cu first digit = {}'.format(dig))

                    selectedNumber = self.utils.get_text("""//div[@class='number-box selected']""")
                    self.utils.click_js(self.util_nr['continua_alegeNr'])
                    time.sleep(3)
                    sqResponse = self.utils.retryAPI(API='simpleAPICall', URL=self.utils.getTempValue('URL')['updateRM'].format(str(selectedNumber)).replace(' ', ''))
                    print('sqResponse', sqResponse)

                    stopVar = 0
                    # Get sim and type it actions
                    # sim = self.utils.retryAPI(API='SIM', URL=self.utils.getTempValue('URL')['getICCID'])
                    # self.utils.type_js(xpath=self.util_nr['newSim'], value=str(sim))
                    # time.sleep(1)
                    # self.utils.click_js("""//button[@class='info_tooltip']""", js=False)
                    # time.sleep(1)
                    #
                    # while self.utils.element_exists(
                    #         "//span[contains(text(),'Rezervarea SIM nu s-a realizat, introdu un alt SIM')]",
                    #         enable_Logging=False):
                    #     sim = self.utils.retryAPI(API='SIM', URL=self.utils.getTempValue('URL')['getICCID'])
                    #     # sim = 3424324213121313213
                    #     self.utils.type_js(xpath=self.util_nr['newSim'], value=str(sim))
                    #     self.utils.click_js("""//button[@class='info_tooltip']""",
                    #                                               enable_Logging=False)
                    #     self.utils.wait_loadingCircle()
                    #     stopVar += 1
                    #     if stopVar >= 3:
                    #         self.utils.log_customStep('Validate newSIM after retry', 'FAIL')
                    #         break
                    #
                    # # Alege urmatoarea data de activare actions
                    # click_date = self.utils.click_js(self.util_nr['data_activare'])
                    # day_to_be_selected = (datetime.datetime.today() + datetime.timedelta(days=1))
                    # customDate = self.custom_strftime('%B {S}, %Y', day_to_be_selected)
                    # self.utils.click_js(
                    #     '//div[@aria-label="Choose {}, {}"]'.format(day_to_be_selected.strftime('%A'), customDate), enable_Logging=False)


                elif self.scenarioName_Utl_Nr == 'Scenariu_Numar_Random':
                    # select one random number
                    self.utils.click_js(self.util_nr['alege_numar'])

                    self.utils.wait_loadingCircle()
                    self.utils.logScenariu('click un numar din cele propuse')
                    selecteaza_un_numar = self.utils.click_js(self.util_nr['first_nr_disponibil'])
                    self.utils.logScenariu('click continua')
                    selectedNumber = self.utils.get_text("""//div[@class='number-box selected']""")
                    self.utils.click_js(self.util_nr['continua_alegeNr'])
                    time.sleep(3)
                    sqResponse = self.utils.retryAPI(API='simpleAPICall', URL=self.utils.getTempValue('URL')['updateRM'].format(str(selectedNumber)).replace(' ', ''))
                    print('sqResponse', sqResponse)
                    # Get sim and type it actions
                    stopVar=0
                    sim = self.utils.callRESTApi(self.utils.getTempValue('URL')['getICCID'], key='ICCID')
                    self.utils.type_js(xpath=self.util_nr['newSim'], value=str(sim))
                    time.sleep(1)
                    self.utils.click_js("""//button[@class='info_tooltip']""", js=False)
                    time.sleep(1)                #type new sim until one is valid
                    while self.utils.element_exists(
                            "//span[contains(text(),'Rezervarea SIM nu s-a realizat, introdu un alt SIM')]",
                            enable_Logging=False):
                        sim = self.utils.retryAPI(API='SIM', URL=self.utils.getTempValue('URL')['getICCID'])
                        # sim = 3424324213121313213
                        self.utils.type_js(xpath=self.util_nr['newSim'], value=str(sim))
                        self.utils.click_js("""//button[@class='info_tooltip']""",
                                                                  enable_Logging=False)
                        self.utils.wait_loadingCircle()
                        stopVar += 1
                        if stopVar >= 3:
                            self.utils.log_customStep('Validate newSIM after retry', 'FAIL')
                            break

                    # Alege urmatoarea data de activare actions
                    click_date = self.utils.click_js(self.util_nr['data_activare'])
                    day_to_be_selected = (datetime.datetime.today() + datetime.timedelta(days=1))
                    customDate = self.custom_strftime('%B {S}, %Y', day_to_be_selected)
                    #select activation Date via custom workdays alg
                    isActDateSelected = self.utils.click_js(
                        '//div[@aria-label="Choose {}, {}"]'.format(day_to_be_selected.strftime('%A'), customDate), enable_Logging=False)
                    if  isActDateSelected:
                        self.utils.log_customStep('Select_Data Activare', 'PASSED')
                    elif isActDateSelected == False:
                        self.utils.log_customStep('Select_Data Activare', 'FAILED')

            elif self.abNouType == 'MNP':
                self.test_page_Notification_On_UtilizatorSiNumar()
                print('WTF', self.tipServiciu, self.scenarioName_Utl_Nr, self.tip_contract)

                if self.tipServiciu == 'PREPAID':
                    self.utils.type_js(xpath=self.util_nr['SIM_Existent'], value='8123991233312')
                elif self.tipServiciu == 'POSTPAID':
                    self.utils.type_js(xpath=self.util_nr['COD_CLIENT_DONOR'], value='8123991233312')
                else:
                    print('Nu am intrat pe tip serviciu')

                # TIP CONTRACT: tip_contract = 'permanent' or 'asteapta' Work in progess
                self.utils.wait_loadingCircle()
                # type cod client donor
                if self.tip_contract == 'permanent':
                    self.utils.click_js(xpath="//div[@class='wrap-checkbox']//input[@name='contractType']", enable_Logging=False)

                    if self.scenarioName_Utl_Nr == 'TipContract_Permanet_Numar_Random':
                        print('WHO DIS')
                        # Alege urmatoarea data de activare actions
                        click_date = self.utils.click_js(self.util_nr['data_activare'])
                        day_to_be_selected = (datetime.datetime.today() + datetime.timedelta(days=1))
                        customDate = self.custom_strftime('%B {S}, %Y', day_to_be_selected)
                        self.utils.click_js(
                            '//div[@aria-label="Choose {}, {}"]'.format(day_to_be_selected.strftime('%A'), customDate))

                        # select Min Date
                        daysPlus = 4
                        isBusinessDay, bDayReturned = self.utils.is_business_day(datetime.datetime.today(),
                                                                                                       daysPlus)
                        while isBusinessDay is not True:
                            daysPlus += 1
                            isBusinessDay, bDayReturned = self.utils.is_business_day(datetime.datetime.today(),
                                                                                                           daysPlus)
                            # Choose Sunday, August 2nd, 2020
                        self.utils.click_js(self.util_nr['data_min_portare'])
                        customDate = self.custom_strftime('%B {S}, %Y', bDayReturned)
                        self.utils.click_js(
                            '//div[@aria-label="Choose {}, {}"]'.format(bDayReturned.strftime('%A'), customDate))

                        # select MAX date
                        daysPlus = 7
                        isBusinessDay, bDayReturned = self.utils.is_business_day(datetime.datetime.today(),
                                                                                                       daysPlus)
                        while isBusinessDay is not True:
                            daysPlus += 1
                            isBusinessDay, bDayReturned = self.utils.is_business_day(datetime.datetime.today(),
                                                                                                           daysPlus)
                        # Choose Sunday, August 2nd, 2020
                        self.utils.click_js(self.util_nr['data_max_portare'])
                        customDate = self.custom_strftime('%B {S}, %Y', bDayReturned)
                        self.utils.click_js(
                            '//div[@aria-label="Choose {}, {}"]'.format(bDayReturned.strftime('%A'), customDate))

                        # select one random number
                        self.utils.logScenariu(self.scenarioName_Utl_Nr)
                        self.utils.logScenariu('click alege numar')
                        self.utils.click_js(self.util_nr['change_number'])

                        self.utils.wait_loadingCircle()
                        self.utils.logScenariu('click un numar din cele propuse')
                        self.utils.click_js(self.util_nr['first_nr_disponibil'])
                        self.utils.logScenariu('click continua')
                        selectedNumber = self.utils.get_text("""//div[@class='number-box selected']""")
                        self.utils.click_js(self.util_nr['continua_alegeNr'])
                        time.sleep(3)
                        sqResponse = self.utils.retryAPI(API='simpleAPICall',
                                                         URL=self.utils.getTempValue('URL')['updateRM'].format(
                                                             str(selectedNumber)).replace(' ', ''))
                        print('sqResponse', sqResponse)
                        # Get sim and type it actions
                        stopVar=0
                        sim = self.utils.retryAPI(API='SIM', URL=self.utils.getTempValue('URL')['getICCID'])
                        #sim = 3424324213121313213
                        self.utils.type_js(xpath=self.util_nr['newSim'], value=str(sim))
                        self.utils.click_js("""//button[@class='info_tooltip']""", enable_Logging=False)
                        self.utils.wait_loadingCircle()
                        # type new sim until one is valid

                        while self.utils.element_exists(
                                "//span[contains(text(),'Rezervarea SIM nu s-a realizat, introdu un alt SIM')]",
                                enable_Logging=False):
                            sim = self.utils.retryAPI(API='SIM', URL=self.utils.getTempValue('URL')['getICCID'])
                            # sim = 3424324213121313213
                            self.utils.type_js(xpath=self.util_nr['newSim'], value=str(sim))
                            self.utils.click_js("""//button[@class='info_tooltip']""",
                                                                      enable_Logging=False)
                            self.utils.wait_loadingCircle()
                            stopVar += 1
                            if stopVar >= 3:
                                self.utils.log_customStep('Validate newSIM after retry', 'FAIL')
                                break

                    elif self.scenarioName_Utl_Nr == 'TipContract_Permanet_Numar_Special':

                        # Alege urmatoarea data de activare actions
                        click_date = self.utils.click_js(self.util_nr['data_activare'])
                        day_to_be_selected = (datetime.datetime.today() + datetime.timedelta(days=1))
                        customDate = self.custom_strftime('%B {S}, %Y', day_to_be_selected)
                        self.utils.click_js(
                            '//div[@aria-label="Choose {}, {}"]'.format(day_to_be_selected.strftime('%A'), customDate))

                        # select Min Date
                        daysPlus = 4
                        isBusinessDay, bDayReturned = self.utils.is_business_day(
                            datetime.datetime.today(),
                            daysPlus)
                        while isBusinessDay is not True:
                            daysPlus += 1
                            isBusinessDay, bDayReturned = self.utils.is_business_day(
                                datetime.datetime.today(),
                                daysPlus)

                            # Choose Sunday, August 2nd, 2020
                        self.utils.click_js(self.util_nr['data_min_portare'])
                        customDate = self.custom_strftime('%B {S}, %Y', bDayReturned)
                        self.utils.click_js(
                            '//div[@aria-label="Choose {}, {}"]'.format(bDayReturned.strftime('%A'), customDate))

                        # select MAX date
                        daysPlus = 7
                        isBusinessDay, bDayReturned = self.utils.is_business_day(
                            datetime.datetime.today(),
                            daysPlus)
                        while isBusinessDay is not True:
                            daysPlus += 1
                            isBusinessDay, bDayReturned = self.utils.is_business_day(
                                datetime.datetime.today(),
                                daysPlus)

                        # Choose Sunday, August 2nd, 2020
                        self.utils.click_js(self.util_nr['data_max_portare'])
                        customDate = self.custom_strftime('%B {S}, %Y', bDayReturned)
                        self.utils.click_js(
                            '//div[@aria-label="Choose {}, {}"]'.format(bDayReturned.strftime('%A'), customDate))
                        # Alege numar special actions
                        self.utils.logScenariu('click alege numar')
                        self.utils.click_js(self.util_nr['change_number'])

                        self.utils.wait_loadingCircle()

                        self.utils.logScenariu('click Numar Special')
                        self.utils.click_js(self.util_nr['numar_special'])

                        self.utils.logScenariu('Introdu search cifre 22')
                        alege_no_spl = self.utils.click_js(self.util_nr['first_digits'])
                        dig = 22
                        # type 22 in first digit
                        self.utils.type_js(self.util_nr['first_digits'], dig)
                        cauta_no_spl = self.utils.click_js(self.util_nr['cauta'])

                        try:
                            while self.browser.find_element_by_xpath("//*[contains(text(),'Nu au fost')]"):
                                input_no_spl_c = self.utils.click_js(self.util_nr['sterge'])
                                dig += 1
                                self.utils.type_js(self.util_nr['first_digits'], dig)
                                cauta_no_spl = self.utils.click_js(self.util_nr['cauta'])
                                time.sleep(2)
                        except NoSuchElementException:
                            print('Numar special a fost gasit cu first digit = {}'.format(dig))

                            selectedNumber = self.utils.get_text("""//div[@class='number-box selected']""")
                            self.utils.click_js(self.util_nr['continua_alegeNr'])
                            time.sleep(3)
                            sqResponse = self.utils.retryAPI(API='simpleAPICall',
                                                             URL=self.utils.getTempValue('URL')['updateRM'].format(
                                                                 str(selectedNumber)).replace(' ', ''))
                            print('sqResponse', sqResponse)
                            # Get sim and type it actions
                        stopVar=0
                        sim = self.utils.callRESTApi(self.utils.getTempValue('URL')['getICCID'],
                                                                           key='ICCID')
                        self.utils.type_js(xpath=self.util_nr['newSim'], value=str(sim))

                        time.sleep(1)
                        self.utils.click_js("""//button[@class='info_tooltip']""", js=False)
                        time.sleep(1)

                        # type new sim until one is valid
                        while self.utils.element_exists(
                                "//span[contains(text(),'Rezervarea SIM nu s-a realizat, introdu un alt SIM')]",
                                enable_Logging=False):

                            sim = self.utils.retryAPI(API='SIM', URL=self.utils.getTempValue('URL')['getICCID'])
                            # sim = 3424324213121313213
                            self.utils.type_js(xpath=self.util_nr['newSim'], value=str(sim))
                            self.utils.click_js("""//button[@class='info_tooltip']""",
                                                                      enable_Logging=False)
                            self.utils.wait_loadingCircle()
                            stopVar += 1
                            if stopVar >= 3:
                                self.utils.log_customStep('Validate newSIM after retry', 'FAIL')
                                break

                elif self.tip_contract == 'asteaptaPortarea':
                    self.utils.click_js(self.util_nr['asteapta_portarea'])
                    if self.scenarioName_Utl_Nr == 'TipContract_asteaptaPortarea_nrtempYes_Numar_Random':
                        # Alege urmatoarea data de activare actions
                        click_date = self.utils.click_js(self.util_nr['data_activare'])
                        day_to_be_selected = (datetime.datetime.today() + datetime.timedelta(days=1))
                        customDate = self.custom_strftime('%B {S}, %Y', day_to_be_selected)
                        self.utils.click_js(
                            '//div[@aria-label="Choose {}, {}"]'.format(day_to_be_selected.strftime('%A'), customDate), enable_Logging=False)

                        # select Min Date
                        daysPlus = 4
                        isBusinessDay, bDayReturned = self.utils.is_business_day(
                            datetime.datetime.today(),
                            daysPlus)
                        while isBusinessDay is not True:
                            daysPlus += 1
                            isBusinessDay, bDayReturned = self.utils.is_business_day(
                                datetime.datetime.today(),
                                daysPlus)
                            # Choose Sunday, August 2nd, 2020
                        self.utils.click_js(self.util_nr['data_min_portare'])
                        customDate = self.custom_strftime('%B {S}, %Y', bDayReturned)
                        self.utils.click_js(
                            '//div[@aria-label="Choose {}, {}"]'.format(bDayReturned.strftime('%A'), customDate), enable_Logging=False)

                        # select MAX date
                        daysPlus = 7
                        isBusinessDay, bDayReturned = self.utils.is_business_day(
                            datetime.datetime.today(),
                            daysPlus)
                        while isBusinessDay is not True:
                            daysPlus += 1
                            isBusinessDay, bDayReturned = self.utils.is_business_day(
                                datetime.datetime.today(),
                                daysPlus)
                        # Choose Sunday, August 2nd, 2020
                        self.utils.click_js(self.util_nr['data_max_portare'])
                        customDate = self.custom_strftime('%B {S}, %Y', bDayReturned)
                        self.utils.click_js(
                            '//div[@aria-label="Choose {}, {}"]'.format(bDayReturned.strftime('%A'), customDate), enable_Logging=False)
                        # select one random number
                        self.utils.logScenariu(self.scenarioName_Utl_Nr)
                        self.utils.logScenariu('click alege numar')
                        self.utils.click_js(self.util_nr['change_number'])

                        self.utils.wait_loadingCircle()
                        self.utils.logScenariu('click un numar din cele propuse')
                        selecteaza_un_numar = self.utils.click_js(self.util_nr['first_nr_disponibil'])
                        self.utils.logScenariu('click continua')
                        selectedNumber = self.utils.get_text("""//div[@class='number-box selected']""")
                        self.utils.click_js(self.util_nr['continua_alegeNr'])
                        time.sleep(3)
                        sqResponse = self.utils.retryAPI(API='simpleAPICall',
                                                         URL=self.utils.getTempValue('URL')['updateRM'].format(
                                                             str(selectedNumber)).replace(' ', ''))
                        print('sqResponse', sqResponse)
                        # Get sim and type it actions
                        stopVar=0
                        sim = self.utils.retryAPI(API='SIM', URL=self.utils.getTempValue('URL')['getICCID'])
                        self.utils.type_js(xpath=self.util_nr['newSim'], value=str(sim))
                        time.sleep(1)
                        self.utils.click_js("""//button[@class='info_tooltip']""", js=False)
                        time.sleep(1)                    # type new sim until one is valid
                        while self.utils.element_exists(
                                "//span[contains(text(),'Rezervarea SIM nu s-a realizat, introdu un alt SIM')]",
                                enable_Logging=False):
                            sim = self.utils.retryAPI(API='SIM', URL=self.utils.getTempValue('URL')['getICCID'])
                            # sim = 3424324213121313213
                            self.utils.type_js(xpath=self.util_nr['newSim'], value=str(sim))
                            self.utils.click_js("""//button[@class='info_tooltip']""",
                                                                      enable_Logging=False)
                            self.utils.wait_loadingCircle()
                            stopVar += 1
                            if stopVar >= 3:
                                self.utils.log_customStep('Validate newSIM after retry', 'FAIL')
                                break

                    elif self.scenarioName_Utl_Nr == 'TipContract_asteaptaPortarea_nrtempYes_Numar_Special':
                        #check nr temp true
                        self.utils.check_button(
                            xpath=self.util_nr['numar_temporar_chk'],
                            xpathCLick=self.util_nr['numar_temporar'],
                            value='value')

                        # Alege urmatoarea data de activare actions
                        click_date = self.utils.click_js(self.util_nr['data_activare'])
                        day_to_be_selected = (datetime.datetime.today() + datetime.timedelta(days=1))
                        customDate = self.custom_strftime('%B {S}, %Y', day_to_be_selected)
                        self.utils.click_js(
                            '//div[@aria-label="Choose {}, {}"]'.format(day_to_be_selected.strftime('%A'), customDate))

                        # select Min Date
                        daysPlus = 4
                        isBusinessDay, bDayReturned = self.utils.is_business_day(
                            datetime.datetime.today(),
                            daysPlus)
                        while isBusinessDay is not True:
                            daysPlus += 1
                            isBusinessDay, bDayReturned = self.utils.is_business_day(
                                datetime.datetime.today(),
                                daysPlus)
                            # Choose Sunday, August 2nd, 2020
                        self.utils.click_js(self.util_nr['data_min_portare'])
                        customDate = self.custom_strftime('%B {S}, %Y', bDayReturned)
                        self.utils.click_js(
                            '//div[@aria-label="Choose {}, {}"]'.format(bDayReturned.strftime('%A'), customDate))

                        # select MAX date
                        daysPlus = 7
                        isBusinessDay, bDayReturned = self.utils.is_business_day(
                            datetime.datetime.today(),
                            daysPlus)
                        while isBusinessDay is not True:
                            daysPlus += 1
                            isBusinessDay, bDayReturned = self.utils.is_business_day(
                                datetime.datetime.today(),
                                daysPlus)

                        # Choose Sunday, August 2nd, 2020
                        self.utils.click_js(self.util_nr['data_max_portare'])
                        customDate = self.custom_strftime('%B {S}, %Y', bDayReturned)
                        self.utils.click_js(
                            '//div[@aria-label="Choose {}, {}"]'.format(bDayReturned.strftime('%A'), customDate))
                        # Alege numar special actions
                        self.utils.logScenariu('click alege numar')
                        self.utils.click_js(self.util_nr['change_number'])

                        self.utils.wait_loadingCircle()

                        self.utils.logScenariu('click Numar Special')
                        self.utils.click_js(self.util_nr['numar_special'])

                        self.utils.logScenariu('Introdu search cifre 22')
                        alege_no_spl = self.utils.click_js(self.util_nr['first_digits'])
                        dig = 22
                        # type 22 in first digit
                        self.utils.type_js(self.util_nr['first_digits'], dig)
                        cauta_no_spl = self.utils.click_js(self.util_nr['cauta'])

                        try:
                            while self.browser.find_element_by_xpath("//*[contains(text(),'Nu au fost')]"):
                                input_no_spl_c = self.utils.click_js(self.util_nr['sterge'])
                                dig += 1
                                self.utils.type_js(self.util_nr['first_digits'], dig)
                                cauta_no_spl = self.utils.click_js(self.util_nr['cauta'])
                                time.sleep(2)
                        except NoSuchElementException:
                            print('Numar special a fost gasit cu first digit = {}'.format(dig))

                            selectedNumber = self.utils.get_text("""//div[@class='number-box selected']""")
                            self.utils.click_js(self.util_nr['continua_alegeNr'])
                            time.sleep(3)
                            sqResponse = self.utils.retryAPI(API='simpleAPICall',
                                                             URL=self.utils.getTempValue('URL')['updateRM'].format(
                                                                 str(selectedNumber)).replace(' ', ''))
                            print('sqResponse', sqResponse)
                        # Insert Sim
                        stopVar=0
                        sim = self.utils.retryAPI(API='SIM', URL=self.utils.getTempValue('URL')['getICCID'])
                        self.utils.type_js(xpath="//input[@name='newSimNumber']", value=str(sim))
                        time.sleep(1)
                        self.utils.click_js("""//button[@class='info_tooltip']""", js=False)
                        time.sleep(1)                    # type new sim until one is valid
                        while self.utils.element_exists(
                                "//span[contains(text(),'Rezervarea SIM nu s-a realizat, introdu un alt SIM')]",
                                enable_Logging=False):
                            sim = self.utils.retryAPI(API='SIM', URL=self.utils.getTempValue('URL')['getICCID'])
                            # sim = 3424324213121313213
                            self.utils.type_js(xpath=self.util_nr['newSim'], value=str(sim))
                            self.utils.click_js("""//button[@class='info_tooltip']""",
                                                                      enable_Logging=False)
                            self.utils.wait_loadingCircle()
                            stopVar += 1
                            if stopVar >= 3:
                                self.utils.log_customStep('Validate newSIM after retry', 'FAIL')
                                break

                    elif self.scenarioName_Utl_Nr == 'TipContract_asteaptaPortarea_nrtempNo':

                        #check nr temp false
                        self.utils.uncheck_button(
                            xpath=self.util_nr['numar_temporar'],
                            xpathCLick='//input[@data-automation-id="assign-temporary-msisdn"]//ancestor::label/span',
                            value='value')

                        # select Min Date
                        daysPlus = 4
                        isBusinessDay, bDayReturned = self.utils.is_business_day(
                            datetime.datetime.today(),
                            daysPlus)
                        while isBusinessDay is not True:
                            daysPlus += 1
                            isBusinessDay, bDayReturned = self.utils.is_business_day(
                                datetime.datetime.today(),
                                daysPlus)
                            # Choose Sunday, August 2nd, 2020
                        self.utils.click_js(self.util_nr['data_min_portare'])
                        customDate = self.custom_strftime('%B {S}, %Y', bDayReturned)
                        self.utils.click_js(
                            '//div[@aria-label="Choose {}, {}"]'.format(bDayReturned.strftime('%A'), customDate))

                        # select MAX date
                        daysPlus = 7
                        isBusinessDay, bDayReturned = self.utils.is_business_day(
                            datetime.datetime.today(),
                            daysPlus)
                        while isBusinessDay is not True:
                            daysPlus += 1
                            isBusinessDay, bDayReturned = self.utils.is_business_day(
                                datetime.datetime.today(),
                                daysPlus)
                        # Choose Sunday, August 2nd, 2020
                        self.utils.click_js(self.util_nr['data_max_portare'])
                        customDate = self.custom_strftime('%B {S}, %Y', bDayReturned)
                        self.utils.click_js(
                            '//div[@aria-label="Choose {}, {}"]'.format(bDayReturned.strftime('%A'), customDate))

                        # Get sim and type it actions
                        stopVar=0
                        sim = self.utils.retryAPI(API='SIM', URL=self.utils.getTempValue('URL')['getICCID'])
                        self.utils.type_js(xpath=self.util_nr['newSim'], value=str(sim))
                        time.sleep(1)
                        self.utils.click_js("""//button[@class='info_tooltip']""", js=False)
                        time.sleep(1)                    # type new sim until one is valid
                        while self.utils.element_exists(
                                "//span[contains(text(),'Rezervarea SIM nu s-a realizat, introdu un alt SIM')]",
                                enable_Logging=False):
                            sim = self.utils.retryAPI(API='SIM', URL=self.utils.getTempValue('URL')['getICCID'])
                            # sim = 3424324213121313213
                            self.utils.type_js(xpath=self.util_nr['newSim'], value=str(sim))
                            self.utils.click_js("""//button[@class='info_tooltip']""",
                                                                      enable_Logging=False)
                            self.utils.wait_loadingCircle()
                            stopVar += 1
                            if stopVar >= 3:
                                self.utils.log_customStep('Validate newSIM after retry', 'FAIL')
                                break

            elif self.abNouType == 'MFP':

                self.test_page_Notification_On_UtilizatorSiNumar()

                if self.scenarioName_Utl_Nr == 'Use_existing_SIM_card_Yes':
                    self.utils.check_button(xpath="//div[@class='useExistingSIMToggle']//input[@type='checkbox'", xpathCLick="//div[@class='useExistingSIMToggle']//span[@class='slider round']//ancestor::label/span", value='value')
                    # Alege urmatoarea data de activare actions
                    click_date = self.utils.click_js(self.util_nr['data_activare'])
                    day_to_be_selected = (datetime.datetime.today() + datetime.timedelta(days=1))
                    customDate = self.custom_strftime('%B {S}, %Y', day_to_be_selected)
                    # select activation Date via custom workdays alg
                    isActDateSelected = self.utils.click_js(
                        '//div[@aria-label="Choose {}, {}"]'.format(day_to_be_selected.strftime('%A'), customDate),
                        enable_Logging=False)
                    if isActDateSelected:
                        self.utils.log_customStep('Select_Data Activare', 'PASSED')
                    elif isActDateSelected == False:
                        self.utils.log_customStep('Select_Data Activare', 'FAILED')

                elif self.scenarioName_Utl_Nr == 'Use_existing_SIM_card_No':
                    self.utils.uncheck_button(
                        xpath="//div[@class='useExistingSIMToggle']//input[@type='checkbox']",
                        xpathCLick="//div[@class='useExistingSIMToggle']//span[@class='slider round']//ancestor::label/span",
                        value='value')

                    stopVar = 0
                    # Get sim and type it actions
                    sim = self.utils.retryAPI(API='SIM', URL=self.utils.getTempValue('URL')['getICCID'])
                    self.utils.type_js(xpath=self.util_nr['newSim'], value=str(sim))
                    time.sleep(1)
                    self.utils.click_js("""//button[@class='info_tooltip']""", js=False)
                    time.sleep(1)
                    while self.utils.element_exists(
                            "//span[contains(text(),'Rezervarea SIM nu s-a realizat, introdu un alt SIM')]",
                            enable_Logging=False):
                        sim = self.utils.retryAPI(API='SIM', URL=self.utils.getTempValue('URL')['getICCID'])
                        # sim = 3424324213121313213
                        self.utils.type_js(xpath=self.util_nr['newSim'], value=str(sim))
                        self.utils.click_js("""//button[@class='info_tooltip']""",
                                                                  enable_Logging=False)
                        self.utils.wait_loadingCircle()
                        stopVar += 1
                        if stopVar >= 3:
                            self.utils.log_customStep('Validate newSIM after retry', 'FAIL')
                            break

                        # Alege urmatoarea data de activare actions
                        click_date = self.utils.click_js(self.util_nr['data_activare'])
                        day_to_be_selected = (datetime.datetime.today() + datetime.timedelta(days=1))
                        customDate = self.custom_strftime('%B {S}, %Y', day_to_be_selected)
                        # select activation Date via custom workdays alg
                        isActDateSelected = self.utils.click_js(
                            '//div[@aria-label="Choose {}, {}"]'.format(day_to_be_selected.strftime('%A'), customDate),
                            enable_Logging=False)
                        if isActDateSelected:
                            self.utils.log_customStep('Select_Data Activare', 'PASSED')
                        elif isActDateSelected == False:
                            self.utils.log_customStep('Select_Data Activare', 'FAILED')
        except Exception as e:
            e = str(e).replace("\n", "")
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(f'{bcolors.WARN}{bcolors.BLUEIC}=====================================\n',
                  f'Error name => ["{exc_type}"]\nIn file => ["{fname}"]\nAt line => ["{exc_tb.tb_lineno}"]\nStack trace => ["{self.utils.format_stacktrace()}"]',
                  f'\n-------------------------------------\n{bcolors.ENDC}')
            self.utils.log_customStep('INTERNAL_CODE_ERROR', 'REASON = [{}]'.format(e))
        finally:
            return (self.utils.VDFloggingObj.method_log)

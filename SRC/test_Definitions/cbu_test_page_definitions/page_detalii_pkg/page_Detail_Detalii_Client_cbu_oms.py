from SRC.test_Definitions.test_baaseline_Classes.page_Detail_Detalii_Client_baseline import Detalii_Client_Layer_baseline
import bcolors, sys, os

class Page_Detail_Detalii_Client_Layer_cbu_oms(Detalii_Client_Layer_baseline):

    def __init__(self, inputDictionary=None, scenarioName_SendNotif=None, scenarioName_isTitular=None,
                 testCaseName=None, abNouType=None, scenarioName_Utl_Nr=None, browser=None, tip_contract=None,
                 scenarioName_GDPR=None, scenarioContNou=None, tipServiciu=None):

        self.dict_xpath = {
                'prenume':'//input[@name="personFirstName"]',
                'nume':'//input[@name="personLastName"]',
                'email':'//input[@name="email"]',
                'judet':"//*[text()='JUDEȚ']//ancestor::div[1]/div[1]/div[1]/div[1]/div[1]/div[1]",
                'localitate':"//*[text()='LOCALITATE']//ancestor::div[1]/div[1]/div[1]/div[1]/div[1]/div[1]",
                'tip_strada':"//*[text()='TIP STRADĂ']//ancestor::div[1]/div[1]/div[1]/div[1]/div[1]/div[1]",
                'nume_strada':"//*[text()='NUME STRADĂ']//ancestor::div[1]/div[1]/div[1]/div[1]/div[1]/div[1]",
                'numar_strada':'//input[@name="streetNumber"]',
                'bloc':"//input[contains(@name,'blockNumber')]",
                'scara':"//input[contains(@name,'blockEntrance')]",
                'apartament':"//input[contains(@name,'apartment')]",
                'cod_postal':"//*[text()='COD POȘTAL']//ancestor::div[1]/div[1]/div[1]/div[1]/div[1]/div[1]"
            }

        self.set_xpath_dictionary(self.dict_xpath)

        Detalii_Client_Layer_baseline.__init__(self, inputDictionary, scenarioName_SendNotif, scenarioName_isTitular, testCaseName, abNouType,
                         scenarioName_Utl_Nr, browser, tip_contract, scenarioName_GDPR, scenarioContNou, tipServiciu)

        self.scenarioName_PageSearch = self.utils.evalDictionaryValue(inputDictionary, 'scenarioName_PageSearch')
        self.informatii_client_dictionary = self.utils.getTempValue('info_clienti')

    def test_page_Detail_detalii_Client(self):
        try:
            if self.abNouType == 'GA':
                if self.scenarioName_PageSearch == 'Completare_Informatii_Client_Yes' or self.scenarioName_PageSearch == 'Completare_Informatii_Client_Yes_CR_False':

                    self.utils.type_js(xpath=self.dict_xpath['email'],value=self.informatii_client_dictionary['email'])

                    for key,value in self.informatii_client_dictionary.items():
                        if self.utils.get_text(xpath = self.dict_xpath[key]) == self.informatii_client_dictionary[key]:
                            self.utils.log_customStep('Campul_'+ key ,'PASS')
                        else:
                            self.utils.log_customStep('Campul_'+ key,'FAIL')

                    cod_postal = self.utils.get_text("//*[text()='COD POȘTAL']//ancestor::div[1]/div[1]/div[1]/div[1]/div[1]/div[1]")

                    if len(cod_postal) > 2 and isinstance(cod_postal,int):
                        self.utils.log_customStep('Campul_Cod_Postal','PASS')
                    else:
                        self.utils.log_customStep('Campul_Cod_Postal','FAIL')

                elif self.scenarioName_PageSearch == 'Completare_Informatii_Client_No':

                    self.utils.type_js(xpath=self.dict_xpath['prenume'],value=self.informatii_client_dictionary['prenume'],enter='True')
                    self.utils.type_js(xpath=self.dict_xpath['nume'],value=self.informatii_client_dictionary['nume'],enter='True')
                    self.utils.type_js(xpath=self.dict_xpath['email'],value=self.informatii_client_dictionary['email'],enter='True')
                    self.utils.type_js(xpath=self.dict_xpath['judet'],value=self.informatii_client_dictionary['judet'],enter='True')
                    self.utils.type_js(xpath=self.dict_xpath['localitate'],value=self.informatii_client_dictionary['localitate'],enter='True')
                    self.utils.type_js(xpath=self.dict_xpath['tip_strada'],value=self.informatii_client_dictionary['tip_strada'],enter='True')
                    self.utils.type_js(xpath=self.dict_xpath['nume_strada'],value=self.informatii_client_dictionary['nume_strada'],enter='True')
                    self.utils.type_js(xpath=self.dict_xpath['numar_strada'],value=self.informatii_client_dictionary['numar_strada'])
                    self.utils.type_js(xpath=self.dict_xpath['bloc'],value=self.informatii_client_dictionary['bloc'])
                    self.utils.type_js(xpath=self.dict_xpath['scara'],value=self.informatii_client_dictionary['scara'])
                    self.utils.type_js(xpath=self.dict_xpath['apartament'],value=self.informatii_client_dictionary['apartament'])
                    cod_postal = self.utils.get_text("//*[text()='COD POȘTAL']//ancestor::div[1]/div[1]/div[1]/div[1]/div[1]/div[1])")


                    if len(cod_postal) > 2 and isinstance(cod_postal,int):
                        self.utils.log_customStep('Campul_Cod_Postal','PASS')
                    else:
                        self.utils.log_customStep('Campul_Cod_Postal','FAIL')

                self.utils.click_js(xpath = "//*[text()='INFORMAȚII UTILIZATOR']//ancestor::div[1]//ancestor::div[1]//child::div[2]/button/img")

# Utilizatorul este Minor

                # if utilizator_minor == 'No':
                #     self.utils.dropDownSelector_By_visible_text(xpath = "//select[contains(@name,'personTitle')]",value='Dl.')
                #     self.utils.type_js(xpath = "(//input[@name='personFirstName'])[2]",value=self.informatii_client_dictionary['prenume'])
                #     self.utils.type_js(xpath = "(//input[@name='personLastName'])[2]",value=self.informatii_client_dictionary['nume'])
                #     self.utils.type_js(xpath = "//input[@name='contactEmail']",value=self.informatii_client_dictionary['email'])
                #     self.utils.type_js(xpath = "//input[@name='contactPhone']",'720123456')
                #     self.utils.click_js(xpath = "//button[@data-automation-id='save-popup-save-button']")
                # else:
                #     self.utils.check_button(xpath = '(//input[@class="wrap-input-radio"])[2]', xpathCLick='True')
                #     self.utils.type_js(xpath = "(//input[@name='personFirstName'])[2]",value=self.informatii_client_dictionary['prenume'])
                #     self.utils.type_js(xpath = "(//input[@name='personLastName'])[2]",value=self.informatii_client_dictionary['nume'])
                #     self.utils.type_js(xpath = "//input[@name='contactEmail']",value=self.informatii_client_dictionary['email'])
                #     self.utils.type_js(xpath = "//input[@name='contactPhone']",'720123456')
                #     self.utils.dropDownSelector_By_visible_text(xpath = '//select[@name="birthDay"]', '12')
                #     self.utils.dropDownSelector_By_visible_text(xpath = '(//select[@name="birthDateMonth"])[2]', '07')
                #     self.utils.dropDownSelector_By_visible_text(xpath = '(//select[@name="birthDateYear"])[2]', '2008')
                #     self.utils.click_js(xpath = "//button[@data-automation-id='save-popup-save-button']")

                    # if self.utils.get_attrValue(xPath='//div[@class="contact-name"]/img', 'src') == 'resources/assets/images/ic_minor.svg':  ############### sau print screen
                    #     self.utils.log_customStep('Pictograma_Minor_18_Visibila_','PASS')
                    # else:
                    #     self.utils.log_customStep('Pictograma_Minor_18_Visibila_','FAIL')

# DETALII FACTURĂ

                # self.utils.dropDownSelector_Index(xpath = '(//select[@name="billingCycle"])/option[2]', index = 2) ###################

                # self.utils.click_js(xpath = '//button[@data-automation-id="summary-configuration-submit-btn"]')
                
            elif self.abNouType == 'MNP':
                pass
            elif self.abNouType == 'MFP':
                pass
        except Exception as e:
            e = str(e).replace("\n", "")
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(f'{bcolors.WARN}{bcolors.BLUEIC}=====================================\n',
                  f'Error name => ["{exc_type}"]\nIn file => ["{fname}"]\nAt line => ["{exc_tb.tb_lineno}"]\nStack trace => ["{self.utils.format_stacktrace()}"]',
                  f'\n-------------------------------------\n{bcolors.ENDC}')
            self.utils.log_customStep('INTERNAL_CODE_ERROR', 'REASON = [{}]'.format(e))
        finally:
            return (self.utils.VDFloggingObj.method_log)

from SRC.utils_pkg import utilities


class GDPR_Layer:

    utils = utilities.utilities()

    def __init__(self, inputDictionary=None, scenarioName_isTitular = None, testCaseName=None, abNouType=None, scenarioName_Sim=None, browser=None, tip_contract=None, scenarioName_GDPR=None, scenarioContNou=None):
        self.abNouType = abNouType
        self.inputDictionary = inputDictionary
        self.scenarioName_Sim = scenarioName_Sim
        self.browser = browser
        self.tip_contract = tip_contract
        self.scenarioName_GDPR = scenarioName_GDPR
        self.scenarioName_isTitular = scenarioName_isTitular
        self.scenarioContNou = scenarioContNou
        self.GDPR_radio = {'vf_basic_chk': '//input[@data-automation-id="automation-vf-basic"]',
                           'vf_basic_click': '//input[@data-automation-id="automation-vf-basic"]//ancestor::label/span',
                           'vf_adv_chk': '//input[@data-automation-id="automation-vf-advanceProfiling"]',
                           'vf_adv_click': '//input[@data-automation-id="automation-vf-advanceProfiling"]//ancestor::label/span',
                           'part_basic_chk': '//input[@data-automation-id="automation-partner-basic"]',
                           'part_basic_click': '//input[@data-automation-id="automation-partner-basic"]//ancestor::label/span',
                           'part_adv_chk': '//input[@data-automation-id="automation-partner-advanceProfiling"]',
                           'part_adv_click': '//input[@data-automation-id="automation-partner-advanceProfiling"]//ancestor::label/span',
                           'Expand_Profil_Utilizator': '//*[@id="root"]/div[4]/div[2]/div/div[2]/div/div[4]/div[2]/div[1]/div[1]',
                           'profilare_adv':'//div[@class ="order-summary-general-data"]/div[3]//div[@ class ="line-layout first-lin"]',
                           'vf_basic_disabled': '//input[@data-automation-id="automation-vf-basic" and @disabled]',
                           'vf_adv_disabled':'//input[@data-automation-id="automation-vf-advanceProfiling" and @disabled]',
                           'part_basic_disabled':'//input[@data-automation-id="automation-partner-basic" and @disabled]',
                           'part_adv_disabled': '//input[@data-automation-id="automation-partner-advanceProfiling" and @disabled]',
                           'acord GDPR minor_Da_1': '//*[@id="root"]/div[4]/div[2]/div/div[2]/div/div[4]/div[2]/div[2]/div[5]/div[2]/label[1]/input',
                           'acord GDPR minor_Da_2': '//*[@id="root"]/div[4]/div[2]/div/div[2]/div/div[4]/div[2]/div[2]/div[5]/div[3]/label[1]/input',
                           'acord GDPR minor_Nu_1': '//*[@id="root"]/div[4]/div[2]/div/div[2]/div/div[4]/div[2]/div[2]/div[5]/div[2]/label[2]/input',
                            'acord GDPR minor_Nu_2' : '//*[@id="root"]/div[4]/div[2]/div/div[2]/div/div[4]/div[2]/div[2]/div[5]/div[3]/label[2]/input',
                           'GDPR_Email_VDF': '(//div[@class="order-summary-general-data"]/div[2]/div[2]/div[1]//input)[2]',
                           'GDPR_Posta_VDF': '(//div[@class="order-summary-general-data"]/div[2]/div[2]/div[2]//input)[2]',
                           'GDPR_SMS_VDF': '(//div[@class="order-summary-general-data"]/div[2]/div[2]/div[3]//input)[2]',
                           'GDPR_ApelareTelefonica_VDF': '(//div[@class="order-summary-general-data"]/div[2]/div[2]/div[4]//input)[2]',
                           'Profil_utilizator_VDF': '(//div[@class="order-summary-general-data"]/div[2]/div[2]/div[5]//input)[2]',
                           'GDPR_Email_Partner': '(//div[@class="order-summary-general-data"]/div[2]/div[2]/div[1]//input)[4]',
                           'GDPR_Posta_Partner': '(//div[@class="order-summary-general-data"]/div[2]/div[2]/div[2]//input)[4]',
                           'GDPR_SMS_Partner': '(//div[@class="order-summary-general-data"]/div[2]/div[2]/div[3]//input)[4]',
                           'GDPR_ApelareTelefonica_Partner': '(//div[@class="order-summary-general-data"]/div[2]/div[2]/div[4]//input)[4]',
                           'Profil_utilizator_Partner': '(//div[@class="order-summary-general-data"]/div[2]/div[2]/div[5]//input)[4]',
                           }


        GDPR_Layer.utils.__init__(browser=self.browser, numeScenariu=testCaseName, xpathDict=self.GDPR_radio, methodName='test_page_Detail_GDPR')
        


    def test_page_Detail_GDPR(self):
        try:
            GDPR_Layer.utils.wait_loadingCircle()

            if self.scenarioName_GDPR == 'Scenariu_GDPR_allYES':

                # Check VDF 1 YES All
                GDPR_Layer.utils.check_button(self.GDPR_radio['vf_basic_chk'], self.GDPR_radio['vf_basic_click'], 'value')
                # #Check Partner 1 YES All
                GDPR_Layer.utils.check_button(self.GDPR_radio['part_basic_chk'], self.GDPR_radio['part_basic_click'], 'value')
                # # # Check VDF 2 YES All
                GDPR_Layer.utils.check_button(self.GDPR_radio['vf_adv_chk'], self.GDPR_radio['vf_adv_click'], 'value')
                # # # Check Partner 2 YES All
                GDPR_Layer.utils.check_button(self.GDPR_radio['part_adv_chk'], self.GDPR_radio['part_adv_click'], 'value')

            elif self.scenarioName_GDPR == 'Scenariu_GDPR_allNO':

                GDPR_Layer.utils.click_js(self.GDPR_radio['Expand_Profil_Utilizator'])

                GDPR_Layer.utils.uncheck_button(self.GDPR_radio['vf_basic_chk'], self.GDPR_radio['vf_basic_click'], 'value')
                GDPR_Layer.utils.uncheck_button(self.GDPR_radio['part_basic_chk'], self.GDPR_radio['part_basic_click'], 'value')
                for i in range(1, 6):
                    GDPR_Layer.utils.click_js('(//div[@class="order-summary-general-data"]/div[2]/div[2]/div[' + str(i)
                                                + ']//input)[2]')

                for i in range(1, 6):
                    GDPR_Layer.utils.click_js('(//div[@class="order-summary-general-data"]/div[2]/div[2]/div[' + str(i)
                                                + ']//input)[4]')


            elif self.scenarioName_GDPR == 'Scenariu_GDPR_VDF_Y_Parner_N':

                GDPR_Layer.utils.click_js(self.GDPR_radio['Expand_Profil_Utilizator'])
                GDPR_Layer.utils.uncheck_button(self.GDPR_radio['part_basic_chk'], self.GDPR_radio['part_basic_click'], 'value')

                for i in range(1, 6):
                    GDPR_Layer.utils.click_js('(//div[@class="order-summary-general-data"]/div[2]/div[2]/div[' + str(i) + ']//input)[4]')

                GDPR_Layer.utils.check_button(self.GDPR_radio['vf_basic_chk'], self.GDPR_radio['vf_basic_click'], 'value')
                GDPR_Layer.utils.check_button(self.GDPR_radio['vf_adv_chk'], self.GDPR_radio['vf_adv_click'], 'value')

            elif self.scenarioName_GDPR == 'Scenariu_GDPR_VDF_N_Parner_Y':

                GDPR_Layer.utils.click_js(self.GDPR_radio['Expand_Profil_Utilizator'])
                GDPR_Layer.utils.uncheck_button(self.GDPR_radio['vf_basic_chk'], self.GDPR_radio['vf_basic_click'], 'value')

                for i in range(1, 6):
                    GDPR_Layer.utils.click_js('(//div[@class="order-summary-general-data"]/div[2]/div[2]/div[' + str(i) + ']//input)[2]')

                GDPR_Layer.utils.check_button(self.GDPR_radio['part_basic_chk'], self.GDPR_radio['part_basic_click'], 'value')
                GDPR_Layer.utils.check_button(self.GDPR_radio['part_adv_chk'], self.GDPR_radio['part_adv_click'], 'value')

            elif self.scenarioName_GDPR == 'Scenariu_GDPR_Minor_Y':

                if self.utils.element_exists(self.GDPR_radio['vf_basic_disabled']) == False:
                    GDPR_Layer.utils.log_customStep('Check if vf_basic_disabled', 'FAIL')
                else:
                    GDPR_Layer.utils.log_customStep('Check if vf_basic_disabled', 'PASSED')

                if self.utils.element_exists(self.GDPR_radio['vf_adv_disabled']) == False:
                    GDPR_Layer.utils.log_customStep('Check if vf_adv_disabled', 'FAIL')
                else:
                    GDPR_Layer.utils.log_customStep('Check if vf_adv_disabled', 'PASSED')
                if self.utils.element_exists(self.GDPR_radio['part_basic_disabled']) == False:
                    GDPR_Layer.utils.log_customStep('part_basic_disabled', 'FAIL')
                else:
                    GDPR_Layer.utils.log_customStep('Check if part_basic_disabled', 'PASSED')
                if self.utils.element_exists(self.GDPR_radio['part_adv_disabled']) == False:
                    GDPR_Layer.utils.log_customStep('Check if part_adv_disabled', 'FAIL')
                else:
                    GDPR_Layer.utils.log_customStep('Check if part_adv_disabled', 'PASSED')

                GDPR_Layer.utils.click_js(self.GDPR_radio['acord GDPR minor_Da_1'])
                GDPR_Layer.utils.click_js(self.GDPR_radio['acord GDPR minor_Da_2'])

            elif self.scenarioName_GDPR == 'Scenariu_GDPR_Minor_N':
                if self.utils.element_exists(self.GDPR_radio['vf_basic_disabled']) == False:
                    GDPR_Layer.utils.log_customStep('Check if vf_basic_disabled', 'FAIL')
                else:
                    GDPR_Layer.utils.log_customStep('Check if vf_basic_disabled', 'PASSED')

                if self.utils.element_exists(self.GDPR_radio['vf_adv_disabled']) == False:
                    GDPR_Layer.utils.log_customStep('Check if vf_adv_disabled', 'FAIL')
                else:
                    GDPR_Layer.utils.log_customStep('Check if vf_adv_disabled', 'Passed')
                if self.utils.element_exists(self.GDPR_radio['part_basic_disabled']) == False:
                    GDPR_Layer.utils.log_customStep('part_basic_disabled', 'FAIL')
                else:
                    GDPR_Layer.utils.log_customStep('Check if part_basic_disabled', 'PASSED')

                if self.utils.element_exists(self.GDPR_radio['part_adv_disabled']) == False:
                    GDPR_Layer.utils.log_customStep('Check if part_adv_disabled', 'FAIL')
                else:
                    GDPR_Layer.utils.log_customStep('Check if part_adv_disabled', 'PASSED')


                GDPR_Layer.utils.click_js(self.GDPR_radio['acord GDPR minor_Nu_1'])
                GDPR_Layer.utils.click_js(self.GDPR_radio['acord GDPR minor_Nu_2'])
        except Exception as e:
            GDPR_Layer.utils.log_customStep('INTERNAL_CODE_ERROR', 'REASON = [{}]'.format(e))
        finally:
            return (GDPR_Layer.utils.VDFloggingObj.method_log)





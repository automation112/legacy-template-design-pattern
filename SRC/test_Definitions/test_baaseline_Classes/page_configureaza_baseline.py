import ast
import os
import sys
from abc import ABC, abstractmethod
from SRC.utils_pkg import utilities
import bcolors
import time

class page_configureaza_baseLine(ABC):
    utils = utilities.utilities()

    global_Wait = 20

    def __init__(self, scenarioNameDC=None, testCaseName=None, abNouType=None, tipServiciu=None, offerChoice=None,
                 scenarioName_PC=None, browser=None, channel=None, configType=None, retention_flow=None,
                 retention_option=None, inputDictionary=None):

        self.offerChoice = offerChoice
        self.scenarioName_PC = scenarioName_PC
        self.browser = browser
        self.channel = channel
        self.tipServiciu = tipServiciu
        self.abNouType = abNouType
        self.configType = configType
        self.scenarioNameDC = scenarioNameDC
        self.retention_flow = retention_flow
        self.retention_option = retention_option
        self.inputDictionary = inputDictionary
        self.pageConfigDict = {
            'submit': "//button[@data-automation-id='summary-configuration-submit-btn']",
            'email': "//input[@name='email']",
            'contact': "//input[@name='phone']",
            'send_to_vos': "//button[@data-automation-id='validate-contact-submit-button']",
            '1Device_D2S_Rate_CardToBeSelected': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 0]',
            '1Device_D2S_Rate_SelectedCard': """//div[@class="card_wrap card-wrap-device device-offer "]//span[contains(text(),'SELECTAT')]//ancestor::div[@class="card_wrap card-wrap-device device-offer "][1]""",
            '1Device_D2S_Rate_Name': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 0]//div[@class="model_phone"]',
            '1Device_D2S_Rate_Upfront': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 0]//span[@class="price"]',
            ############################ Check stock xpaths ##################################################
            'PachetePromo_Stock: 0': '//span[contains(text(),"STOC: 0")]//ancestor::div[@class="card mobile-card"]//button[@class="general_btn"]',
            'PachetePromo_Stock INDISPONIBIL': '//span[contains(text(),"STOC INDISPONIBIL")]//ancestor::div[@class="card mobile-card"]//button[@class="general_btn"]',
            'PachetePromo_Stock > 0': '//span[contains(text(),"STOC: ")]//ancestor::div[@class="card mobile-card"]//button[@class="general_btn"]',
            'PachetePromo_Continua': '//button[@data-automation-id="summary-configuration-submit-btn"]',
            'PachetePromo_Rate_and_Discount': '//div[@class="card mobile-card"][position() > 3]//*[contains(text(),"DISC")]//ancestor::div[@class="card mobile-card"]//ancestor::div[@class="card mobile-card"]',
            'PachetePromo_Discount': '//div[@class="card mobile-card"][position() > 3]//*[contains(text(),"DISC")]//ancestor::div[@class="card mobile-card"]//*[not (contains(@title,"rate"))]//ancestor::div[@class="card mobile-card"]',
            # 'PachetePromo_Rate': '//div[@class="card mobile-card"][position() > 3]//*[contains(text(),"rate")]//ancestor::div[@class="card mobile-card"]//*[not (contains(@title,"DISC"))]//ancestor::div[@class="card mobile-card"]',
            'PachetePromo_Rate': '//div[@class="card mobile-card"][position() > 3]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]',
            'PachetePromo_Default': '//div[@class="card mobile-card"][position() > 3]//*[(contains(@title, "SIM")) or (contains(text(), "SIM"))]//ancestor::div[@class="card mobile-card"]//*[not (contains(@title,"DISC"))]//ancestor::div[@class="card mobile-card"]//*[not (contains(@title,"rate"))]//ancestor::div[@class="card mobile-card"]',

            'PachetePromo_Retentie_Rate_and_Discount': '//div[@class="card mobile-card"][position() > 3]//*[contains(text(),"DISC")]//ancestor::div[@class="card mobile-card"]//span[contains(@title,"rate")]//ancestor::div[@class="card mobile-card"]',
            'PachetePromo_Retentie_Discount': '//div[@class="card mobile-card"][position() > 3]//*[contains(text(),"DISC")]//ancestor::div[@class="card mobile-card"]//*[not (contains(@title,"rate"))]//ancestor::div[@class="card mobile-card"]',
            # 'PachetePromo_Rate': '//div[@class="card mobile-card"][position() > 3]//*[contains(text(),"rate")]//ancestor::div[@class="card mobile-card"]//*[not (contains(@title,"DISC"))]//ancestor::div[@class="card mobile-card"]',
            'PachetePromo_Retentie_Rate': '//div[@class="card mobile-card"][position() > 3]//ancestor::div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]',
            'PachetePromo_Retentie_Default': '//div[@class="card mobile-card"][position() > 3]//*[(contains(@title, "SIM")) or (contains(text(), "SIM"))]//ancestor::div[@class="card mobile-card"]//*[not (contains(@title,"DISC"))]//ancestor::div[@class="card mobile-card"]//*[not (contains(@title,"rate"))]//ancestor::div[@class="card mobile-card"]',

            'cfg_DS_Continua': '//button[@data-automation-id="summary-configuration-submit-btn"]',
            'cfg_DS_Stock: 0': '//span[contains(text(),"STOC: 0")]//ancestor::div[@class="card_wrap card-wrap-device device-offer "]//button[@class="general_btn "]',
            'cfg_DS_Stock INDISPONIBIL': '//span[contains(text(),"STOC INDISPONIBIL")]//ancestor::div[@class="card_wrap card-wrap-device device-offer "]//button[@class="general_btn "]',
            'cfg_DS_Stock: > 0': '//span[contains(text(),"STOC: ")]//ancestor::div[@class="card_wrap card-wrap-device device-offer "]//button[@class="general_btn "]',
            'cfg_DS_Click Oferta D2S': "//div[@class='cards-section space-unset']/div[1]//*[contains(text(),'Select')]",
            'cfg_SD_Stock: 0': '//span[contains(text(),"STOC: 0")]//ancestor::div[@class="card_wrap card-wrap-device  "]//button[@class="general_btn "]',
            'cfg_SD_Stock INDISPONIBIL': '//span[contains(text(),"STOC INDISPONIBIL")]//ancestor::div[@class="card_wrap card-wrap-device  "]//button[@class="general_btn "]',
            'cfg_SD_Stock > 0': '//span[contains(text(),"STOC: ")]//ancestor::div[@class="card_wrap card-wrap-device  "]//button[@class="general_btn "]',

            'Oferta_S2D_RateDiscount_Discount': """//div[@class="card mobile-card"]//*[contains(text(),'rate')]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]""",
            'Oferta_S2D_RateDiscount_Selecteaza': """//div[@class="card mobile-card"]//*[contains(text(),'rate')]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'Selec')]""",
            'Oferta_S2D_RateDiscount_PerioadaContractuala': """//div[@class="card mobile-card"]//*[contains(text(),'rate')]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
            'Oferta_S2D_RateDiscount_NumeOferta': """//div[@class="card mobile-card"]//*[contains(text(),'rate')]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
            'Oferta_S2D_RateDiscount_oPrice': """//div[@class="card mobile-card"]//*[contains(text(),'rate')]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",

            'ShoppingCart_Continua': "//button[contains(@class,'secondary bold')]",
            'Oferta_S2D_Rate': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]""",
            'Oferta_S2D_Rate_Select': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'Select')]""",
            'Oferta_S2D_Rate_NumeOferta': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
            'Oferta_S2D_Rate_PerioadaContractuala': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
            'Oferta_S2D_Rate_oPrice': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",

            #########VMM###########
            'Oferta_S2D_VMM_Rate': """//div[@class="card mobile-card"][position() > 0]//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]""",
            'Oferta_S2D_VMM_Rate_Select': """//div[@class="card mobile-card"][position() > 0]//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'Select')]""",
            'Oferta_S2D_VMM_Rate_NumeOferta': """//div[@class="card mobile-card"][position() > 0]//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
            'Oferta_S2D_VMM_Rate_PerioadaContractuala': """//div[@class="card mobile-card"][position() > 0]//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
            'Oferta_S2D_VMM_Rate_oPrice': """//div[@class="card mobile-card"][position() > 0]//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",

            'Oferta_S2D_VMM_Rate_si_Discount': """//div[@class="card mobile-card"][position() > 0]//div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]""",
            'Oferta_S2D_VMM_Rate_si_Discount_Select': """//div[@class="card mobile-card"][position() > 0]//div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'Select')]""",
            'Oferta_S2D_VMM_Rate_si_Discount_NumeOferta': """//div[@class="card mobile-card"][position() > 0]//div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
            'Oferta_S2D_VMM_Rate_si_Discount_PerioadaContractuala': """//div[@class="card mobile-card"][position() > 0]//div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
            'Oferta_S2D_VMM_Rate_si_Discount_oPrice': """//div[@class="card mobile-card"][position() > 0]//div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",

            'Oferta_VMM_SimOnly_Discount': """//div[@class="card mobile-card"][position() > 0]//*[contains(text(),'DISCOU')]""",
            'Oferta_VMM_SimOnly_Discount_PerioadaContractuala': """//div[@class="card mobile-card"][position() > 0]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
            'Oferta_VMM_SimOnly_Discount_NumeOferta': """//div[@class="card mobile-card"][position() > 0]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
            'Oferta_VMM_SimOnly_Discount_oPrice': """//div[@class="card mobile-card"][position() > 0]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
            'Oferta_VMM_SimOnly_Discount_Selecteaza': """//div[@class="card mobile-card"][position() > 0]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'Select')]""",

            'Oferta_SimOnly_Discount': """//div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]""",
            'Oferta_SimOnly_Discount_PerioadaContractuala': """//div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
            'Oferta_SimOnly_Discount_NumeOferta': """//div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
            'Oferta_SimOnly_Discount_oPrice': """//div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
            'Oferta_SimOnly_Discount_Selecteaza': """//div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'Select')]""",

            'Oferta_VMM_SimOnly_PerioadaContractuala': """//div[@class="card mobile-card"][position() > 0]//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
            'Oferta_VMM_SimOnly_NumeOferta': """//div[@class="card mobile-card"][position() > 0]//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
            'Oferta_VMM_SimOnly_oPrice': """//div[@class="card mobile-card"][position() > 0]//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
            'Oferta_VMM_SimOnly_Selecteaza': """//div[@class="card mobile-card"][position() > 0]//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'Select')]""",

            'Oferta_SimOnly_PerioadaContractuala': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
            'Oferta_SimOnly_NumeOferta': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
            'Oferta_SimOnly_oPrice': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
            'Oferta_SimOnly_Selecteaza': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'Select')]""",

            'Device_Rate_RateMultiple': '//span[@class="link-title blue bold"]',
            'Device_Rate_LastInstallment': '//div[@class="white_tooltip box_tooltip installment-wrapper visible-tooltip"]/div',
            'Device_Rate3YRS_LastInstallment': """//div[@class='white_tooltip box_tooltip installment-wrapper visible-tooltip']//div[@class='tooltip_txt selected-settings']//span[contains(@class,'Instalments_last_txt')][contains(text(),'36')]""",
            'Device_Rate_Name': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="model_phone"]',
            'Device_Rate_Upfront': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
            'Device_Rate_InstallmentValue': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify two_sides undefined"]//span[@class="txt_right_side after_discount"]',
            'Device_Rate_Stock': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify final_p"]/span[2]',
            'Device_Rate_Selecteaza': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//button[@class="general_btn "]',
            'Device_Rate_CardToBeSelected': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]',
            'Device_Rate_SelectedCard': """//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class='card-footer card-footer-button']//span[contains(text(),'SELECTAT')]//ancestor::div[@class="card_wrap card-wrap-device  "][1]""",

            ########Dev Discount#########
            'Device_Rate_OfferPriceDiscount': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="Overview with_tooltip"]//span[@class="txt_right_side after_discount"]',
            'Total_RateDisc_TotalLunar': """//div[@class="card-footer card-footer-button"]//div[@class="price total-device-price"]""",
            'Device_RateDisc_Avans': '//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',

            ########VMM#######
            'Device_RateDiscVMM_LastInstallment': '//div[@class="white_tooltip box_tooltip  visible-tooltip"]/div[last()]',
            'Oferta_S2D_RateDiscVMM_Selecteaza': """//div[@class="card mobile-card"][position() > 0]//*[contains(text(),'rate')]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'Selec')]""",
            'Device_RateDiscVMM_RateMultiple': '//div[@class="card_wrap card-wrap-device  "][position() > 0]//span[@class="link-title blue bold"]',
            'Device_RateDiscVMM_Selecteaza': '//div[@class="card_wrap card-wrap-device  "][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//button[@class="general_btn "]',

            'Device_VMM_Rate_RateMultiple': '//div[@class="card_wrap card-wrap-device  "][position() > 0]//span[@class="link-title blue bold"]',
            'Device_VMM_Rate_LastInstallment': '//div[@class="white_tooltip box_tooltip  visible-tooltip"]/div[last()]',
            'Device_VMM_Rate_Name': '//div[@class="card_wrap card-wrap-device  "][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="model_phone"]',
            'Device_VMM_Rate_Upfront': '//div[@class="card_wrap card-wrap-device  "][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
            'Device_VMM_Rate_InstallmentValue': '//div[@class="card_wrap card-wrap-device  "][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify two_sides undefined"]//span[@class="txt_right_side after_discount"]',
            'Device_VMM_Rate_Stock': '//div[@class="card_wrap card-wrap-device  "][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify final_p"]/span[2]',
            'Device_VMM_Rate_Selecteaza': '//div[@class="card_wrap card-wrap-device  "][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//button[@class="general_btn "]',

            'Device_VMM_Rate_si_Discount_RateMultiple': '//div[@class="card_wrap card-wrap-device  "][position() > 0]//span[@class="link-title blue bold"]',
            'Device_VMM_Rate_si_Discount_LastInstallment': '//div[@class="white_tooltip box_tooltip  visible-tooltip"]/div[last()]',
            'Device_VMM_Rate_si_Discount_Name': '//div[@class="card_wrap card-wrap-device  "][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="model_phone"]',
            'Device_VMM_Rate_si_Discount_Upfront': '//div[@class="card_wrap card-wrap-device  "][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
            'Device_VMM_Rate_si_Discount_rPrice': '//div[@class="card_wrap card-wrap-device  "][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify two_sides undefined"]//span[@class="txt_right_side after_discount"]',
            'Device_VMM_Rate_si_Discount_Stock': '//div[@class="card_wrap card-wrap-device  "][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify final_p"]/span[2]',
            'Device_VMM_Rate_si_Discount_Selecteaza': '//div[@class="card_wrap card-wrap-device  "][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//button[@class="general_btn "]',

            'Device_VMM_SimOnly_Name': '//div[@class="card_wrap card-wrap-device  "][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="model_phone"]',
            'Device_VMM_SimOnly_Price': '//div[@class="card_wrap card-wrap-device  "][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
            'Device_VMM_SimOnly_Discount': '//div[@class="card_wrap card-wrap-device  "][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify two_sides undefined"]//span[@class="txt_right_side after_discount"]',
            'Device_VMM_SimOnly_Stock': '//div[@class="card_wrap card-wrap-device  "][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify final_p"]/span[2]',
            'Device_VMM_SimOnly_Selecteaza': '//div[@class="card_wrap card-wrap-device  "][position() > 0]//ancestor::div[@class="card_wrap card-wrap-device  "]//button[@class="general_btn "]',

            'Device_SimOnly_Name': '//div[@class="card_wrap card-wrap-device  "]//div[@class="model_phone"]',
            'Device_SimOnly_Price': '//div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
            'Device_SimOnly_Discount': '//div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify two_sides undefined"]//span[@class="txt_right_side after_discount"]',
            'Device_SimOnly_Stock': '//div[@class="card_wrap card-wrap-device  "]//div[@class="align-justify final_p"]/span[2]',
            'Device_SimOnly_Selecteaza': '//div[@class="card_wrap card-wrap-device  "]//button[@class="general_btn "]',

            'Total_VMM_RateDisc_OfferPriceDiscount': '//div[@class="card_wrap card-wrap-device  "][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="Overview with_tooltip"]//span[@class="txt_right_side after_discount"]',
            'Total_VMM_RateDisc_TotalLunar': """//div[@class="card_wrap card-wrap-device  "][position() > 0]//div[@class="price total-device-price"]""",
            'Device_VMM_RateDisc_Avans': '//div[@class="card_wrap card-wrap-device  "][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="Overview with_tooltip"]//span[@class="txt_right_side after_discount"]',

            'ShoppingCart': "//button[contains(@class,'img-wrap')]//img",
            'Vezi Mai Multe_1stPannel': """//div[@class="fixed-bundle-offers"][1]//*[(contains(text(), 'VEZI'))]""",
            'Vezi Mai Multe_2ndPannel': """//div[@class="fixed-bundle-offers"][2]//*[(contains(text(), 'VEZI'))]""",

            'Total_SimOnly_TotalLunar': """//div[@class="card-footer card-footer-button"]//div[@class="price total-device-price"]""",
            'Device_SimOnly_Avans': '//div[@class="align-justify two_sides undefined"]//div[@class="tooltip_price_wrap"]',
            'Total_S2D_VMM_Rate_TotalLunar': """//div[@class="card_wrap card-wrap-device  "][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//div[@class="price total-device-price"]/h4""",
            'Device_VMM_Rate_Avans': '//div[@class="card_wrap card-wrap-device  "][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card_wrap card-wrap-device  "]//span[@class="price"]',
            # //div[@class="card mobile-card"]//*[contains(text(),'rate')]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'Selec')]
            'cfg_SD_Continua': '//button[@data-automation-id="summary-configuration-submit-btn"]',

            ############################# Page Config xpaths ###############################################
            'configureaza_oferte': """//h6[@class='bold']//span[contains(text(),'CONFIGUREAZ')]""",
            'incepe cu servicii': "//span[contains(text(),'ÎNCEPE CU SERVICII')]",
            'doar_servicii_chk': '//input[@data-automation-id="mobileDeviceOffers.mobileOfferLabel"]',
            'doar_servicii': '//input[@data-automation-id="mobileDeviceOffers.mobileOfferLabel"]//ancestor::label/span',
            'first_bundleOffer': "//div[contains(@class,'cards-section space-unset')]//div[1]//div[3]//div[1]//button[1]",
            'continua': "//button[contains(@data-automation-id,'summary-configuration-submit-btn')]",
            'vezi mai multe': "//div[@data-automation-id='show-more']/h6/span",
            'bundle offer': """//div[@class="card mobile-card"][4]//button[@class="general_btn"]""",
            'continua_VMM': "//button[contains(@class,'footer-btn-primary')]",

            #################################RETENTIE##################################
            'Retentie_PachetPromo_Stock': "//div[@class='card mobile-card'][1]//span[@class='gray_label ']",
            'Retention_firstCardSelected': "//div[@class='card mobile-card'][1]//button[contains(@class,'general_btn button-selected') and @disabled]",
            'Pachete_Promo_Banner': "//span[contains(text(),'PACHETE PROMO')]",

            ##########################DEVICE 2 SERVICE(D2S)###############################
            'Offer_D2S_Rate_CardToBeSelected': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]""",
            'Offer_D2S_Rate_SelectedOfferCard': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//button[contains(@class,'general_btn button-selected')]//ancestor::div[@class="card mobile-card"][1]""",
            'Oferta_D2S_Rate_NumeOferta': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
            'Oferta_D2S_Rate_PerioadaContractuala': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
            'Oferta_D2S_Rate_oPrice': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",

            'Device_D2S_Rate_CardToBeSelected': '//div[@class="card_wrap card-wrap-device device-offer "]',
            'Device_D2S_Rate_SelectedCard': """//div[@class="card_wrap card-wrap-device device-offer "]//span[contains(text(),'SELECTAT')]//ancestor::div[@class="card_wrap card-wrap-device device-offer "][1]""",
            'Device_D2S_Rate_Name': '//div[@class="card_wrap card-wrap-device device-offer "]//div[@class="model_phone"]',
            'Device_D2S_Rate_Upfront': '//div[@class="card_wrap card-wrap-device device-offer "]//span[@class="price"]',

            'Total_D2S_Rate_TotalLunar': """//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
            'Device_D2S_Rate_Avans': """//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//span[@class="price"]""",

            'Oferta_D2S_Rate_RateMultiple': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//span[@class="link-title blue bold"]""",
            'Oferta_D2S_Rate_LastInstallment': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="white_tooltip box_tooltip installment-wrapper visible-tooltip"]/div[1]""",
            'Oferta_D2S3YRS_Rate_LastInstallment': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="white_tooltip box_tooltip installment-wrapper visible-tooltip"]//span[@class='Instalments_last_txt' and contains(text(), '36')]""",
            'Device_D2S_Rate_Selecteaza': '//div[@class="card_wrap card-wrap-device device-offer "]//button[@class="general_btn "]',
            'Oferta_D2S_Rate_InstallmentValue': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//span[@class="txt_right_side after_discount"]""",
            'Oferta_D2S_Rate_Select': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'Select')]""",

            'Oferta_D2S_RateDiscount_RateMultiple': """//div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//span[@class="link-title blue bold"]""",
            'Oferta_D2S_RateDiscount_LastInstallment': """//div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="white_tooltip box_tooltip installment-wrapper visible-tooltip"]/div[last()]""",
            'Oferta_D2S3YRS_RateDiscount_LastInstallment': """//div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="white_tooltip box_tooltip installment-wrapper visible-tooltip"]//span[@class='Instalments_last_txt' and contains(text(), '36')]""",
            'Device_D2S_RateDiscount_Selecteaza': '//div[@class="card_wrap card-wrap-device device-offer "]//button[@class="general_btn "]',
            'Oferta_D2S_RateDiscount_InstallmentValue': """//div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//span[@class="txt_right_side after_discount"]""",
            'Oferta_D2S_RateDiscount_Select': """//div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'Select')]""",

            'Offer_S2D_Rate_CardToBeSelected': """//div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]""",
            'Offer_S2D_Rate_SelectedOfferCard': """//div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//button[contains(@class,'general_btn button-selected')]//ancestor::div[@class="card mobile-card"][1]""",

            'Oferta_D2S_SimOnly_RateMultiple': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//span[@class="link-title blue bold"]""",
            'Oferta_D2S_SimOnly_LastInstallment': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="white_tooltip box_tooltip  visible-tooltip"]/div[last()]""",
            'Oferta_D2S_SimOnly_Select': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'Select')]""",

            'Oferta_D2S_SimOnlyDiscount_RateMultiple': """//div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//span[@class="link-title blue bold"]""",
            'Oferta_D2S_SimOnlyDiscount_LastInstallment': """//div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="white_tooltip box_tooltip  visible-tooltip"]/div[last()]""",
            'Oferta_D2S_SimOnlyDiscount_Select': """//div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'Select')]""",

            'Offer_D2S_SinOnly_CardToBeSelected': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]""",
            'Offer_D2S_SinOnly_SelectedOfferCard': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//button[contains(@class,'general_btn button-selected')]//ancestor::div[@class="card mobile-card"][1]""",
            'Oferta_D2S_SinOnly_NumeOferta': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
            'Oferta_D2S_SinOnly_PerioadaContractuala': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
            'Oferta_D2S_SinOnly_oPrice': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
            'Oferta_D2S_SinOnly_LastInstallment': '//div[@class="white_tooltip box_tooltip  visible-tooltip"]/div[last()]',
            'Oferta_D2S_SinOnly_InstallmentValue': """//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//span[@class="txt_right_side after_discount"]""",
            'Device_D2S_SinOnly_CardToBeSelected': '//div[@class="card_wrap card-wrap-device device-offer "]',
            'Device_D2S_SinOnly_SelectedCard': """//div[@class="card_wrap card-wrap-device device-offer "]//span[contains(text(),'SELECTAT')]//ancestor::div[@class="card_wrap card-wrap-device device-offer "][1]""",
            'Device_D2S_SinOnly_Name': '//div[@class="card_wrap card-wrap-device device-offer "]//div[@class="model_phone"]',
            'Device_D2S_SinOnly_Upfront': '//div[@class="card_wrap card-wrap-device device-offer "]//span[@class="price"]',
            'Total_D2S_SinOnly_TotalLunar': """//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
            'Device_D2S_SinOnly_Avans': """//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//span[@class="price"]""",

            # D2S VMM:
            'Oferta_D2S_VMM_Rate_Select': """//div[@class="card mobile-card"][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'Select')]""",
            'Oferta_D2S_VMM_RateDiscount_Select': """//div[@class="card mobile-card"][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'Select')]""",
            'Oferta_D2S_VMM_SimOnly_Select': """//div[@class="card mobile-card"][position() > 0]//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'Select')]""",
            'Oferta_D2S_VMM_SimOnlyDiscount_Select': """//div[@class="card mobile-card"][position() > 0]//div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'Select')]""",
            'Oferta_D2S_VMM_Rate_RateMultiple': """//div[@class="card mobile-card"][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//span[@class="link-title blue bold"]""",
            'Oferta_D2S_VMM_Rate_LastInstallment': """//div[@class="card mobile-card"][position() > 0]//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//div[@class="tooltip_txt "][last()]""",
            'Device_D2S_VMM_Rate_Selecteaza': '//div[@class="card_wrap card-wrap-device device-offer "][position()>0]//button[@class="general_btn "]',

            'Oferta_D2S_VMM_RateDiscount_RateMultiple': """//div[@class="card mobile-card"][position() > 0]//div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//span[@class="link-title blue bold"]""",
            'Oferta_D2S_VMM_RateDiscount_LastInstallment': """//div[@class="card mobile-card"][position() > 0]//div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="white_tooltip box_tooltip installment-wrapper visible-tooltip"]/div[last()]""",
            'Device_D2S_VMM_RateDiscount_Selecteaza': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 0]//button[@class="general_btn "]',
            'Oferta_D2S_VMM_RateDiscount_InstallmentValue': """//div[@class="card mobile-card"][position() > 0]//div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//span[@class="txt_right_side after_discount"]""",
            # 'Oferta_D2S_VMM_RateDiscount_Select': """//div[@class="card mobile-card"][position() > 0]//div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'Select')]""",

            ######VMM RATE DISC##############
            'Offer_D2S_VMMRateDiscount_CardToBeSelected': """//div[@class="card mobile-card"][position() > 0]//*[(contains(@title, 'VMMRateDiscount')) or (contains(text(), 'VMMRateDiscount'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]""",
            'Offer_D2S_VMMRateDiscount_SelectedOfferCard': """//div[@class="card mobile-card"]//*[(contains(@title, 'VMMRateDiscount')) or (contains(text(), 'VMMRateDiscount'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//button[contains(@class,'general_btn button-selected')]//ancestor::div[@class="card mobile-card"][1]""",
            'Oferta_D2S_VMMRateDiscount_NumeOferta': """//div[@class="card mobile-card"][position() > 0]//*[(contains(@title, 'VMMRateDiscount')) or (contains(text(), 'VMMRateDiscount'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="block-level-tooltip color_title ellipsis-tooltip"]""",
            'Oferta_D2S_VMMRateDiscount_PerioadaContractuala': """//div[@class="card mobile-card"][position() > 0]//*[(contains(@title, 'VMMRateDiscount')) or (contains(text(), 'VMMRateDiscount'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="det_months uppercase"]""",
            # 'Oferta_D2S_VMMRateDiscount_oPrice': """//div[@class="card mobile-card"][position() > 0]//*[(contains(@title, 'VMMRateDiscount')) or (contains(text(), 'VMMRateDiscount'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="price"]/h4""",
            # 'Oferta_D2S_VMMRateDiscount_LastInstallment': '//div[@class="white_tooltip box_tooltip  visible-tooltip"]/div[last()]',
            'Oferta_D2S_VMMRateDiscount_InstallmentValue': """//div[@class="card mobile-card"][position() > 0]//*[(contains(@title, 'VMMRateDiscount')) or (contains(text(), 'VMMRateDiscount'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//span[@class="txt_right_side after_discount"]""",
            'Device_D2S_VMMRateDiscount_CardToBeSelected': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 0]',
            'Device_D2S_VMMRateDiscount_SelectedCard': """//div[@class="card_wrap card-wrap-device device-offer "]//span[contains(text(),'SELECTAT')]//ancestor::div[@class="card_wrap card-wrap-device device-offer "][1]""",
            'Device_D2S_VMMRateDiscount_Name': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 0]//div[@class="model_phone"]',
            'Device_D2S_VMMRateDiscount_Upfront': '//div[@class="card_wrap card-wrap-device device-offer "][position() > 0]//span[@class="price"]',
            'Total_D2S_VMMRateDiscount_TotalLunar': """//div[@class="card mobile-card"][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'VMMRateDiscount')) or (contains(text(), 'VMMRateDiscount'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//div[@class="price"]""",
            'Device_D2S_VMMRateDiscount_Avans': """//div[@class="card mobile-card"][position() > 0]//span[@class="link-title blue bold"]//ancestor::div[@class="card mobile-card"]//*[(contains(@title, 'VMMRateDiscount')) or (contains(text(), 'VMMRateDiscount'))]//ancestor::div[@class="det_phone"][count(div)=2]//ancestor::div[@class="card mobile-card"]//span[@class="price"]""",
            'Oferta_D2S_VMMRateDiscount_RateMultiple': '//div[@class="card mobile-card"]//span[@class="link-title blue bold"]',

            #######VMMServiceOnly

            'Oferta_VMMServiceOnly_Selecteaza': """//div[@class="card mobile-card"][position() > 0]//div[@class="det_phone"][count(div)=1]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'Select')]""",
            'Oferta_VMMServiceOnlyDiscount_Selecteaza': """//div[@class="card mobile-card"][position() > 0]//*[contains(text(),'DISCOU')]//ancestor::div[@class="card mobile-card"]//*[contains(text(),'Select')]""",

        }

        page_configureaza_baseLine.utils.__init__(browser=self.browser, methodName='test_page_configureaza',
                                         numeScenariu=testCaseName, xpathDict=self.pageConfigDict,
                                         configType=self.configType)


    def dict_compare(self, d1, d2):
        d1_keys = set(d1.keys())
        d2_keys = set(d2.keys())
        shared_keys = d1_keys.intersection(d2_keys)
        added = d1_keys - d2_keys
        removed = d2_keys - d1_keys
        modified = {o: (d1[o], d2[o]) for o in shared_keys if d1[o] != d2[o]}
        same = set(o for o in shared_keys if d1[o] == d2[o])
        return added, removed, modified, same, shared_keys

    def getPegaOfferName(self):
        return self.utils.getPegaOfferName(self.inputDictionary)

    def send2VOS(self):

        self.utils.click_js(xpath=self.pageConfigDict['submit'])
        self.utils.type_js(xpath=self.pageConfigDict['email'], value='dltvoisvfrotestingautomation@vodafone.com')
        self.utils.type_js(xpath=self.pageConfigDict['contact'], value='730044448')
        self.utils.is_element_Enabled(self.pageConfigDict['send_to_vos'])

    def checkMinimBanners(self, expandElement=None):
        time.sleep(4)
        if expandElement == 'Servicii':
            print('checkMinimBanners --->>', expandElement, self.scenarioName_PC)

            if self.utils.element_exists(
                    xPath='//div[@class="fixed-bundle-offers"]//div[@class="accordion    "]//span[contains(text(),"SERVICII MOBILE")]') == False:
                self.utils.log_customStep('Check if banner Service - collapsed', 'FAIL')
            else:
                self.utils.log_customStep('Check if banner Service - collapsed', 'PASSED')

            if self.utils.element_exists(
                    xPath='//div[@class="fixed-bundle-offers"]//div[@class="accordion    "]//span[contains(text(),"TELEFOANE")]') == False:
                self.utils.log_customStep('Check if banner Device - collapsed', 'FAIL')
            else:
                self.utils.log_customStep('Check if banner Device - collapsed', 'PASSED')

            self.utils.click_js(
                xpath='//div[@class="fixed-bundle-offers"]//div[@class="accordion    "]//span[contains(text(),"SERVICII MOBILE")]')

        elif expandElement == 'Device':
            print('checkMinimBanners --->>', expandElement, self.scenarioName_PC)

            self.utils.click_js(
                xpath='//div[@class="fixed-bundle-offers"]//div[@class="accordion  open-accordion  "]//span[contains(text(),"SERVICII MOBILE")]')

            if self.utils.element_exists(
                    xPath='//div[@class="fixed-bundle-offers"]//div[@class="accordion    "]//span[contains(text(),"TELEFOANE")]') == False:
                self.utils.log_customStep('Check if banner Device - collapsed', 'FAIL')
            else:
                self.utils.log_customStep('Check if banner Device - collapsed', 'PASSED')

            self.utils.click_js(
                xpath='//div[@class="fixed-bundle-offers"]//div[@class="accordion    "]//span[contains(text(),"TELEFOANE")]')

            self.utils.click_js(self.pageConfigDict['ShoppingCart'])

        elif expandElement == 'Device_D2S':
            print('checkMinimBanners --->>', expandElement, self.scenarioName_PC)

            if self.utils.element_exists(
                    xPath='//div[@class="fixed-bundle-offers"]//div[@class="accordion    "]//span[contains(text(),"TELEFOANE")]') == False:
                self.utils.log_customStep('Check if banner Device - collapsed', 'FAIL')
            else:
                self.utils.log_customStep('Check if banner Device - collapsed', 'PASSED')

            self.utils.click_js(
                xpath='//div[@class="fixed-bundle-offers"]//div[@class="accordion    "]//span[contains(text(),"TELEFOANE")]')

            self.utils.click_js(self.pageConfigDict['ShoppingCart'])

        elif expandElement == 'Servicii_D2S':
            print('checkMinimBanners --->>', expandElement, self.scenarioName_PC)
            self.utils.click_js("//span[contains(text(),'TELEFOANE')]")
            self.utils.click_js(xpath='//span[contains(text(),"SERVICII MOBILE")]')
            self.utils.click_js(self.pageConfigDict['ShoppingCart'])

        elif expandElement == 'ServiceOnly':

            if self.utils.element_exists(
                    xPath='//div[@class="fixed-bundle-offers"]//div[@class="accordion    "]//span[contains(text(),"SERVICII MOBILE")]') == False:
                self.utils.log_customStep('Check if banner Service - collapsed', 'FAIL')
            else:
                self.utils.click_js(
                    '//div[@class="fixed-bundle-offers"]//div[@class="accordion    "]//span[contains(text(),"SERVICII MOBILE")]')
                self.utils.log_customStep('Check if banner Service - collapsed', 'PASSED')

            self.utils.click_js(self.pageConfigDict['ShoppingCart'])

    def filterDevice(self):

        self.utils.click_js('''//span[contains(text(),'TELEFOANE')]//ancestor::div[@class="accordion_head"]//div[@data-automation-id="open-filter"]''')
        self.utils.type_js("//input[contains(@type,'text')]", 'Samsung A40 Dual SIM Negru 4G')
        # self.utils.type_js("//input[contains(@type,'text')]", 'auriu')

        self.utils.click_js("""//span[contains(text(),'APLICĂ')]""")

    @abstractmethod
    def test_page_configureaza(self):
        pass

    @abstractmethod
    def testStepsExec(self):
        pass



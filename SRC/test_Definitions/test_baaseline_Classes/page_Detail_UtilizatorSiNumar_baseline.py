from SRC.utils_pkg import utilities
import datetime
from selenium.common.exceptions import NoSuchElementException
import time
from abc import ABC, abstractmethod

class UtilizatorSiNumarLayer_baseline(ABC):

    utils = utilities.utilities()

    def __init__(self, inputDictionary=None, scenarioName_SendNotif=None, scenarioName_isTitular=None, testCaseName=None, abNouType=None, scenarioName_Utl_Nr=None, browser=None, tip_contract=None, scenarioName_GDPR=None, scenarioContNou=None, tipServiciu=None):
        self.abNouType = abNouType
        self.scenarioName_Utl_Nr = scenarioName_Utl_Nr
        self.browser = browser
        self.inputDictionary = inputDictionary
        self.tip_contract = tip_contract
        self.scenarioName_GDPR = scenarioName_GDPR
        self.scenarioName_isTitular = scenarioName_isTitular
        self.scenarioContNou = scenarioContNou
        self.tipServiciu = tipServiciu
        self.scenarioName_SendNotif = scenarioName_SendNotif
        self.util_nr = {'alege_numar': '//h6[contains(text(),"ALEGE NUM")]',
                        'numar_special': '//div[@data-automation-id="number-allocation-popup-special-number-button"]',
                        'first_digits': '//input[@name="firstDigits"]',
                        'cauta': "//div[@data-automation-id='special-number-form-find-button']",
                        'sterge': "//div[@data-automation-id='special-number-form-clear-button']",
                        'continua_alegeNr': "//button[@data-automation-id='number-allocation-popup-continue-button']",
                        'newSim': "//input[@name='newSimNumber']",
                        'data_activare': '//input[@name="activationDate"]',
                        'first_nr_disponibil': "//div[contains(@class,'regular-numbers-container')]/div[1]",
                        'data_min_portare': '//input[@name="minDatePicker"]',
                        'data_max_portare': '//input[@name="maxDatePicker"]',
                        # 'change_number': """//h6[contains(text(),'ALEGE NUM')]""",
                        'change_number': """//h6[contains(text(),'CHANGE NUMBER')]""",
                        'numar_temporar_chk': '//input[@data-automation-id="assign-temporary-msisdn"]',
                        'asteapta_portarea': '//input[@value="pendingSuccessfulPortIn"]',
                        'numar_temporar': '//input[@data-automation-id="assign-temporary-msisdn"]//ancestor::label/span',
                        'SIM_Existent': '//input[@name="existingSim"]',
                        'COD_CLIENT_DONOR': '//input[@name="customerId"]',
                        'Use_Existing_sim_card_check': "//div[@class='useExistingSIMToggle']//input[@type='checkbox']",
                        'Use_Existing_sim_card': "//div[@class='useExistingSIMToggle']//span[@class='slider round']//ancestor::label/span",
                        'Modifica Notificare':"//h6[contains(text(),'MODIFIC')]",
                        'Numar Notificare':"//input[@name='notificationPhoneNumber']",
                        'X': "//img[@class='x']",
                        'Anuleaza': "//button[@class='secondary bold']",
                        'salveaza': "//button[@class='primary bold right-align']"}

        UtilizatorSiNumarLayer_baseline.utils.__init__(browser=self.browser, numeScenariu=testCaseName, xpathDict=self.util_nr, methodName='test_page_detalii_Utilizator_Si_Numar')

    @abstractmethod
    def test_page_Detail_UtilizatorSiNumar(self):
        pass
    
    def suffix(self, d):
        return 'th' if 11<=d<=13 else {1:'st', 2:'nd', 3:'rd'}.get(d % 10, 'th')

    def custom_strftime(self, format, t):
        return t.strftime(format).replace('{S}', str(t.day) + UtilizatorSiNumarLayer_baseline.suffix(self, t.day))
        #print custom_strftime('%B {S}, %Y', dt.now())
    
    
    def test_page_Notification_On_UtilizatorSiNumar(self):
        ported_no = UtilizatorSiNumarLayer_baseline.utils.getTempValue('ported_no')

        if self.scenarioName_SendNotif == 'Change_Notif_Number':
            UtilizatorSiNumarLayer_baseline.utils.click_js(xpath="//h6[contains(text(),'MODIFIC')]")
            UtilizatorSiNumarLayer_baseline.utils.type_js(xpath=self.util_nr['Numar Notificare'], value=ported_no)
            print(ported_no)
            UtilizatorSiNumarLayer_baseline.utils.click_js(xpath=self.util_nr['salveaza'])

        elif self.scenarioName_SendNotif == 'Anuleaza_Change_Notif_Number':
            UtilizatorSiNumarLayer_baseline.utils.click_js(xpath="//h6[contains(text(),'MODIFIC')]")
            UtilizatorSiNumarLayer_baseline.utils.click_js(xpath=self.util_nr['Anuleaza'])

        elif self.scenarioName_SendNotif == 'Click_X_Change_Notif_Number':
            UtilizatorSiNumarLayer_baseline.utils.click_js(xpath="//h6[contains(text(),'MODIFIC')]")
            UtilizatorSiNumarLayer_baseline.utils.click_js(xpath=self.util_nr['X'])


from abc import ABC, abstractmethod
from SRC.utils_pkg import utilities


class page_dashboard_baseline(ABC):

    utils = utilities.utilities()

    def __init__(self, retention_option=None, testCaseName=None, configType = None, abNouType = None, scenarioNameDC = None, browser=None, tipServiciu=None, retention_flow=None, inputDictionary=None):
        self.configType = configType
        self.abNouType = abNouType
        self.scenarioNameDC = scenarioNameDC
        self.inputDictionary = inputDictionary
        self.browser = browser
        self.tipServiciu = tipServiciu
        self.retention_option = retention_option
        self.retention_flow = retention_flow
        self.dasboardDict = {'Abonament_nou':"//div[@data-automation-id='new-subscriber-link']",
                             'Numar_Nou(GA)': '//li[@data-automation-id="tab-tab-key-0"]',
                             'Credit_Rate_Da': '//label[1]//*[@name="isCustomerConsent"]',
                             'Credit_Rate_Nu': '//label[2]//*[@name="isCustomerConsent"]',
                             'Numar_Contact': '//input[@name="contactNumber"]',
                             'continua': "//button[contains(@class,'primary bold right-align')]",
                             'Portare': '//li[@data-automation-id="tab-tab-key-1"]',
                             'Numar_Portat': '//input[@name="portedNumber"]',
                             'Donor': '//select[@name="provider"]',
                             'Numar_Contact_Portare': "//div[2]/div[@class='fields-wrap' and 1]/div[1]/input[1]",
                             'POSTPAID': '//input[@name="portedNumberType" and @value="postpaid"]',
                             'Prepaid': '//input[@name="portedNumberType" and @value="prepaid"]',
                             'Migrare_de_la_Prepaid': "//li[@data-automation-id='tab-tab-key-2']",
                             'Number_To_Migrate': '//input[@name="numberToMigrate"]',
                             'Trimtie Token': "//span[contains(text(),'TRIMITE TOKEN')]",
                             'Introdu Token': "//input[@placeholder='Introdu TOKEN']",
                             'Valideaza Token': """//a[@data-automation-id="token-consent-validate-token"]""",
                             'Factura_1': "//input[@name='invoice1']",
                             'Factura_2': "//input[@name='invoice2']",
                             'Factura_3': "//input[@name='invoice3']",
                              'adauga': "//button[contains(@class,'bold nbo-button')]",
                             'BundleBannerOffer': "//span[contains(text(),'Împreuna cu')]",
                             '360_OferteMobile': """//span[contains(text(),'OFERTE MOBILE')]""",
                             '360_Doar_Servicii': "//span[contains(text(),'DOAR SERVICII')]",
                             }
        page_dashboard_baseline.utils.__init__(browser=self.browser, numeScenariu=testCaseName, xpathDict=self.dasboardDict, methodName='test_page_Dashboard')


    @abstractmethod
    def test_dashboard_Config(self):
        pass
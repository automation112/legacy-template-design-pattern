from SRC.utils_pkg import utilities
import time
from random import randint
from abc import ABC, abstractmethod

class page_search_baseline(ABC):
    utils = utilities.utilities()

    def __init__(self, testCaseName=None, searchType=None, searchValue=None, browser=None, configType=None, inputDictionary=None):
        self.searchType = searchType
        self.browser = browser
        self.searchValue = searchValue
        self.inputDictionary = inputDictionary
        self.configType = self.utils.evalDictionaryValue(inputDictionary, 'configType')
        self.tipServiciu = self.utils.evalDictionaryValue(inputDictionary, 'tipServiciu')
        self.abNouType = self.utils.evalDictionaryValue(inputDictionary, 'abNouType')
        self.scenarioNameDC = self.utils.evalDictionaryValue(inputDictionary, 'scenarioNameDC')
        self.searchDict = {'MSISDN': "//span[contains(text(),'MSISDN')]",
                           'searchKey_MSISDN': '//input[@name="search"]',
                           'cauta': "//button[@data-automation-id='search-button']",
                           'CNP': "//span[contains(text(),'CNP')]",
                           'searchKey_CNP': '//input[@name="search"]',
                           'CONT_CLIENT': "//span[contains(text(),'CONT CLIENT')]",
                           'searchKey_CONT_CLIENT': '//input[@name="search"]',
                           'searchKey_ADDRESS': '//input[@name="search"]',
                           'ADDRESS': "//span[contains(text(),'INSTALARE')]",
                           'Abonament_nou': "//div[@data-automation-id='new-subscriber-link']",
                           'Numar_Nou(GA)': '//li[@data-automation-id="tab-tab-key-0"]',
                           'Credit_Rate_Da': '//label[1]//*[@name="isCustomerConsent"]',
                           'Credit_Rate_Nu': '//label[2]//*[@name="isCustomerConsent"]',
                           'Numar_Contact': '//input[@name="contactNumber"]',
                           'continua': "//button[contains(@class,'primary bold right-align')]",
                           'Portare': '//li[@data-automation-id="tab-tab-key-1"]',
                           'Numar_Portat': '//input[@name="portedNumber"]',
                           'Donor': '//select[@name="provider"]',
                           'Numar_Contact_Portare': "//div[2]/div[@class='fields-wrap' and 1]/div[1]/input[1]",
                           'POSTPAID': '//input[@name="portedNumberType" and @value="postpaid"]',
                           'Prepaid': '//input[@name="portedNumberType" and @value="prepaid"]',
                           'Migrare_de_la_Prepaid': "//li[@data-automation-id='tab-tab-key-2']",
                           'Number_To_Migrate': '//input[@name="numberToMigrate"]',
                           'Trimtie Token': "//span[contains(text(),'TRIMITE TOKEN')]",
                           'Introdu Token': "//input[@placeholder='Introdu TOKEN']",
                           'Valideaza Token': """//a[@data-automation-id="token-consent-validate-token"]""",
                           'Factura_1': "//input[@name='invoice1']",
                           'Factura_2': "//input[@name='invoice2']",
                           'Factura_3': "//input[@name='invoice3']",
                           'adauga': "//button[contains(@class,'bold nbo-button')]",
                           'BundleBannerOffer': "//span[contains(text(),'Împreuna cu')]",
                           '360_OferteMobile': """//span[contains(text(),'OFERTE MOBILE')]""",
                           '360_Doar_Servicii': "//span[contains(text(),'DOAR SERVICII')]"
                           }


        page_search_baseline.utils.__init__(browser=self.browser, numeScenariu=testCaseName, xpathDict=self.searchDict,
                                            methodName='test_page_Search')
    @abstractmethod
    def page_search(self):
      pass

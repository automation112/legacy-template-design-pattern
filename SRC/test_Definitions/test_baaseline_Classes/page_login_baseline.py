import time
from SRC.utils_pkg import utilities
from abc import ABC, abstractmethod


class page_login_baseline(ABC):

    utils = utilities.utilities()
    def __init__(self, browser=None, testCaseName=None, channel=None):
        self.browser = browser
        self.channel = channel
        self.testCaseName = testCaseName
        self.login_dict = {'OK': '//*[@id="root"]/div[3]/div/button',
                       'UserName': '//input[@name="username"]',
                       'Password': '//input[@name="password"]',
                       'Login': '//button[@data-automation-id="login-button"]',
                       'DealerSelect': "//select[@data-automation-id='select-dealer']",
                       'Continua': "//button[@data-automation-id='dealer-login-button']"}
        page_login_baseline.utils.__init__(browser=self.browser, numeScenariu=testCaseName, xpathDict=self.login_dict, methodName='test_page_Login')

    @abstractmethod
    def test_Login(self):
        pass

from SRC.utils_pkg import utilities
from abc import ABC, abstractmethod


class Detalii_Client_Layer_baseline(ABC):
    utils = utilities.utilities()

    def __init__(self, inputDictionary=None, scenarioName_SendNotif=None, scenarioName_isTitular=None,
                 testCaseName=None, abNouType=None, scenarioName_Utl_Nr=None, browser=None, tip_contract=None,
                 scenarioName_GDPR=None, scenarioContNou=None, tipServiciu=None):
        self.abNouType = abNouType
        self.scenarioName_Utl_Nr = scenarioName_Utl_Nr
        self.browser = browser
        self.inputDictionary = inputDictionary
        self.tip_contract = tip_contract
        self.scenarioName_GDPR = scenarioName_GDPR
        self.scenarioName_isTitular = scenarioName_isTitular
        self.scenarioContNou = scenarioContNou
        self.tipServiciu = tipServiciu
        self.scenarioName_SendNotif = scenarioName_SendNotif
        self.xpaths_DetaliiClient = {}

        Detalii_Client_Layer_baseline.utils.__init__(browser=self.browser, numeScenariu=testCaseName,
                                                       xpathDict=self.xpaths_DetaliiClient,
                                                       methodName='test_page_detalii_Detalii_Client', inputDictionary=inputDictionary)

    @abstractmethod
    def test_page_Detail_detalii_Client(self):
        pass

    def set_xpath_dictionary(self, xpath_dictionary):
        self.xpaths_DetaliiClient.update(xpath_dictionary)
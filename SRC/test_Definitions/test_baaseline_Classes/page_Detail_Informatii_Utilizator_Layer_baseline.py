from SRC.utils_pkg import utilities
import datetime
import random
import calendar
from abc import ABC, abstractmethod


class Informatii_Utilizator_Layer_baseline(ABC):

    utils = utilities.utilities()

    def __init__(self, scenarioName_isTitular = None, testCaseName=None, browser=None, inputDictionary=None):

        self.browser = browser
        self.inputDictionary = inputDictionary
        self.scenarioName_isTitular = scenarioName_isTitular
        self.utl_Titular = {'utilizator_titular': '//input[@data-automation-id="apply-personal-details-to-contact"]//ancestor::label/span',
                           'minor_nu': '//input[@name="minor" and @value="false"]',
                           'minor_da': '//input[@name="minor" and @value="true"]',
                           'nume': "//input[@name='personFirstName']",
                           'prenume': "//input[@name='personLastName']",
                           'contact_email': "//input[@name='contactEmail']",
                           'contact_phone': "//input[@name='contactPhone']",
                           'titlu': "//select[@name='personTitle']",
                           'ziua_nasterii': "//select[@name='birthDay']",
                           'luna_nasterii': "//select[@name='birthDateMonth']",
                           'anul_nasterii': "//select[@name='birthDateYear']",
                           }
        Informatii_Utilizator_Layer_baseline.utils.__init__(browser=self.browser, numeScenariu=testCaseName, xpathDict=self.utl_Titular, methodName='test_page_detalii_UtilizatorTitular')

    @abstractmethod
    def test_page_Detalil_Informatii_Utilizator_Layer(self):
        pass
from SRC.utils_pkg import utilities
from abc import ABC, abstractmethod


class contNou_Layer_baseline(ABC):
    utils = utilities.utilities()
    def __init__(self, inputDictionary=None, scenarioName_isTitular=None, testCaseName=None, abNouType=None, scenarioName_Sim=None,
                 browser=None, tip_contract=None, scenarioName_GDPR=None, scenarioContNou=None):
        self.abNouType = abNouType
        self.inputDictionary = inputDictionary
        self.scenarioName_Sim = scenarioName_Sim
        self.browser = browser
        self.tip_contract = tip_contract
        contNou_Layer_baseline.utils.__init__(self.browser, numeScenariu=testCaseName)
        self.scenarioName_GDPR = scenarioName_GDPR
        self.scenarioName_isTitular = scenarioName_isTitular
        self.scenarioContNou = scenarioContNou
        self.contNou_dict = {'Factura_Ok': '//*[@data-automation-id="radio-btn-name-email"]',
                        'Cont_Nou': '//h6[contains(text(),"CONT NOU")]',
                        'Tara': '//div[@class="popup-line align-content-address"][1]//div[@class="wrap-input '
                                'input-spacing"][1]//div[@class="Select-control"][1]/div[1]/div[2]/input[1]',
                        'Judet': '//div[@class="popup-line align-content-address"][1]//div[@class="wrap-input '
                                 'input-spacing"][2]//div[@class="Select-control"][1]/div[1]/div[2]/input[1]',
                        'Localitate': '//div[@class="popup-line align-content-address"][1]//div[@class="wrap-input '
                                      'input-spacing"][3]//div[@class="Select-control"][1]/div[1]/div[2]/input[1]',
                        'Type': '//div[@class="popup-line align-content-address"][2]//div[@class="wrap-input '
                                'input-spacing"][1]//div[@class="Select-control"][1]/div[1]/div[2]/input[1]',
                        'Strada': '//div[@class="popup-line align-content-address"][2]//div[@class="wrap-input '
                                  'input-spacing"][2]//div[@class="Select-control"][1]/div[1]/div[2]/input[1]',
                        'Numar': '//input[@name="streetNumber"]',
                        'Email': "//input[@class='margin-2em']",
                        'Continua': "//button[@data-automation-id='summary-configuration-submit-btn']",
                        'Factura_Tiparita': '//input[@data-automation-id="radio-btn-bill-media-paper"]',
                        'cod_postal': '//div[@class="popup-line align-content-address"][3]//div[@class="wrap-input input-spacing"][4]//div[@class="Select-control"][1]',
                        'DATA FACTURARE_Generat': '//div[@class="personal-details"]/div[3]',
                        'DATA FACTURARE CURENTA': '//div[@class="personal-details"]/div[2]'}

        contNou_Layer_baseline.utils.__init__(browser=self.browser, methodName='test_page_Cont_Nou', numeScenariu=testCaseName, xpathDict=self.contNou_dict)


    @abstractmethod
    def test_page_Detail_ContNou(self):
     pass
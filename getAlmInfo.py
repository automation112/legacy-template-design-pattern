import requests
import requests
import json
import time

duplicate_check = []

def loginAlm():
    pwd="vdf123"
    user="laurentiu.faget"
    auth_str=tuple((user,pwd))
    base_url='http://10.140.206.63:8080/qcbin'
    auth_url=base_url+'/authentication-point/authenticate'
    post_ses=base_url+'/rest/site-session'
    headers = {'Accept': 'application/json'}
    s=requests.Session()
    s=requests.Session()
    auth=s.get(auth_url,auth=auth_str)
    p_sess=s.post(post_ses)
    print('one session')
    return s

def queryAlm(session):
    headers = {'Accept': 'application/json'}
    base_url='http://10.140.206.63:8080/qcbin'
    prepared='?query={user-08[Automation];user-07[InProgress]}'
    get_entity= base_url + '/rest/domains/Other/projects/Configuration/defects' + prepared
    g_run=session.get(get_entity,headers=headers)
    retobj=g_run.json()

    ticketList = []
    for dictionar in retobj['entities']:
        release_dict = {}
        for f in dictionar['Fields']:
            if f['Name'] == 'user-13':
                rel = (f['values'][0]['value'])
                release_dict['rls']=rel
            elif f['Name'] == 'user-09':
                env = f['values'][0]['value']
                if env == 'DEX.UAT3':
                    env = 'dexuat3localhost'
                elif env == 'DEX.UAT2':
                    env = '20'
                elif env == 'DEX.UAT1':
                    env = 'dexuat1localhost'
                release_dict['env']=env
            elif f['Name'] == 'user-15':
                runType = f['values'][0]['value']
                release_dict['runType']=runType
            elif f['Name'] == 'id':
                sdid = f['values'][0]['value']
                release_dict['sdid']=sdid            
        ticketList.append(release_dict)
    
    commonstr="Query number"
    return ticketList

def quitAlm(session):
    base_url='http://10.140.206.63:8080/qcbin'
    sign_out = base_url + '/authentication-point/logout'
    s_out = session.get(sign_out)
    print(s_out.status_code)
    print('quit session')


def triggerBuild(almList=None):
    for ticketInfo in almList:
        almid = ticketInfo['sdid']

        if almid not in duplicate_check:
            duplicate_check.append(almid)
            print('we started build for '+almid)
            ticketInfo['rls']
            req = requests.get('http://devops02.connex.ro:8080/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)', auth=('razvan_dumitrascuta', 'razvan123'))
            crumb = (req.text)
            print(crumb)
            rls_id = ticketInfo['rls'].split(' ')[-1]+'_'+almid
            if ticketInfo['runType'] == 'Y':
                modRulare = 'F'
            else:
                modRulare = 'S'
            headers = dict()
            headers[crumb.split(":")[0]] = crumb.split(":")[1]

            url = "http://devops02.connex.ro:8080/job/DexAutoTesting/buildWithParameters?RLS_VERSION={}&RUN_TYPE={}&ENV={}".format(rls_id,modRulare,ticketInfo['env'])
            my_auth = {'razvan_dumitrascuta','110f4ba0ecb5949918000f401fa9ee3e72'}
            resp = requests.post(url=url,auth=('razvan_dumitrascuta','110f4ba0ecb5949918000f401fa9ee3e72'),headers=headers)
        else:
            print(almid + ' already started the build')
            pass
    # return resp.status_code

if __name__ == '__main__':
    print(__name__ )
    s_id = loginAlm()
    a = 0
    while True:
        tickets = queryAlm(s_id)
        triggerBuild(tickets)
        time.sleep(120)
        a+=1
        if a == 30:
            break
    quitAlm(s_id)


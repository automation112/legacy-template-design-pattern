from basedeximage
COPY ./Resources/requirments.txt /tmp/
RUN pip3 install --no-cache-dir --trusted-host pypi.org --trusted-host files.pythonhosted.org --trusted-host pypi.python.org -e undetected-chromedriver/ && pip3 install --no-cache-dir --trusted-host pypi.org --trusted-host files.pythonhosted.org --trusted-host pypi.python.org -r /tmp/requirments.txt
ADD ./SRC/ /Code/SRC/
ADD ./main.py /Code
WORKDIR /Code
CMD ["/usr/bin/python3", "-u", "main.py"]
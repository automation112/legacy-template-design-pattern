#!/bin/bash

REPORT_PATH=~/dex/Output
CONTAINER_HOME=$(dirname `which $0`)
IMAGE_NAME=dextest
CONTAINER_NAME=dex_worker
IMAGE_EXISTS=$(sudo docker ps | grep -i $IMAGE_NAME | wc -l)

cd $CONTAINER_HOME
if [ $# -eq 0 ]
  then
  echo 'No argument, we will use Develop and S'
  RLS='Develop'
  RUN_TYPE='S'
  ENV='Jenkins'
else
	RLS=$1
	RUN_TYPE=$2
	ENV=$3
fi
echo $RLS
echo $RUN_TYPE

docker-compose down -v
python3 ~/dex/generate_compose.py $RLS $RUN_TYPE $ENV> docker-compose.yaml


if [[ -d ~/dex/Output/$RLS ]]
then
    echo "$RLS exists on your filesystem."
else
	echo "$RLS does not exists on your filesystem. it will be created"
	mkdir -p ~/dex/Output/$RLS
	mkdir -p ~/dex/Output/$RLS/ScreenShots
	mkdir -p ~/dex/Output/$RLS/TraceFiles
fi
if [ $(docker ps | grep $CONTAINER_NAME | wc -l) -gt 0 ]
then 
	for container in $(sudo docker ps | awk -v awkvarr="$CONTAINER_NAME" '$12~awkvarr{print $1}');do echo "docker container stop $container" && docker container stop $container && docker container rm $container -f; done
fi
# docker rmi -f $IMAGE_NAME
docker build --build-arg http_proxy=http://10.140.206.118:3128 --build-arg https_proxy=https://10.140.206.118:3128 -t $IMAGE_NAME .
docker-compose up
echo $?
# if [ $? -ne 0 ]
# then
# mv $REPORT_PATH/*html $REPORT_PATH/$RLS
# find $REPORT_PATH/ScreenShots/ -type f -name "*png" -exec mv '{}' ~/dex/Output/$RLS/ScreenShots \;
# mv $REPORT_PATH/TraceFiles/*txt ~/dex/Output/$RLS/TraceFiles
# fi
